package com.delimeal.subscription.model.dao;

import com.delimeal.subscription.model.dto.DeliveryDetailDTO;
import com.delimeal.subscription.model.dto.SearchDeliverVO;

public interface CheckIsDeliveredMapper {

	DeliveryDetailDTO selectAllDeliveryDetail(SearchDeliverVO searchCriteria);

}
