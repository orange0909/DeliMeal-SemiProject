package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class PayHistoryAndDateGapDTO implements Serializable{
	private static final long serialVersionUID = -6260763552212864158L;
	
	private String payCode;
	private java.sql.Date payDate;
	private java.sql.Date startDate;
	private java.sql.Date exprieDate;
	private String isRefund;
	private String isExpire;
	private int discountPrice;
	private int finalPrice;
	private String cpnCode;
	private String subsCode;
	private int dateGap;
	private int deliveryZipcode;
	private String deliveryAddr;
	private String deliveryAddrDetail;
	private String memberCode;
	private String mealCategoryCode;
	
	public PayHistoryAndDateGapDTO() { }

	public PayHistoryAndDateGapDTO(String payCode, Date payDate, Date startDate, Date exprieDate, String isRefund,
			String isExpire, int discountPrice, int finalPrice, String cpnCode, String subsCode, int dateGap,
			int deliveryZipcode, String deliveryAddr, String deliveryAddrDetail, String memberCode,
			String mealCategoryCode) {
		this.payCode = payCode;
		this.payDate = payDate;
		this.startDate = startDate;
		this.exprieDate = exprieDate;
		this.isRefund = isRefund;
		this.isExpire = isExpire;
		this.discountPrice = discountPrice;
		this.finalPrice = finalPrice;
		this.cpnCode = cpnCode;
		this.subsCode = subsCode;
		this.dateGap = dateGap;
		this.deliveryZipcode = deliveryZipcode;
		this.deliveryAddr = deliveryAddr;
		this.deliveryAddrDetail = deliveryAddrDetail;
		this.memberCode = memberCode;
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public java.sql.Date getPayDate() {
		return payDate;
	}

	public void setPayDate(java.sql.Date payDate) {
		this.payDate = payDate;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getExprieDate() {
		return exprieDate;
	}

	public void setExprieDate(java.sql.Date exprieDate) {
		this.exprieDate = exprieDate;
	}

	public String getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}

	public String getIsExpire() {
		return isExpire;
	}

	public void setIsExpire(String isExpire) {
		this.isExpire = isExpire;
	}

	public int getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(int discountPrice) {
		this.discountPrice = discountPrice;
	}

	public int getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(int finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getCpnCode() {
		return cpnCode;
	}

	public void setCpnCode(String cpnCode) {
		this.cpnCode = cpnCode;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public int getDateGap() {
		return dateGap;
	}

	public void setDateGap(int dateGap) {
		this.dateGap = dateGap;
	}

	public int getDeliveryZipcode() {
		return deliveryZipcode;
	}

	public void setDeliveryZipcode(int deliveryZipcode) {
		this.deliveryZipcode = deliveryZipcode;
	}

	public String getDeliveryAddr() {
		return deliveryAddr;
	}

	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}

	public String getDeliveryAddrDetail() {
		return deliveryAddrDetail;
	}

	public void setDeliveryAddrDetail(String deliveryAddrDetail) {
		this.deliveryAddrDetail = deliveryAddrDetail;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	@Override
	public String toString() {
		return "PayHistoryAndDateGapDTO [payCode=" + payCode + ", payDate=" + payDate + ", startDate=" + startDate
				+ ", exprieDate=" + exprieDate + ", isRefund=" + isRefund + ", isExpire=" + isExpire
				+ ", discountPrice=" + discountPrice + ", finalPrice=" + finalPrice + ", cpnCode=" + cpnCode
				+ ", subsCode=" + subsCode + ", dateGap=" + dateGap + ", deliveryZipcode=" + deliveryZipcode
				+ ", deliveryAddr=" + deliveryAddr + ", deliveryAddrDetail=" + deliveryAddrDetail + ", memberCode="
				+ memberCode + ", mealCategoryCode=" + mealCategoryCode + "]";
	}
}
