package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class DeliveryDetailDTO implements Serializable{
	
	private static final long serialVersionUID = -67218803890209297L;
	
	private String deliveryCode;
	private int zipcode;
	private String detail;
	private String address;
	private String isFinish;
	private java.sql.Date date;
	private String subsCode;
	private String mealCode;
	private String mealName;
	
	public DeliveryDetailDTO() {
	}

	public DeliveryDetailDTO(String deliveryCode, int zipcode, String detail, String address, String isFinish,
			Date date, String subsCode, String mealCode, String mealName) {
		super();
		this.deliveryCode = deliveryCode;
		this.zipcode = zipcode;
		this.detail = detail;
		this.address = address;
		this.isFinish = isFinish;
		this.date = date;
		this.subsCode = subsCode;
		this.mealCode = mealCode;
		this.mealName = mealName;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public int getZipcode() {
		return zipcode;
	}

	public void setZipcode(int zipcode) {
		this.zipcode = zipcode;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getIsFinish() {
		return isFinish;
	}

	public void setIsFinish(String isFinish) {
		this.isFinish = isFinish;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "DeliveryDetailDTO [deliveryCode=" + deliveryCode + ", zipcode=" + zipcode + ", detail=" + detail
				+ ", address=" + address + ", isFinish=" + isFinish + ", date=" + date + ", subsCode=" + subsCode
				+ ", mealCode=" + mealCode + ", mealName=" + mealName + "]";
	}
	
}
