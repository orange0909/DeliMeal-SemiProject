package com.delimeal.subscription.model.dao;

import java.util.List;

import com.delimeal.subscription.model.dto.MealCategoryDTO;

public interface SelectMealCategoryMapper {

	List<MealCategoryDTO> selectAllMeal(String memberMealCode);

	int selectMealPrice(String meal);

}
