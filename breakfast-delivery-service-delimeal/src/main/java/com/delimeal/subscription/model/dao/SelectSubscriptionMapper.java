package com.delimeal.subscription.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.subscription.model.dto.PayHistoryAndDateGapDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.SearchDeliverVO;
import com.delimeal.subscription.model.dto.SelectSubscriptionDTO;
import com.delimeal.subscription.model.dto.SubscriptionDTO;
import com.delimeal.subscription.model.dto.ViewDeliveryDeatilDTO;

public interface SelectSubscriptionMapper {

	SelectSubscriptionDTO selectSubscription(String subsCode);
	
	String selectCurrentMeal(String memberCode);

	String selectCurrentSubsCode(String memberCode);

	SelectSubscriptionDTO selectCurrentSubscription(String memberCode);

	List<SelectSubscriptionDTO> selectCurrentSubscriptions(String subsCode);

	String selectCurrentMealBySubsCode(String subsCode);

	ViewDeliveryDeatilDTO selectDeliveryDetails(SearchDeliverVO searchCriteria);

	SubscriptionDTO selectSubscriptionBySubsCode(String subsCode);
	List<SelectSubscriptionDTO> selectCurrentSubscriptionsByDate(Map<String, String> parameter);

}
