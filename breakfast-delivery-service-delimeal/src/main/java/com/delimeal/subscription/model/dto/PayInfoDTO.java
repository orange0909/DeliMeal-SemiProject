package com.delimeal.subscription.model.dto;

import java.io.Serializable;

public class PayInfoDTO implements Serializable {
	private static final long serialVersionUID = 7024964488362348632L;

	private String memberCode;
	private String payMethod;
	private String useDisposableYN;
	private String addrCode;
	
	public PayInfoDTO() { }

	public PayInfoDTO(String memberCode, String payMethod, String useDisposableYN, String addrCode) {
		this.memberCode = memberCode;
		this.payMethod = payMethod;
		this.useDisposableYN = useDisposableYN;
		this.addrCode = addrCode;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getUseDisposableYN() {
		return useDisposableYN;
	}

	public void setUseDisposableYN(String useDisposableYN) {
		this.useDisposableYN = useDisposableYN;
	}

	public String getAddrCode() {
		return addrCode;
	}

	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}

	@Override
	public String toString() {
		return "PayInfoDTO [memberCode=" + memberCode + ", payMethod=" + payMethod + ", useDisposableYN="
				+ useDisposableYN + ", addrCode=" + addrCode + "]";
	}
}
