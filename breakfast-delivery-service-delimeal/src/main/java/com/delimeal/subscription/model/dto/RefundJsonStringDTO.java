package com.delimeal.subscription.model.dto;

import java.io.Serializable;

public class RefundJsonStringDTO implements Serializable{
	private static final long serialVersionUID = 8837728947227381527L;
	
	private String merchant_uid;
	private String price;
	private String reason;

	public RefundJsonStringDTO() {
		super();
	}

	public RefundJsonStringDTO(String merchant_uid, String price, String reason) {
		this.merchant_uid = merchant_uid;
		this.price = price;
		this.reason = reason;
	}

	public String getMerchant_uid() {
		return merchant_uid;
	}

	public void setMerchant_uid(String merchant_uid) {
		this.merchant_uid = merchant_uid;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "ResfundJsonStringDTO [merchant_uid=" + merchant_uid + ", price=" + price + ", reason=" + reason + "]";
	}

}
