package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class SelectSubscriptionDTO implements Serializable{

	private static final long serialVersionUID = 6647653565484878444L;
	
	private String subsCode;
	private String memberCode;
	private String mealCategoryCode;
	private String mealCategoryName;
	private java.sql.Date expireDate;
	private java.sql.Date startDate;
	private String isExpire;
	
	public SelectSubscriptionDTO(String subsCode, String memberCode, String mealCategoryCode, String mealCategoryName,
			Date expireDate, Date startDate, String isExpire) {
		super();
		this.subsCode = subsCode;
		this.memberCode = memberCode;
		this.mealCategoryCode = mealCategoryCode;
		this.mealCategoryName = mealCategoryName;
		this.expireDate = expireDate;
		this.startDate = startDate;
		this.isExpire = isExpire;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getMealCategoryName() {
		return mealCategoryName;
	}

	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

	public java.sql.Date getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(java.sql.Date expireDate) {
		this.expireDate = expireDate;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public String getIsExpire() {
		return isExpire;
	}

	public void setIsExpire(String isExpire) {
		this.isExpire = isExpire;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "SelectSubscriptionDTO [subsCode=" + subsCode + ", memberCode=" + memberCode + ", mealCategoryCode="
				+ mealCategoryCode + ", mealCategoryName=" + mealCategoryName + ", expireDate=" + expireDate
				+ ", startDate=" + startDate + ", isExpire=" + isExpire + "]";
	}
	
}
