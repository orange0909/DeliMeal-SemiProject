package com.delimeal.subscription.model.dao;

import java.sql.Date;
import java.util.Map;

import com.delimeal.subscription.model.dto.PayAndSubscriptionDTO;
import com.delimeal.subscription.model.dto.PayHistoryAndDateGapDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.selectDateGapDTO;

public interface ChangePayHistoryMapper {

	PayHistoryDTO selectPayHistory(String currentSubsCode);

	int udatePayHistory(String subsCode);

	int insertSubscription(Map<String, String> memberCodeAndSelectMealMap);

	int insertPayHistory(java.sql.Date exprieDate);

	int selectDateGap(String currentSubsCode);

	int selectFinalPrice(String currentSubsCode);

	String selectCurrentServiceName(String currentSubsCode);

	int insertSubscriptionSchedule(PayHistoryAndDateGapDTO payHistoryAndDateGap);

	int insertScheduleChangePayHistory(PayHistoryAndDateGapDTO payHistoryAndDateGap);

	int selectDateBetweenSpecificDate(Map<String, String> dateMap);

	PayAndSubscriptionDTO SelectSubsDetailByCode(String subsCode);

	int selectStartDateGap(String subsCode);

	int udatePayHistoryUnderStartDay(String subsCode);

}
