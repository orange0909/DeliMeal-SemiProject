package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class PayAndSubscriptionDTO implements Serializable{

	private static final long serialVersionUID = 1013260736007528349L;
	
	private String subsCode;
	private String memberCode;
	private String mealCategoryCode;
	private String payCode;
	private java.sql.Date payDate;
	private java.sql.Date startDate;
	private java.sql.Date exprieDate;
	private String isRefund;
	private String isExpired;
	private int discountPrice;
	private int finalPrice;
	private String couponCode;
	private String mealCategoryName;
	
	public PayAndSubscriptionDTO() {
	}

	public PayAndSubscriptionDTO(String subsCode, String memberCode, String mealCategoryCode, String payCodoe,
			Date payDate, Date startDate, Date exprieDate, String isRefund, String isExpired, int discountPrice,
			int finalPrice, String couponCode, String mealCategoryName) {
		this.subsCode = subsCode;
		this.memberCode = memberCode;
		this.mealCategoryCode = mealCategoryCode;
		this.payCode = payCodoe;
		this.payDate = payDate;
		this.startDate = startDate;
		this.exprieDate = exprieDate;
		this.isRefund = isRefund;
		this.isExpired = isExpired;
		this.discountPrice = discountPrice;
		this.finalPrice = finalPrice;
		this.couponCode = couponCode;
		this.mealCategoryName = mealCategoryName;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getPayCodoe() {
		return payCode;
	}

	public void setPayCodoe(String payCodoe) {
		this.payCode = payCodoe;
	}

	public java.sql.Date getPayDate() {
		return payDate;
	}

	public void setPayDate(java.sql.Date payDate) {
		this.payDate = payDate;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getExprieDate() {
		return exprieDate;
	}

	public void setExprieDate(java.sql.Date exprieDate) {
		this.exprieDate = exprieDate;
	}

	public String getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}

	public String getIsExpired() {
		return isExpired;
	}

	public void setIsExpired(String isExpired) {
		this.isExpired = isExpired;
	}

	public int getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(int discountPrice) {
		this.discountPrice = discountPrice;
	}

	public int getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(int finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public String getMealCategoryName() {
		return mealCategoryName;
	}

	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PayAndSubscriptionDTO [subsCode=" + subsCode + ", memberCode=" + memberCode + ", mealCategoryCode="
				+ mealCategoryCode + ", payCode=" + payCode + ", payDate=" + payDate + ", startDate=" + startDate
				+ ", exprieDate=" + exprieDate + ", isRefund=" + isRefund + ", isExpired=" + isExpired
				+ ", discountPrice=" + discountPrice + ", finalPrice=" + finalPrice + ", couponCode=" + couponCode
				+ ", mealCategoryName=" + mealCategoryName + "]";
	}

}