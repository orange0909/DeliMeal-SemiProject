package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class PayHistoryDTO implements Serializable{
	private static final long serialVersionUID = -4537493381667299889L;
	
	private String payCode;
	private java.sql.Date payDate;
	private java.sql.Date startDate;
	private java.sql.Date exprieDate;
	private String isRefund;
	private String isExpire;
	private int discountPrice;
	private int finalPrice;
	private String cpnCode;
	private String subsCode;

	public PayHistoryDTO() {
		super();
	}

	public PayHistoryDTO(String payCode, Date payDate, Date startDate, Date exprieDate, String isRefund,
			String isExpire, int discountPrice, int finalPrice, String cpnCode, String subsCode) {
		this.payCode = payCode;
		this.payDate = payDate;
		this.startDate = startDate;
		this.exprieDate = exprieDate;
		this.isRefund = isRefund;
		this.isExpire = isExpire;
		this.discountPrice = discountPrice;
		this.finalPrice = finalPrice;
		this.cpnCode = cpnCode;
		this.subsCode = subsCode;
	}

	public String getPayCode() {
		return payCode;
	}

	public void setPayCode(String payCode) {
		this.payCode = payCode;
	}

	public java.sql.Date getPayDate() {
		return payDate;
	}

	public void setPayDate(java.sql.Date payDate) {
		this.payDate = payDate;
	}

	public java.sql.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.sql.Date startDate) {
		this.startDate = startDate;
	}

	public java.sql.Date getExprieDate() {
		return exprieDate;
	}

	public void setExprieDate(java.sql.Date exprieDate) {
		this.exprieDate = exprieDate;
	}

	public String getIsRefund() {
		return isRefund;
	}

	public void setIsRefund(String isRefund) {
		this.isRefund = isRefund;
	}

	public String getIsExpire() {
		return isExpire;
	}

	public void setIsExpire(String isExpire) {
		this.isExpire = isExpire;
	}

	public int getDiscountPrice() {
		return discountPrice;
	}

	public void setDiscountPrice(int discountPrice) {
		this.discountPrice = discountPrice;
	}

	public int getFinalPrice() {
		return finalPrice;
	}

	public void setFinalPrice(int finalPrice) {
		this.finalPrice = finalPrice;
	}

	public String getCpnCode() {
		return cpnCode;
	}

	public void setCpnCode(String cpnCode) {
		this.cpnCode = cpnCode;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PayHistoryDTO [payCode=" + payCode + ", payDate=" + payDate + ", startDate=" + startDate
				+ ", exprieDate=" + exprieDate + ", isRefund=" + isRefund + ", isExpire=" + isExpire
				+ ", discountPrice=" + discountPrice + ", finalPrice=" + finalPrice + ", cpnCode=" + cpnCode
				+ ", subsCode=" + subsCode + "]";
	}
	
}
