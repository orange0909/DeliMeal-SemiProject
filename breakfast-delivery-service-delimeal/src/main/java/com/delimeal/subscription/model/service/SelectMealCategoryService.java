package com.delimeal.subscription.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.subscription.model.dao.SelectMealCategoryMapper;
import com.delimeal.subscription.model.dto.MealCategoryDTO;

public class SelectMealCategoryService {

	public List<MealCategoryDTO> selectAllMeal(String memberMealCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectMealCategoryMapper mapper = sqlSession.getMapper(SelectMealCategoryMapper.class);
		
		List<MealCategoryDTO> result = null;
		result = mapper.selectAllMeal(memberMealCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public int selectMealPrice(String meal) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectMealCategoryMapper mapper = sqlSession.getMapper(SelectMealCategoryMapper.class);
		
		int result = 0;
		result = mapper.selectMealPrice(meal);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}
}
