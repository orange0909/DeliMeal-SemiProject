package com.delimeal.subscription.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.subscription.model.dao.ChangePayHistoryMapper;
import com.delimeal.subscription.model.dto.PayAndSubscriptionDTO;
import com.delimeal.subscription.model.dto.PayHistoryAndDateGapDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;

public class ChangeMealService {

	public PayHistoryDTO selectPayHistroy(String currentSubsCode) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("checkIsDelivered Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		PayHistoryDTO result = null;
		result = mapper.selectPayHistory(currentSubsCode);
		System.out.println("checkIsDelivered Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public int changeSubscriptionTransaction(PayHistoryDTO payHistory, Map<String, String> memberCodeAndSelectMealMap) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("changeSubscriptionTransaction객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		/* 1. 현재 구독중인 서비스 만료처리 */
		System.out.println(payHistory.getSubsCode());
		int updateResult = mapper.udatePayHistory(payHistory.getSubsCode());
		System.out.println("changeSubscriptionTransaction객체에서 DAO간 후 udatePayHistory실행" + mapper);
		
		/* 2. 변경한 서비스 insert */
		System.out.println("memberCode: " + memberCodeAndSelectMealMap.get("memberCode"));
		int insertSubsResult = mapper.insertSubscription(memberCodeAndSelectMealMap);
		System.out.println("changeSubscriptionTransaction객체에서 DAO간 후 insertSubscription실행" + mapper);
		
		/* 3. 변경한 서비스의 결제내역 insert */
		int insertPayHistoryResult = mapper.insertPayHistory(payHistory.getExprieDate());
		System.out.println("changeSubscriptionTransaction객체에서 DAO간 후 insertPayHistory실행" + mapper);
		
		
		if(updateResult > 0 && insertSubsResult > 0 && insertPayHistoryResult > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return updateResult + insertSubsResult + insertPayHistoryResult;
	}

	public int selectDateGap(String currentSubsCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectDateGap Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		int result = 0;
		result = mapper.selectDateGap(currentSubsCode);
		System.out.println("selectDateGap Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public int selectFinalPrice(String currentSubsCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectDateGap Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		int result = 0;
		result = mapper.selectFinalPrice(currentSubsCode);
		System.out.println("selectDateGap Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public String selectCurrentServiceName(String currentSubsCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectDateGap Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		String result = null;
		result = mapper.selectCurrentServiceName(currentSubsCode);
		System.out.println("selectDateGap Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public int changeSubscriptionScheduleTransaction(PayHistoryAndDateGapDTO payHistoryAndDateGap) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("changeSubscriptionScheduleTransaction객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		/* 1. 현재 구독중인 서비스 만료처리 */
		System.out.println(payHistoryAndDateGap.getSubsCode());
		int updateResult = mapper.udatePayHistory(payHistoryAndDateGap.getSubsCode());
		System.out.println("changeSubscriptionTransaction객체에서 DAO간 후 udatePayHistory실행" + mapper);
		
		/* 2. 변경한 서비스 insert */
		System.out.println("memberCode: " + payHistoryAndDateGap.getMemberCode());
		int insertSubsResult = mapper.insertSubscriptionSchedule(payHistoryAndDateGap);
		System.out.println("changeSubscriptionTransaction객체에서 DAO간 후 insertSubscription실행" + mapper);
		
		/* 3. 변경한 서비스의 결제내역 insert */
		int insertScheduleChangePayHistoryResult = mapper.insertScheduleChangePayHistory(payHistoryAndDateGap);
		System.out.println("changeSubscriptionTransaction객체에서 DAO간 후 insertPayHistory실행" + mapper);
		
		
		if(updateResult > 0 && insertSubsResult > 0 && insertScheduleChangePayHistoryResult > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return updateResult + insertSubsResult + insertScheduleChangePayHistoryResult;
	}

	public int selectDateBetweenSpecificDate(Map<String, String> dateMap) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectDateBetweenSpecificDate Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		int result = 0;
		result = mapper.selectDateBetweenSpecificDate(dateMap);
		System.out.println("selectDateBetweenSpecificDate Service객체에서 DAO 간후");
		
		sqlSession.close();
		
		return result;
	}

	public PayAndSubscriptionDTO SelectSubsDetailByCode(String subsCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectDateGap Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		PayAndSubscriptionDTO result = null;
		result = mapper.SelectSubsDetailByCode(subsCode);
		System.out.println("selectDateGap Service객체에서 DAO 간후");
		
		sqlSession.close();
		
		return result;
	}

	public int selectStartDateGap(PayAndSubscriptionDTO refundDTO) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectDateGap Service객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		int result = 0;
		result = mapper.selectStartDateGap(refundDTO.getSubsCode());
		System.out.println("selectDateGap Service객체에서 DAO 간후");
		
		sqlSession.close();
		
		return result;
	}

	public int refundTransactionOverTwoDays(PayAndSubscriptionDTO refundDTO) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("changeSubscriptionScheduleTransaction객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		/* 현재 구독중인 서비스 만료처리 */
		System.out.println(refundDTO.getSubsCode());
		int updateResult = mapper.udatePayHistoryUnderStartDay(refundDTO.getSubsCode());
		System.out.println("refundTransaction객체에서 DAO간 후 udatePayHistory실행");
		
		if(updateResult > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return updateResult;
	}

	public int refundTransaction(PayAndSubscriptionDTO refundDTO) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("changeSubscriptionScheduleTransaction객체에서 DAO 가기전");
		ChangePayHistoryMapper mapper = sqlSession.getMapper(ChangePayHistoryMapper.class);
		
		/* 현재 구독중인 서비스 만료처리 */
		System.out.println(refundDTO.getSubsCode());
		int updateResult = mapper.udatePayHistory(refundDTO.getSubsCode());
		System.out.println("refundTransaction객체에서 DAO간 후 udatePayHistory실행");
		
		if(updateResult > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return updateResult;
	}

}
