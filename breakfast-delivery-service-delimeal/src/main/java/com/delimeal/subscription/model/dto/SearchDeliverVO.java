package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class SearchDeliverVO implements Serializable{
	
	private static final long serialVersionUID = 7721891173556818668L;
	
	private String code;
	private java.sql.Date date;

	public SearchDeliverVO() {
	}

	public SearchDeliverVO(String code, Date date) {
		this.code = code;
		this.date = date;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "searchDeliverVO [code=" + code + ", date=" + date + "]";
	}
	
}
