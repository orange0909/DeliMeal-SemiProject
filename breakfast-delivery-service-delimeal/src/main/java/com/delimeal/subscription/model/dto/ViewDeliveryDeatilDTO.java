package com.delimeal.subscription.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class ViewDeliveryDeatilDTO implements Serializable{

	private static final long serialVersionUID = -6670336154053281976L;
	
	private String subsCode;
	private String memberCode;
	private String mealCategoryCode;
	private String deliveryCode;
	private int zipCode;
	private String addrDetail;
	private String addr;
	private String isFinished;
	private java.sql.Date deliveryDate;
	private String mealName;
	
	public ViewDeliveryDeatilDTO() {
		super();
	}

	public ViewDeliveryDeatilDTO(String subsCode, String memberCode, String mealCategoryCode, String deliveryCode,
			int zipCode, String addrDetail, String addr, String isFinished, Date deliveryDate, String mealName) {
		super();
		this.subsCode = subsCode;
		this.memberCode = memberCode;
		this.mealCategoryCode = mealCategoryCode;
		this.deliveryCode = deliveryCode;
		this.zipCode = zipCode;
		this.addrDetail = addrDetail;
		this.addr = addr;
		this.isFinished = isFinished;
		this.deliveryDate = deliveryDate;
		this.mealName = mealName;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public int getZipCode() {
		return zipCode;
	}

	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}

	public String getAddrDetail() {
		return addrDetail;
	}

	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getIsFinished() {
		return isFinished;
	}

	public void setIsFinished(String isFinished) {
		this.isFinished = isFinished;
	}

	public java.sql.Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(java.sql.Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		
		return "{\"subsCode\":\"" + subsCode + "\"," + "\"memberCode\":\"" + memberCode + "\"," + "\"mealCategoryCode\":\"" + mealCategoryCode
				+ "\"," + "\"deliveryCode\":\"" + deliveryCode + "\"," + "\"zipCode\":\"" + zipCode + "\"," + "\"addrDetail\":\"" + addrDetail
				+ "\"," + "\"addr\":\"" + addr + "\"," + "\"isFinished\":\"" + isFinished + "\"," + "\"deliveryDate\":\"" + deliveryDate + "\"," + "\"mealName\":\"" + mealName + "\"}";
		
	}

}