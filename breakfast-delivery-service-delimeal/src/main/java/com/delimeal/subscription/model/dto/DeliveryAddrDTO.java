package com.delimeal.subscription.model.dto;

import java.io.Serializable;

public class DeliveryAddrDTO implements Serializable {
	private static final long serialVersionUID = 579114697777087102L;
	
	private String addrCode;
	private String addrName;
	private int addrZipcode;
	private String addr;
	private String addrDetail;
	private String deliveryREQ;
	private String memberCode;
	
	public DeliveryAddrDTO() { }

	public DeliveryAddrDTO(String addrCode, String addrName, int addrZipcode, String addr, String addrDetail,
			String deliveryREQ, String memberCode) {
		this.addrCode = addrCode;
		this.addrName = addrName;
		this.addrZipcode = addrZipcode;
		this.addr = addr;
		this.addrDetail = addrDetail;
		this.deliveryREQ = deliveryREQ;
		this.memberCode = memberCode;
	}

	public String getAddrCode() {
		return addrCode;
	}

	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}

	public String getAddrName() {
		return addrName;
	}

	public void setAddrName(String addrName) {
		this.addrName = addrName;
	}

	public int getAddrZipcode() {
		return addrZipcode;
	}

	public void setAddrZipcode(int addrZipcode) {
		this.addrZipcode = addrZipcode;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getAddrDetail() {
		return addrDetail;
	}

	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}

	public String getDeliveryREQ() {
		return deliveryREQ;
	}

	public void setDeliveryREQ(String deliveryREQ) {
		this.deliveryREQ = deliveryREQ;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	@Override
	public String toString() {
		return "DeliveryAddr [addrCode=" + addrCode + ", addrName=" + addrName + ", addrZipcode=" + addrZipcode
				+ ", addr=" + addr + ", addrDetail=" + addrDetail + ", deliveryREQ=" + deliveryREQ + ", memberCode="
				+ memberCode + "]";
	}
}
