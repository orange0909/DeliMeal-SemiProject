package com.delimeal.subscription.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.subscription.model.dao.CheckIsDeliveredMapper;
import com.delimeal.subscription.model.dto.DeliveryDetailDTO;
import com.delimeal.subscription.model.dto.SearchDeliverVO;

public class CheckIsDeliveredService {

	public DeliveryDetailDTO checkIsDelivered(SearchDeliverVO searchCriteria) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("checkIsDelivered Service객체에서 DAO 가기전");
		CheckIsDeliveredMapper mapper = sqlSession.getMapper(CheckIsDeliveredMapper.class);
		
		DeliveryDetailDTO result = new DeliveryDetailDTO();
		result = mapper.selectAllDeliveryDetail(searchCriteria);
		System.out.println("checkIsDelivered Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

}
