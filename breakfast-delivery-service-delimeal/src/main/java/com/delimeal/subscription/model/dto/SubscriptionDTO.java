package com.delimeal.subscription.model.dto;

import java.io.Serializable;

public class SubscriptionDTO implements Serializable{
	private static final long serialVersionUID = -1118279690208873476L;
	
	private String subsCode;
	private String cancelYN;
	private int deliveryZipcode;
	private String deliveryAddr;
	private String deliveryAddrDetail;
	private String memberCode;
	private String mealCategoryCode;
	
	public SubscriptionDTO() { }

	public SubscriptionDTO(String subsCode, String cancelYN, int deliveryZipcode, String deliveryAddr,
			String deliveryAddrDetail, String memberCode, String mealCategoryCode) {
		this.subsCode = subsCode;
		this.cancelYN = cancelYN;
		this.deliveryZipcode = deliveryZipcode;
		this.deliveryAddr = deliveryAddr;
		this.deliveryAddrDetail = deliveryAddrDetail;
		this.memberCode = memberCode;
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public String getCancelYN() {
		return cancelYN;
	}

	public void setCancelYN(String cancelYN) {
		this.cancelYN = cancelYN;
	}

	public int getDeliveryZipcode() {
		return deliveryZipcode;
	}

	public void setDeliveryZipcode(int deliveryZipcode) {
		this.deliveryZipcode = deliveryZipcode;
	}

	public String getDeliveryAddr() {
		return deliveryAddr;
	}

	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}

	public String getDeliveryAddrDetail() {
		return deliveryAddrDetail;
	}

	public void setDeliveryAddrDetail(String deliveryAddrDetail) {
		this.deliveryAddrDetail = deliveryAddrDetail;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	@Override
	public String toString() {
		return "SubscriptionDTO [subsCode=" + subsCode + ", cancelYN=" + cancelYN + ", deliveryZipcode="
				+ deliveryZipcode + ", deliveryAddr=" + deliveryAddr + ", deliveryAddrDetail=" + deliveryAddrDetail
				+ ", memberCode=" + memberCode + ", mealCategoryCode=" + mealCategoryCode + "]";
	}
}
