package com.delimeal.subscription.model.dto;

import java.io.Serializable;

import com.delimeal.member.model.dto.MemberDTO;

public class PayDTO implements Serializable {
	private static final long serialVersionUID = -7122985578244230053L;
	
	private String subsCode;
	private MemberDTO buyer;
	private String mealCategoryCode;
	private String mealCategoryName;
	private int mealCategoryPrice;
	private double cpnDiscountRate;
	private DeliveryAddrDTO addr;
	
	public PayDTO() { }

	public PayDTO(String subsCode, MemberDTO buyer, String mealCategoryCode, String mealCategoryName, int mealCategoryPrice, double cpnDiscountRate, DeliveryAddrDTO addr) {
		this.subsCode = subsCode;
		this.buyer = buyer;
		this.mealCategoryCode = mealCategoryCode;
		this.mealCategoryName = mealCategoryName;
		this.mealCategoryPrice = mealCategoryPrice;
		this.cpnDiscountRate = cpnDiscountRate;
		this.addr = addr;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public MemberDTO getBuyer() {
		return buyer;
	}

	public void setBuyer(MemberDTO buyer) {
		this.buyer = buyer;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getMealCategoryName() {
		return mealCategoryName;
	}

	public void setMealCategoryName(String mealCategoryName) {
		this.mealCategoryName = mealCategoryName;
	}

	public int getMealCategoryPrice() {
		return mealCategoryPrice;
	}

	public void setMealCategoryPrice(int mealCategoryPrice) {
		this.mealCategoryPrice = mealCategoryPrice;
	}

	public double getCpnDiscountRate() {
		return cpnDiscountRate;
	}

	public void setCpnDiscountRate(double cpnDiscountRate) {
		this.cpnDiscountRate = cpnDiscountRate;
	}

	public DeliveryAddrDTO getAddr() {
		return addr;
	}

	public void setAddr(DeliveryAddrDTO addr) {
		this.addr = addr;
	}

	@Override
	public String toString() {
		return "PayDTO [subsCode=" + subsCode + ", buyer=" + buyer + ", mealCategoryCode=" + mealCategoryCode + ", mealCategoryName=" + mealCategoryName + ", mealCategoryPrice=" + mealCategoryPrice + ", cpnDiscountRate=" + cpnDiscountRate + ", addr=" + addr + "]";
	}
}
