package com.delimeal.subscription.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.dto.MealPerDayDTO;
import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.member.model.dto.IssuedCouponDTO;
import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.DeliveryAddrDTO;
import com.delimeal.subscription.model.dto.MealCategoryDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.PayInfoDTO;
import com.delimeal.subscription.model.dto.SubscriptionDTO;

public interface SubscriptionMapper {

	List<CategoryDTO> selectAllCategory();

	List<CategoryDTO> selectAllCategoryByPopularity();

	List<CategoryDTO> selectHealthyCategory();

	List<CategoryDTO> selectPremiumCategory();

	CategoryDTO selectCategoryByCode(String categoryCode);

	List<MealPerDayDTO> selectMealPerDayByCategoryCode(String categoryCode);

	CategoryDTO selectCategoryByMealCode(String mealCode);

	MealDTO selectMealByCode(String mealCode);

	List<MenuDTO> selectMenuByMealCode(String mealCode);

	MemberDTO selectMemberByIdForPayment(String id);

	PayInfoDTO selectPayInfoByMemberCodeForPayment(String memberCode);

	DeliveryAddrDTO selectDefaultAddrByMemberCodeForPayment(String memberCode);

	List<IssuedCouponDTO> selectAllIssuedCouponForPayment(String memberCode);

	String getSubsCode();

	MemberDTO selectMemberInfoByMember(String memberCode);

	MealCategoryDTO selectCategoryInfoByCategoryCode(String categoryCode);

	IssuedCouponInfoDTO selectCouponInfoByCouponCode(String issuedCouponCode);

	DeliveryAddrDTO selectAddrInfoByAddrCode(String addrCode);

	MemberDTO selectMemberByNameAndEmail(Map<String, String> nameAndEmail);

	int insertSubscriptionForPayment(SubscriptionDTO subs);

	String getLastSubsCode();

	int updateMemberSubscriptionForPayment(MemberDTO member);

	int updateCouponValidateForPayment(IssuedCouponInfoDTO coupon);

	int insertPayHistoryForPayment(PayHistoryDTO coupon);

	CategoryDTO selectCategoryByName(String categoryName);
	
}
