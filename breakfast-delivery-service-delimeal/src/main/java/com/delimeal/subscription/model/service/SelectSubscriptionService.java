package com.delimeal.subscription.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.subscription.model.dao.SelectSubscriptionMapper;
import com.delimeal.subscription.model.dto.PayHistoryAndDateGapDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.SearchDeliverVO;
import com.delimeal.subscription.model.dto.SelectSubscriptionDTO;
import com.delimeal.subscription.model.dto.SubscriptionDTO;
import com.delimeal.subscription.model.dto.ViewDeliveryDeatilDTO;

public class SelectSubscriptionService {

	public SelectSubscriptionDTO SelectSubscription(String subsCode) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		SelectSubscriptionDTO result = null;
		result = mapper.selectSubscription(subsCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public String selectCurrentMeal(String memberCode) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		String result = null;
		result = mapper.selectCurrentMeal(memberCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public String selectCurrentSubsCode(String memberCode) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		String result = null;
		result = mapper.selectCurrentSubsCode(memberCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public SelectSubscriptionDTO selectCurrentSubscription(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		SelectSubscriptionDTO result = null;
		result = mapper.selectCurrentSubscription(memberCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public List<SelectSubscriptionDTO> selectCurrentSubscriptions(String subsCode) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectCurrentSubscriptions Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		List<SelectSubscriptionDTO> result = new ArrayList<>();
		result = mapper.selectCurrentSubscriptions(subsCode);
		System.out.println("selectCurrentSubscriptions Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public String selectCurrentMealBySubsCode(String subsCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		String result = null;
		result = mapper.selectCurrentMealBySubsCode(subsCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public ViewDeliveryDeatilDTO selectDeliveryDetails(SearchDeliverVO searchCriteria) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		ViewDeliveryDeatilDTO result = null;
		result = mapper.selectDeliveryDetails(searchCriteria);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public SubscriptionDTO selectSubscriptionBySubsCode(String subsCode) {
		SqlSession sqlSession = getSqlSession();
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		SubscriptionDTO result = mapper.selectSubscriptionBySubsCode(subsCode);
		
		sqlSession.close();
		
		return result;
	}
	
	public List<SelectSubscriptionDTO> selectCurrentSubscriptionsByDate(Map<String, String> parameter) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectSubscriptionMapper mapper = sqlSession.getMapper(SelectSubscriptionMapper.class);
		
		List<SelectSubscriptionDTO> result = null;
		result = mapper.selectCurrentSubscriptionsByDate(parameter);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

}