package com.delimeal.subscription.model.dto;

import java.io.Serializable;

public class selectDateGapDTO implements Serializable{
	/* 구독중인 서비스를 바꿀때, 바꿀 서비스가 다른 가격일 경우 두 만료일과 현재 날짜의 차이를 받아오는 DTO */
	private static final long serialVersionUID = 2857785314193383569L;
	
	private String subsCode;
	private int dateGap;
	
	public selectDateGapDTO(String subsCode, int dateGap) {
		super();
		this.subsCode = subsCode;
		this.dateGap = dateGap;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public int getDateGap() {
		return dateGap;
	}

	public void setDateGap(int dateGap) {
		this.dateGap = dateGap;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "selectDateGapDTO [subsCode=" + subsCode + ", dateGap=" + dateGap + "]";
	}
	
}
