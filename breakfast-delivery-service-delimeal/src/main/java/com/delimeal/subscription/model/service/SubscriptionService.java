package com.delimeal.subscription.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.dto.MealPerDayDTO;
import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.member.model.dto.IssuedCouponDTO;
import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dao.SubscriptionMapper;
import com.delimeal.subscription.model.dto.DeliveryAddrDTO;
import com.delimeal.subscription.model.dto.MealCategoryDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.PayInfoDTO;
import com.delimeal.subscription.model.dto.SubscriptionDTO;

public class SubscriptionService {

	public List<CategoryDTO> selectAllCategory() {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 식단 카테고리 전체 요청 */
		List<CategoryDTO> categoryList = mapper.selectAllCategory();
		
		sqlSession.close();
		
		return categoryList;
	}

	public List<CategoryDTO> selectAllCategoryByPopularity() {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/*
		 * 식단 카테고리를 전체 요청하되 인기순서대로 정렬함
		 * 정렬 순서
		 * 1. 인기순 (NUMBER_OF_SUBS)
		 * 2. 식단 카테고리 번호 순 (MEAL_CATE_CODE)
		 */
		
		List<CategoryDTO> categoryList = mapper.selectAllCategoryByPopularity();
		
		sqlSession.close();
		
		return categoryList;
	}

	public List<CategoryDTO> selectHealthyCategory() {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 건강 식단 카테고리 요청 */
		List<CategoryDTO> categoryList = mapper.selectHealthyCategory();
		
		sqlSession.close();
		
		return categoryList;
	}

	public List<CategoryDTO> selectPremiumCategory() {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 프리미엄 식단 카테고리 요청 */
		List<CategoryDTO> categoryList = mapper.selectPremiumCategory();
		
		sqlSession.close();
		
		return categoryList;
	}

	public CategoryDTO selectCategoryByCode(String categoryCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 카테고리 코드로 식단 카테고리 요청 */
		CategoryDTO category = mapper.selectCategoryByCode(categoryCode);
		
		sqlSession.close();
		
		return category;
	}
	
	public CategoryDTO selectCategoryByName(String categoryName) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 카테고리 코드로 식단 카테고리 요청 */
		CategoryDTO category = mapper.selectCategoryByName(categoryName);
		
		sqlSession.close();
		
		return category;
	}

	public List<MealPerDayDTO> selectMealPerDayByCategoryCode(String categoryCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 카테고리 코드로 식단 카테고리 요청 */
		List<MealPerDayDTO> mealPerDayList = mapper.selectMealPerDayByCategoryCode(categoryCode);
		
		sqlSession.close();
		
		return mealPerDayList;
	}

	public CategoryDTO selectCategoryByMealCode(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 식단 코드로 식단 카테고리 요청 */
		CategoryDTO category = mapper.selectCategoryByMealCode(mealCode);
		
		sqlSession.close();
		
		return category;
	}

	public MealDTO selectMealByCode(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 식단 코드로 식단 요청 */
		MealDTO meal = mapper.selectMealByCode(mealCode);

		sqlSession.close();
		
		return meal;
	}

	public List<MenuDTO> selectMenuByMealCode(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 식단 코드로 메뉴리스트 요청 */
		List<MenuDTO> menuList = mapper.selectMenuByMealCode(mealCode);

		sqlSession.close();
		
		return menuList;
	}

	public MemberDTO selectMemberByIdForPayment(String id) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 회원 아이디로 회원정보 요청 */
		MemberDTO member = mapper.selectMemberByIdForPayment(id);

		sqlSession.close();
		
		return member;
	}

	public PayInfoDTO selectPayInfoByMemberCodeForPayment(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 회원 아이디로 결제정보 요청 */
		PayInfoDTO payInfo = mapper.selectPayInfoByMemberCodeForPayment(memberCode);

		sqlSession.close();
		
		return payInfo;
	}

	public DeliveryAddrDTO selectDefaultAddrByMemberCodeForPayment(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 회원 아이디로 기본 배송지 요청 */
		DeliveryAddrDTO deliveryAddr = mapper.selectDefaultAddrByMemberCodeForPayment(memberCode);

		sqlSession.close();
		
		return deliveryAddr;
	}

	public List<IssuedCouponDTO> selectAllIssuedCouponForPayment(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		/* 회원 아이디로 기본 배송지 요청 */
		List<IssuedCouponDTO> couponList = mapper.selectAllIssuedCouponForPayment(memberCode);

		sqlSession.close();
		
		return couponList;
	}

	public String getSubsCode() {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		String subsCode = mapper.getSubsCode();

		sqlSession.close();
		
		return subsCode;
	}

	public MemberDTO selectMemberInfoByMember(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		MemberDTO memberInfo = mapper.selectMemberInfoByMember(memberCode);

		sqlSession.close();
		
		return memberInfo;
	}

	public MealCategoryDTO selectCategoryInfoByCategoryCode(String categoryCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		MealCategoryDTO categoryInfo = mapper.selectCategoryInfoByCategoryCode(categoryCode);

		sqlSession.close();
		
		return categoryInfo;
	}

	public IssuedCouponInfoDTO selectCouponInfoByCouponCode(String issuedCouponCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		IssuedCouponInfoDTO couponInfo = mapper.selectCouponInfoByCouponCode(issuedCouponCode);

		sqlSession.close();
		
		return couponInfo;
	}

	public DeliveryAddrDTO selectAddrInfoByAddrCode(String addrCode) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		DeliveryAddrDTO addrInfo = mapper.selectAddrInfoByAddrCode(addrCode);

		sqlSession.close();
		
		return addrInfo;
	}

	public MemberDTO selectMemberByNameAndEmail(Map<String, String> nameAndEmail) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		
		MemberDTO member = mapper.selectMemberByNameAndEmail(nameAndEmail);

		sqlSession.close();
		
		return member;
	}

	public boolean insertPaymentInformation(SubscriptionDTO subs, MemberDTO member, IssuedCouponInfoDTO coupon, PayHistoryDTO history) {
		SqlSession sqlSession = getSqlSession();
		SubscriptionMapper mapper = sqlSession.getMapper(SubscriptionMapper.class);
		boolean result = false;
		
		int dmlResult = mapper.insertSubscriptionForPayment(subs);
		
		dmlResult += mapper.updateMemberSubscriptionForPayment(member);
		
		if(coupon != null) {
			dmlResult += mapper.updateCouponValidateForPayment(coupon);
		} else {
			dmlResult += 1;
		}
		
		dmlResult += mapper.insertPayHistoryForPayment(history);
		
		if(dmlResult == 4) {
			sqlSession.commit();
			result = true;
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

}
