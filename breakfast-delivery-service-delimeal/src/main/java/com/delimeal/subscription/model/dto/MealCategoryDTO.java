package com.delimeal.subscription.model.dto;

import java.io.Serializable;

public class MealCategoryDTO implements Serializable{
	private static final long serialVersionUID = -1332334796717521293L;
	
	private String code;
	private String name;
	private String detail;
	private int price;
	private String type;
	private String image;
	private int numberOfSubs;
	
	public MealCategoryDTO() {
	}

	public MealCategoryDTO(String code, String name, String detail, int price, String type, String image,
			int numberOfSubs) {
		super();
		this.code = code;
		this.name = name;
		this.detail = detail;
		this.price = price;
		this.type = type;
		this.image = image;
		this.numberOfSubs = numberOfSubs;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public int getNumberOfSubs() {
		return numberOfSubs;
	}

	public void setNumberOfSubs(int numberOfSubs) {
		this.numberOfSubs = numberOfSubs;
	}
	
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "MealCategoryDTO [code=" + code + ", name=" + name + ", detail=" + detail + ", price=" + price
				+ ", type=" + type + ", image=" + image + ", numberOfSubs=" + numberOfSubs + "]";
	}
	
}
