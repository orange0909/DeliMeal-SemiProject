package com.delimeal.subscription.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.MealCategoryDTO;
import com.delimeal.subscription.model.service.SelectMealCategoryService;
import com.delimeal.subscription.model.service.SelectSubscriptionService;

@WebServlet("/subs/GoToChangeService")
public class GoToChangeService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 세션에서 회원번호 뽑아오기(구현해야함) */
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		
		/* 해당 회원의 구독중인 식단 조회해오기 */
		SelectSubscriptionService selectSubsService = new SelectSubscriptionService();
		String memberMealCode = selectSubsService.selectCurrentMeal(memberCode);
		
//		System.out.println(memberMealCode);
		request.setAttribute("mealCode", memberMealCode);
		
		List<MealCategoryDTO> mealList = null;
		SelectMealCategoryService selectMealService = new SelectMealCategoryService();
		/* 현재 회원이 구독중이지 않은 식단만 조회 해온다 */
		mealList = selectMealService.selectAllMeal(memberMealCode);
//		System.out.println(mealList);
		request.setAttribute("mealList", mealList);
		
		request.getRequestDispatcher("/WEB-INF/views/subs/memberChangeService.jsp").forward(request, response);
			
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
