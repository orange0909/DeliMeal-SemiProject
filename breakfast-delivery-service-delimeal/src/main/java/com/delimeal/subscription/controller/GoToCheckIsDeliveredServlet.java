package com.delimeal.subscription.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.SelectSubscriptionDTO;
import com.delimeal.subscription.model.service.SelectSubscriptionService;

@WebServlet("/subs/goToCheckIsDelivered")
public class GoToCheckIsDeliveredServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		/* 로그인 하지 않은 경우 로그인 페이지로 이동 */
		if(request.getSession().getAttribute("loginMember") == null) {
			response.sendRedirect("/delimeal/login");
		} else {
				request.getRequestDispatcher("/WEB-INF/views/subs/memberCheckIsDeliveredPage.jsp").forward(request, response);
			} 
		}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
