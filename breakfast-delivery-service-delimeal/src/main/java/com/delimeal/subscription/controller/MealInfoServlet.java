package com.delimeal.subscription.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealPerDayDTO;
import com.delimeal.subscription.model.service.SubscriptionService;

@WebServlet("/subs/meal/info")
public class MealInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SubscriptionService subsService = new SubscriptionService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String categoryCode = request.getParameter("pid");

		/* 입력된 카테고리 코드를 사용해 카테고리 가져와서 request scope의 속성으로 추가 */
		CategoryDTO category = subsService.selectCategoryByCode(categoryCode);
		request.setAttribute("category", category);
		
		/* 입력된 카테고리 코드를 사용해 2주간의 식단을 가져와서 request scope의 속성으로 추가 */
		List<MealPerDayDTO> mealList = subsService.selectMealPerDayByCategoryCode(categoryCode);
		request.setAttribute("mealList", mealList);
		
		request.getRequestDispatcher("/WEB-INF/views/subs/memberMealInfoPage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
