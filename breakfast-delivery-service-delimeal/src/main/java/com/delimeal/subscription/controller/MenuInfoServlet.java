package com.delimeal.subscription.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.subscription.model.service.SubscriptionService;

@WebServlet("/subs/menu/info")
public class MenuInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SubscriptionService subsService = new SubscriptionService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mealCode = request.getParameter("pid");

		/* 입력된 식단 코드를 사용해 카테고리를 가져와서 request scope의 속성으로 추가 */
		CategoryDTO category = subsService.selectCategoryByMealCode(mealCode);
		request.setAttribute("category", category);
		
		/* 입력된 식단 코드를 사용해 식단을 가져와서 request scope의 속성으로 추가 */
		MealDTO meal = subsService.selectMealByCode(mealCode);
		request.setAttribute("meal", meal);
		
		/* 입력된 식단 코드를 사용해 식단에 포함된 메뉴를 리스트로 가져와서 request scope의 속성으로 추가 */
		List<MenuDTO> menuList = subsService.selectMenuByMealCode(mealCode);
		request.setAttribute("menuList", menuList);
		
		request.getRequestDispatcher("/WEB-INF/views/subs/memberMenuInfoPage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
