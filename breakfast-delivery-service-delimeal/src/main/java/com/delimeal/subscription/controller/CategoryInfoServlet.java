package com.delimeal.subscription.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.subscription.model.service.SubscriptionService;

@WebServlet("/subs/meal")
public class CategoryInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private SubscriptionService subsService = new SubscriptionService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String by = request.getParameter("by");
		
		/* null 이면 switch에서 exception이 일어나므로 "" 로 치환해서 default로 처리되도록 유도 */
		if(by == null) {
			by = "";
		}
		
		List<CategoryDTO> categoryList = null;
		
		/* 요청 형태에 따라 다른 리스트 받아옴 */
		switch (by) {
			case "all":
				categoryList = subsService.selectAllCategory();
				break;
			case "popularity":
				categoryList = subsService.selectAllCategoryByPopularity();
				break;
			case "healthy":
				categoryList = subsService.selectHealthyCategory();
				break;
			case "premium":
				categoryList = subsService.selectPremiumCategory();
				break;
			default:
				/* 정의된 요청 형태가 아니면 전체를 의미하는 all로 redirect 시킴 */
				response.sendRedirect(request.getContextPath() + "/subs/meal?by=all");
				return;
		}
		
		/* 카테고리 리스트를 리퀘스트 스코프에 categoryList 라는 이름의 키로 저장 */
		request.setAttribute("categoryList", categoryList);
		/* 요청 형태도 리퀘스트 스코프에 저장 */
		request.setAttribute("by", by);
		
		request.getRequestDispatcher("/WEB-INF/views/subs/memberCategoryInfoPage.jsp").forward(request, response);
	}
}
