package com.delimeal.subscription.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.SubscriptionDTO;
import com.delimeal.subscription.model.service.SubscriptionService;
import com.google.gson.Gson;

@WebServlet("/subs/payProcess")
public class PayProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	/**
	 * buyerAddr:				구매자 주소
	 * buyerEmail:				구매자 이메일
	 * buyerName:				구매자 이름
	 * buyerPostcode:			구매자 우편번호
	 * buyerTel:				구매자 핸드폰 번호
	 * subsCode:				구독(결제) 고유 아이디
	 * categoryName: 			구매한 카테고리 이름
	 * payMethod: 				결제수단
	 * payId: 					결제 ID != SUBS_CODE
	 * paidPrice: 				최종 결제 금액
	 * discountPriceByCoupon:	쿠폰으로 할인받은 금액
	 * useCouponCode:			사용한 쿠폰 코드
	 * discountPriceByPoint: 	사용한 포인트
	 * status: 					결제 상태값
	 * deliveryDate:			배송 시작일
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 검증 결과 담는 bool 변수 */
		boolean result = true;
		
		SubscriptionService service = new SubscriptionService();
		
		String subsCode = request.getParameter("subsCode");
		String buyerName = request.getParameter("buyerName");
		String buyerEmail = request.getParameter("buyerEmail");
		String mealCategoryName = request.getParameter("categoryName");
		String discountPriceByCoupon = request.getParameter("discountPriceByCoupon");
		String discountPriceByPoint = request.getParameter("discountPriceByPoint");
		String useCouponCode = request.getParameter("useCouponCode");
		String paidPrice = request.getParameter("paidPrice");
		String buyerPostcode = request.getParameter("buyerPostcode");
		String buyerAddr = request.getParameter("buyerAddr");
		String deliveryDate = request.getParameter("deliveryDate");
		
		/* 이름과 이메일로 회원정보 가져오기 */
		Map<String, String> nameAndEmail = new HashMap<String, String>();
		nameAndEmail.put("memberName", buyerName);
		nameAndEmail.put("memberEmail", buyerEmail);
		MemberDTO member = service.selectMemberByNameAndEmail(nameAndEmail);
		
		/* 카테고리 코드로 식단 카테고리 정보 가져오기 */
		CategoryDTO category = service.selectCategoryByName(mealCategoryName);
		
		/* 발급된 쿠폰 코드로 쿠폰 정보(할인율) 가져오기 */
		IssuedCouponInfoDTO coupon = null;
		if(useCouponCode != null && !"none".equals(useCouponCode)) {
			coupon = service.selectCouponInfoByCouponCode(useCouponCode);
		}
		
		/* 
		 * 결제 가격 검증 
		 * 1. 포인트 최대치 검증
		 * 2. 쿠폰 할인금액 검증
		 * 3. 최종 검증
		 */
		
		/* 1. 포인트 검증 */
		int usedPoint = 0, usedCoupon = 0;
		if(discountPriceByPoint != null && !"".equals(discountPriceByPoint)) {
			usedPoint = Integer.valueOf(discountPriceByPoint);
			if(member.getPoint() < usedPoint) {
				System.out.println("fail - 1");
				sendResult(response, "failure");
				return;
			}
			member.setPoint(member.getPoint() - usedPoint);
		}
		
		/* 2. 쿠폰 검증 */
		if(discountPriceByCoupon != null && !"".equals(discountPriceByCoupon)) {
			usedCoupon = Integer.valueOf(discountPriceByCoupon);
			if(coupon == null) {
				if(0 != usedCoupon) {
					System.out.println("fail - 2");
					sendResult(response, "failure");
					return;
				}
			} else {
				if(category.getCategoryPrice() * coupon.getCpnDiscountRate() * -1 != usedCoupon) {
					System.out.println("category.getCategoryPrice(): " + category.getCategoryPrice());
					System.out.println("coupon.getCpnDiscountRate(): " + coupon.getCpnDiscountRate());
					System.out.println("usedCoupon: " + usedCoupon);
					System.out.println("fail - 3");
					sendResult(response, "failure");
					return;
				}
			}
		}
		
		/* 최종금액 검증 */
		if(paidPrice != null && !"".equals(paidPrice)) {
			if(Integer.valueOf(paidPrice) != category.getCategoryPrice() + usedCoupon - usedPoint) {
				System.out.println("fail - 4");
				sendResult(response, "failure");
				return;
			}
		}
		
		/* PayHistoryDTO (결제 내역) 정보 채우기 */
		PayHistoryDTO history = new PayHistoryDTO();
		java.sql.Date startDate = java.sql.Date.valueOf(deliveryDate);
		GregorianCalendar temp = new GregorianCalendar();	// 그레고리언 캘린더로 변환해서 7일 추가
		temp.setTime(startDate);
		temp.add(Calendar.DATE, 7);
		java.sql.Date endDate = new java.sql.Date(temp.getTimeInMillis());
		history.setStartDate(startDate);
		history.setExprieDate(endDate);
		history.setDiscountPrice(usedPoint + usedCoupon);
		history.setFinalPrice(Integer.valueOf(paidPrice));
		history.setCpnCode(useCouponCode);
		history.setSubsCode(subsCode);
		
		/* Subscription 테이블에 구독 정보 삽입 */
		SubscriptionDTO subs = new SubscriptionDTO();
		if(member != null) {
			subs.setSubsCode(subsCode);
			subs.setSubsCode(subsCode);
			subs.setMemberCode(member.getCode());
			subs.setMealCategoryCode(category.getCategoryCode());
			String[] addrArray =  buyerAddr.split(", ");
			subs.setDeliveryZipcode(Integer.valueOf(buyerPostcode));
			subs.setDeliveryAddr(addrArray[0]);
			subs.setDeliveryAddrDetail(addrArray[1]);
			
			if(service.insertPaymentInformation(subs, member, coupon, history)) {
				System.out.println("성공!!!!!!!!!!!!!!!!!!!!!!");
				// 삽입 성공
			} else {
				// 삽입 실패
				System.out.println("실패!!!!!!!!!!!!!!!!!!!!!");
				result = false;
			}
		} else {
			System.out.println("실패!!!!!!!!!!!!!!!!!!!!!");
			// 멤버가 없을때 -> 실패 
			result = false;
		}
		
		if(result) {
			sendResult(response, "success");			
		} else {
			sendResult(response, "failure");
		}
	}
	
	private void sendResult(HttpServletResponse response, String code) throws ServletException, IOException {
		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		out.print(new Gson().toJson(code));
		
		out.flush();
		out.close();
	}
}
