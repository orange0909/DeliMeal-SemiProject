package com.delimeal.subscription.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.SelectSubscriptionDTO;
import com.delimeal.subscription.model.service.SelectSubscriptionService;

@WebServlet("/subs/GoToSelectSubscriptionServlet")
public class GoToSelectSubscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		/* 세션에서 회원 구독번호, 구독여부 뽑아오기 */
		MemberDTO loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		System.out.println(loginMember);
		
		/* 해당 회원이 구독중인 모든 구독상품 list 조회해오기 */
		SelectSubscriptionService selectSubsService = new SelectSubscriptionService();
		List<SelectSubscriptionDTO> subsList = new ArrayList<>();
		
		subsList = selectSubsService.selectCurrentSubscriptions(loginMember.getCode());
		System.out.println(subsList);
		
		if(subsList.size() == 0) {
			/* 메뉴 선택 페이지로 이동(지금은 일단 메인으로 이동시킴) */
			request.getRequestDispatcher("/WEB-INF/views/main/main.jsp").forward(request, response);;
		}
		if(subsList.get(0).getIsExpire().equals("N")) {
			/* 구독번호로 구독정보 조회해오기 */
			request.setAttribute("subscriptionInfos", subsList);
			
			request.getRequestDispatcher("/WEB-INF/views/subs/memberSelectSubscribPage.jsp").forward(request, response);;
		} else { // 구독 여부가 "N"일 경우 식단선택 페이지로 이동
			request.getRequestDispatcher("/WEB-INF/views/subs/meal.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
