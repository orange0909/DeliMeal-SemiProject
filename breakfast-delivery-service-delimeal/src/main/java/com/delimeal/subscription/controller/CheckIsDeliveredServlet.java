package com.delimeal.subscription.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.SearchDeliverVO;
import com.delimeal.subscription.model.dto.SelectSubscriptionDTO;
import com.delimeal.subscription.model.dto.ViewDeliveryDeatilDTO;
import com.delimeal.subscription.model.service.SelectSubscriptionService;

@WebServlet("/subs/checkIsDelivered")
public class CheckIsDeliveredServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		
		System.out.println("Ajax로 Servlet도착");
		/* 먼저 세션에 저장되어 있는 회원의 구독번호를 뽑아오고, 배송정보 테이블에서 해당 구독번호와 날짜를 이용해서 조회를 해와야 한다.(여기 수정되어야 함) */
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		SelectSubscriptionService selectSubsService = new SelectSubscriptionService();
		List<SelectSubscriptionDTO> subsLists = new ArrayList<>();
		
		String searchDate = request.getParameter("date");
		System.out.println("front에서 가져온 date: " + searchDate);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			java.util.Date date = new java.text.SimpleDateFormat("MM/dd/yyyy").parse(searchDate);
			System.out.println(date);
			/* java.util.Date로 바꾼후, java.sql.Date로 변환해준다 */
			java.sql.Date dateSql = new java.sql.Date(date.getTime());
			System.out.println(dateSql);
			String selectDate = formatter.format(date);
			System.out.println("selectDate: " + formatter.format(date));
			Map<String, String> parameter = new HashMap<>();
			parameter.put("date", formatter.format(date));
			parameter.put("member", memberCode);
			subsLists = selectSubsService.selectCurrentSubscriptionsByDate(parameter);
			System.out.println(subsLists);
			
			/* 구독 코드들을 담을 List */
			List<String> subsCodes = new ArrayList<>();
			for(int i = 0; i < subsLists.size(); i++) {
				subsCodes.add(subsLists.get(i).getSubsCode());
			}
			
			/* 조회할 구독코드가 있어야 한다 */
			if(subsCodes.size() > 0) {
				SearchDeliverVO searchCriteria = new SearchDeliverVO();
				/* 구독 코드로 화면에 뿌릴 정보들을 조회해 담는 List */
				List<ViewDeliveryDeatilDTO> deliveryDetails = new ArrayList<>();
				for(int i = 0; i< subsCodes.size(); i++) {
					System.out.println(i+"번째 subsCode: " + subsCodes.get(i));
					searchCriteria.setCode(subsCodes.get(i));
					searchCriteria.setDate(dateSql);
					/* subsCode와 해당일의 날짜를 통해 구체적인 배송정보를 조회해 온다 */
					ViewDeliveryDeatilDTO deliveryDetail = selectSubsService.selectDeliveryDetails(searchCriteria);
					if(deliveryDetail.getIsFinished().equals("Y")) {
						deliveryDetail.setIsFinished("배송 완료");
					} else {
						deliveryDetail.setIsFinished("배송 미완료");
					}

					deliveryDetails.add(deliveryDetail);
				}
				System.out.println(deliveryDetails);
				/* deliveryDetails의 toString은 json문자열 형태로 되어 있다 */
				String jsonArray = deliveryDetails.toString();
				
				PrintWriter out = response.getWriter();
				out.print(jsonArray);
				out.flush();
				out.close();
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		
//		/* 넘어온 날짜는 String 형이기 때문에, DB에서 검색 하려면 SQL데이트 형으로 바꾸어 주어야 한다. */
//		System.out.println(request.getParameter("date").getClass());
//		String parseDate = request.getParameter("date");
//		System.out.println(parseDate);
//		
//		try {
//			/* java.sql.Date로 변환하기 위해 먼저 java.util.Date로 변환한다 */
//			java.util.Date date = new java.text.SimpleDateFormat("MM/dd/yyyy").parse(parseDate);
//			System.out.println(date);
//			/* java.util.Date로 바꾼후, java.sql.Date로 변환해준다 */
//			java.sql.Date dateSql = new java.sql.Date(date.getTime());
//			System.out.println(dateSql);
//			
//			SearchDeliverVO searchCriteria = new SearchDeliverVO(SUBS_CODE, dateSql);
//			
//			CheckIsDeliveredService service = new CheckIsDeliveredService();
//			DeliveryDetailDTO delivery = service.checkIsDelivered(searchCriteria); 
//			if(delivery.getIsFinish().equals("Y")) {
//				delivery.setIsFinish("배송 완료");
//			} else {
//				delivery.setIsFinish("배송 미완료");
//			}
//			
//			String jsonString = new Gson().toJson(delivery);
//			
//			response.setContentType("application/json; charset=UTF-8");
//			PrintWriter out = response.getWriter();
//			out.print(jsonString);
//			out.flush();
//			out.close();
//
//		} catch (ParseException e) {
//			e.printStackTrace();
//		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
