package com.delimeal.subscription.controller;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.PayHistoryAndDateGapDTO;
import com.delimeal.subscription.model.dto.PayHistoryDTO;
import com.delimeal.subscription.model.dto.SubscriptionDTO;
import com.delimeal.subscription.model.service.ChangeMealService;
import com.delimeal.subscription.model.service.SelectSubscriptionService;

@WebServlet("/subs/ChangeSubscriptionSchedule")
public class ChangeSubscriptionScheduleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 기존 구독중인 식단을 조회하기 위해 session에서 회원 코드를 뽑아낸다. */
		MemberDTO loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = loginMember.getCode();
		System.out.println(memberCode);
		/* 변경할 서비스의 categoryCode */
		String subsCode = request.getParameter("currnetsubsCode");
		System.out.println("currnetmealCategoryCode: " + subsCode);

		/* 현재 구독 중인 서비스의 만료기한, 환불여부, 만료일을 update해줘야 하니 DM_PAY_HISTORY 테이블을 읽어온다 */
		ChangeMealService changeService = new ChangeMealService();
		PayHistoryDTO payHistory = changeService.selectPayHistroy(subsCode);
		System.out.println(payHistory);
		
		/* 날짜 비교를 위해 받아온 날짜와 현재 날짜 변수를 선언하고 날짜 비교릃 한다 */
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		String startDate = fmt.format(payHistory.getStartDate());
		String changeDate = request.getParameter("changeDate");
		System.out.println(startDate);
		System.out.println(changeDate);
		
		SimpleDateFormat isWeekend = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date isWeekendDate;
		try {
			isWeekendDate = isWeekend.parse(changeDate);
			/* 선택한 날짜가 영업일이 아닌 경우 예외 처리 */
			if(isWeekendDate.getDay() == 0 || isWeekendDate.getDay() == 6) {
				request.getRequestDispatcher("/WEB-INF/views/subs/memeberSelectWeekendDatePage.jsp").forward(request, response);
			} else {
				/* 두 날짜간의 차이 계산 */
				Map<String, String> dateMap = new HashMap<>();
				dateMap.put("startDate", startDate);
				dateMap.put("changeDate", changeDate);
				
				int compare = changeService.selectDateBetweenSpecificDate(dateMap);
				System.out.println(compare);
				
				/* 바꿀 날짜가 2일 이후여야 수행 가능하다 */
				if(compare > 2) {
					
					/* 현재 이용중인 서비스의 카테고리 코드를 조회해온다. */
					SelectSubscriptionService selectSubsService = new SelectSubscriptionService();
					String mealCategoryCode = selectSubsService.selectCurrentMealBySubsCode(subsCode);
					SubscriptionDTO subs = selectSubsService.selectSubscriptionBySubsCode(subsCode);
					
					/* Service계층에 넘겨줄 회원코드와 날짜차이 정보를 DTO에 담는다. */
					PayHistoryAndDateGapDTO payHistoryAndDateGap = new PayHistoryAndDateGapDTO();
					payHistoryAndDateGap.setPayCode(payHistory.getPayCode());
					payHistoryAndDateGap.setPayDate(payHistory.getPayDate());
					payHistoryAndDateGap.setStartDate(payHistory.getStartDate());
					payHistoryAndDateGap.setExprieDate(payHistory.getExprieDate());
					payHistoryAndDateGap.setIsRefund(payHistory.getIsRefund());
					payHistoryAndDateGap.setIsExpire(payHistory.getIsExpire());
					payHistoryAndDateGap.setDiscountPrice(payHistory.getDiscountPrice());
					payHistoryAndDateGap.setFinalPrice(payHistory.getFinalPrice());
					payHistoryAndDateGap.setCpnCode(payHistory.getCpnCode());
					payHistoryAndDateGap.setSubsCode(payHistory.getSubsCode());
					payHistoryAndDateGap.setDateGap(compare);
					payHistoryAndDateGap.setDeliveryZipcode(subs.getDeliveryZipcode());
					payHistoryAndDateGap.setDeliveryAddr(subs.getDeliveryAddr());
					payHistoryAndDateGap.setDeliveryAddrDetail(subs.getDeliveryAddrDetail());
					payHistoryAndDateGap.setMemberCode(memberCode);
					payHistoryAndDateGap.setMealCategoryCode(mealCategoryCode);
					
					/* 현재 구독중인 서비스의 상태 변경 후 변경한 날짜에 서비스를 생성한다. */
					int result = changeService.changeSubscriptionScheduleTransaction(payHistoryAndDateGap);
					
					if(result > 0) {
						/* 조회 페이지로 이동해 변경 내역 확인할 수 있게 한다 */
						response.sendRedirect(request.getContextPath()+"/subs/GoToSelectSubscriptionServlet");
					} else {
						/* 실패 페이지로 이동 */
						request.getRequestDispatcher("/WEB-INF/views/subs/memeberChangeScheduleFail urePage.jsp").forward(request, response);
					}
					
				} else {
					String alertMessage = "변경 날짜는 3일 이후부터 설정 가능합니다.";
					System.out.println(alertMessage);
					request.getRequestDispatcher("/WEB-INF/views/subs/memberErrorPageAfterThreeDaysPage.jsp").forward(request, response);
				}
				
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
