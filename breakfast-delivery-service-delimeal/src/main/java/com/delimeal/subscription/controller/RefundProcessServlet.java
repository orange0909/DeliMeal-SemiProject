package com.delimeal.subscription.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.subscription.model.dto.PayAndSubscriptionDTO;
import com.delimeal.subscription.model.service.ChangeMealService;

@WebServlet("/subs/RefundProcessServlet")
public class RefundProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String subsCode = request.getParameter("currnetsubsCode");
		System.out.println(subsCode);
		
		/* 구독번호로 구독 정보들을 조회해 온다 */
		PayAndSubscriptionDTO refundDTO = new PayAndSubscriptionDTO();
		ChangeMealService changeService = new ChangeMealService();
		refundDTO = changeService.SelectSubsDetailByCode(subsCode);
		System.out.println(refundDTO);
		
		int startDateGap = changeService.selectStartDateGap(refundDTO);
		System.out.println(startDateGap);
		
		/* 구독 시작 날짜보다 3일 이상 전이면 전액 환불 처리해준다 */
		if(startDateGap > 2) {
			/* DML로 만료 처리 */
			int result = changeService.refundTransactionOverTwoDays(refundDTO);
			request.setAttribute("currentServiceName", refundDTO.getMealCategoryName());
			request.setAttribute("refundPrice", refundDTO.getFinalPrice());
			
			request.getRequestDispatcher("/WEB-INF/views/subs/memberRefundPage.jsp").forward(request, response);
		
		/* 구독 시작 날짜보다 2일 이하로 남은 경우 */
		} else {
			double EndDateGap = (double)changeService.selectDateGap(subsCode);
			System.out.println("dateGap: " + EndDateGap);
			/* 환불 처리를 하려면 최소 만료일 3일 전이어야 한다 */
			if(EndDateGap > 0) {
				/* DML로 만료 처리 */
				int result = changeService.refundTransaction(refundDTO);
				double finalPrice = (double)refundDTO.getFinalPrice();
				/* 환불 금액 계산 */
				finalPrice *= EndDateGap / 5;
				int refundPrice = (int)finalPrice;
				System.out.println(finalPrice);
				request.setAttribute("subsCode", subsCode);
				request.setAttribute("currentServiceName", refundDTO.getMealCategoryName());
				request.setAttribute("refundPrice", refundPrice);
				request.getRequestDispatcher("/WEB-INF/views/subs/memberRefundPage.jsp").forward(request, response);
			} else {
				/* 환불 불가, 예외처리 페이지로 이동 */
				request.getRequestDispatcher("/WEB-INF/views/subs/memberErrorPageAfterThreeDaysPage.jsp").forward(request, response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
