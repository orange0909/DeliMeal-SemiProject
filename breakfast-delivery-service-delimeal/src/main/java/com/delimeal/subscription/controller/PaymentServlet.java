package com.delimeal.subscription.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.member.model.dto.IssuedCouponDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.DeliveryAddrDTO;
import com.delimeal.subscription.model.dto.PayInfoDTO;
import com.delimeal.subscription.model.service.SubscriptionService;

@WebServlet("/subs/payment")
public class PaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SubscriptionService subsService = new SubscriptionService();

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 
		 * 페이지 출력에 필요한 내용
		 * 
		 * 1. 회원 정보 - DM_MEMBER
		 * 2. 결제 정보 - DM_PAY_INFO
		 * 3. 배송지 정보 - DM_DELIVERY_ADDR
		 * 
		 * 4. 발급된 쿠폰 정보 - DM_ISSUED_COUPON
		 * 
		 * 5. 식단 카테고리 정보 - DM_MEAL_CATEGORY
		 */
		
		/* 0. 세션에 저장되어 있는 로그인 정보와 자주 사용하는 값들 초기화 */
		String mealCategoryCode = request.getParameter("pid"); 
		Date deliveryDate = Date.valueOf(request.getParameter("deliveryDate"));
		request.setAttribute("deliveryDate", deliveryDate);
		MemberDTO loginMember = (MemberDTO) request.getSession().getAttribute("loginMember");
		String id = loginMember.getId();
		String code = loginMember.getCode();
		
		System.out.println(":: PaymentServlet ::");
		System.out.println("logged in ID : " + id + ", MemberCode : " + code);
		System.out.println("request Meal : " + mealCategoryCode);
		System.out.println("request DeliveryDate : " + deliveryDate);
		
		
		/* 1. ID를 사용해 회원정보 가져오기 */
		MemberDTO member = subsService.selectMemberByIdForPayment(id);
		System.out.println(":: PaymentServlet ::");
		System.out.println("selectMemberByIdForPayment : " + member);
		request.setAttribute("member", member);
		
		/* 2. MEMBER_CODE를 사용해 결제정보 가져오기 */
		PayInfoDTO payInfo = subsService.selectPayInfoByMemberCodeForPayment(code);
		System.out.println(":: PaymentServlet :: ");
		System.out.println("selectPayInfoByMemberCodeForPayment : " + payInfo);
		request.setAttribute("payInfo", payInfo);
		
		/* 3. MEMBER_CODE를 사용해 배송지 정보 (기본 배송지) 가져오기 */
		DeliveryAddrDTO deliveryAddr = subsService.selectDefaultAddrByMemberCodeForPayment(code);
		System.out.println(":: PaymentServlet :: ");
		System.out.println("selectDefaultAddrByMemberCodeForPayment : " + deliveryAddr);
		request.setAttribute("deliveryAddr", deliveryAddr);
		
		/* 4. MEMBER_CODE를 사용해 사용자의 쿠폰 가져오기 */
		List<IssuedCouponDTO> couponList = subsService.selectAllIssuedCouponForPayment(code);
		System.out.println(":: PaymentServlet :: ");
		System.out.println("selectAllIssuedCouponForPayment : " + couponList);
		request.setAttribute("couponList", couponList);
		
		/* 5. GET 메소드를 통해 파라메터로 던져진 MEAL_CATE_CODE를 사용해 카테고리 정보 가져오기 */
		CategoryDTO category = subsService.selectCategoryByCode(mealCategoryCode);
		System.out.println(":: PaymentServlet :: ");
		System.out.println("selectCategoryByCode : " + category);
		request.setAttribute("category", category);
		
		request.getRequestDispatcher("/WEB-INF/views/subs/memberPaymentPage.jsp").forward(request, response);
	}
}
