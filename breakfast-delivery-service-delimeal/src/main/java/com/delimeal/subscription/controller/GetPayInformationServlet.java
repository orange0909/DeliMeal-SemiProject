package com.delimeal.subscription.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.DeliveryAddrDTO;
import com.delimeal.subscription.model.dto.MealCategoryDTO;
import com.delimeal.subscription.model.dto.PayDTO;
import com.delimeal.subscription.model.service.SubscriptionService;
import com.google.gson.Gson;

@WebServlet("/subs/getPayInfo")
public class GetPayInformationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		SubscriptionService service = new SubscriptionService();
		MemberDTO loginMember = (MemberDTO) request.getSession().getAttribute("loginMember");

		String subsCode = service.getSubsCode();
		
		MemberDTO memberInfo = service.selectMemberInfoByMember(loginMember.getCode());
		
		MealCategoryDTO categoryInfo = service.selectCategoryInfoByCategoryCode(request.getParameter("categoryCode"));
		
		String couponCode = request.getParameter("couponCode");
		IssuedCouponInfoDTO couponInfo = service.selectCouponInfoByCouponCode(couponCode);
		
		DeliveryAddrDTO addrInfo = service.selectAddrInfoByAddrCode(request.getParameter("addrCode"));

		boolean result = true;

		if(subsCode == null || "".equals(subsCode)) {
			System.out.println("1");
			result = false;
		}
		
		if(memberInfo == null) {
			System.out.println("2");
			result = false;
		}
		
		if(categoryInfo == null) {
			System.out.println("3");
			result = false;
		}
		
		if("none".equals(couponCode)) {
			if(couponInfo != null) {
				System.out.println("4");
				result = false;
			}
		} else {
			if(couponInfo == null) {
				System.out.println("5");
				result = false;
			}
		}
		
		if(addrInfo == null) {
			System.out.println("6");
			result = false;
		}
		
		
		if(result) {
			System.out.println(":: GetPayInformationServlet ::");
			PayDTO payInfo = new PayDTO();
			payInfo.setSubsCode(subsCode);
			payInfo.setBuyer(memberInfo);
			payInfo.setMealCategoryCode(categoryInfo.getCode());
			payInfo.setMealCategoryName(categoryInfo.getName());
			payInfo.setMealCategoryPrice(categoryInfo.getPrice());
			if(couponInfo == null) {
				payInfo.setCpnDiscountRate(0);
			} else {
				payInfo.setCpnDiscountRate(couponInfo.getCpnDiscountRate());
			}
			payInfo.setAddr(addrInfo);
			
			System.out.println("payInfo: " + payInfo);

			response.setContentType("application/json; charset=UTF-8");
			
			PrintWriter out = response.getWriter();
			
			out.print(new Gson().toJson(payInfo));
			out.flush();
			out.close();
		} else {
			System.out.println("실패 !!");
		}
		
	}
}
