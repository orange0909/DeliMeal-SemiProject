package com.delimeal.common.controller;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import com.delimeal.common.config.ConfigLocation;

// WAS 실행시 로드됨
@WebServlet(urlPatterns = {}, loadOnStartup = 1)
public class InitialLoadingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public void init(ServletConfig config) throws ServletException {
		
		// web.xml에 등록한 mybatis 설정 정보 가져와서 초기화
		ConfigLocation.mybatisConfigLocation = config.getServletContext().getInitParameter("mybatis-config-location");
		
		System.out.println("InitialLoadingServlet > init(): ");
		System.out.println("MyBatis 설정 정보 로드됨: " + ConfigLocation.mybatisConfigLocation);
	}

}
