package com.delimeal.common.paging;

public class Pagingnation {

	public static SelectChriteria getSelectChriteria(int pageNo, int totalCount, int limit, int buttonAmount) {
		return getSelectChriteria(pageNo, totalCount, limit, buttonAmount, null, null);
	}

	public static SelectChriteria getSelectChriteria(int pageNo, int totalCount, int limit, int buttonAmount,
			String searchCondition, String searchValue) {

		/*페이지 버튼과 연관 됨
		 * 가장 마지막 페이지 보여줘!
		 * 이걸 눌렀을때 표시 될 페이지 버튼은 몇번?
		 * 시작이 그렇다면 그럼 끝나는 숫자는??*/
		int maxPage;
		int startPage;
		int endPage;
		/* 게시글과 연관됨.
		 * 다음 페이지로 넘겼을때 보여줄번호!
		 * 끝나는 번호!*/
		int startRow;
		int endRow;
		
		/*총 페이지 수 계산*/
		maxPage = (int)Math.ceil((double) totalCount/ limit);
		
		/* 현재 페이지에서 보여줄 페이징 첫번째 버튼 숫자*/
		startPage = (int)(Math.ceil((double) pageNo / buttonAmount) -1) * buttonAmount + 1;
		
		/* 현재 페이지에서 보여줄 페이징 마지막 버튼 숫자*/
		endPage = (startPage + buttonAmount) -1;
		
		/*현재 보여줄 페이지 보다 총 페이지가 더 적을 경우는 마지막 페이지가 max페이지*/
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		/* 보여줄 게시글이 없는 상태 == 총 페이지도, 끝나는 페이지도 없는 상태.*/
		if(maxPage == 0 && endPage == 0) {
			maxPage = startPage;
			endPage = startPage;
		}
		
		/* 화면에 나타날 게시글 번호 계산 */
		
		startRow = (pageNo - 1) * limit + 1;
		endRow = startRow + limit - 1;
		
		SelectChriteria selectChriteria = new SelectChriteria(pageNo, totalCount, limit, buttonAmount, maxPage, startPage, endPage, startRow, endRow, searchCondition, searchValue);
		return selectChriteria;
	}

}












