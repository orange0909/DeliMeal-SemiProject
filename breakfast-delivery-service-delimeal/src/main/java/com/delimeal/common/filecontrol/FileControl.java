package com.delimeal.common.filecontrol;

import java.io.File;

public class FileControl {
	
	public static void deleteFile(String fileUploadDirectory, String oldFileName) {
		File deleteFile = new File(fileUploadDirectory + "/" + oldFileName);
		if(deleteFile.exists()) {
			if(deleteFile.delete()) {
				System.out.println("파일 삭제 성공");
			}else {
				System.out.println("파일 삭제 실패");
			}
				
		}
	}
}
