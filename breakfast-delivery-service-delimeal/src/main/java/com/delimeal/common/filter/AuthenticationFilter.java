package com.delimeal.common.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delimeal.member.model.dto.MemberDTO;

@WebFilter(urlPatterns = {"/member/*", "/admin/*", "/subs/*", "/qna/*", "/mypage/*", "/feedback/*"})
public class AuthenticationFilter implements Filter {
	Map<String, List<String>> permitURIList;

	@Override
	public void destroy() { }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest hrequest = (HttpServletRequest) request;
		String uri = hrequest.getRequestURI();
		String intent = uri.replace(hrequest.getContextPath(), "");
		
		/* 세션에 권한이 있는지 확인하여 허용된 url에만 접근 가능하도록 설정한다. */
		HttpSession requestSession = hrequest.getSession();
		MemberDTO loginMember = (MemberDTO) requestSession.getAttribute("loginMember");
		
		boolean isAuthorized = false;
		
		/* 로그인 함 */
		if(loginMember != null) {
			
			boolean isPermitAdmin = permitURIList.get("adminPermitList").contains(intent);
			boolean isPermitMember = permitURIList.get("memberPermitList").contains(intent);
			boolean isPermitAll = permitURIList.get("allPermitList").contains(intent);
			
			/* 관리자 */
			if("M".equals(loginMember.getType())) {
				
				if(isPermitAdmin || isPermitMember || isPermitAll) {
					isAuthorized = true;
				}
			
			/* 회원 */
			} else if("U".equals(loginMember.getType())) {
				
				if((isPermitMember || isPermitAll) && !isPermitAdmin) {
					isAuthorized = true;
				}
				
			}
			
			if(isAuthorized) {
				chain.doFilter(request, response);
			} else {
				((HttpServletResponse) response).sendError(403);
			}
		
		/* 로그인 안함 */
		} else {
			
			if(permitURIList.get("allPermitList").contains(intent)) {
				chain.doFilter(request, response);
			} else {
				String referer = hrequest.getHeader("Referer");
				if(referer != null) {
					String url = hrequest.getContextPath() + "/login?returnUrl=" + referer;
					((HttpServletResponse) response).sendRedirect(url);
				} else {
					String url = hrequest.getContextPath() + "/login";
					((HttpServletResponse) response).sendRedirect(url);
				}
			}
		}
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		permitURIList = new HashMap<>();
		List<String> adminPermitList = new ArrayList<>();
		List<String> memberPermitList = new ArrayList<>();
		List<String> allPermitList = new ArrayList<>();
		
		/* 관리자만 접근 가능한 주소 (관리자로 로그인) */
		adminPermitList.add("/admin/menuUpdate");
		adminPermitList.add("/admin/updateDeliveryFinish");
		adminPermitList.add("/notice/insert");
		adminPermitList.add("/qna/insertAnswer");
		adminPermitList.add("/qna/waitingList");
		adminPermitList.add("/qna/UpdateAnswer");
		adminPermitList.add("/qna/deleteAnswer");
		adminPermitList.add("/feedback/adminSelectList");
		adminPermitList.add("/feedback/updateAnswer");
		adminPermitList.add("/admin/subscriberList");
		adminPermitList.add("/admin/notSubscriberList");
		adminPermitList.add("/admin/deliverySearch");
		adminPermitList.add("/admin/memberInfo");
		adminPermitList.add("/admin/menuInsert");
		adminPermitList.add("/admin/menuDelete");
		adminPermitList.add("/admin/menuSelectDetail");
		adminPermitList.add("/admin/menu");
		adminPermitList.add("/admin/mealUpdate");
		adminPermitList.add("/admin/mealInsert");
		adminPermitList.add("/admin/mealDelete");
		adminPermitList.add("/admin/mealSelectDetail");
		adminPermitList.add("/admin/meal");
		adminPermitList.add("/admin/categoryUpdate");
		adminPermitList.add("/admin/categoryInsert");
		adminPermitList.add("/admin/categoryDelete");
		adminPermitList.add("/admin/categorySelectDetail");
		adminPermitList.add("/admin/category");
		adminPermitList.add("/pa/CouponRegisterProcess");
		adminPermitList.add("/pa/CouponRegister");
		adminPermitList.add("/pa/IssueCouponProcess");
		adminPermitList.add("/pa/IssueCoupounPage");
		adminPermitList.add("/pa/IssueNewCouponSuccess");
		adminPermitList.add("/pa/ManageBirthdayMenu");
		adminPermitList.add("/pa/PromotionMangeRedirect");
		adminPermitList.add("/pa/PromotionMangeRedirect");
		adminPermitList.add("/pa/PromotionMange");
		adminPermitList.add("/pa/PromotionView");
		adminPermitList.add("/pa/RegisterBirthdayMenu");
		adminPermitList.add("/RegisterNewCouponFailureServlet");
		adminPermitList.add("/pa/RegisterNewCouponSuccess");
		adminPermitList.add("/pa/RegisterProcessServlet");
		adminPermitList.add("/pa/RegisterSuccessPage");
		adminPermitList.add("/pa/UpdateBirthdayMenuAjax");
		adminPermitList.add("/pa/UpdateBirthdayMenu");
		adminPermitList.add("/pa/UpdateProcessServlet");
		adminPermitList.add("/pa/UpdatePromotionProcess");
		adminPermitList.add("/pa/UpdateSuccessPage");
		adminPermitList.add("/feedback/deleteAnswer");
		adminPermitList.add("/feedback/adminWaithingList");
		adminPermitList.add("/feedback/waitingList");
		
		
		/* 회원만 접근 가능한 주소 (회원으로 로그인) */
		memberPermitList.add("/subs/payment");
		memberPermitList.add("/mypage");
		memberPermitList.add("/member/getCouponInfo");
		memberPermitList.add("/mypage/coupons");
		memberPermitList.add("/mypage/updateMember");
		memberPermitList.add("/mypage/deleteAccount");
		memberPermitList.add("/qna/delete");
		memberPermitList.add("/qna/insert");
		memberPermitList.add("/qna/selectMyQnA");
		memberPermitList.add("/qna/update");
		memberPermitList.add("/mypage/delete");
		memberPermitList.add("/feedback/insert");
		memberPermitList.add("/mypage/feedbackList");
		memberPermitList.add("/feedback/detail");
		memberPermitList.add("/feedback/selectMeal");
		memberPermitList.add("/mypage/updateFeedback");
		memberPermitList.add("/member/manageAddress");
		memberPermitList.add("/subs/payProcess");
		memberPermitList.add("/subs/getPayInfo");
		memberPermitList.add("/pc/ChangeBirthdayMenu");
		memberPermitList.add("/pc/ChangeBirthdayMenu");
		memberPermitList.add("/pc/GotoChangeSuccessPage");
		memberPermitList.add("/register/BirthdayMenuMain");
		memberPermitList.add("/register/registBirthdayMenu");
		memberPermitList.add("/pc/selectBirthdayMenu");
		memberPermitList.add("/pc/toselectBirthdayMenu");
		memberPermitList.add("/subs/BeforeChangePayment");
		memberPermitList.add("/subs/ChangeSubscriptionSchedule");
		memberPermitList.add("/subs/checkIsDelivered");
		memberPermitList.add("/subs/GoToChangeSchedule");
		memberPermitList.add("/subs/GoToChangeService");
		memberPermitList.add("/subs/goToCheckIsDelivered");
		memberPermitList.add("/subs/GoToSelectSubscriptionServlet");
		memberPermitList.add("/subs/Refund");
		memberPermitList.add("/subs/GoToRefund");
		memberPermitList.add("/subs/RefundProcessServlet");
		memberPermitList.add("/mypage/favMenu");
		memberPermitList.add("/mypage/feedbackList");
		

		
		

		/* 아무나 가능 (로그인 안한 상태) */
		allPermitList.add("/subs/meal/info");
		allPermitList.add("/subs/meal");
		allPermitList.add("/subs/menu/info");
		allPermitList.add("/qna/list");
		allPermitList.add("/qna/detail");
		allPermitList.add("/member/findId");
		allPermitList.add("/member/findPwd");
		allPermitList.add("/member/regist");
		allPermitList.add("/member/login");
		allPermitList.add("/pc/GotoInfluencer");
		allPermitList.add("/subs/goToHubView");
		allPermitList.add("/member/modifyPwd");

		
		
		
		permitURIList.put("adminPermitList", adminPermitList);
		permitURIList.put("memberPermitList", memberPermitList);
		permitURIList.put("allPermitList", allPermitList);
	}
}
