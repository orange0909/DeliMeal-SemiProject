package com.delimeal.feedback.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.paging.Pagingnation;
import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.feedback.model.dto.ChoiceMealWhenInsertFeedbackDTO;
import com.delimeal.feedback.model.service.FeedbackService;
import com.delimeal.member.model.dto.MemberDTO;

@WebServlet("/feedback/selectMeal")
public class SelectMealServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FeedbackService feedbackService = new FeedbackService();
		
		String loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember")).getCode();
		
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		searchMap.put("loginMember", loginMember);
		
		int totalCount = feedbackService.countAteMeal(searchMap);
		int limit = 5;
		int buttonAmount = 5;
		
		SelectChriteria selectChriteria = null;
		
		if(searchCondition != null && !"".equals(searchValue)) {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		}else {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		Map<String, Object> insertMap = new HashMap<>();
		insertMap.put("selectChriteria", selectChriteria);
		insertMap.put("loginMember", loginMember);
		
		List<ChoiceMealWhenInsertFeedbackDTO> ateMealList = feedbackService.ateMealList(insertMap);
		
		//데이터 확인
		for(ChoiceMealWhenInsertFeedbackDTO dto : ateMealList) {
			System.out.println(dto);
		}
		
		String path="";
		if(ateMealList != null) {
			/* 배송 받아 보았을때*/
			path = "/WEB-INF/views/feedback/memberSelectMealFeedback.jsp";
			request.setAttribute("meal",  ateMealList);
			request.setAttribute("selectCriteria", selectChriteria);

		}else {
			/* 배송 받은 것이 없을 때 -> alert 진행 후 리스트로 갈 것임. */
			path ="/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "noDeliHistory");
		}
		request.getRequestDispatcher(path).forward(request, response);
	
	}

}
