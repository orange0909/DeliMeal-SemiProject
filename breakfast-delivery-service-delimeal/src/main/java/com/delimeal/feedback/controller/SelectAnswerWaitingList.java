package com.delimeal.feedback.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.paging.Pagingnation;
import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.service.FeedbackService;

@WebServlet("/feedback/adminWaithingList")
public class SelectAnswerWaitingList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		FeedbackService feedbackService = new FeedbackService();

		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		String searchCondition = request.getParameter("searchCondition");
		String searchValue = request.getParameter("searchValue");
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		int totalCount = feedbackService.selectWaitingCount(searchMap);
		int limit = 5;
		int buttonAmount = 5;

		SelectChriteria selectChriteria = null;
		
		if(searchCondition != null && !"".equals(searchValue)) {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		}else {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		Map<String, Object> feedbackMap = new HashMap<>();
		feedbackMap.put("selectChriteria", selectChriteria);
		
		List<DetailDTO> detailDTO = feedbackService.selectWaitingList(feedbackMap);
		
		for(DetailDTO dto : detailDTO) {
			System.out.println(dto);
		}
		
		String path = "";
		if(detailDTO != null) {
			path = "/WEB-INF/views/feedback/adminSelectWaitingList.jsp";

			for(DetailDTO dto : detailDTO) {
				/*글번호 원하는 모양으로 나타내기*/
				String feedbackNum = dto.getFeedbackNum();
				dto.setFeedbackNum(Integer.valueOf(feedbackNum.substring(feedbackNum.lastIndexOf("_")+1))+"");
				
				System.out.println(dto);
				/*답변 여부 나타내기*/
				if("Y".equals(dto.getAnswerYN())) {
					dto.setAnswerYN("답변 완료");
				}else {
					dto.setAnswerYN("답변 대기중");
				}
			}
			request.setAttribute("feedback",  detailDTO);
			/* 페이징 처리에 사용할 정보들 */
			request.setAttribute("selectCriteria", selectChriteria);
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		request.getSession().setAttribute("listType", "waiting");
	}

}
