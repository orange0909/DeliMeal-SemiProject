package com.delimeal.feedback.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.delimeal.common.filecontrol.FileControl;
import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.service.FeedbackService;

@WebServlet("/mypage/updateFeedback")
public class UpdateFeedbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		FeedbackService service = new FeedbackService();
		/* 피드백 번호 */
		String feedNum = request.getParameter("feedNum");
		/*디테일 들고오기*/
		DetailDTO feedbackDetail = service.selectFeedbackDetail(feedNum);

		/* 값 넘기기 */
		request.setAttribute("feedback", feedbackDetail);
		request.setAttribute("feedNum", feedNum);
		
		String path = "/WEB-INF/views/feedback/memberUpdateFeedback.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(ServletFileUpload.isMultipartContent(request)) {
			String rootLocation = getServletContext().getRealPath("/") ;
			int maxFileSize = 1024 * 1024 * 10;
			String encodingType = "UTF-8";
			
			System.out.println("rootLocation" + rootLocation);
			System.out.println("maxFileSize" + maxFileSize);
			System.out.println("encodingType" + maxFileSize);
			
			String fileUploadDirectory = rootLocation + "resources/cs/";
			
			File directory = new File(fileUploadDirectory);
			
			/* 만일 저장경로가 존재하지 않는다면 만들거다. */
			if(!directory.exists()) {
				System.out.println("폴더 생성 : " + directory.mkdirs());
			}
			
			Map<String, String> parameter = new HashMap<>();
			List<Map<String, String>> fileList = new ArrayList<>();
			
			/*파일 업로드 할때 최대 크기나 임시 저장할 폴더의 경로등을 포함하기 위한 인스턴스*/
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			/* 임시 경로 */
	        fileItemFactory.setRepository(new File(fileUploadDirectory));
	        /* 최대 크기 */
	        fileItemFactory.setSizeThreshold(maxFileSize);
	        
	        /* 데이터 하나하나를 fileItem 타입의 인스턴스들로 변환 할 것이다. (List로 묶어 반환)*/
	        ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			
	        try {
				List<FileItem> fileItems = fileUpload.parseRequest(request);
				
				for(int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);
					
					if(!item.isFormField()) {
						if(item.getSize() > 0) {
							String filedName = item.getFieldName();
							String originFileName = item.getName();
							
							System.out.println("인풋 태그 이름: " + filedName);
							System.out.println("올린 파일 이름(원래의 이름)   " + originFileName );
							
							/*저장 이름 바꾸는 중*/
							int dot = originFileName.lastIndexOf(".");
							String ext = originFileName.substring(dot);
							
							String randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;
							
							/*업로드시 올리는 경로/파일*/
							File storeFile = new File(fileUploadDirectory + "/" + randomFileName);
							
							
							/*바뀐 이름으로 저장*/
							item.write(storeFile);
							
							/*컬렉션으로 묶어서 file정보만을 담는다.*/
							Map<String, String> fileMap = new HashMap<>();
							fileMap.put("filedName", filedName);
							fileMap.put("originFileName", originFileName);
							fileMap.put("savedFileName", randomFileName);
							fileMap.put("savePath", fileUploadDirectory);
							
							
							fileMap.put("thumbnailPath", "/resources/upload/thumbnail/thumbnail_" + randomFileName);
							
							fileList.add(fileMap);
						}
					} else {
						parameter.put(item.getFieldName(), new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
					}
				}
				
				DetailDTO detailDTO = new DetailDTO();
				detailDTO.setFeedbackTitle(parameter.get("title"));
				detailDTO.setFeedbackContent(parameter.get("body"));
				detailDTO.setFeedbackNum(parameter.get("feedbackNum"));
				
				for(int i = 0; i < fileList.size(); i++) {
					Map<String, String> file = fileList.get(i);
					detailDTO.setFileName(file.get("savedFileName"));
				}
				
				String oldFileName = parameter.get("oldFileName");
				String deleteYn = parameter.get("deleteYN");
				
				int result = 0;
				FeedbackService service = new FeedbackService();
				
				if(fileList.size() != 0) {
					/* 사진 업로드 요청이 들어옴*/
					if(oldFileName == "" || oldFileName == null) {
						/* 기존 사진이 없다. */
						result = service.updateFBandFile(detailDTO);
					} else {
						/* 기존 사진이 있다. */
						/*기존 사진 삭제*/
						FileControl.deleteFile(fileUploadDirectory, oldFileName);
						result = service.updateFileAndFB(detailDTO);
					}
				}else {
					/*사진 업로드 요청이 안들어옴*/
					if("true".equals(deleteYn)) {
						/* 이전 사진을 지우겠다고 요청 들어왔을때 */
						FileControl.deleteFile(fileUploadDirectory, oldFileName);
						/* 파일 데이터 삭제 */
						result = service.deleteFeedbackFile(detailDTO.getFeedbackNum());
						System.out.println("파일 삭제 잘 됐누?"+result);
						System.out.println(result);
						System.out.println(result);
						System.out.println(result);
						System.out.println(result);
					}
					
					result = service.updateFeedback(detailDTO);
				}
				
				System.out.println(detailDTO);
				System.out.println(detailDTO);
				System.out.println(detailDTO);
				System.out.println(detailDTO);
					
	        
				String path ="";
				if(result > 0) {
					path ="/WEB-INF/views/common/success.jsp";
					request.setAttribute("successCode", "updateFeedbackmember");
					request.setAttribute("feedNum", parameter.get("feedNum"));
					System.out.println(parameter.get("feedNum"));
					System.out.println(parameter.get("feedNum"));
					System.out.println(parameter.get("feedNum"));
					System.out.println(parameter.get("feedNum"));
					System.out.println(parameter.get("feedNum"));
					System.out.println(parameter.get("feedNum"));
				} else {
					System.out.println("실패");
				}
				
				request.getRequestDispatcher(path).forward(request, response);
				
	        } catch (Exception e) {
				e.printStackTrace();
			}// catch end
			
		}//
		
	}

}
