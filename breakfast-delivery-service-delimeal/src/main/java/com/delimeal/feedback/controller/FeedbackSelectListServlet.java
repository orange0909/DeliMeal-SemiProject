package com.delimeal.feedback.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.paging.Pagingnation;
import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.feedback.model.dto.FeedbackAndMealDTO;
import com.delimeal.feedback.model.service.FeedbackService;
import com.delimeal.member.model.dto.MemberDTO;

@WebServlet("/mypage/feedbackList")
public class FeedbackSelectListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String path ="";

			FeedbackService feedbackService = new FeedbackService();
			String loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember")).getCode();
			
			String currentPage = request.getParameter("currentPage");
			int pageNo = 1;
			
			if(currentPage != null && !"".equals(currentPage)) {
				pageNo = Integer.parseInt(currentPage);
			}
			
			String searchCondition = request.getParameter("searchCondition");
			String searchValue = request.getParameter("searchValue");
			
			Map<String, String> searchMap = new HashMap<>();
			searchMap.put("searchCondition", searchCondition);
			searchMap.put("searchValue", searchValue);
			searchMap.put("loginMember", loginMember);
			
			int totalCount = feedbackService.selectTotalCount(searchMap);
			int limit = 5;
			int buttonAmount = 5;
			
			SelectChriteria selectChriteria = null;
			
			if(searchCondition != null && !"".equals(searchValue)) {
				selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
			}else {
				selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount);
			}
			
			Map<String, Object> feedbackMap = new HashMap<>();
			feedbackMap.put("selectChriteria", selectChriteria);
			feedbackMap.put("loginMember", loginMember);
			List<FeedbackAndMealDTO> feedbackList = feedbackService.selectFeedbackList(feedbackMap);
			
			/*jsp로 이동*/
			if(feedbackList != null) {
				path = "/WEB-INF/views/feedback/memberSelectFeedbackList.jsp";
				
				for(FeedbackAndMealDTO feedback : feedbackList) {
					/*글번호 원하는 모양으로 나타내기*/
					String feedbackNum = feedback.getNum();
					feedback.setNum(Integer.valueOf(feedbackNum.substring(feedbackNum.lastIndexOf("_")+1))+"");
					
					System.out.println(feedback);
					/*답변 여부 나타내기*/
					if("Y".equals(feedback.getAnswerYN())) {
						feedback.setAnswerYN("답변 완료");
					}else {
						feedback.setAnswerYN("답변 대기중");
					}
				}
				
				/*조회된 문의페이지에 전달할 내용*/
				request.setAttribute("feedback",  feedbackList);
				/* 페이징 처리에 사용할 정보들 */
				request.setAttribute("selectCriteria", selectChriteria);
			}
		
		request.getRequestDispatcher(path).forward(request, response);
	
	}// doget end

}// class end
