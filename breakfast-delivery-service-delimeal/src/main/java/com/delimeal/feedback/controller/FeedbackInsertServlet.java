package com.delimeal.feedback.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.delimeal.feedback.model.dto.ChoiceMealWhenInsertFeedbackDTO;
import com.delimeal.feedback.model.dto.FeedBackDTO;
import com.delimeal.feedback.model.dto.FeedBackFileDTO;
import com.delimeal.feedback.model.service.FeedbackService;
import com.delimeal.member.model.dto.MemberDTO;

@WebServlet("/feedback/insert")
public class FeedbackInsertServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mealCode = request.getParameter("mealCode");
		String loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember")).getCode();
		FeedbackService service = new FeedbackService();
		
		String path ="";
		if(mealCode != null && mealCode != "") {
			
			Map<String, String> selectMeal = new HashMap<>();
			selectMeal.put("mealCode", mealCode);
			selectMeal.put("loginMember", loginMember);
			
			ChoiceMealWhenInsertFeedbackDTO selectmeal = service.selectMeal(selectMeal);
			String mealName = selectmeal.getMealName();
			String mealCate = selectmeal.getMealCate();
			
			request.setAttribute("mealName", mealName);
			request.setAttribute("mealCate", mealCate);
			request.setAttribute("mealCode", mealCode);
			path ="/WEB-INF/views/feedback/memberInsertFeedback.jsp";
		}else {
			path ="/WEB-INF/views/common/failed.jsp";
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
		
		
		
	}//dpGet end

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		System.out.println("포스트 도착!");
		
		if(ServletFileUpload.isMultipartContent(request)) {
			String rootLocation = getServletContext().getRealPath("/") ;
			int maxFileSize = 1024 * 1024 * 10;
			String encodingType = "UTF-8";
			
			String fileUploadDirectory = rootLocation + "resources/cs/";
			
			File directory = new File(fileUploadDirectory);
			
			if(!directory.exists()) {
				System.out.println("폴더 생성 : " + directory.mkdirs());
			}
			
			Map<String, String> parameter = new HashMap<>();
			List<Map<String, String>> fileList = new ArrayList<>();
			
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
	        fileItemFactory.setRepository(new File(fileUploadDirectory));
	        fileItemFactory.setSizeThreshold(maxFileSize);
	        
	        ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			
	        try {
				List<FileItem> fileItems = fileUpload.parseRequest(request);
				for(FileItem item : fileItems) {
					System.out.println(item);
				}
				
				for(int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);
					
					if(!item.isFormField()) {
						
						if(item.getSize() > 0) {
							
							String filedName = item.getFieldName();
							String originFileName = item.getName();
							
							int dot = originFileName.lastIndexOf(".");
							String ext = originFileName.substring(dot);
							
							String randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;
							
							File storeFile = new File(fileUploadDirectory + "/" + randomFileName);
							
							item.write(storeFile);
							
							Map<String, String> fileMap = new HashMap<>();
							fileMap.put("filedName", filedName);
							fileMap.put("originFileName", originFileName);
							fileMap.put("savedFileName", randomFileName);
							fileMap.put("savePath", fileUploadDirectory);
							
							
							fileMap.put("thumbnailPath", "/resources/upload/thumbnail/thumbnail_" + randomFileName);
							
							fileList.add(fileMap);
							
						}
						
					} else {
						parameter.put(item.getFieldName(), new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
						
					}
				}
				
				System.out.println("parameter : " + parameter);
				System.out.println("fileList : " + fileList);
				
				FeedBackDTO feedbackDTO = new FeedBackDTO();
				feedbackDTO.setTitle(parameter.get("title"));
				feedbackDTO.setContent(parameter.get("body"));
				feedbackDTO.setMemberCode(((MemberDTO)request.getSession().getAttribute("loginMember")).getCode());
				feedbackDTO.setMealCode(parameter.get("mealCode"));
				feedbackDTO.setFeedbackFileList(new ArrayList<FeedBackFileDTO>());
				
				List<FeedBackFileDTO> list = feedbackDTO.getFeedbackFileList();
				
				for(int i =0; i < fileList.size(); i++) {
					Map<String, String> file = fileList.get(i);
					
					FeedBackFileDTO feedbackFile = new FeedBackFileDTO();
					feedbackFile.setFileName(file.get("savedFileName"));
					
					list.add(feedbackFile);
				}
				
				/* 인서트 */
				int result = new FeedbackService().insertFeedback(feedbackDTO);

				/*화면이동*/
				String path ="";
				if(result > 0) {
					path ="/WEB-INF/views/common/success.jsp";
					request.setAttribute("successCode", "insertfeedbackmember");
				} else {
					System.out.println("실패");
				}
				
				request.getRequestDispatcher(path).forward(request, response);
				
			} catch (Exception e) {
				int cnt = 0;
				for(int i = 0; i < fileList.size(); i++) {
					Map<String, String> file = fileList.get(i);
					
					File deleteFile = new File(fileUploadDirectory + "/" + file.get("savedFileName"));
					boolean isDeleted = deleteFile.delete();
					
					if(isDeleted) {
						cnt++;
					}
				}
			}//catch end
	        
		}//if end
	}

}
