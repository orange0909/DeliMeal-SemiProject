package com.delimeal.feedback.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.service.FeedbackService;
import com.delimeal.member.model.dto.MemberDTO;

@WebServlet("/feedback/detail")
public class SelectFeedbackDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		/* 접근 확인 */
		String memberType = "";
		MemberDTO loginMember = (MemberDTO) request.getSession().getAttribute("loginMember");

		System.out.println(loginMember);
		System.out.println(loginMember);
		System.out.println(loginMember);

		if (loginMember != null) {
			memberType = loginMember.getType();
		} else {
			memberType = "NoMember";
		}

		FeedbackService service = new FeedbackService();
		String feedNum = request.getParameter("feedNum");

		DetailDTO feedbackDetail = service.selectFeedbackDetail(feedNum);

		String path = "";
		if ((memberType.equals("U"))) {
			/* 이용자 */
			if (feedbackDetail != null) {

				path = "/WEB-INF/views/feedback/selectFeedbackDetail.jsp";
				
				if ("Y".equals(feedbackDetail.getAnswerYN())) {
					feedbackDetail.setAnswerYN("답변 완료");
				} else {
					feedbackDetail.setAnswerYN("답변 대기중");
					feedbackDetail.setAnswerContent("아직 등록된 답변이 없습니다.");
				}
				request.setAttribute("feedback", feedbackDetail);
				request.setAttribute("feedNum", feedNum);
			}

		} else {
			/* 관리자 */
			if (feedbackDetail != null) {
				/* 조회 내용 있 */
				path = "/WEB-INF/views/feedback/adminFeedbackDetail.jsp";
				request.setAttribute("feedback", feedbackDetail);
				request.setAttribute("feedNum", feedNum);
			}
		}

		request.getRequestDispatcher(path).forward(request, response);
	}

}
