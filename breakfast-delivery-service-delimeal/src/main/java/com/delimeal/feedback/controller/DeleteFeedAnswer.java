package com.delimeal.feedback.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.service.FeedbackService;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.service.QnAService;


@WebServlet("/feedback/deleteAnswer")
public class DeleteFeedAnswer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		String feedbackNum = request.getParameter("feedbackNum");
		String feedNum = request.getParameter("feedNum");
		FeedbackService service = new FeedbackService();
		
		System.out.println("피드백 답변 삭제");

		String path = "";
	
		int deleteResult = service.deleteAnswer(feedbackNum);
		
		if(deleteResult > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteFeedbackAnswer");
			request.setAttribute("feedNum", feedNum);
		}
			
		request.getRequestDispatcher(path).forward(request, response);
		
		

		
	}

}
