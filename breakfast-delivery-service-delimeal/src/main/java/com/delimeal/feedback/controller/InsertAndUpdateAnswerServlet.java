package com.delimeal.feedback.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.service.FeedbackService;
import com.delimeal.member.model.dto.MemberDTO;

@WebServlet("/feedback/updateAnswer")
public class InsertAndUpdateAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FeedbackService service = new FeedbackService();
		/*답변이 있다면 Y-> 수정, 없다면 N-> 작성*/
		String answerYN = request.getParameter("answerYN");
		String feedNum = request.getParameter("feedNum");
		
		System.out.println(feedNum);
		System.out.println(answerYN);
		
		DetailDTO detail = service.selectFeedbackDetail(feedNum);

		
		String path = "";
		
		path = "/WEB-INF/views/feedback/adminInsertAndUpdateAnswer.jsp";
		
		request.setAttribute("feedNum", feedNum);
		request.setAttribute("feedback", detail);
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("피드백 답변 업데이트");
		
		FeedbackService service = new FeedbackService();
		
		request.setCharacterEncoding("UTF-8");
		
		String answerContent = request.getParameter("answer");
		String feedbackNumber = request.getParameter("feedbackNumber");
		String loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember")).getCode();
		String feedNum = request.getParameter("feedNum");
		
		System.out.println("answerContent" + answerContent);
		System.out.println("feedbackNumber" + feedbackNumber);
		System.out.println("loginMember" + loginMember);
		
		Map<String, String> answer = new HashMap<>();
		answer.put("answerContent", answerContent);
		answer.put("feedbackNumber", feedbackNumber);
		answer.put("loginMember", loginMember);
		
		int result = service.updateAnswer(answer); 

		System.out.println(" 피드백 업데이트 결과" + result);

		String path="";
		if(result > 0) {
			/* 인서트 되었을때 */
			System.out.println("답변 업데이트 가나요~? 뭘 넘길껀가요??" +feedNum);
	
			path ="/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateFeedbackmember");
			request.setAttribute("feedNum", feedNum);
		}else {
			/* 인서트 안되었을때 */
		}
		
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
