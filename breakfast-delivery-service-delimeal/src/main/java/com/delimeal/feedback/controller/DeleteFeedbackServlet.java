package com.delimeal.feedback.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.filecontrol.FileControl;
import com.delimeal.feedback.model.service.FeedbackService;

@WebServlet("/mypage/delete")
public class DeleteFeedbackServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		FeedbackService service = new FeedbackService();
		String feedbackNum = request.getParameter("feedbackNum");
		String file = request.getParameter("file");
		
		/* 최종 결과 */
		int result = 0;
		/* 사진 있는 게시글 삭제 결과*/
		int deleteWithFile = 0;
		/* 사진 없는 게시글 삭제 결과*/
		int deleteFeedback = 0;
		if(file != null && file != "") {
			/* 사진이 있다면 */
			String fileUploadDirectory = "C:\\projectSemiHimedia\\breakfast-delivery-service-delimeal\\web\\resources/cs/";
			
			/*경로서 해당 파일 삭제*/
			FileControl.deleteFile(fileUploadDirectory, file);
			/* 테이블도 삭제*/
			result = service.deleteFileAndFB(feedbackNum);
			
		}else {
			result = service.deleteFeedback(feedbackNum);
		}
		

		String path="";
		if(result > 0) {
				path ="/WEB-INF/views/common/success.jsp";
				request.setAttribute("successCode", "deleteFeedbackmember");
		} else {
			path ="/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "deleteFeedbackmember");
		}
	
		request.getRequestDispatcher(path).forward(request, response);
	
	
	}

}
