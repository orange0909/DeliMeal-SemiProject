package com.delimeal.feedback.model.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class FeedBackDTO implements Serializable{
	private static final long serialVersionUID = 1149049736223010095L;
	
	private String num;
	private String title;
	private String content;
	private java.sql.Date date;
	private String answerYN;
	private String answerContent;
	private java.sql.Date answerDate;
	private String memberCode;
	private String mealCode;
	private String managerCode;
	private List<FeedBackFileDTO> feedbackFileList;
	
	public FeedBackDTO() {
	}
	public FeedBackDTO(String num, String title, String content, Date date, String answerYN, String answerContent,
			Date answerDate, String memberCode, String mealCode, String managerCode,
			List<FeedBackFileDTO> feedbackFileList) {
		this.num = num;
		this.title = title;
		this.content = content;
		this.date = date;
		this.answerYN = answerYN;
		this.answerContent = answerContent;
		this.answerDate = answerDate;
		this.memberCode = memberCode;
		this.mealCode = mealCode;
		this.managerCode = managerCode;
		this.feedbackFileList = feedbackFileList;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public java.sql.Date getDate() {
		return date;
	}
	public void setDate(java.sql.Date date) {
		this.date = date;
	}
	public String getAnswerYN() {
		return answerYN;
	}
	public void setAnswerYN(String answerYN) {
		this.answerYN = answerYN;
	}
	public String getAnswerContent() {
		return answerContent;
	}
	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}
	public java.sql.Date getAnswerDate() {
		return answerDate;
	}
	public void setAnswerDate(java.sql.Date answerDate) {
		this.answerDate = answerDate;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getMealCode() {
		return mealCode;
	}
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
	public String getManagerCode() {
		return managerCode;
	}
	public void setManagerCode(String managerCode) {
		this.managerCode = managerCode;
	}
	public List<FeedBackFileDTO> getFeedbackFileList() {
		return feedbackFileList;
	}
	public void setFeedbackFileList(List<FeedBackFileDTO> feedbackFileList) {
		this.feedbackFileList = feedbackFileList;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "FeedBackDTO [num=" + num + ", title=" + title + ", content=" + content + ", date=" + date
				+ ", answerYN=" + answerYN + ", answerContent=" + answerContent + ", answerDate=" + answerDate
				+ ", memberCode=" + memberCode + ", mealCode=" + mealCode + ", managerCode=" + managerCode
				+ ", feedbackFileList=" + feedbackFileList + "]";
	}
	
	
	
	
	
	
}
