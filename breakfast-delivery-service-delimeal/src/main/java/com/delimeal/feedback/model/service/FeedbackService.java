package com.delimeal.feedback.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.feedback.model.dao.FeedbackMapper;
import com.delimeal.feedback.model.dto.ChoiceMealWhenInsertFeedbackDTO;
import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.dto.FeedBackDTO;
import com.delimeal.feedback.model.dto.FeedBackFileDTO;
import com.delimeal.feedback.model.dto.FeedbackAndMealDTO;
import com.delimeal.qna.model.dao.QnAMapper;

public class FeedbackService {

	private FeedbackMapper mapper;
	
	/*전체 피드백 개수 - 리스트 조회시(고객) */
	public int selectTotalCount(Map<String, String> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int totalCount = mapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	/*나의 피드백 전체 조회*/
	public List<FeedbackAndMealDTO> selectFeedbackList(Map<String, Object> feedbackMap) {

		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		List<FeedbackAndMealDTO> feedbackList = mapper.selectFeedbackList(feedbackMap);
		
		sqlSession.close();
		
		return feedbackList;
	}

	/* 내게 배송된 식단 개수 - 인서트시 (고객) */
	public int countAteMeal(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int totalCount = mapper.countAteMeal(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	/* 내게 배송된 식단 - 인서트시 (고객) */
	public List<ChoiceMealWhenInsertFeedbackDTO> ateMealList(Map<String, Object> insertMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		List<ChoiceMealWhenInsertFeedbackDTO> selectMealList = mapper.selectMealList(insertMap);
		
		sqlSession.close();
		
		return selectMealList;
	}

	public ChoiceMealWhenInsertFeedbackDTO selectMeal(Map<String, String> selectMeal) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		ChoiceMealWhenInsertFeedbackDTO selectmeal = mapper.selectMeal(selectMeal);
		
		sqlSession.close();
		
		return selectmeal;
	}

	public int insertFeedback(FeedBackDTO feedbackDTO) {
		
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(FeedbackMapper.class);

		int result = 0;
		int feedbackResult = 0;
		int fileResult = 0;

		feedbackResult = mapper.insertFeedback(feedbackDTO);

		/*디티오에 인서트 태그에서 셀렉한 새로운 피드백 넘버 값 피드백 파일에 넣기 */
		List<FeedBackFileDTO> feedbackFile = feedbackDTO.getFeedbackFileList();
		for(int i = 0; i < feedbackFile.size(); i++) {
			feedbackFile.get(i).setFeedbackNum(feedbackDTO.getNum());
		}

		/*파일이 있을경우 인서트*/
		for(int i = 0; i < feedbackFile.size(); i++) {
			fileResult += mapper.insertFile(feedbackFile.get(i));
		}
		
		/*커밋 롤백 */
		if(feedbackResult > 0 && fileResult == feedbackFile.size()) {
			sqlSession.commit();
			result = 1;
		}else {
			sqlSession.rollback();
		}

		sqlSession.close();
		
		return result;
	}

	public DetailDTO selectFeedbackDetail(String feedNum) {

		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		DetailDTO feedback = mapper.selectFeedbackDetail(feedNum);
	
		sqlSession.close();
		
		return feedback;
	}

	public String findMealName(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		String mealName = mapper.findMealName(mealCode);
		
		sqlSession.close();
		
		return mealName;
	}

	public int updateFBandFile(DetailDTO detailDTO) {
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		FeedBackFileDTO fileDTO = new FeedBackFileDTO();
		
		fileDTO.setFeedbackNum(detailDTO.getFeedbackNum());
		fileDTO.setFileName(detailDTO.getFileName());
		
		/* 최종값 */
		int result = 0;
		
		/* 파일 인서트 */
		int fileInsert = mapper.insertFile(fileDTO);
		
		int feedbackUpdate = mapper.updateFeedback(detailDTO);
		
		if(fileInsert > 0 && feedbackUpdate > 0) {
			result = 1;
		}
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();

		return result;
	}

	public int updateFileAndFB(DetailDTO detailDTO) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		FeedBackFileDTO fileDTO = new FeedBackFileDTO();
		
		fileDTO.setFeedbackNum(detailDTO.getFeedbackNum());
		fileDTO.setFileName(detailDTO.getFileName());
		
		int result = 0;
		int fileUpdate = mapper.updateFile(fileDTO);
		int feedbackUpdate = mapper.updateFeedback(detailDTO);
		
		if(fileUpdate > 0 && feedbackUpdate > 0) {
			result = 1;
		}
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteFeedbackFile(String feedbackNum) {
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int result = mapper.deleteFile(feedbackNum);
				
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
				
		return result;
	}

	public int updateFeedback(DetailDTO detailDTO) {
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int result = mapper.updateFeedback(detailDTO);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteFeedback(String feedbackNum) {

		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int result = mapper.deleteFeedback(feedbackNum);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteFileAndFB(String feedbackNum) {
		SqlSession sqlSession = getSqlSession();
		/*파일 삭제 결과 */
		int deleteFileResult = 0;
		/*그냥 삭제 결과*/
		int deleteFeedback = 0;
		int result = 0;
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		deleteFileResult = mapper.deleteFile(feedbackNum);
		deleteFeedback = mapper.deleteFeedback(feedbackNum);
		
		if(deleteFileResult > 0 && deleteFeedback > 0) {
			sqlSession.commit();
			result = 1;
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}

	public int selectAllCount(Map<String, String> searchMap) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int selectAllCount = mapper.selectAllCount(searchMap);
		
		sqlSession.close();
		
		return selectAllCount;
	}

	public List<DetailDTO> selectAllList(Map<String, Object> feedbackMap) {

		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		List<DetailDTO> detail = mapper.selectAllList(feedbackMap);
		
		sqlSession.close();
		
		return detail;
	}

	public int updateAnswer(Map<String, String> answer) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int result = mapper.updateAnswer(answer);
		
		if(result> 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public String selectAnswerWriteMemberCode(String feedbackNum) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		String memberCode = mapper.selectAnswerWriteMemberCode(feedbackNum);
		
		sqlSession.close();
		
		return memberCode;
	}

	public int deleteAnswer(String loginMember) {

		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);

		int resetAnswer = mapper.resetAnswer(loginMember);
		
		if(resetAnswer> 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return resetAnswer;
	}

	public List<DetailDTO> selectWaitingList(Map<String, Object> feedbackMap) {

		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);

		List<DetailDTO> detailList = mapper.selectWaitingList(feedbackMap);
		
		sqlSession.close();
		
		return detailList;
	}

	public int selectWaitingCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(FeedbackMapper.class);
		
		int result = mapper.selectWaitingCount(searchMap);
		
		sqlSession.close();

		return result;
	}





}
