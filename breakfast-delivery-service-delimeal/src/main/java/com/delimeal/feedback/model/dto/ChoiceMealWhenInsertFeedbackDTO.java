package com.delimeal.feedback.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class ChoiceMealWhenInsertFeedbackDTO implements Serializable {
	private static final long serialVersionUID = -3268547430603764106L;

	private String mealName;
	private String mealCate;
	private java.sql.Date deliveryDate;
	private String mealCode;
	public ChoiceMealWhenInsertFeedbackDTO() {
	}
	public ChoiceMealWhenInsertFeedbackDTO(String mealName, String mealCate, Date deliveryDate, String mealCode) {
		this.mealName = mealName;
		this.mealCate = mealCate;
		this.deliveryDate = deliveryDate;
		this.mealCode = mealCode;
	}
	public String getMealName() {
		return mealName;
	}
	public void setMealName(String mealName) {
		this.mealName = mealName;
	}
	public String getMealCate() {
		return mealCate;
	}
	public void setMealCate(String mealCate) {
		this.mealCate = mealCate;
	}
	public java.sql.Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(java.sql.Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getMealCode() {
		return mealCode;
	}
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "ChoiceMealWhenInsertFeedbackDTO [mealName=" + mealName + ", mealCate=" + mealCate + ", deliveryDate="
				+ deliveryDate + ", mealCode=" + mealCode + "]";
	}
	
	
	
}
