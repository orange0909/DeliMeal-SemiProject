package com.delimeal.feedback.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class DetailDTO implements Serializable{
   private static final long serialVersionUID = 906887055364439830L;
   
   private String feedbackNum;
   private String feedbackTitle;
   private String feedbackContent;
   private java.sql.Date feedbackDate;
   private String answerYN;
   private String answerContent;
   private java.sql.Date answerDate;
   private String fileNum;
   private String fileName;
   private String memberName;
   private String mealName;
   private String mealCateName;
   private String managerCode;
public DetailDTO() {
}
public DetailDTO(String feedbackNum, String feedbackTitle, String feedbackContent, Date feedbackDate, String answerYN,
		String answerContent, Date answerDate, String fileNum, String fileName, String memberName, String mealName,
		String mealCateName, String managerCode) {
	this.feedbackNum = feedbackNum;
	this.feedbackTitle = feedbackTitle;
	this.feedbackContent = feedbackContent;
	this.feedbackDate = feedbackDate;
	this.answerYN = answerYN;
	this.answerContent = answerContent;
	this.answerDate = answerDate;
	this.fileNum = fileNum;
	this.fileName = fileName;
	this.memberName = memberName;
	this.mealName = mealName;
	this.mealCateName = mealCateName;
	this.managerCode = managerCode;
}
public String getFeedbackNum() {
	return feedbackNum;
}
public void setFeedbackNum(String feedbackNum) {
	this.feedbackNum = feedbackNum;
}
public String getFeedbackTitle() {
	return feedbackTitle;
}
public void setFeedbackTitle(String feedbackTitle) {
	this.feedbackTitle = feedbackTitle;
}
public String getFeedbackContent() {
	return feedbackContent;
}
public void setFeedbackContent(String feedbackContent) {
	this.feedbackContent = feedbackContent;
}
public java.sql.Date getFeedbackDate() {
	return feedbackDate;
}
public void setFeedbackDate(java.sql.Date feedbackDate) {
	this.feedbackDate = feedbackDate;
}
public String getAnswerYN() {
	return answerYN;
}
public void setAnswerYN(String answerYN) {
	this.answerYN = answerYN;
}
public String getAnswerContent() {
	return answerContent;
}
public void setAnswerContent(String answerContent) {
	this.answerContent = answerContent;
}
public java.sql.Date getAnswerDate() {
	return answerDate;
}
public void setAnswerDate(java.sql.Date answerDate) {
	this.answerDate = answerDate;
}
public String getFileNum() {
	return fileNum;
}
public void setFileNum(String fileNum) {
	this.fileNum = fileNum;
}
public String getFileName() {
	return fileName;
}
public void setFileName(String fileName) {
	this.fileName = fileName;
}
public String getMemberName() {
	return memberName;
}
public void setMemberName(String memberName) {
	this.memberName = memberName;
}
public String getMealName() {
	return mealName;
}
public void setMealName(String mealName) {
	this.mealName = mealName;
}
public String getMealCateName() {
	return mealCateName;
}
public void setMealCateName(String mealCateName) {
	this.mealCateName = mealCateName;
}
public String getManagerCode() {
	return managerCode;
}
public void setManagerCode(String managerCode) {
	this.managerCode = managerCode;
}
public static long getSerialversionuid() {
	return serialVersionUID;
}
@Override
public String toString() {
	return "DetailDTO [feedbackNum=" + feedbackNum + ", feedbackTitle=" + feedbackTitle + ", feedbackContent="
			+ feedbackContent + ", feedbackDate=" + feedbackDate + ", answerYN=" + answerYN + ", answerContent="
			+ answerContent + ", answerDate=" + answerDate + ", fileNum=" + fileNum + ", fileName=" + fileName
			+ ", memberName=" + memberName + ", mealName=" + mealName + ", mealCateName=" + mealCateName
			+ ", managerCode=" + managerCode + "]";
}

	}
