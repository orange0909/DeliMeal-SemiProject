package com.delimeal.feedback.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.feedback.model.dto.ChoiceMealWhenInsertFeedbackDTO;
import com.delimeal.feedback.model.dto.DetailDTO;
import com.delimeal.feedback.model.dto.FeedBackDTO;
import com.delimeal.feedback.model.dto.FeedBackFileDTO;
import com.delimeal.feedback.model.dto.FeedbackAndMealDTO;

public interface FeedbackMapper {

	int selectTotalCount(Map<String, String> searchMap);

	List<FeedbackAndMealDTO> selectFeedbackList(Map<String, Object> feedbackMap);

	int countAteMeal(Map<String, String> searchMap);

	List<ChoiceMealWhenInsertFeedbackDTO> selectMealList(Map<String, Object> insertMap);

	ChoiceMealWhenInsertFeedbackDTO selectMeal(Map<String, String> selectMeal);

	int insertFeedback(FeedBackDTO feedbackDTO);

	int insertFile(FeedBackFileDTO feedBackFileDTO);

	DetailDTO selectFeedbackDetail(String feedNum);

	String findMealName(String mealCode);

	int updateFeedback(DetailDTO detailDTO);

	int updateFile(FeedBackFileDTO fileDTO);

	int deleteFile(String oldFileName);

	int deleteFeedback(String feedbackNum);

	int selectAllCount(Map<String, String> searchMap);

	List<DetailDTO> selectAllList(Map<String, Object> feedbackMap);

	int updateAnswer(Map<String, String> answer);

	String selectAnswerWriteMemberCode(String feedbackNum);

	int resetAnswer(String loginMember);

	List<DetailDTO> selectWaitingList(Map<String, Object> feedbackMap);

	int selectWaitingCount(Map<String, String> searchMap);

	

}
