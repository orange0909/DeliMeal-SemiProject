package com.delimeal.feedback.model.dto;

import java.io.Serializable;

public class FeedBackFileDTO implements Serializable {
	private static final long serialVersionUID = 9141842539085465187L;
	
	private String fileNum;
	private String fileName;
	private String feedbackNum;
	
	public FeedBackFileDTO() {
	}
	public FeedBackFileDTO(String fileNum, String fileName, String feedbackNum) {
		this.fileNum = fileNum;
		this.fileName = fileName;
		this.feedbackNum = feedbackNum;
	}
	public String getFileNum() {
		return fileNum;
	}
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFeedbackNum() {
		return feedbackNum;
	}
	public void setFeedbackNum(String feedbackNum) {
		this.feedbackNum = feedbackNum;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "FeedBackFileDTO [fileNum=" + fileNum + ", fileName=" + fileName + ", feedbackNum=" + feedbackNum + "]";
	}
	
	

}
