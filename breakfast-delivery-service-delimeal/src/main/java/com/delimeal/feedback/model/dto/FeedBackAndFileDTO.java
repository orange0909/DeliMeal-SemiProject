package com.delimeal.feedback.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class FeedBackAndFileDTO implements Serializable {
	private static final long serialVersionUID = -2068682989026246898L;
	
	private String num;
	private String title;
	private String content;
	private java.sql.Date date;
	private String answerYN;
	private String answerContent;
	private java.sql.Date answerDate;
	private String memberCode;
	private String mealCode;
	private String managerCode;
	private FeedBackFileDTO fileDTO;
	public FeedBackAndFileDTO() {
	}
	public FeedBackAndFileDTO(String num, String title, String content, Date date, String answerYN,
			String answerContent, Date answerDate, String memberCode, String mealCode, String managerCode,
			FeedBackFileDTO fileDTO) {
		this.num = num;
		this.title = title;
		this.content = content;
		this.date = date;
		this.answerYN = answerYN;
		this.answerContent = answerContent;
		this.answerDate = answerDate;
		this.memberCode = memberCode;
		this.mealCode = mealCode;
		this.managerCode = managerCode;
		this.fileDTO = fileDTO;
	}
	public String getNum() {
		return num;
	}
	public void setNum(String num) {
		this.num = num;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public java.sql.Date getDate() {
		return date;
	}
	public void setDate(java.sql.Date date) {
		this.date = date;
	}
	public String getAnswerYN() {
		return answerYN;
	}
	public void setAnswerYN(String answerYN) {
		this.answerYN = answerYN;
	}
	public String getAnswerContent() {
		return answerContent;
	}
	public void setAnswerContent(String answerContent) {
		this.answerContent = answerContent;
	}
	public java.sql.Date getAnswerDate() {
		return answerDate;
	}
	public void setAnswerDate(java.sql.Date answerDate) {
		this.answerDate = answerDate;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getMealCode() {
		return mealCode;
	}
	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}
	public String getManagerCode() {
		return managerCode;
	}
	public void setManagerCode(String managerCode) {
		this.managerCode = managerCode;
	}
	public FeedBackFileDTO getFileDTO() {
		return fileDTO;
	}
	public void setFileDTO(FeedBackFileDTO fileDTO) {
		this.fileDTO = fileDTO;
	}
	@Override
	public String toString() {
		return "FeedBackAndFileDTO [num=" + num + ", title=" + title + ", content=" + content + ", date=" + date
				+ ", answerYN=" + answerYN + ", answerContent=" + answerContent + ", answerDate=" + answerDate
				+ ", memberCode=" + memberCode + ", mealCode=" + mealCode + ", managerCode=" + managerCode
				+ ", fileDTO=" + fileDTO + "]";
	}
	
	
	
}
