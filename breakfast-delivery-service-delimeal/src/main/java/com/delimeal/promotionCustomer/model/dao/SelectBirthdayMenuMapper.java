package com.delimeal.promotionCustomer.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.promotionCustomer.model.dto.PromotionDTO;

public interface SelectBirthdayMenuMapper {

	List<BirthDayMenuDTO> selectMenu();

	int changeBirthdayMenu(Map<String, String> map);

	BirthDayMenuDTO selectMemeberBirthdayMenuPicture(String menuCode);

	BirthDayMenuDTO selectBirthDayMenuByCode(String menuCode);

	String selectPicture(String memberBirthdayMenu);

	BirthDayMenuDTO selectBirthdayMenuByMemberCode(String memberCode);

}
