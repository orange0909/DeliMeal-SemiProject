package com.delimeal.promotionCustomer.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionCustomer.model.dao.SelectPromotionMapper;
import com.delimeal.promotionCustomer.model.dto.PromotionDTO;

public class SelectPromotionService {

	public PromotionDTO selectPictureLeft() {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectPromotionMapper mapper = sqlSession.getMapper(SelectPromotionMapper.class);
		
		PromotionDTO result = new PromotionDTO();
		result = mapper.selectPictureLeft();
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public PromotionDTO selectPictureMiddle() {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectPromotionMapper mapper = sqlSession.getMapper(SelectPromotionMapper.class);
		
		PromotionDTO result = new PromotionDTO();
		result = mapper.selectPictureMiddle();
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public PromotionDTO selectPictureRight() {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectPromotionMapper mapper = sqlSession.getMapper(SelectPromotionMapper.class);
		
		PromotionDTO result = new PromotionDTO();
		result = mapper.selectPictureRight();
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

}
