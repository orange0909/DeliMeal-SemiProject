package com.delimeal.promotionCustomer.model.dto;

import java.io.Serializable;

public class BirthDayMenuDTO implements Serializable{

	private static final long serialVersionUID = -19807758115433698L;
	
	private String code;
	private String name;
	private String allergy;
	private String calorie;
	private String nutrient;
	private String picture;
	
	public BirthDayMenuDTO() {
	}

	public BirthDayMenuDTO(String code, String name, String allergy, String calorie, String nutrient, String picture) {
		this.code = code;
		this.name = name;
		this.allergy = allergy;
		this.calorie = calorie;
		this.nutrient = nutrient;
		this.picture = picture;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAllergy() {
		return allergy;
	}

	public void setAllergy(String allergy) {
		this.allergy = allergy;
	}

	public String getCalorie() {
		return calorie;
	}

	public void setCalorie(String calorie) {
		this.calorie = calorie;
	}

	public String getNutrient() {
		return nutrient;
	}

	public void setNutrient(String nutrient) {
		this.nutrient = nutrient;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "BirthDayMenuDTO [code=" + code + ", name=" + name + ", allergy=" + allergy + ", calorie=" + calorie
				+ ", nutrient=" + nutrient + ", picture=" + picture + "]";
	}
	
}
