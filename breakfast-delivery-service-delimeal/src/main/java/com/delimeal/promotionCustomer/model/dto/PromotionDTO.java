package com.delimeal.promotionCustomer.model.dto;

import java.io.Serializable;

public class PromotionDTO implements Serializable{
	private static final long serialVersionUID = -3636318120625119914L;
	
	private String code;
	private String name;
	private String description;
	private String url;
	private int classifiaction;
	
	public PromotionDTO() {
	}

	public PromotionDTO(String code, String name, String description, String url, int classifiaction) {
		this.code = code;
		this.name = name;
		this.description = description;
		this.url = url;
		this.classifiaction = classifiaction;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getClassifiaction() {
		return classifiaction;
	}

	public void setClassifiaction(int classifiaction) {
		this.classifiaction = classifiaction;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "PromotionDTO [code=" + code + ", name=" + name + ", description=" + description + ", url=" + url
				+ ", classifiaction=" + classifiaction + "]";
	}
	
}
