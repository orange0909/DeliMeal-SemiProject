package com.delimeal.promotionCustomer.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionCustomer.model.dao.SelectBirthdayMenuMapper;

public class RegisterBirthDayMenuService {

	public int updateBirthdayMenu(Map<String, String> map) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		int result = mapper.changeBirthdayMenu(map);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		System.out.println(result);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

}
