package com.delimeal.promotionCustomer.model.dao;

import com.delimeal.promotionCustomer.model.dto.PromotionDTO;

public interface SelectPromotionMapper {
	
	PromotionDTO selectPictureLeft();

	PromotionDTO selectPictureMiddle();

	PromotionDTO selectPictureRight();

}
