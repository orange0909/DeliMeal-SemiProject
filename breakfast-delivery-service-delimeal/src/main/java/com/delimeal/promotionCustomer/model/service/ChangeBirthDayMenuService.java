package com.delimeal.promotionCustomer.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionCustomer.model.dao.SelectBirthdayMenuMapper;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

public class ChangeBirthDayMenuService {
	
	public List<BirthDayMenuDTO> selectBirthdayMenu() {
	
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		List<BirthDayMenuDTO> menuList = new ArrayList<>();
		menuList = mapper.selectMenu();
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return menuList;
	}

	public int changeBirthdayMenu(Map<String, String> map) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		int result = mapper.changeBirthdayMenu(map);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		System.out.println(result);
		
		
		if(result > 0 ) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

	public BirthDayMenuDTO selectBirthdayMenuPic(String menuCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		BirthDayMenuDTO birthdayMenu = new BirthDayMenuDTO();
		birthdayMenu = mapper.selectMemeberBirthdayMenuPicture(menuCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return birthdayMenu;
	}

	public BirthDayMenuDTO selectMemberBirthdayMenu(String menuCode) {
		
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		BirthDayMenuDTO birthdayMenu = new BirthDayMenuDTO();
		birthdayMenu = mapper.selectMemeberBirthdayMenuPicture(menuCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return birthdayMenu;
	}

	public String selectPicture(String memberBirthdayMenu) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectPicture Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		String result = null;
		result = mapper.selectPicture(memberBirthdayMenu);
		System.out.println("selectPicture Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public BirthDayMenuDTO selectBirthdayMenuByMemberCode(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("selectPicture Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		BirthDayMenuDTO result = null;
		result = mapper.selectBirthdayMenuByMemberCode(memberCode);
		System.out.println("selectPicture Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

}
