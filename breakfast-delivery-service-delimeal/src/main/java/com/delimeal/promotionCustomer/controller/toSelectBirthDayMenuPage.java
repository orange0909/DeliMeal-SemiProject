package com.delimeal.promotionCustomer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.promotionCustomer.model.service.ChangeBirthDayMenuService;

@WebServlet("/pc/toselectBirthdayMenu")
public class toSelectBirthDayMenuPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 세션에서 멤버정보 불러오기 */
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		String memberBirthdayMenu = member.getBdayMenuCode();
		System.out.println(memberBirthdayMenu);
		ChangeBirthDayMenuService birthdayMenuService = new ChangeBirthDayMenuService();
		BirthDayMenuDTO BDayMenuDTO = new BirthDayMenuDTO(); 
		BDayMenuDTO = birthdayMenuService.selectMemberBirthdayMenu(memberBirthdayMenu);
		
		System.out.println(BDayMenuDTO.getName());
		
		request.setAttribute("memberBirthdayMenu", BDayMenuDTO);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/Promotion-SelectBirthdayMenu.jsp");
		rd.forward(request, response);
	}

}
