package com.delimeal.promotionCustomer.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.promotionCustomer.model.service.ChangeBirthDayMenuService;
import com.delimeal.promotionCustomer.model.service.RegisterBirthDayMenuService;

@WebServlet("/register/registBirthdayMenu")
public class RegisterBirthdayMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		System.out.println(request.getParameter("birthday_menu"));
		
		// 세션에서 회원코드 불러오기
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		String birthdayMenu = request.getParameter("birthday_menu");
		
		Map<String, String> map = new HashMap<>();
		map.put("memberCode", memberCode);
		map.put("menuCode", birthdayMenu);
		
		ChangeBirthDayMenuService birthdayMenuService = new ChangeBirthDayMenuService();
		int result = birthdayMenuService.changeBirthdayMenu(map);
		
		if(result > 0) {
			BirthDayMenuDTO birthday = new BirthDayMenuDTO();
			
			/* 성공 했을 경우기 때문에 회원이 선택한 메뉴 번호를 그대로 사용해서 조회한다 */
			birthday = birthdayMenuService.selectBirthdayMenuPic(birthdayMenu);
			/* 로그인한 현재 회원의 정보도 Update해준다 */
			((MemberDTO)request.getSession().getAttribute("loginMember")).setBdayMenuCode(birthday.getCode());
			String path = "../resources/food/";
			request.setAttribute("resultName", birthday.getName());
			System.out.println(birthday.getName());
			request.setAttribute("resultPic", path + birthday.getPicture());
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/registerBirthdayMenuSuccessjsp.jsp");
			rd.forward(request, response);
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/registerBirthdayMenuFailure.jsp");
			rd.forward(request, response);
		}
		
	}

}
