package com.delimeal.promotionCustomer.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.promotionCustomer.model.service.ChangeBirthDayMenuService;

@WebServlet("/pc/selectBirthdayMenu")
public class SelectBirthdayMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();


		/* 생일메뉴 DTO들 불러오고 */
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		String memberBirthdayMenu = member.getBdayMenuCode();
		System.out.println(memberBirthdayMenu);
		/* 선택한 생일메뉴가 없는 경우 등록 페이지로 이동 시킨다 */
		if(memberBirthdayMenu == null || memberBirthdayMenu.equals("")) {
			RequestDispatcher rd = request.getRequestDispatcher("/register/BirthdayMenuMain");
			rd.forward(request, response);
		} else {
			ChangeBirthDayMenuService birthdayMenuService = new ChangeBirthDayMenuService();
			
			List<BirthDayMenuDTO> BDayMenuList = null; 
			BDayMenuList = birthdayMenuService.selectBirthdayMenu();
			List<BirthDayMenuDTO> returnList = new ArrayList<>();
			
			/* remove를 사용하면 element가 삭제되면서 반복문 수행 하지 않기 때문에 새 ArrayList에 옮겨 닮는다 */
			for(BirthDayMenuDTO bday : BDayMenuList) {
				/* memberBirthdayMenu는 나중에 세션에서 꺼낸걸로 바꾸어 주어야 한다. */
				if(!memberBirthdayMenu.equals(bday.getCode())) {
					returnList.add(bday);
				}
			}
			
			request.setAttribute("BirthdayMenu", returnList);
			
			/* forward작업 수행 */
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/Promotion-ChangeBirthdayMenu.jsp");
			rd.forward(request, response);
		}
	}
}
