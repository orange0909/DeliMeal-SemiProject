package com.delimeal.promotionCustomer.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.promotionCustomer.model.service.ChangeBirthDayMenuService;

@WebServlet("/register/BirthdayMenuMain")
public class GotoRegisterBirthdayMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 선택한 생일메뉴가 있는 경우 변경 페이지로 이동시킨다 */
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberBirthdayMenu = member.getBdayMenuCode();
		System.out.println(memberBirthdayMenu);
		if(memberBirthdayMenu != null && !memberBirthdayMenu.equals("")) {
			RequestDispatcher rd = request.getRequestDispatcher("/pc/selectBirthdayMenu");
			rd.forward(request, response);
		} else {
			ChangeBirthDayMenuService birthdayMenuService = new ChangeBirthDayMenuService();
			List<BirthDayMenuDTO> BDayMenuList = birthdayMenuService.selectBirthdayMenu();
			
			request.setAttribute("BirthdayMenu", BDayMenuList);
//		for(BirthDayMenuDTO bday: BDayMenuList) {
//			System.out.println(bday.getName());
//		}
			
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/Promotion-RegisterBirthdayMenu.jsp");
			rd.forward(request, response);
		}
	}
}
