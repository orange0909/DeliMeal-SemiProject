package com.delimeal.promotionCustomer.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.delimeal.promotionCustomer.model.service.ChangeBirthDayMenuService;

@WebServlet("/pc/GotoChangeSuccessPage")
public class GotoChangeSuccessPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		ChangeBirthDayMenuService birthdayMenuService = new ChangeBirthDayMenuService();
		BirthDayMenuDTO BDayMenuDTO = new BirthDayMenuDTO(); 
		BDayMenuDTO = birthdayMenuService.selectBirthdayMenuByMemberCode(memberCode);
		request.setAttribute("bdayMenu", BDayMenuDTO);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/changeBirthdayMenuSuccessjsp.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
