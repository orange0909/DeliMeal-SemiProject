package com.delimeal.promotionCustomer.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionCustomer.model.dto.PromotionDTO;
import com.delimeal.promotionCustomer.model.service.SelectPromotionService;

@WebServlet("/pc/GotoInfluencer")
public class GotoInfluencer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		SelectPromotionService service = new SelectPromotionService();
		PromotionDTO promotionPictureLeft = service.selectPictureLeft();
		PromotionDTO promotionPictureMiddle = service.selectPictureMiddle();
		PromotionDTO promotionPictureRight = service.selectPictureRight();
		
		request.setAttribute("promotionPictureLeft", promotionPictureLeft);
		request.setAttribute("promotionPictureMiddle", promotionPictureMiddle);
		request.setAttribute("promotionPictureRight", promotionPictureRight);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/Promotion-Infulencer.jsp");
		rd.forward(request, response);
	}

}