package com.delimeal.promotionCustomer.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.promotionCustomer.model.service.ChangeBirthDayMenuService;

@WebServlet("/pc/ChangeBirthdayMenu")
public class ChangeBirthdayMenu extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		
		/* 세션에서 멤버코드 뽑아내기 */
		MemberDTO member = (MemberDTO)request.getSession().getAttribute("loginMember");
		String memberCode = member.getCode();
		String menuCode = request.getParameter("birthday_menu");
		System.out.println(menuCode);
		
		Map<String, String> map = new HashMap<>();
		map.put("memberCode", memberCode);
		map.put("menuCode", menuCode);
		
		ChangeBirthDayMenuService birthdayMenuService = new ChangeBirthDayMenuService();
		int result = birthdayMenuService.changeBirthdayMenu(map);
		
		if(result > 0) {
			/* 바뀐 메뉴코드 세션에 적용 */
			((MemberDTO)request.getSession().getAttribute("loginMember")).setBdayMenuCode(menuCode);
			response.sendRedirect(request.getContextPath()+"/pc/GotoChangeSuccessPage");
		}else {
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-customer/changeBirthdayMenuFailure.jsp");
			rd.forward(request, response);
		}
	}
}
