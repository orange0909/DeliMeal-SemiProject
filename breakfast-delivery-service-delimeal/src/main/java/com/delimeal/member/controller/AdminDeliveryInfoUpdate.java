package com.delimeal.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.service.MemberService;
import com.delimeal.subscription.model.dto.DeliveryDetailDTO;
import com.google.gson.Gson;


@WebServlet("/admin/updateDeliveryFinish")
public class AdminDeliveryInfoUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberService service = new MemberService();
		
		String deliveryCode = request.getParameter("deliveryCode");
		String deliveryAddrFinishYn = request.getParameter("deliveryAddrFinishYn");
		
		System.out.println("deliveryAddrFinishYn: " + deliveryAddrFinishYn);
		
		DeliveryDetailDTO detail = new DeliveryDetailDTO();
		detail.setDeliveryCode(deliveryCode);
		detail.setIsFinish(deliveryAddrFinishYn);
		
		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		if(service.updateDeliveryFinish(detail)) {
			out.print(new Gson().toJson("success"));
		} else {
			out.print(new Gson().toJson("failed"));
		}
		
		out.flush();
		out.close();
	}

}
