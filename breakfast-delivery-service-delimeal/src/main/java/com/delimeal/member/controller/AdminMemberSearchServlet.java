package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.member.model.service.AdminMemberSearchService;

@WebServlet("/admin/memberInfo")
public class AdminMemberSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/adminMemberSearch.jsp");
		rd.forward(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		MemberAndDefaultAddrDTO requestMember = new MemberAndDefaultAddrDTO();
		AdminMemberSearchService adminMemberSearchService = new AdminMemberSearchService();
		
		requestMember.setName(request.getParameter("userName"));
		
		String hp1 = request.getParameter("hp1");
		String hp2 = request.getParameter("hp2");
		String hp3 = request.getParameter("hp3");
		
		String phone = hp1 + "-" + hp2 + "-" + hp3;
		
		requestMember.setPhone(phone);
		  
		MemberAndDefaultAddrDTO searchMember = adminMemberSearchService.searchByNameAndPhone(requestMember);
		System.out.println(searchMember);
		request.setAttribute("searchMember", searchMember);
		
		request.getRequestDispatcher("/WEB-INF/views/member/adminMemberSearchResult.jsp").forward(request, response);
		System.out.println("수정이 완료되었습니다.");
	
	}

}
