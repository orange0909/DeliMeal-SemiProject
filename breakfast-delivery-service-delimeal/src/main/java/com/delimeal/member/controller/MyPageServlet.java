package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/mypage")
public class MyPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String path = "/WEB-INF/views/member/mypageView.jsp";
		
		
		
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		loginMember.getName();
		request.setAttribute("memberName", loginMember.getName());
		System.out.println("회원이름: " + loginMember.getName());
		
		String code = loginMember.getCode();
		
		int result = new MemberService().countCoupons(code);
		System.out.println("회원 이름: " + loginMember.getCode());
		
		String page= "";
		
		if(result >= 0) {
		
			page = "/WEB-INF/views/member/mypageView.jsp";
			System.out.println("회원이름: " + loginMember.getName());
			request.setAttribute("result", result);
			System.out.println("보유 쿠폰 개수 조회 결과: " + result);
		
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			
		}
		
		request.getRequestDispatcher(page).forward(request, response);

	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
		System.out.println("뭐가 되긴 되는거여..?");
	
	}

}
