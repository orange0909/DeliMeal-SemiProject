package com.delimeal.member.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/mypage/coupons")
public class MyPageCouponServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		loginMember.getCode();
		
		System.out.println("코드 겟하는지: " + loginMember.getCode());
		/* */
		
		
		String path = "";
		
//		System.out.println("");
	
		List<IssuedCouponInfoDTO> issuedCouponList = MemberService.selectIssuedCouponList(loginMember.getCode());
		
		System.out.println("서블릿까지 돌아오나요..?"+ issuedCouponList);
		
		if(issuedCouponList != null) {
			
			System.out.println("요까지 와?");
			path = "/WEB-INF/views/member/mypageCouponView.jsp";
			request.setAttribute("issuedCouponList", issuedCouponList);
			
		} else {
			path = "/WEB-INF/views/member/mypageMainView.jsp";
			request.setAttribute("message", "");
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}
}
	
	
		
	

