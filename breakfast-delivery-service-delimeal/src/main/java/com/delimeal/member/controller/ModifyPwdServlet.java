	
  package com.delimeal.member.controller;
  
  import java.io.IOException;
  
  import javax.servlet.ServletException; import
  javax.servlet.annotation.WebServlet; import javax.servlet.http.HttpServlet;
  import javax.servlet.http.HttpServletRequest; import
  javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;
  
  @WebServlet("/member/modifyPwd")
  public class ModifyPwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	      protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	      
	  		String path = "/WEB-INF/views/member/modifyPwdView.jsp";
	  		
			request.getRequestDispatcher(path).forward(request, response);
	      
	      }

		  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			  
			  request.setCharacterEncoding("UTF-8");
	
			  String id = request.getParameter("id");
			  String password = request.getParameter("password");
			  
			  System.out.println("비찾에서 넘어온 값 확인: " + id + "/" + password);
			  MemberDTO findMemberPwd = new MemberDTO();
			  
			  findMemberPwd.setId(id);
			  findMemberPwd.setPassword(password);
			  
			  int result = new MemberService().modifyPwd(findMemberPwd);
			  System.out.println("서블릿에서 성공여부 확인: " + result);
			  
			  String page = "";
			  
			  if(result > 0) {
				  page = "/WEB-INF/views/common/success.jsp";
				  request.setAttribute("successCode", "modifyPwd");
			  } else {
			  
				  page = "/WEB-INF/views/common/failed.jsp";
				  request.setAttribute("failedCode", "modifyPwd");

			  
		  }
			  request.getRequestDispatcher(page).forward(request, response); 
	 }
 }
