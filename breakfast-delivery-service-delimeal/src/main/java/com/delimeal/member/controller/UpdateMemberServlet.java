package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/mypage/updateMember")
public class UpdateMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		System.out.println("updateMemberServlet 아이디 겟하는지: " + loginMember.getId());
		
		/* 회원 정보 조회 */
		MemberDTO updateMember = new MemberDTO();

//		System.out.println("서블릿에서 MemberDTO 조회: " + updateMember);
	
		MemberDTO result = new MemberService().selectMemberById(loginMember.getId());
	
		System.out.println("회원정보 수정 서블릿으로 돌아오는 값 : " + result);

		request.setAttribute("member", result);
		
		String page= "";

		if(result != null ) {
			
			page = "/WEB-INF/views/member/updateMemberView.jsp";
		
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
		}
		
		request.getRequestDispatcher(page).forward(request, response);
	}
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		request.setCharacterEncoding("UTF-8");
		
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		
		/* 회원 정보 수정 */
		
		String id = loginMember.getId();
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");

		MemberDTO updateMember = new MemberDTO();
		
		updateMember.setId(id);
		updateMember.setPassword(password);
		updateMember.setEmail(email);
		updateMember.setPhone(phone);
		
		System.out.println("deleteMemberServlet 아이디 겟하는지: " + updateMember.getId());
		System.out.println("입력받은 값 확인: " + password + email + "/" + phone);
		
		int result = new MemberService().updateMember(updateMember);
		
		String page = "";
		if(result > 0 ) {
			
			page = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateMember");
		
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
		}
		
		request.getRequestDispatcher(page).forward(request, response);
		
		
		}
}
