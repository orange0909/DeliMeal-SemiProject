package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/mypage/deleteAccount")
public class DeleteMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private MemberService memberService = new MemberService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String path = "/WEB-INF/views/member/deleteMemberView.jsp";
		request.getRequestDispatcher(path).forward(request, response);
	}
		
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		loginMember.getCode();
		
		System.out.println("deleteMemberServlet 코드 겟하는지: " + loginMember.getCode());
		
		String page = "";
		System.out.println("deleteMemberServlet");
		int result = memberService.deleteMember(loginMember.getCode());
		System.out.println("서블릿까지 돌아오나요..?"+ result);

		if(result > 0) {
			page = "/WEB-INF/views/common/success.jsp";
			System.out.println("성공?");
			request.setAttribute("successCode", "deleteMember");

		} else {
			page = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("", result);

		}
		request.getRequestDispatcher(page).forward(request, response);

	}
}
