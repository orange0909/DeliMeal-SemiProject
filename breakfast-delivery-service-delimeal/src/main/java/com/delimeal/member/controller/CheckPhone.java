package com.delimeal.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.service.MemberService;
import com.google.gson.Gson;

@WebServlet("/CheckPhone")
public class CheckPhone extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String phone = request.getParameter("phone");
	    System.out.println("입력받은 휴대폰번호: " + phone);
	    
	    int result = new MemberService().checkPhone(phone);
	    System.out.println("돌아오는지 서블릿에서 확인: " + result);
		String jsonString = new Gson().toJson(result);
		
		System.out.println(result);

		response.setContentType("application/json; charset=UTF-8");
		
		System.out.println(result);
		/* 인코딩 부분 확인해보기 */
//	    response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    out.print(jsonString);
	    out.flush();
	    out.close();
	}

}
