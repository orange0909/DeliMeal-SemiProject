package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/member/regist")
public class MemberRegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String path = "/WEB-INF/views/member/registFormView.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		String id = request.getParameter("id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		java.sql.Date bday = java.sql.Date.valueOf(request.getParameter("bday"));
		String gender = request.getParameter("gender");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		
		MemberDTO requestMember = new MemberDTO();
		
		requestMember.setId(id);
		requestMember.setPassword(password);
		requestMember.setName(name);
		requestMember.setBday(bday);
		requestMember.setGender(gender);
		requestMember.setEmail(email);
		requestMember.setPhone(phone);
		
		System.out.println("회원가입 서블릿으로 입력받은 값 : " + requestMember);
	
		int result = new MemberService().registMember(requestMember);
		
		System.out.println("회원가입 서블릿으로 돌아오는 값 : " + result);
		
		String page= "";
		
		if(result > 0 ) {
		
			page = "/WEB-INF/views/member/successSignUp.jsp";
			
			request.getRequestDispatcher(page).forward(request, response);

//			request.setAttribute("successCode", "insertMember");
		
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			
			request.getRequestDispatcher(page).forward(request, response);
		}
	}
}
