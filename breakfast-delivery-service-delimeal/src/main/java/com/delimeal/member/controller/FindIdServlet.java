package com.delimeal.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/member/findId")
public class FindIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String path = "/WEB-INF/views/member/findIdView.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");

		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		
		System.out.println("입력받은 값: " + name +"/ " + phone);
		
		MemberDTO findIdMember = new MemberDTO();
		
		findIdMember.setName(name);
		findIdMember.setPhone(phone);
		
		MemberDTO result = new MemberService().selectId(findIdMember);
		System.out.println("서블릿에 돌아온 값 확인: " + result);
		
		String page= "";          
		
		if(result != null) {
			
			page = "/WEB-INF/views/member/findIdSuccess.jsp";
			request.setAttribute("result", result);
			System.out.println(result);
		
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "findId");

		}
		request.getRequestDispatcher(page).forward(request, response);
	}
}
