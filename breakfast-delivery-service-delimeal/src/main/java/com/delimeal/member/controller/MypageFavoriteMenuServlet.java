package com.delimeal.member.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/mypage/favMenu")
public class MypageFavoriteMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		/* 선호 메뉴 조회 */
		String path = "";
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		String code = loginMember.getCode();
		String result = new MemberService().myFavoriteMenu(code);
		
		if(result != null || result == null) {
			
			path = "/WEB-INF/views/member/mypageFavoriteMenu.jsp";
			request.setAttribute("result", result);
		
	    }
		
		/* 메뉴 불러오기 */
		List<MenuDTO> favoriteMenuList = MemberService.selectAllMenuList();
		
	    request.setAttribute("menuList", favoriteMenuList);
		
		if(favoriteMenuList != null) {
			
			path = "/WEB-INF/views/member/mypageFavoriteMenu.jsp";
			request.setAttribute("message", "");
		
	    } else {
	    	
	    }
		
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		/* 선호 메뉴 등록 */
		
		MemberDTO loginMember = null;
		
		loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		
		String id = loginMember.getId();
		String menuName = request.getParameter("favoriteMenu");
		System.out.println("입력받은 값 확인: " + menuName);
		
		Map<String, String> favoriteMenu = new HashMap<>();
		favoriteMenu.put("id", id);
		favoriteMenu.put("menuName", menuName);
		
		int result = new MemberService().updateFavoriteMenu(favoriteMenu);
		
		System.out.println("선호메뉴 서블릿에서 결과: " + result);
		
		String page="";
		if(result > 0) {
			
			page = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateFavoriteMenu");
			
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "updateFavoriteMenu");
	}
		request.getRequestDispatcher(page).forward(request, response);
	}
}
