package com.delimeal.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String redirectURL = request.getParameter("returnUrl");
		
		HttpSession session = request.getSession();
		
		if(session.getAttribute("loginMember") == null) {
			// 로그인 안되어있음
			System.out.println("LogoutServlet > doGet()");
			System.out.println("Logout failed");			
		} else {
			// 로그인 되어있음
			System.out.println("LogoutServlet > doGet()");
			System.out.println("Logout success");
			session.setAttribute("loginMember", null);
		}
		
		// 원래 있던 페이지로 되돌아가거나 원래 있던 페이지가 없다면 메인페이지로 이동함
		if("".equals(redirectURL) || redirectURL == null) {
			redirectURL = request.getContextPath();
		}
		
		System.out.println("LogoutServlet > doGet()");
		System.out.println("redirect to " + redirectURL);
		
		response.sendRedirect(redirectURL);
	}

}
