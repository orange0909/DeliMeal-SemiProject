package com.delimeal.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.service.MemberService;
import com.google.gson.Gson;

/**
 * 쿠폰 정보를 Ajax로 전달하는 서블릿
 */
@WebServlet("/member/getCouponInfo")
public class GetCouponInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String issuedCouponCode = request.getParameter("issuedCouponCode");
		
		MemberService memberService = new MemberService();
		
		/* 쿠폰 번호로 쿠폰 정보 가져오기 */
		IssuedCouponInfoDTO issuedCouponInfo = memberService.selectIssuedCouponInfoByCode(issuedCouponCode);
		System.out.println(":: GetCouponInfoServlet ::");
		System.out.println("selectIssuedCouponInfoByCode: " + issuedCouponInfo);
		
		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		out.print(new Gson().toJson(issuedCouponInfo));
		out.flush();
		out.close();
	}

}
