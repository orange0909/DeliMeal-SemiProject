package com.delimeal.member.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/admin/subscriberList")
public class AdminSubscriberListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	MemberService memberService = new MemberService();
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<MemberAndDefaultAddrDTO> subscriberList = memberService.selectAllSubscriber();
		
		request.setAttribute("subscriberList", subscriberList);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/adminSubscriberListPage.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
