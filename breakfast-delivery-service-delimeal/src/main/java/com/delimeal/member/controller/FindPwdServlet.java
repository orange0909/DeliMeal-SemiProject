package com.delimeal.member.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/member/findPwd")
public class FindPwdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		String path = "/WEB-INF/views/member/findPwdView.jsp";
	
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");

		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String phone = request.getParameter("phone");
		System.out.println("비찾에서 입력받은 값: " + id +"/ " + name +"/ " + phone);
		
		MemberDTO findPwdMember = new MemberDTO();
		
		findPwdMember.setId(id);
		findPwdMember.setName(name);
		findPwdMember.setPhone(phone);
		
		int result = new MemberService().findPwd(findPwdMember);
		System.out.println("서블릿에 돌아온 값 확인: " + result);
		
		request.setAttribute("result", result);
		String page = "";
		
		if(result > 0) {
			
			page = "/WEB-INF/views/member/modifyPwdView.jsp";
			request.setAttribute("id", id);

			
			System.out.println(id);
		
		} else {
			
			page = "/WEB-INF/views/common/failed.jsp";
		    request.setAttribute("failedCode", "findPwd");

		}
		request.getRequestDispatcher(page).forward(request, response);

	}
}
