package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.AdminMemberSearchService;

@WebServlet("/admin/memberUpdate")
public class AdminMemberUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/adminMemberSearchResult.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		MemberDTO requestMember = new MemberDTO();
		AdminMemberSearchService adminMemberSearchService = new AdminMemberSearchService();
		
		String code = re(request.getParameter("code"));
		String password = re(request.getParameter("password"));
		String phone = re(request.getParameter("phone"));
		int point = Integer.parseInt(re(request.getParameter("point")));
		
		requestMember.setCode(code);
		requestMember.setPassword(password);
		requestMember.setPhone(phone);
		requestMember.setPoint(point);
		System.out.println(requestMember);
		  
		MemberAndDefaultAddrDTO searchMember = adminMemberSearchService.updateMemberInfo(requestMember);
		System.out.println(searchMember);
		request.setAttribute("searchMember", searchMember);
		
		request.getRequestDispatcher("/WEB-INF/views/member/adminMemberSearchResult.jsp").forward(request, response);
	}
	
	private static String re(String str) {
		if(str==null || str.trim().equals(""))
			str = "0";

		return str;
	}

}
