package com.delimeal.member.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private MemberService memberService = new MemberService();
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getSession().getAttribute("loginMember") != null) {
			// 이미 로그인 되어있어서 원래 있던 페이지로 되돌아가거나 원래 있던 페이지가 없다면 메인페이지로 이동함
			System.out.println("LoginServlet > doGet()");
			System.out.println("Already logged in!");
			this.redirectPrev(request, response);
		} else {
			// 로그인 안되어 있어서 로그인 페이지 띄워줌
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/loginView.jsp");
			rd.forward(request, response);
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		MemberDTO requestMember = new MemberDTO();
		
		requestMember.setId(request.getParameter("userId"));
		requestMember.setPassword(request.getParameter("password"));
		
		// 체크박스가 체크되어 있으면 기본값으로 on을 전달해줌 이 외의 값이면 false로 처리함
		boolean isKeepLoggedIn = "on".equals(request.getParameter("keepLoggedIn"));
		
		// 아이디와 비밀번호 매칭 성공하면 MemberDTO를 반환, 실패하면 null 반환 
		MemberDTO loginMember = memberService.login(requestMember);
		
		if(loginMember == null) {
			// 아이디 또는 비밀번호가 잘못 됨
			request.setAttribute("loginFailureMessage", "아이디 또는 비밀번호가 맞지 않습니다.");
			request.setAttribute("inputtedId", requestMember.getId());
			
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/loginView.jsp");
			rd.forward(request, response);
		} else {
			// 로그인 성공
			request.getSession().setAttribute("loginMember", loginMember);
			System.out.println("LoginServlet > doPost()");
			System.out.println(loginMember.getId() + " login success!");
			
			// 원래 있던 페이지로 되돌아가거나 원래 있던 페이지가 없다면 메인페이지로 이동함
			this.redirectPrev(request, response);
		}
	}
	
	public void redirectPrev(HttpServletRequest request, HttpServletResponse response) throws IOException {
		String redirectURL = request.getParameter("returnUrl");
		
		if("".equals(redirectURL) || redirectURL == null) {
			redirectURL = request.getContextPath();
		}
		
		System.out.println("LoginServlet > redirectPrev()");
		System.out.println("redirect to " + redirectURL);
		
		response.sendRedirect(redirectURL);
	}
}
