package com.delimeal.member.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.member.model.service.AdminMemberSearchService;
import com.delimeal.member.model.service.MemberService;

@WebServlet("/admin/deliverySearch")
public class AdminDeliverySearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberService memberService = new MemberService();
		
		String date = request.getParameter("date");
		request.setAttribute("date", date);
		
		java.sql.Date deliveryDate = null;
		
		if(date != null && date != "") {
			deliveryDate =  java.sql.Date.valueOf(date);
		}
		
		List<MemberAndDefaultAddrDTO> deliverySearch = memberService.selectDeliverySearch(deliveryDate);
		
		System.out.println(":: AdminDeliverySearchServlet ::");
		System.out.println("deliverySearch: " + deliverySearch);

		request.setAttribute("deliverySearch", deliverySearch);
		
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/adminDeliverySearch.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			request.setCharacterEncoding("UTF-8");		
		MemberService memberService = new MemberService();
		
//		List<MemberAndDefaultAddrDTO> updateDelivery = memberService.updateDeliverySearch();
		
//		request.setAttribute("updateDelivery", updateDelivery);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/member/adminNotSubscriberListPage.jsp");
		rd.forward(request, response);
		
	}	
}