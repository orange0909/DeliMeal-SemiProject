package com.delimeal.member.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;
import com.google.gson.Gson;

@WebServlet("/idCheck")
public class CheckIdServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    String id = request.getParameter("id");
	    System.out.println("입력받은 아이디: " + id);
	    
	    int result = new MemberService().idCheck(id);
	    System.out.println("돌아오는지 서블릿에서 확인: " + result);
		String jsonString = new Gson().toJson(result);
		
		System.out.println(result);

		response.setContentType("application/json; charset=UTF-8");
		
		System.out.println(result);
		/* 인코딩 부분 확인해보기 */
//	    response.setCharacterEncoding("UTF-8");
	    PrintWriter out = response.getWriter();
	    out.print(jsonString);
	    out.flush();
	    out.close();
	}
}
