package com.delimeal.member.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.DeliveryAddrDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.member.model.service.MemberService;
import com.google.gson.Gson;

/**
 * GET은 배송지 관리가 가능한 페이지를 띄워줌
 * POST는 배송지 관리를 AJAX로 처리함
 */
@WebServlet("/member/manageAddress")
public class AddressManagerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberService memberService = new MemberService();
		
		MemberDTO loginMember = (MemberDTO) request.getSession().getAttribute("loginMember");
		String memberCode = null;
		List<DeliveryAddrDTO> addrList = null;
		
		if(loginMember != null) {
			memberCode = loginMember.getCode();
			
			addrList = memberService.selectDeliveryAddrByMemberCode(memberCode);
		}
		
		request.setAttribute("addrList", addrList);
		
		request.getRequestDispatcher("/WEB-INF/views/member/addressManagerPage.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MemberService service = new MemberService();
		
		String dmlType = request.getParameter("dmlType");
		
		/* dmlType 파라메터가 delete 일때 -> 삭제 진행 */
		if("delete".equals(dmlType)) {
			String addrCode = request.getParameter("addrCode");
			
			response.setContentType("application/json; charset=UTF-8");
			PrintWriter out = response.getWriter();
			
			if(service.deleteDeliveryAddrByCode(addrCode)) {
				out.print(new Gson().toJson("success"));
			} else {
				out.print(new Gson().toJson("failure"));
			}
			out.flush();
			out.close();
			
		/* dmlType 파라메터가 update 일때 -> 수정 진행 */
		} else if ("update".equals(dmlType)) {
			String addrCode = request.getParameter("addrCode");
			String addrName = request.getParameter("addrName");
			int addrZipcode = Integer.valueOf(request.getParameter("addrZipcode"));
			String addr = request.getParameter("addr");
			String addrDetail = request.getParameter("addrDetail");
			String deliveryReq = request.getParameter("deliveryReq");
			
			DeliveryAddrDTO deliveryAddr = new DeliveryAddrDTO();
			deliveryAddr.setAddrCode(addrCode);
			deliveryAddr.setAddrName(addrName);
			deliveryAddr.setAddrZipcode(addrZipcode);
			deliveryAddr.setAddr(addr);
			deliveryAddr.setAddrDetail(addrDetail);
			deliveryAddr.setDeliveryReq(deliveryReq);
			
			response.setContentType("application/json; charset=UTF-8");
			PrintWriter out = response.getWriter();
			
			if(service.updateDeliveryAddr(deliveryAddr)) {
				out.print(new Gson().toJson("success"));
			} else {
				out.print(new Gson().toJson("failure"));
			}
			out.flush();
			out.close();
		} else if ("insert".equals(dmlType)) {
			String addrName = request.getParameter("addrName");
			int addrZipcode = Integer.valueOf(request.getParameter("addrZipcode"));
			String addr = request.getParameter("addr");
			String addrDetail = request.getParameter("addrDetail");
			String deliveryReq = request.getParameter("deliveryReq");
			String memberCode = ((MemberDTO) request.getSession().getAttribute("loginMember")).getCode();
			
			DeliveryAddrDTO deliveryAddr = new DeliveryAddrDTO();
			deliveryAddr.setAddrName(addrName);
			deliveryAddr.setAddrZipcode(addrZipcode);
			deliveryAddr.setAddr(addr);
			deliveryAddr.setAddrDetail(addrDetail);
			deliveryAddr.setDeliveryReq(deliveryReq);
			deliveryAddr.setMemberCode(memberCode);
			
			response.setContentType("application/json; charset=UTF-8");
			PrintWriter out = response.getWriter();
			
			if(service.insertDeliveryAddr(deliveryAddr)) {
				out.print(new Gson().toJson("success"));
			} else {
				out.print(new Gson().toJson("failure"));
			}
			out.flush();
			out.close();
		}
	}
}
