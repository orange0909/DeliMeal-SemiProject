package com.delimeal.member.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class IssuedCouponInfoDTO implements Serializable{
	private static final long serialVersionUID = 1L;
	

//	
//	ISSUED_CPN_CODE	VARCHAR2(30 BYTE)
//	ISSUED_EXP_DATE	DATE
//	ISSUED_IS_VALIDATE	VARCHAR2(10 BYTE)
//	MEMBER_CODE	VARCHAR2(30 BYTE)
//	CPN_NUMBER	VARCHAR2(30 BYTE)
//	
//	CPN_NUMBER	VARCHAR2(30 BYTE)
//	CPN_NAME	VARCHAR2(200 BYTE)
//	CPN_DISCOUNT_RATE	NUMBER
//	CPN_EXP_DATE	DATE
//	CPN_DESCRIPTION	VARCHAR2(300 BYTE)
	
	private java.sql.Date issuedExpDate;
	private String issuedIsValidate;
	private String issuedCpnCode;
	private String memberCode;
	private String cpnNumber;
	private String cpnName;
	private double cpnDiscountRate;
	private java.sql.Date cpnExpDate;
	private String cpnDescription;
	
	public IssuedCouponInfoDTO() {
	}
	public IssuedCouponInfoDTO(String issuedCpnCode, Date issuedExpDate, String issuedIsValidate, String memberCode,
			String cpnNumber, String cpnName, double cpnDiscountRate, Date cpnExpDate, String cpnDescription) {
		this.issuedCpnCode = issuedCpnCode;
		this.issuedExpDate = issuedExpDate;
		this.issuedIsValidate = issuedIsValidate;
		this.memberCode = memberCode;
		this.cpnNumber = cpnNumber;
		this.cpnName = cpnName;
		this.cpnDiscountRate = cpnDiscountRate;
		this.cpnExpDate = cpnExpDate;
		this.cpnDescription = cpnDescription;
	}
	public String getIssuedCpnCode() {
		return issuedCpnCode;
	}
	public void setIssuedCpnCode(String issuedCpnCode) {
		this.issuedCpnCode = issuedCpnCode;
	}
	public java.sql.Date getIssuedExpDate() {
		return issuedExpDate;
	}
	public void setIssuedExpDate(java.sql.Date issuedExpDate) {
		this.issuedExpDate = issuedExpDate;
	}
	public String getIssuedIsValidate() {
		return issuedIsValidate;
	}
	public void setIssuedIsValidate(String issuedIsValidate) {
		this.issuedIsValidate = issuedIsValidate;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getCpnNumber() {
		return cpnNumber;
	}
	public void setCpnNumber(String cpnNumber) {
		this.cpnNumber = cpnNumber;
	}
	public String getCpnName() {
		return cpnName;
	}
	public void setCpnName(String cpnName) {
		this.cpnName = cpnName;
	}
	public double getCpnDiscountRate() {
		return cpnDiscountRate;
	}
	public void setCpnDiscountRate(double cpnDiscountRate) {
		this.cpnDiscountRate = cpnDiscountRate;
	}
	public java.sql.Date getCpnExpDate() {
		return cpnExpDate;
	}
	public void setCpnExpDate(java.sql.Date cpnExpDate) {
		this.cpnExpDate = cpnExpDate;
	}
	public String getCpnDescription() {
		return cpnDescription;
	}
	public void setCpnDescription(String cpnDescription) {
		this.cpnDescription = cpnDescription;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "IssuedCouponDTO [issuedCpnCode=" + issuedCpnCode + ", issuedExpDate=" + issuedExpDate
				+ ", issuedIsValidate=" + issuedIsValidate + ", memberCode=" + memberCode + ", cpnNumber=" + cpnNumber
				+ ", cpnName=" + cpnName + ", cpnDiscountRate=" + cpnDiscountRate + ", cpnExpDate=" + cpnExpDate
				+ ", cpnDescription=" + cpnDescription + "]";
	}
}
