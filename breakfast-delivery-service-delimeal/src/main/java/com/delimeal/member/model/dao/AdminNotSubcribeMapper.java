package com.delimeal.member.model.dao;

import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;

public interface AdminNotSubcribeMapper {
	
	MemberAndDefaultAddrDTO selectNotMemberByNameAndPhone(MemberAndDefaultAddrDTO requestNotMember);
}

