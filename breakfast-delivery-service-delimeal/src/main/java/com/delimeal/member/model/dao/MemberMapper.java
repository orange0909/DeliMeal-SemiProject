package com.delimeal.member.model.dao;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.member.model.dto.DeliveryAddrDTO;
import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.DeliveryDetailDTO;

public interface MemberMapper {

	String selectEncryptedPasswordById(String userId);

	MemberDTO selectLoginMember(MemberDTO requestMember);

	/* 구독자 매퍼 */
	List<MemberAndDefaultAddrDTO> selectAllSubscriber();

	/* 비구독자 매퍼 */
	List<MemberAndDefaultAddrDTO> selectNotSubscriber();

	/* 배송 관리 매퍼 */
	List<MemberAndDefaultAddrDTO> selectDeliverySearch(java.sql.Date deliveryDate);

	
	MemberAndDefaultAddrDTO selectMemberByNameAndPhone(MemberAndDefaultAddrDTO requestMember);
	
	MemberAndDefaultAddrDTO selectMemberByCode(MemberDTO requestMember);
	
	int updateMemberInfoByCodeForInputData(MemberDTO requestMember);

	int updateDelivery(MemberAndDefaultAddrDTO requestMember);

	int updateDeliverySearch(MemberAndDefaultAddrDTO requestDelivery);

	int registMember(MemberDTO requestMember);

	int idCheck(String id);

	MemberDTO findId(MemberDTO findIdMember);

	int findPwd(MemberDTO findPwdMember);

	List<IssuedCouponInfoDTO> selectIssuedInfoCoupon(String code);

	int deleteMember(String code);

	MemberDTO selectMemberById(String id);

	int modifyPwd(MemberDTO findMemberPwd);

	int updateMemberInfo(MemberDTO updateMember);

	List<MenuDTO> selectAllMenu();

	int updateFavoriteMenu(Map<String, String> favoriteMenu);

	int countCoupons(String code);

	String myFavoriteMenu(String code);

	IssuedCouponInfoDTO selectIssuedCouponInfoByCode(String issuedCouponCode);

	List<DeliveryAddrDTO> selectDeliveryAddrByMemberCode(String memberCode);

	int updateDeliveryAddr(DeliveryAddrDTO deliveryAddr);

	int deleteDeliveryAddrByCode(String addrCode);

	int insertDeliveryAddr(DeliveryAddrDTO deliveryAddr);

	int checkPhone(String phone);

	int updateDeliveryFinish(DeliveryDetailDTO detail);

}




