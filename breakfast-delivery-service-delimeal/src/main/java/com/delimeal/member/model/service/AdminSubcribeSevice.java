package com.delimeal.member.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.member.model.dao.MemberMapper;
import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;

public class AdminSubcribeSevice {
	
	public MemberAndDefaultAddrDTO Search(MemberAndDefaultAddrDTO requestSubcribe) {
		
		SqlSession sqlSession = getSqlSession();
		MemberAndDefaultAddrDTO searchSubcribe = null;
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		searchSubcribe = mapper.selectMemberByNameAndPhone(requestSubcribe);
		
		sqlSession.close();
		System.out.println("service : " + searchSubcribe);
		return searchSubcribe;
	}
}
