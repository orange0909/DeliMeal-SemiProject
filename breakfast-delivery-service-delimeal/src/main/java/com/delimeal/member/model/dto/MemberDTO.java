package com.delimeal.member.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class MemberDTO implements Serializable {
	private static final long serialVersionUID = 4554471341262682921L;
	
	private String code;
	private String id;
	private String password;
	private String name;
	private java.sql.Date bday;
	private String subscriptionYn;
	private String phone;
	private String gender;
	private String email;
	private String freeTrialYn;
	private int point;
	private String type;
	private String sleepYn;
	private java.sql.Date enrollDate;
	private String leaveYn;
	private java.sql.Date leaveDate;
	private java.sql.Date lastModifiedDate;
	private java.sql.Date lastLoggedInDate;
	private String favMenuCode;
	private String bdayMenuCode;
	
	public MemberDTO() { }

	public MemberDTO(String code, String id, String password, String name, Date bday, String subscriptionYn,
			String phone, String gender, String email, String freeTrialYn, int point, String type, String sleepYn,
			Date enrollDate, String leaveYn, Date leaveDate, Date lastModifiedDate, Date lastLoggedInDate,
			String favMenuCode, String bdayMenuCode) {
		this.code = code;
		this.id = id;
		this.password = password;
		this.name = name;
		this.bday = bday;
		this.subscriptionYn = subscriptionYn;
		this.phone = phone;
		this.gender = gender;
		this.email = email;
		this.freeTrialYn = freeTrialYn;
		this.point = point;
		this.type = type;
		this.sleepYn = sleepYn;
		this.enrollDate = enrollDate;
		this.leaveYn = leaveYn;
		this.leaveDate = leaveDate;
		this.lastModifiedDate = lastModifiedDate;
		this.lastLoggedInDate = lastLoggedInDate;
		this.favMenuCode = favMenuCode;
		this.bdayMenuCode = bdayMenuCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public java.sql.Date getBday() {
		return bday;
	}

	public void setBday(java.sql.Date bday) {
		this.bday = bday;
	}

	public String getSubscriptionYn() {
		return subscriptionYn;
	}

	public void setSubscriptionYn(String subscriptionYn) {
		this.subscriptionYn = subscriptionYn;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFreeTrialYn() {
		return freeTrialYn;
	}

	public void setFreeTrialYn(String freeTrialYn) {
		this.freeTrialYn = freeTrialYn;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSleepYn() {
		return sleepYn;
	}

	public void setSleepYn(String sleepYn) {
		this.sleepYn = sleepYn;
	}

	public java.sql.Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(java.sql.Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getLeaveYn() {
		return leaveYn;
	}

	public void setLeaveYn(String leaveYn) {
		this.leaveYn = leaveYn;
	}

	public java.sql.Date getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(java.sql.Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public java.sql.Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(java.sql.Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public java.sql.Date getLastLoggedInDate() {
		return lastLoggedInDate;
	}

	public void setLastLoggedInDate(java.sql.Date lastLoggedInDate) {
		this.lastLoggedInDate = lastLoggedInDate;
	}

	public String getFavMenuCode() {
		return favMenuCode;
	}

	public void setFavMenuCode(String favMenuCode) {
		this.favMenuCode = favMenuCode;
	}

	public String getBdayMenuCode() {
		return bdayMenuCode;
	}

	public void setBdayMenuCode(String bdayMenuCode) {
		this.bdayMenuCode = bdayMenuCode;
	}

	@Override
	public String toString() {
		return "MemberDTO [code=" + code + ", id=" + id + ", password=" + password + ", name=" + name + ", bday=" + bday
				+ ", subscriptionYn=" + subscriptionYn + ", phone=" + phone + ", gender=" + gender + ", email=" + email
				+ ", freeTrialYn=" + freeTrialYn + ", point=" + point + ", type=" + type + ", sleepYn=" + sleepYn
				+ ", enrollDate=" + enrollDate + ", leaveYn=" + leaveYn + ", leaveDate=" + leaveDate
				+ ", lastModifiedDate=" + lastModifiedDate + ", lastLoggedInDate=" + lastLoggedInDate + ", favMenuCode="
				+ favMenuCode + ", bdayMenuCode=" + bdayMenuCode + "]";
	}
}
