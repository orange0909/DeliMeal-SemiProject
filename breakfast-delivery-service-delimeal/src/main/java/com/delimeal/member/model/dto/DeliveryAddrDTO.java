package com.delimeal.member.model.dto;

import java.io.Serializable;

public class DeliveryAddrDTO implements Serializable {
	private static final long serialVersionUID = 3648662833558172483L;
	
	private String addrCode;	//	ADDR_CODE
	private String addrName;	//	ADDR_NAME
	private int addrZipcode;	//	ADDR_ZIPCODE
	private String addr;		//	ADDR
	private String addrDetail;	//	ADDR_DETAIL
	private String deliveryReq;	//	DELIVERY_REQ
	private String memberCode;	//	MEMBER_CODE
	
	public DeliveryAddrDTO() { }

	public DeliveryAddrDTO(String addrCode, String addrName, int addrZipcode, String addr, String addrDetail,
			String deliveryReq, String memberCode) {
		this.addrCode = addrCode;
		this.addrName = addrName;
		this.addrZipcode = addrZipcode;
		this.addr = addr;
		this.addrDetail = addrDetail;
		this.deliveryReq = deliveryReq;
		this.memberCode = memberCode;
	}

	public String getAddrCode() {
		return addrCode;
	}

	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}

	public String getAddrName() {
		return addrName;
	}

	public void setAddrName(String addrName) {
		this.addrName = addrName;
	}

	public int getAddrZipcode() {
		return addrZipcode;
	}

	public void setAddrZipcode(int addrZipcode) {
		this.addrZipcode = addrZipcode;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getAddrDetail() {
		return addrDetail;
	}

	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}

	public String getDeliveryReq() {
		return deliveryReq;
	}

	public void setDeliveryReq(String deliveryReq) {
		this.deliveryReq = deliveryReq;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	@Override
	public String toString() {
		return "DeliveryAddrDTO [addrCode=" + addrCode + ", addrName=" + addrName + ", addrZipcode=" + addrZipcode
				+ ", addr=" + addr + ", addrDetail=" + addrDetail + ", deliveryReq=" + deliveryReq + ", memberCode="
				+ memberCode + "]";
	}
}
