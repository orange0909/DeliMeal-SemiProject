package com.delimeal.member.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.member.model.dao.MemberMapper;
import com.delimeal.member.model.dto.DeliveryAddrDTO;
import com.delimeal.member.model.dto.IssuedCouponInfoDTO;
import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.subscription.model.dto.DeliveryDetailDTO;

public class MemberService {

	public MemberDTO login(MemberDTO requestMember) {
		SqlSession sqlSession = getSqlSession();
		MemberDTO loginMember = null;
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		// 아이디를 이용해 서버에 저장된 비밀번호 가져오기
		String encPwd = mapper.selectEncryptedPasswordById(requestMember.getId());
		
		// 매칭된 아이디가 없으면 null 반환
		if("".equals(encPwd) || encPwd == null) {
			return null;
		}
		
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		
		if(passwordEncoder.matches(requestMember.getPassword(), encPwd)) {
			// 비밀번호 일치 -> 로그인 성공
			loginMember = mapper.selectLoginMember(requestMember);
		} else {
			// 비밀번호 불일치 -> 로그인 실패
			return null;
		}
		
		sqlSession.close();
		
		return loginMember;
	}

	/* 구독회원 조회 */
	public List<MemberAndDefaultAddrDTO> selectAllSubscriber() {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		List<MemberAndDefaultAddrDTO> subscriberList = mapper.selectAllSubscriber();
		
		sqlSession.close();
		
		return subscriberList;
	}
	
	/* 비구독회원 조회 */
	public List<MemberAndDefaultAddrDTO> selectNotSubscriber() {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		List<MemberAndDefaultAddrDTO> NotsubscriberList = mapper.selectNotSubscriber();
		
		sqlSession.close();
		
		return NotsubscriberList;
	}
	
	/* 배송조회 */
	public List<MemberAndDefaultAddrDTO> selectDeliverySearch(java.sql.Date deliveryDate) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		List<MemberAndDefaultAddrDTO> deliverySearch = mapper.selectDeliverySearch(deliveryDate);
		
		sqlSession.close();
		
		return deliverySearch;
	}
	
	/* 배송 여부 수정 */
	/*public List<MemberAndDefaultAddrDTO> updateDeliverySearch() {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		List<MemberAndDefaultAddrDTO> updateDelivery = mapper.updateDeliverySearch(null);
		
		sqlSession.close();
		
		return updateDelivery;
	
	}*/
	
	/* 배송 여부 수정 */
	/*public MemberAndDefaultAddrDTO updateDeliverySearch(MemberAndDefaultAddrDTO requestDelivery) {
		
		SqlSession sqlSession = getSqlSession();
		MemberAndDefaultAddrDTO updateDelivery = null;

		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		mapper.updateDeliverySearch(requestDelivery);
		updateDelivery = mapper.updateDeliverySearch(requestDelivery);
		
		sqlSession.close();
		
		return updateDelivery;
	}*/

//	public List<MemberAndDefaultAddrDTO> updateDeliverySearch() {
//		SqlSession sqlSession = getSqlSession();
//		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
//		
//		List<MemberAndDefaultAddrDTO> updateDelivery = mapper.updateDeliverySearch();
//		
//		sqlSession.close();
//		
//		return updateDelivery;
//	}
	
	/* 회원가입용 메소드 */
	
	public int registMember(MemberDTO requestMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.registMember(requestMember);
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
	}


	/* 아이디 중복체크 */
	
	public int idCheck(String id) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		System.out.println("서비스");
		
		int result = mapper.idCheck(id);
		
		sqlSession.close();
		
		System.out.println("매퍼 갔다가 서비스로 돌아오는 길: " + result);
		return result;
		
	}


	public MemberDTO selectId(MemberDTO findIdMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
//		System.out.println("서비스에서 가는 길");
		
		MemberDTO result = mapper.findId(findIdMember);
		
		sqlSession.close();
//		System.out.println("서비스에서 오는 길: " + result);
		return result;
	}


	/* 비밀번호 찾기 */
	public int findPwd(MemberDTO findPwdMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
//		System.out.println("서비스에서 가는 길");
		
		int result = mapper.findPwd(findPwdMember);
		
		sqlSession.close();
		
//		System.out.println("서비스에서 오는 길: " + result);
		return result;
	}


	/* 마이페이지 쿠폰 조회 */
	
	public static List<IssuedCouponInfoDTO> selectIssuedCouponList(String code) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);

		List<IssuedCouponInfoDTO> issuedCouponList = mapper.selectIssuedInfoCoupon(code);
		
		sqlSession.close();
		
		return issuedCouponList;
	}


	/* 회원 탈퇴 메소드 */
	public int deleteMember(String code) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		System.out.println("회원탈퇴 서비스에서 가는 길");
		
		int result = mapper.deleteMember(code);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		System.out.println("서비스에서 오는 길: " + result);
		return result;
		
	}

	public MemberDTO selectMemberById(String id) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		System.out.println("서비스에서 가는 길");
		
		MemberDTO result = mapper.selectMemberById(id);
		
		sqlSession.close();
		
		System.out.println("서비스에서 오는 길: " + result);
		
		return result;
	}


	public int modifyPwd(MemberDTO findMemberPwd) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		System.out.println("서비스에서 가는 길");
		
		System.out.println("MemberMapper: ");
		int result = mapper.modifyPwd(findMemberPwd);
		System.out.println("MemberMapper: " + result);
		
		if(result > 0) {
			System.out.println("서비스에서 오는 길: " + result);
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		return result;
	}


	public int updateMember(MemberDTO updateMember) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.updateMemberInfo(updateMember);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


	public static List<MenuDTO> selectAllMenuList() {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);

		List<MenuDTO> selectAllMenuList = mapper.selectAllMenu();
		
		sqlSession.close();
		return selectAllMenuList;
	}

	/* 선호 메뉴 등록 */
	public int updateFavoriteMenu(Map<String, String> favoriteMenu) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);

		int result = mapper.updateFavoriteMenu(favoriteMenu);
		
		if(result > 0) {
			System.out.println("서비스로 오는 길: " + result);
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return result;
		
	}


	public int countCoupons(String code) {

		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		System.out.println("서비스에서 가는 길");
		
		int result = mapper.countCoupons(code);
		
		sqlSession.close();
		
		System.out.println("서비스에서 오는 길: " + result);
		
		return result;
		
	}


	public String myFavoriteMenu(String code) {
		
		SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
//		System.out.println("서비스에서 가는 길");
		
		String result = mapper.myFavoriteMenu(code);
		
		sqlSession.close();
//		System.out.println("서비스에서 오는 길: " + result);
		return result;
		
		
	}


	public IssuedCouponInfoDTO selectIssuedCouponInfoByCode(String issuedCouponCode) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		IssuedCouponInfoDTO couponInfo = mapper.selectIssuedCouponInfoByCode(issuedCouponCode);
		
		sqlSession.close();
		
		return couponInfo;
	}


	public List<DeliveryAddrDTO> selectDeliveryAddrByMemberCode(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		List<DeliveryAddrDTO> addrList = mapper.selectDeliveryAddrByMemberCode(memberCode);
		
		sqlSession.close();
		
		return addrList;
	}


	/* 배송지 정보 업데이트 */
	public boolean updateDeliveryAddr(DeliveryAddrDTO deliveryAddr) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.updateDeliveryAddr(deliveryAddr);
		
		if(result == 1) {
			sqlSession.commit();
			return true;
		} else {
			sqlSession.rollback();
			return false;
		}
	}

	/* 배송지 정보 삭제 */
	public boolean deleteDeliveryAddrByCode(String addrCode) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.deleteDeliveryAddrByCode(addrCode);
		
		if(result == 1) {
			sqlSession.commit();
			return true;
		} else {
			sqlSession.rollback();
			return false;
		}
	}

	/* 배송지 정보 추가 */
	public boolean insertDeliveryAddr(DeliveryAddrDTO deliveryAddr) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.insertDeliveryAddr(deliveryAddr);
		
		if(result == 1) {
			sqlSession.commit();
			return true;
		} else {
			sqlSession.rollback();
			return false;
		}
	}


	public int checkPhone(String phone) {
	SqlSession sqlSession = getSqlSession();
		
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		System.out.println("서비스");
		
		int result = mapper.checkPhone(phone);
		
		sqlSession.close();
		
		System.out.println("매퍼 갔다가 서비스로 돌아오는 길: " + result);
		return result;
	}

	public boolean updateDeliveryFinish(DeliveryDetailDTO detail) {
		SqlSession sqlSession = getSqlSession();
		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.updateDeliveryFinish(detail);
		
		boolean isSuccess = false;
		
		if(result > 0) {
			sqlSession.commit();
			isSuccess = true;
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return isSuccess;
	}
}



	











