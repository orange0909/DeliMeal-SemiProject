package com.delimeal.member.model.service;

import org.apache.ibatis.session.SqlSession;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import com.delimeal.member.model.dao.AdminNotSubcribeMapper;
import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;

public class AdminNotSubcribeService {
	 
	public MemberAndDefaultAddrDTO Search(MemberAndDefaultAddrDTO requestNotMember) {
		
		SqlSession sqlSession = getSqlSession();
		MemberAndDefaultAddrDTO searchNotMember = null;
		
		AdminNotSubcribeMapper mapper = sqlSession.getMapper(AdminNotSubcribeMapper.class);
		
		searchNotMember = mapper.selectNotMemberByNameAndPhone(requestNotMember);
		
		sqlSession.close();
		System.out.println("service : " + searchNotMember);
		return searchNotMember;
	}
}
