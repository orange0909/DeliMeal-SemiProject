package com.delimeal.member.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class MemberAndDefaultAddrDTO implements Serializable {

	private static final long serialVersionUID = -6378880774808265978L;
	
	private String code;
	private String id;
	private String password;
	private String name;
	private java.sql.Date bday;
	private String subscriptionYn;
	private String phone;
	private String gender;
	private String email;
	private String freeTrialYn;
	private int point;
	private String type;
	private String sleepYn;
	private java.sql.Date enrollDate;
	private String leaveYn;
	private java.sql.Date leaveDate;
	private java.sql.Date lastModifiedDate;
	private java.sql.Date lastLoggedInDate;
	private String favMenuCode;
	private String bdayMenuCode;
	private String addrCode;
	private int addrZipcode;
	private String addr;
	private String addrDetail;
	private String deliveryCode;
	private String deliveryZipCode;
	private String deliveryAddrDetail;
	private String deliveryAddr;
	private String deliveryAddrFinishYn;
	private String subsCode;
	private String payMethod;
	private String mealName;
	private java.sql.Date deliveryDate;
	
	public MemberAndDefaultAddrDTO() {
	}

	public MemberAndDefaultAddrDTO(String code, String id, String password, String name, Date bday,
			String subscriptionYn, String phone, String gender, String email, String freeTrialYn, int point,
			String type, String sleepYn, Date enrollDate, String leaveYn, Date leaveDate, Date lastModifiedDate,
			Date lastLoggedInDate, String favMenuCode, String bdayMenuCode, String addrCode, int addrZipcode,
			String addr, String addrDetail, String deliveryCode, String deliveryZipCode, String deliveryAddrDetail,
			String deliveryAddr, String deliveryAddrFinishYn, String subsCode, String payMethod, String mealName,
			Date deliveryDate) {
		this.code = code;
		this.id = id;
		this.password = password;
		this.name = name;
		this.bday = bday;
		this.subscriptionYn = subscriptionYn;
		this.phone = phone;
		this.gender = gender;
		this.email = email;
		this.freeTrialYn = freeTrialYn;
		this.point = point;
		this.type = type;
		this.sleepYn = sleepYn;
		this.enrollDate = enrollDate;
		this.leaveYn = leaveYn;
		this.leaveDate = leaveDate;
		this.lastModifiedDate = lastModifiedDate;
		this.lastLoggedInDate = lastLoggedInDate;
		this.favMenuCode = favMenuCode;
		this.bdayMenuCode = bdayMenuCode;
		this.addrCode = addrCode;
		this.addrZipcode = addrZipcode;
		this.addr = addr;
		this.addrDetail = addrDetail;
		this.deliveryCode = deliveryCode;
		this.deliveryZipCode = deliveryZipCode;
		this.deliveryAddrDetail = deliveryAddrDetail;
		this.deliveryAddr = deliveryAddr;
		this.deliveryAddrFinishYn = deliveryAddrFinishYn;
		this.subsCode = subsCode;
		this.payMethod = payMethod;
		this.mealName = mealName;
		this.deliveryDate = deliveryDate;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public java.sql.Date getBday() {
		return bday;
	}

	public void setBday(java.sql.Date bday) {
		this.bday = bday;
	}

	public String getSubscriptionYn() {
		return subscriptionYn;
	}

	public void setSubscriptionYn(String subscriptionYn) {
		this.subscriptionYn = subscriptionYn;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFreeTrialYn() {
		return freeTrialYn;
	}

	public void setFreeTrialYn(String freeTrialYn) {
		this.freeTrialYn = freeTrialYn;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getSleepYn() {
		return sleepYn;
	}

	public void setSleepYn(String sleepYn) {
		this.sleepYn = sleepYn;
	}

	public java.sql.Date getEnrollDate() {
		return enrollDate;
	}

	public void setEnrollDate(java.sql.Date enrollDate) {
		this.enrollDate = enrollDate;
	}

	public String getLeaveYn() {
		return leaveYn;
	}

	public void setLeaveYn(String leaveYn) {
		this.leaveYn = leaveYn;
	}

	public java.sql.Date getLeaveDate() {
		return leaveDate;
	}

	public void setLeaveDate(java.sql.Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	public java.sql.Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(java.sql.Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public java.sql.Date getLastLoggedInDate() {
		return lastLoggedInDate;
	}

	public void setLastLoggedInDate(java.sql.Date lastLoggedInDate) {
		this.lastLoggedInDate = lastLoggedInDate;
	}

	public String getFavMenuCode() {
		return favMenuCode;
	}

	public void setFavMenuCode(String favMenuCode) {
		this.favMenuCode = favMenuCode;
	}

	public String getBdayMenuCode() {
		return bdayMenuCode;
	}

	public void setBdayMenuCode(String bdayMenuCode) {
		this.bdayMenuCode = bdayMenuCode;
	}

	public String getAddrCode() {
		return addrCode;
	}

	public void setAddrCode(String addrCode) {
		this.addrCode = addrCode;
	}

	public int getAddrZipcode() {
		return addrZipcode;
	}

	public void setAddrZipcode(int addrZipcode) {
		this.addrZipcode = addrZipcode;
	}

	public String getAddr() {
		return addr;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public String getAddrDetail() {
		return addrDetail;
	}

	public void setAddrDetail(String addrDetail) {
		this.addrDetail = addrDetail;
	}

	public String getDeliveryCode() {
		return deliveryCode;
	}

	public void setDeliveryCode(String deliveryCode) {
		this.deliveryCode = deliveryCode;
	}

	public String getDeliveryZipCode() {
		return deliveryZipCode;
	}

	public void setDeliveryZipCode(String deliveryZipCode) {
		this.deliveryZipCode = deliveryZipCode;
	}

	public String getDeliveryAddrDetail() {
		return deliveryAddrDetail;
	}

	public void setDeliveryAddrDetail(String deliveryAddrDetail) {
		this.deliveryAddrDetail = deliveryAddrDetail;
	}

	public String getDeliveryAddr() {
		return deliveryAddr;
	}

	public void setDeliveryAddr(String deliveryAddr) {
		this.deliveryAddr = deliveryAddr;
	}

	public String getDeliveryAddrFinishYn() {
		return deliveryAddrFinishYn;
	}

	public void setDeliveryAddrFinishYn(String deliveryAddrFinishYn) {
		this.deliveryAddrFinishYn = deliveryAddrFinishYn;
	}

	public String getSubsCode() {
		return subsCode;
	}

	public void setSubsCode(String subsCode) {
		this.subsCode = subsCode;
	}

	public String getPayMethod() {
		return payMethod;
	}

	public void setPayMethod(String payMethod) {
		this.payMethod = payMethod;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public java.sql.Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(java.sql.Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Override
	public String toString() {
		return "MemberAndDefaultAddrDTO [code=" + code + ", id=" + id + ", password=" + password + ", name=" + name
				+ ", bday=" + bday + ", subscriptionYn=" + subscriptionYn + ", phone=" + phone + ", gender=" + gender
				+ ", email=" + email + ", freeTrialYn=" + freeTrialYn + ", point=" + point + ", type=" + type
				+ ", sleepYn=" + sleepYn + ", enrollDate=" + enrollDate + ", leaveYn=" + leaveYn + ", leaveDate="
				+ leaveDate + ", lastModifiedDate=" + lastModifiedDate + ", lastLoggedInDate=" + lastLoggedInDate
				+ ", favMenuCode=" + favMenuCode + ", bdayMenuCode=" + bdayMenuCode + ", addrCode=" + addrCode
				+ ", addrZipcode=" + addrZipcode + ", addr=" + addr + ", addrDetail=" + addrDetail + ", deliveryCode="
				+ deliveryCode + ", deliveryZipCode=" + deliveryZipCode + ", deliveryAddrDetail=" + deliveryAddrDetail
				+ ", deliveryAddr=" + deliveryAddr + ", deliveryAddrFinishYn=" + deliveryAddrFinishYn + ", subsCode="
				+ subsCode + ", payMethod=" + payMethod + ", mealName=" + mealName + ", deliveryDate=" + deliveryDate
				+ "]";
	}
	
	
	
	}	