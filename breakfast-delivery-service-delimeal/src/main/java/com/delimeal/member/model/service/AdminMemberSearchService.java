package com.delimeal.member.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.member.model.dao.MemberMapper;
import com.delimeal.member.model.dto.MemberAndDefaultAddrDTO;
import com.delimeal.member.model.dto.MemberDTO;

public class AdminMemberSearchService {
	
	public MemberAndDefaultAddrDTO searchByNameAndPhone(MemberAndDefaultAddrDTO requestMember) {
		
		SqlSession sqlSession = getSqlSession();
		MemberAndDefaultAddrDTO searchMember = null;

		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);

		searchMember = mapper.selectMemberByNameAndPhone(requestMember);
		
		sqlSession.close();
		System.out.println("service : " + searchMember);
		return searchMember;
	}
	
	// 멤버 정보 수정 후 수정된 멤버 정보를 받아온다
	// 회원 번호를 이용하여 테이블 선택
	public MemberAndDefaultAddrDTO updateMemberInfo(MemberDTO requestMember) {
		
		SqlSession sqlSession = getSqlSession();
		MemberAndDefaultAddrDTO searchMember = null;

		MemberMapper mapper = sqlSession.getMapper(MemberMapper.class);
		
		int result = mapper.updateMemberInfoByCodeForInputData(requestMember);

		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}

		searchMember = mapper.selectMemberByCode(requestMember);
		
		sqlSession.close();

		return searchMember;
	}

}
