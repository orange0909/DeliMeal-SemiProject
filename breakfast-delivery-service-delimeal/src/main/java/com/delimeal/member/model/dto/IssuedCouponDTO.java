package com.delimeal.member.model.dto;

import java.sql.Date;

public class IssuedCouponDTO {

	
//	ISSUED_CPN_CODE	VARCHAR2(30 BYTE)
//	ISSUED_EXP_DATE	DATE
//	ISSUED_IS_VALIDATE	VARCHAR2(10 BYTE)
//	MEMBER_CODE	VARCHAR2(30 BYTE)
//	CPN_NUMBER	VARCHAR2(30 BYTE)
	
	
	private String issuedCpnCode;
	private java.sql.Date issuedExpDate;
	private String issuedIsValidate;
	private String memberCode;
	private String cpnNumber;
	
	public IssuedCouponDTO() {
	}

	public IssuedCouponDTO(String issuedCpnCode, Date issuedExpDate, String issuedIsValidate, String memberCode,
			String cpnNumber) {
		this.issuedCpnCode = issuedCpnCode;
		this.issuedExpDate = issuedExpDate;
		this.issuedIsValidate = issuedIsValidate;
		this.memberCode = memberCode;
		this.cpnNumber = cpnNumber;
	}

	public String getIssuedCpnCode() {
		return issuedCpnCode;
	}

	public void setIssuedCpnCode(String issuedCpnCode) {
		this.issuedCpnCode = issuedCpnCode;
	}

	public java.sql.Date getIssuedExpDate() {
		return issuedExpDate;
	}

	public void setIssuedExpDate(java.sql.Date issuedExpDate) {
		this.issuedExpDate = issuedExpDate;
	}

	public String getIssuedIsValidate() {
		return issuedIsValidate;
	}

	public void setIssuedIsValidate(String issuedIsValidate) {
		this.issuedIsValidate = issuedIsValidate;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getCpnNumber() {
		return cpnNumber;
	}

	public void setCpnNumber(String cpnNumber) {
		this.cpnNumber = cpnNumber;
	}

	@Override
	public String toString() {
		return "IssuedCouponDTO [issuedCpnCode=" + issuedCpnCode + ", issuedExpDate=" + issuedExpDate
				+ ", issuedIsValidate=" + issuedIsValidate + ", memberCode=" + memberCode + ", cpnNumber=" + cpnNumber
				+ "]";
	}
}
