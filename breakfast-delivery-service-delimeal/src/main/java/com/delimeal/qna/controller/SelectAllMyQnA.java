package com.delimeal.qna.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.paging.Pagingnation;
import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTOAndMemberDTO;
import com.delimeal.qna.model.service.QnAService;

@WebServlet("/qna/selectMyQnA")
public class SelectAllMyQnA extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("MyQnaList Servlet");
	
		String path="";
		
		QnAService qnaService = new QnAService();
		/*로그인 한 사람의 코드*/
		String loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember")).getCode();
				
		/* 보려고 하는 페이지 */
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/*검색조건*/
		String searchCondition = request.getParameter("searchCondition");
		/*검색어*/
		String searchValue = request.getParameter("searchValue");
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		searchMap.put("loginMember", loginMember);
		
		/*전체 게시글 수(문제의 그것)*/
		int totalCount = qnaService.selectMyQnaTotalCount(searchMap);

		/*한 페이지에 보여줄 게시글 수*/
		int limit = 5;
		/*한번에 보여질 페이징 버튼(1~5) 개수*/
		int buttonAmount = 5;
		/*페이징 처리에 관한 정보를 담고있는 DTO개념*/
		SelectChriteria selectChriteria = null;
		
		/*검색 조건과 검색값의 유무에 따라 페이지를 보여주는게 다르다. 같은 메소드이나 넘겨지는 값이 다를때*/
		if(searchCondition != null && !"".equals(searchValue)) {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		}else {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		/*매퍼에 담아 보낼것임.*/
		Map<String, Object> myQnA = new HashMap<>();
		myQnA.put("selectChriteria", selectChriteria);
		myQnA.put("loginMember", loginMember);
		
		List<QnADTOAndMemberDTO> qnaList = qnaService.selectMyAllQna(myQnA);
			
			/* 조회할 내용이 있다면 */
			if(qnaList != null) {
				System.out.println("나오니?");
				path = "/WEB-INF/views/qna/memberSelectMyQnAList.jsp";

				for(QnADTOAndMemberDTO qna : qnaList) {
					/*글번호 원하는 모양으로 나타내기*/
					String qnaNum = qna.getQnaNumber();
					/*!!*/
					qna.setQnaNumber(Integer.valueOf(qnaNum.substring(qnaNum.lastIndexOf("_")+1))+"");
					
					System.out.println(qna);
					/*답변 여부 나타내기*/
					if("Y".equals(qna.getQnaAnswerYN())) {
						qna.setQnaAnswerYN("답변 완료");
					}else {
						qna.setQnaAnswerYN("답변 대기중");
					}
				}
				/*조회된 문의페이지에 전달할 내용*/
				request.setAttribute("qnaList",  qnaList);
				/* 페이징 처리에 사용할 정보들 */
				request.setAttribute("selectCriteria", selectChriteria);
			} else {
				/* 조회할 내용이 없다면 */
				path= "/WEB-INF/views/qna/noSelectQnAList";
			}
		
		request.getSession().setAttribute("listType", "my");
		request.getRequestDispatcher(path).forward(request, response);
		
}
}