package com.delimeal.qna.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.service.QnAService;

@WebServlet("/qna/UpdateAnswer")
public class UpdateAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String qnaNum = request.getParameter("qnaNum");
		QnAService service = new QnAService();
		QnADTO qna = service.selectQnaDetail(qnaNum);

		System.out.println(qna);
		System.out.println(qna);
		System.out.println(qna);
		
		request.setAttribute("qnaDetail", qna);
		request.setAttribute("qnaNum", qnaNum);

		if(qna.getQnaFileList() != null && ! qna.getQnaFileList().isEmpty()) {
			String fileName = qna.getQnaFileList().get(0).getFileName();
			request.setAttribute("fileName", fileName);
		}
		
		String path ="/WEB-INF/views/qna/adminUpdateAnswer.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	
	}

}
