package com.delimeal.qna.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.service.QnAService;


@WebServlet("/qna/detail")
public class SelectQnaDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* 접근 확인 */
		String memberType = "";
		MemberDTO loginMember = (MemberDTO) request.getSession().getAttribute("loginMember");
		
		
		if(loginMember != null) {
			memberType = loginMember.getType();
		} else {
			memberType = "NoMember";
		}
		/*----------------------------------------------------------------------------*/
		
		QnAService qnaService = new QnAService();
		String qnaNum = request.getParameter("qnaNum");
		System.out.println("상세조회-----------------------------------------------------");
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		System.out.println(qnaNum);
		request.getSession().setAttribute("qnaNum", qnaNum);
		
		String listType = request.getParameter("listType");
		
		QnADTO qnaList = qnaService.selectQnaDetail(qnaNum);
		
		
		if(!qnaList.getQnaFileList().isEmpty()) {
			String fileName = qnaList.getQnaFileList().get(0).getFileName();
			request.setAttribute("fileName",  fileName);
		}
		
		/*글번호 원하는 모양으로 나타내기*/
		qnaList.setQnaNumber(qnaNum);
		/*답변 여부 나타내기*/
		if("Y".equals(qnaList.getQnaAnswerYN())) {
			qnaList.setQnaAnswerYN("답변 완료");
		}else {
			qnaList.setQnaAnswerYN("답변 대기중");
			qnaList.setQnaAnswer("아직 등록된 답변이 없습니다.");
		}
		
		
		String path="";
		if((memberType.equals("U") || memberType.equals("NoMember"))) {
			/* 이용자 */
			if(qnaList != null) {
				/*조회 내용 있*/
				path = "/WEB-INF/views/qna/selectQnaDetail.jsp";
				
				request.setAttribute("qnaDetail",  qnaList);
				request.setAttribute("listType", listType);
			}
			
		}else {
			/*관리자*/
			if(qnaList != null) {
				/*조회 내용 있*/
				path = "/WEB-INF/views/qna/adminSelectQnaDetail.jsp";
				request.setAttribute("qnaDetail",  qnaList);
			} 
		}

		request.getRequestDispatcher(path).forward(request, response);
	}

}
