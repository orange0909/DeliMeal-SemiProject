package com.delimeal.qna.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.paging.Pagingnation;
import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTOAndMemberDTO;
import com.delimeal.qna.model.service.QnAService;

@WebServlet("/qna/list")
public class SelectAllQnaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String memberType = "";
		MemberDTO loginMember = (MemberDTO) request.getSession().getAttribute("loginMember");
		
		
		if(loginMember != null) {
			memberType = loginMember.getType();
		} else {
			memberType = "NoMember";
		}
		
		System.out.println(memberType);
		System.out.println(memberType);
		System.out.println(memberType);
		System.out.println(memberType);
		System.out.println(memberType);
		System.out.println(memberType);
		
		
		QnAService qnaService = new QnAService();
		
		/* 보려고 하는 페이지 */
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/*검색조건*/
		String searchCondition = request.getParameter("searchCondition");
		/*검색어*/
		String searchValue = request.getParameter("searchValue");
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		/*전체 게시글 수(문제의 그것)*/
		int totalCount = qnaService.selectTotalCount(searchMap);
		/*한 페이지에 보여줄 게시글 수*/
		int limit = 5;
		/*한번에 보여질 페이징 버튼(1~5) 개수*/
		int buttonAmount = 5;
		
		/*페이징 처리에 관한 정보를 담고있는 DTO개념*/
		SelectChriteria selectChriteria = null;
		
		/*검색 조건과 검색값의 유무에 따라 페이지를 보여주는게 다르다. 같은 메소드이나 넘겨지는 값이 다를때*/
		if(searchCondition != null && !"".equals(searchValue)) {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		}else {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		Map<String, Object> AllQnA = new HashMap<>();
		AllQnA.put("selectChriteria", selectChriteria);

		
		List<QnADTOAndMemberDTO> qnaList = qnaService.selectAllQna(AllQnA);

		for(QnADTOAndMemberDTO qna : qnaList) {
			/*원래 글번호 = qnaNumber글번호 원하는 모양으로 나타내기*/
			String qnaNumber = qna.getQnaNumber();
			/*!!*/
			qna.setQnaNumber(Integer.valueOf(qnaNumber.substring(qnaNumber.lastIndexOf("_")+1))+"");
			
			/*답변 여부 나타내기*/
			if("Y".equals(qna.getQnaAnswerYN())) {
				qna.setQnaAnswerYN("답변 완료");
			}else {
				qna.setQnaAnswerYN("답변 대기중");
			}
		}
		
		String path="";
		if((memberType.equals("U") || memberType.equals("NoMember"))) {
			/* 이용자 */
			if(qnaList != null) {
				/*조회 내용 있*/
				path = "/WEB-INF/views/qna/selectQnAList.jsp";
				/*조회된 문의*/
				request.setAttribute("qnaList",  qnaList);
				/* 페이징 처리에 사용하면 좋을. */
				request.setAttribute("selectCriteria", selectChriteria);
			} else {
				/*조회 내용 없 */
				path= "/WEB-INF/views/qna/noSelectQnAList";
			}
			
		}else {
			/*관리자*/
			if(qnaList != null) {
				/*조회 내용 있*/
				path = "/WEB-INF/views/qna/adminSelectQnAList.jsp";
				/*조회된 문의*/
				request.setAttribute("qnaList",  qnaList);
				/* 페이징 처리에 사용하면 좋을. */
				request.setAttribute("selectCriteria", selectChriteria);
			} else {
				/*조회 내용 없 */
				path= "/WEB-INF/views/qna/adimNoSelectQnAList";
			}
			
		}
		request.getSession().setAttribute("listType", "all");
		request.getRequestDispatcher(path).forward(request, response);
		
	}

}
