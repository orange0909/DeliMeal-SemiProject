package com.delimeal.qna.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.filecontrol.FileControl;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.service.QnAService;

@WebServlet("/qna/delete")
public class DeleteQnAServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("삭제서블릿 어서오고~");
		String path="";
		String listType = request.getSession().getAttribute("listType").toString();
		String qnaNumPara = request.getParameter("qnaNum");
		QnAService qnaService = new QnAService();
		MemberDTO loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember"));
		QnADTO qnaList = qnaService.selectQnaDetail(qnaNumPara);
		
		if( loginMember != null) {
			/* 로그인이 되었다. */
			/*여기서 본인 글인지 확인해야한다.*/
			if(qnaList.getMemberCode().equals(loginMember.getCode())) {
				
				String qnaNum = qnaList.getQnaNumber();

				Map<String, String> qnaNumMap = new HashMap();
		
				qnaNumMap.put("qnaNum", qnaNum);
		
				/*사진 파일 없는 게시글 삭제 결과*/
				int deleteResult = 0;
				/*사진 파일 있는 게시글 삭제 결과*/
				int filedeleteResult = 0;
				/*최종 결과*/
				int result = 0;
		
				if(!qnaList.getQnaFileList().isEmpty()) {
					/*사진이 있다면 file테이블도 삭제 */
					filedeleteResult = qnaService.deleteWithFileQnA(qnaNumMap);

					String fileUploadDirectory = "C:\\projectSemiHimedia\\breakfast-delivery-service-delimeal\\web\\resources/cs/";
					String oldFileName = qnaList.getQnaFileList().get(0).getFileName();
			
					FileControl.deleteFile(fileUploadDirectory, oldFileName);

					if(filedeleteResult > 0) {
						result = 1;
					}
				} else {
					/*사진 파일이 없다면 그냥 테이블 삭제*/
					deleteResult = qnaService.deleteQnA(qnaNumMap);
					if(deleteResult > 0) {
						result = 1;
					}
				}
		
					if(result > 0) {
						path ="/WEB-INF/views/common/success.jsp";
						request.setAttribute("qnaNum", qnaNum);
						request.setAttribute("successCode", "deleteQnAmember");
						request.setAttribute("listType", listType);
					}
			}else {
				/* 본인 글이 아닐때 */
				path ="/WEB-INF/views/common/failed.jsp";
				request.setAttribute("failedCode", "notYourPost");
			}
		}else {
			/*로그인이 되지 않았을때 */
			path ="/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "haveToLoginWhenDelete");
			request.setAttribute("qnaNum", qnaNumPara);
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}
}
