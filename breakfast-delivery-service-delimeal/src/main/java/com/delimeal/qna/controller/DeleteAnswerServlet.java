package com.delimeal.qna.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.service.QnAService;

@WebServlet("/qna/deleteAnswer")
public class DeleteAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	String qnaNum = request.getParameter("qnaNum");
	
	QnAService service = new QnAService();
	QnADTO qna = service.selectQnaDetail(qnaNum);
	String qnaNumber = qna.getQnaNumber();
	
	System.out.println("지우러 출발한다~!~!~!");

	String path = "";
	
		/* 답변 작성자가 같다면 */
		int deleteResult = service.deleteAnswer(qnaNumber);

		if(deleteResult > 0) {
			path = "/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "deleteQnAanswer");
			request.setAttribute("qnaNum", qnaNum);
		}
		
	request.getSession().setAttribute("listType", "all");
	request.getRequestDispatcher(path).forward(request, response);
	
	}

}

