package com.delimeal.qna.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.service.QnAService;


@WebServlet("/qna/insertAnswer")
public class InsertAnswerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String qnaNum = request.getParameter("qnaNum");
		QnAService service = new QnAService();
		QnADTO qna = service.selectQnaDetail(qnaNum);
		
		System.out.println(qna);
		System.out.println(qna);
		System.out.println(qna);
		
		request.setAttribute("qnaDetail", qna);
		request.setAttribute("qnaNum", qnaNum);
		
		if(qna.getQnaFileList() != null && ! qna.getQnaFileList().isEmpty()) {
			String fileName = qna.getQnaFileList().get(0).getFileName();
			request.setAttribute("fileName", fileName);
		}
		
		String path ="/WEB-INF/views/qna/adminInsertAnswer.jsp";
		
		request.getRequestDispatcher(path).forward(request, response);
	
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		
		String answerContent = request.getParameter("answer");
		String qnaNumber = request.getParameter("qnaNumber");
		String loginMember = ((MemberDTO)request.getSession().getAttribute("loginMember")).getCode();
		String qnaNum = request.getParameter("qnaNum");
		
		QnAService service = new QnAService();
		Map<String, String> answer = new HashMap<>();
		answer.put("qnaNumber", qnaNumber);
		answer.put("answerContent", answerContent);
		answer.put("loginMember", loginMember);
		
		int insertAnswer = service.insertAnswer(answer);

		String path ="";

		if(insertAnswer > 0) {
			/* 인서트 되었을때 */
			path ="/WEB-INF/views/common/success.jsp";
			request.setAttribute("successCode", "updateQnAmember");
			request.setAttribute("qnaNum", qnaNum);

		}else {
			/* 인서트 안되었을때 */
		}
		
		request.getRequestDispatcher(path).forward(request, response);
	}

}
