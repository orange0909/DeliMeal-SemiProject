package com.delimeal.qna.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.delimeal.common.filecontrol.FileControl;
import com.delimeal.member.model.dto.MemberDTO;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.dto.QnAFileDTO;
import com.delimeal.qna.model.service.QnAService;

@WebServlet("/qna/update")
public class UpdateQnAServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		
		QnAService qnaService = new QnAService();
		String qnaNum = request.getParameter("qnaNum");
		String listType = request.getParameter("listType");
		request.setAttribute("listType", listType);
		QnADTO qna = qnaService.selectQnaDetail(qnaNum);
		MemberDTO loginMember = (MemberDTO)request.getSession().getAttribute("loginMember");
		String path ="";
		qna.setQnaNumber(Integer.valueOf(qnaNum.substring(qnaNum.lastIndexOf("_")+1))+"");
		if(qna.getMemberCode().equals(loginMember.getCode())) {
			/*작성자가 동일하다면 실행 될것이다.*/
			
			/* 사진 파일이 있다면 사진파일의 이름을 스코프로 전달 */
			if(!qna.getQnaFileList().isEmpty()) {
				String fileName = qna.getQnaFileList().get(0).getFileName();
				/*사진 파일 전달할것임*/
				request.setAttribute("fileName", fileName);
			} 
			
			path = "/WEB-INF/views/qna/memberUpdateQnA.jsp";
			request.setAttribute("qna", qna);
			
		} else {
			/* 작성자가 동일하지 않다면 */
			path ="/WEB-INF/views/common/failed.jsp";
			request.setAttribute("failedCode", "notYourPost");
		}
		request.getRequestDispatcher(path).forward(request, response);
	}

	
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		System.out.println("Update Servlet 도착");
		/*요청 헤더 content-type이 multipart~ 인경우*/
		if(ServletFileUpload.isMultipartContent(request)) {
			
			String rootLocation = getServletContext().getRealPath("/") ;
			int maxFileSize = 1024 * 1024 * 10;
			String encodingType = "UTF-8";
			
			System.out.println("rootLocation" + rootLocation);
			System.out.println("maxFileSize" + maxFileSize);
			System.out.println("encodingType" + maxFileSize);
			
			String fileUploadDirectory = rootLocation + "resources/cs/";
			
			File directory = new File(fileUploadDirectory);
			
			/* 만일 저장경로가 존재하지 않는다면 만들거다. */
			if(!directory.exists()) {
				System.out.println("폴더 생성 : " + directory.mkdirs());
			}
			
			/* 사용자 요청 - parameter , file에 대한 모든 정보 fileList*/
			Map<String, String> parameter = new HashMap<>();
			List<Map<String, String>> fileList = new ArrayList<>();
			
			/*파일 업로드 할때 최대 크기나 임시 저장할 폴더의 경로등을 포함하기 위한 인스턴스*/
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			/* 임시 경로 */
	        fileItemFactory.setRepository(new File(fileUploadDirectory));
	        /* 최대 크기 */
	        fileItemFactory.setSizeThreshold(maxFileSize);
	        
	        /* 데이터 하나하나를 fileItem 타입의 인스턴스들로 변환 할 것이다. (List로 묶어 반환)*/
	        ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
	        
	        /*인풋 태그에 담긴 값들 모두 파싱*/
	        try {
				List<FileItem> fileItems = fileUpload.parseRequest(request);
				
				/*jsp에서 사용자가 입력한 값들이 나와있다.*/
				for(FileItem item : fileItems) {
				}
				
				for(int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);
					
					if(!item.isFormField()) {
						
						if(item.getSize() > 0) {
							
							String filedName = item.getFieldName();
							String originFileName = item.getName();
							
							System.out.println("인풋 태그 이름: " + filedName);
							System.out.println("올린 파일 이름(원래의 이름)   " + originFileName );
							
							/*저장 이름 바꾸는 중*/
							int dot = originFileName.lastIndexOf(".");
							String ext = originFileName.substring(dot);
							
							String randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;
							
							/*업로드시 올리는 경로/파일*/
							File storeFile = new File(fileUploadDirectory + "/" + randomFileName);
							
							
							/*바뀐 이름으로 저장*/
							item.write(storeFile);
							
							/*컬렉션으로 묶어서 file정보만을 담는다.*/
							Map<String, String> fileMap = new HashMap<>();
							fileMap.put("filedName", filedName);
							fileMap.put("originFileName", originFileName);
							fileMap.put("savedFileName", randomFileName);
							fileMap.put("savePath", fileUploadDirectory);
							
							
							fileMap.put("thumbnailPath", "/resources/upload/thumbnail/thumbnail_" + randomFileName);
							
							fileList.add(fileMap);

						}//
					}else {
						
						/*파일이 아닌 데이터들을 Map에(사용자 요청에) 담는다.*/
						parameter.put(item.getFieldName(), new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));
					
					}
				}//
				
				QnADTO qnaDTO = new QnADTO();
				qnaDTO.setQnaName(parameter.get("title"));
				qnaDTO.setQnaContent(parameter.get("body"));
				qnaDTO.setQnaNumber(parameter.get("qnaNum"));
				
				qnaDTO.setQnaFileList(new ArrayList<QnAFileDTO>());
				
				List<QnAFileDTO> list = qnaDTO.getQnaFileList();
				
				/* 파일 요청이 들어오는걸 알수있음 */
				for(int i = 0; i< fileList.size(); i++) {
					Map<String, String> file = fileList.get(i);
					
					QnAFileDTO qnaFile = new QnAFileDTO();
					qnaFile.setFileName(file.get("savedFileName"));
					qnaFile.setQnaNum(qnaDTO.getQnaNumber());
					list.add(qnaFile);
				}
								
				String oldFileName = parameter.get("oldFileName");
				String deleteYn = parameter.get("deleteYN");
				
				/*새로운 사진 업로드 요청이 들어온다면(if 1) -> deleteYn이 true라면(if 1-1) 기존사진 경로에서 삭제 후 업로드(파일,게시글 업데이트), 없다면(else1-2) 그대로 사진 파일 추가해 올리기(파일 인서트 -> 게시글 업데이트)*/
				/*새로운 사진 업로드 요청이 안들어온다면(else 2) -> deleteYn true라면(if 2-1) 기존 사진 경로에서 삭제, 테이블 삭제 후 게시글 업데이트, 없다면(2-2) 게시글 업데이트 */

				/* 사진 요청이 들어왔다면 (if)
				 *  기존 사진이 있다면(if)
				 *  엑스박스 누른다면(if)
				 *  안누른다(else)
				 *  => 업데이트만 되면 됨.
				 *  기존 사진이 없다
				 *  => 파일테이블에 인서트 하기
				 *  => 게시글 업데이트하기
				 *  
				 * 사진 요청이 안들어왔을때
				 *  기존 사진이 있다
				 *  => 엑스박스를 누른다
				 *    => 파일 지운다
				 *    => 게시글 업데이트 한다.
				 *  => 엑스박스를 안누른다
				 *  	=>게시글을 업데이트 한다.
				 *  기존 사진이 없다 
				 *  => 게시글 업데이트
				 *  
				 */
				
				int result = 0;
				QnAService qnaService = new QnAService();
				/*사진 업로드 요청 확인*/
				if(fileList.size() != 0) {//1
					if(oldFileName == "" || oldFileName == null) {
						/*기존 사진 없을때*/
						result = qnaService.updateQandInsertF(qnaDTO);  
					} else {
						/*기존 사진 있을때*/
						/*경로에서 저장된 사진(oldFileName) 삭제*/ 
						FileControl.deleteFile(fileUploadDirectory, oldFileName);
						/* 업데이트 파일, 게시글*/
						 result = qnaService.updateFileAndQna(qnaDTO);
					}
				}else {//2
					if("true".equals(deleteYn)) {//2-1
						/*기존 경로에 저장된 사진 파일 삭제*/
						FileControl.deleteFile(fileUploadDirectory, oldFileName);
						/* 파일 데이터 삭제*/
						result = qnaService.deleteQnaFile(parameter);
					}
					//2-2
					result = qnaService.updateQnA(qnaDTO);
				}
				
				/*화면이동*/
				String path ="";
				if(result > 0) {
					path ="/WEB-INF/views/common/success.jsp";
					request.setAttribute("successCode", "updateQnAmember");
					request.setAttribute("qnaNum", qnaDTO.getQnaNumber());
					
				} else {
					System.out.println("실패");
				}
				
				request.getRequestDispatcher(path).forward(request, response);
				
				
/*try*/		} catch (Exception e) {
				e.printStackTrace();
			}// catch
			
		}
		
		
	}// dopost end

}// servlet end
