package com.delimeal.qna.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.dto.QnADTOAndMemberDTO;
import com.delimeal.qna.model.dto.QnAFileDTO;

public interface QnAMapper {

	/*검색 조건 들고  매핑 기다리겠음.*/
	public int selectTotalCount(Map<String, String> searchMap);

	public List<QnADTOAndMemberDTO> selectAllQna(Map<String, Object> myQnA);

	public QnADTO selectQnaDetail(String qnaNum);

	public int insertQnA(QnADTO qnaDTO);

	public int insertFile(QnAFileDTO file);

	public int deleteQnA(Map<String, String> qnaNumMap);

	public int fileDeleteQnA(Map<String, String> qnaNumMap);

	public int updateQnAFile(QnAFileDTO qnAFileDTO);

	public int updateQnA(QnADTO qnaDTO);

	public int selectFileTableTotalCount(QnADTO qnaDTO);

	public int insertWhenUpdateFile(QnAFileDTO qnAFileDTO);

	public int fileDeleteQnAWhenUpdate(Map<String, String> parameter);

	public List<QnADTOAndMemberDTO> selectAllMyQna(Map<String, Object> myQnA);

	public int selectMyQnaTotalCount(Map<String, String> searchMap);

	public int selectWaithingTotalCount(Map<String, String> searchMap);

	public List<QnADTOAndMemberDTO> selectWaithingQnA(Map<String, Object> waithingQnA);

	public int insertAnswer(Map<String, String> answer);

	public int deleteAnswer(String qnaNumber);

	

	
	

}
