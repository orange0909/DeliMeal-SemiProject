package com.delimeal.qna.model.dto;

import java.sql.Date;

import com.delimeal.member.model.dto.MemberDTO;

public class QnADTOAndMemberDTO {

	private String qnaNumber;
	private String qnaName;
	private String qnaContent;
	private java.sql.Date qnaDate;
	private String qnaAnswerYN;
	private String qnaAnswer;
	private String qnaAnswerDate;
	private String memberCode;
	private String managerCode;
	private MemberDTO memberDTO;
	public QnADTOAndMemberDTO() {
	}
	public QnADTOAndMemberDTO(String qnaNumber, String qnaName, String qnaContent, Date qnaDate, String qnaAnswerYN,
			String qnaAnswer, String qnaAnswerDate, String memberCode, String managerCode, MemberDTO memberDTO) {
		this.qnaNumber = qnaNumber;
		this.qnaName = qnaName;
		this.qnaContent = qnaContent;
		this.qnaDate = qnaDate;
		this.qnaAnswerYN = qnaAnswerYN;
		this.qnaAnswer = qnaAnswer;
		this.qnaAnswerDate = qnaAnswerDate;
		this.memberCode = memberCode;
		this.managerCode = managerCode;
		this.memberDTO = memberDTO;
	}
	public String getQnaNumber() {
		return qnaNumber;
	}
	public void setQnaNumber(String qnaNumber) {
		this.qnaNumber = qnaNumber;
	}
	public String getQnaName() {
		return qnaName;
	}
	public void setQnaName(String qnaName) {
		this.qnaName = qnaName;
	}
	public String getQnaContent() {
		return qnaContent;
	}
	public void setQnaContent(String qnaContent) {
		this.qnaContent = qnaContent;
	}
	public Date getQnaDate() {
		return qnaDate;
	}
	public void setQnaDate(Date qnaDate) {
		this.qnaDate = qnaDate;
	}
	public String getQnaAnswerYN() {
		return qnaAnswerYN;
	}
	public void setQnaAnswerYN(String qnaAnswerYN) {
		this.qnaAnswerYN = qnaAnswerYN;
	}
	public String getQnaAnswer() {
		return qnaAnswer;
	}
	public void setQnaAnswer(String qnaAnswer) {
		this.qnaAnswer = qnaAnswer;
	}
	public String getQnaAnswerDate() {
		return qnaAnswerDate;
	}
	public void setQnaAnswerDate(String qnaAnswerDate) {
		this.qnaAnswerDate = qnaAnswerDate;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public String getManagerCode() {
		return managerCode;
	}
	public void setManagerCode(String managerCode) {
		this.managerCode = managerCode;
	}
	public MemberDTO getMemberDTO() {
		return memberDTO;
	}
	public void setMemberDTO(MemberDTO memberDTO) {
		this.memberDTO = memberDTO;
	}
	@Override
	public String toString() {
		return "QnADTOAndMemberDTO [qnaNumber=" + qnaNumber + ", qnaName=" + qnaName + ", qnaContent=" + qnaContent
				+ ", qnaDate=" + qnaDate + ", qnaAnswerYN=" + qnaAnswerYN + ", qnaAnswer=" + qnaAnswer
				+ ", qnaAnswerDate=" + qnaAnswerDate + ", memberCode=" + memberCode + ", managerCode=" + managerCode
				+ ", memberDTO=" + memberDTO + "]";
	}
	
	
	
}
