package com.delimeal.qna.model.dto;

import java.io.Serializable;

public class QnAFileDTO implements Serializable{
	private static final long serialVersionUID = -5381001085443191214L;
	
	private String fileNum;
	private String fileName;
	private String qnaNum;
	public QnAFileDTO() {
	}
	public QnAFileDTO(String fileNum, String fileName, String qnaNum) {
		this.fileNum = fileNum;
		this.fileName = fileName;
		this.qnaNum = qnaNum;
	}
	public String getFileNum() {
		return fileNum;
	}
	public void setFileNum(String fileNum) {
		this.fileNum = fileNum;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getQnaNum() {
		return qnaNum;
	}
	public void setQnaNum(String qnaNum) {
		this.qnaNum = qnaNum;
	}
	@Override
	public String toString() {
		return "QnAFileDTO [fileNum=" + fileNum + ", fileName=" + fileName + ", qnaNum=" + qnaNum + "]";
	}
	
	
}
