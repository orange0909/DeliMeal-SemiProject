package com.delimeal.qna.model.dto;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

public class QnADTO implements Serializable {
	private static final long serialVersionUID = -3151648933535102688L;
	
	private String qnaNumber;
	private String qnaName;
	private String qnaContent;
	private java.sql.Date qnaDate;
	private String qnaAnswerYN;
	private String qnaAnswer;
	private java.sql.Date qnaAnswerDate;
	private String memberName;
	private String managerName;
	private List<QnAFileDTO> qnaFileList;
	private String memberCode;
	public QnADTO() {
	}
	public QnADTO(String qnaNumber, String qnaName, String qnaContent, Date qnaDate, String qnaAnswerYN,
			String qnaAnswer, Date qnaAnswerDate, String memberName, String managerName, List<QnAFileDTO> qnaFileList,
			String memberCode) {
		this.qnaNumber = qnaNumber;
		this.qnaName = qnaName;
		this.qnaContent = qnaContent;
		this.qnaDate = qnaDate;
		this.qnaAnswerYN = qnaAnswerYN;
		this.qnaAnswer = qnaAnswer;
		this.qnaAnswerDate = qnaAnswerDate;
		this.memberName = memberName;
		this.managerName = managerName;
		this.qnaFileList = qnaFileList;
		this.memberCode = memberCode;
	}
	public String getQnaNumber() {
		return qnaNumber;
	}
	public void setQnaNumber(String qnaNumber) {
		this.qnaNumber = qnaNumber;
	}
	public String getQnaName() {
		return qnaName;
	}
	public void setQnaName(String qnaName) {
		this.qnaName = qnaName;
	}
	public String getQnaContent() {
		return qnaContent;
	}
	public void setQnaContent(String qnaContent) {
		this.qnaContent = qnaContent;
	}
	public java.sql.Date getQnaDate() {
		return qnaDate;
	}
	public void setQnaDate(java.sql.Date qnaDate) {
		this.qnaDate = qnaDate;
	}
	public String getQnaAnswerYN() {
		return qnaAnswerYN;
	}
	public void setQnaAnswerYN(String qnaAnswerYN) {
		this.qnaAnswerYN = qnaAnswerYN;
	}
	public String getQnaAnswer() {
		return qnaAnswer;
	}
	public void setQnaAnswer(String qnaAnswer) {
		this.qnaAnswer = qnaAnswer;
	}
	public java.sql.Date getQnaAnswerDate() {
		return qnaAnswerDate;
	}
	public void setQnaAnswerDate(java.sql.Date qnaAnswerDate) {
		this.qnaAnswerDate = qnaAnswerDate;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public String getManagerName() {
		return managerName;
	}
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}
	public List<QnAFileDTO> getQnaFileList() {
		return qnaFileList;
	}
	public void setQnaFileList(List<QnAFileDTO> qnaFileList) {
		this.qnaFileList = qnaFileList;
	}
	public String getMemberCode() {
		return memberCode;
	}
	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "QnADTO [qnaNumber=" + qnaNumber + ", qnaName=" + qnaName + ", qnaContent=" + qnaContent + ", qnaDate="
				+ qnaDate + ", qnaAnswerYN=" + qnaAnswerYN + ", qnaAnswer=" + qnaAnswer + ", qnaAnswerDate="
				+ qnaAnswerDate + ", memberName=" + memberName + ", managerName=" + managerName + ", qnaFileList="
				+ qnaFileList + ", memberCode=" + memberCode + "]";
	}
	

	
	
}
