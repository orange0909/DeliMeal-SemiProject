package com.delimeal.qna.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.qna.model.dao.QnAMapper;
import com.delimeal.qna.model.dto.QnADTO;
import com.delimeal.qna.model.dto.QnADTOAndMemberDTO;
import com.delimeal.qna.model.dto.QnAFileDTO;

public class QnAService {
			
	private QnAMapper mapper;

	public List<QnADTOAndMemberDTO> selectAllQna(Map<String, Object> myQnA) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		List<QnADTOAndMemberDTO> qnaList = mapper.selectAllQna(myQnA);
		
		
		sqlSession.close();
		return qnaList;
	}

	public int selectTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		/*검색조건 들고 xml한테 갈것임*/
		int totalCount = mapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	public QnADTO selectQnaDetail(String qnaNum) {
		
			SqlSession sqlSession = getSqlSession();
			
			mapper = sqlSession.getMapper(QnAMapper.class);
			
			QnADTO qnaList = mapper.selectQnaDetail(qnaNum);
			
			
			sqlSession.close();
			
		return qnaList;
	}

	public int insertQnA(QnADTO qnaDTO) {

		SqlSession sqlSession = getSqlSession();
		int result = 0;
		mapper = sqlSession.getMapper(QnAMapper.class);
		/*게시글 인서트*/
		int QnAresult = mapper.insertQnA(qnaDTO);
		/*파일 리스트 불러옴*/
		List<QnAFileDTO> qnaFile = qnaDTO.getQnaFileList();
		
		System.out.println(qnaDTO.getQnaNumber());
		/*게시글 번호 넣을거임.*/
		for(int i = 0; i < qnaFile.size(); i++) {
			qnaFile.get(i).setQnaNum(qnaDTO.getQnaNumber());
		}
		System.out.println(qnaFile);
		/*file insert*/
		int fileResult = 0;
		/*파일이 없으면 안담아짐.*/
		for(int i = 0; i < qnaFile.size(); i++) {
			fileResult += mapper.insertFile(qnaFile.get(i));
		}
		if (QnAresult > 0  && fileResult == qnaFile.size()  ) {
			sqlSession.commit();
			result = 1;
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		return result;
	}
	
	public int deleteWithFileQnA(Map<String, String> qnaNumMap) {
		SqlSession sqlSession = getSqlSession();
		/*파일 삭제 결과 */
		int deleteFileResult = 0;
		/*그냥 삭제 결과*/
		int deleteResult = 0;
		mapper = sqlSession.getMapper(QnAMapper.class);
		/*파일 삭제*/
		deleteFileResult = mapper.fileDeleteQnA(qnaNumMap);
		
		deleteResult = mapper.deleteQnA(qnaNumMap);
		
		if(deleteFileResult > 0 && deleteResult > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return deleteFileResult;
	}

	public int deleteQnA(Map<String, String> qnaNumMap) {
		/*파일 없는 삭제 */
		SqlSession sqlSession = getSqlSession();
		
		int deleteResult = 0;
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		deleteResult = mapper.deleteQnA(qnaNumMap);
		
		if(deleteResult > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		return deleteResult;
	}

	public int updateQnA(QnADTO qnaDTO) {
		
		System.out.println("업데이트 서비스 어서오고~");
		
		SqlSession sqlSession = getSqlSession();
		
		/*최종값*/
		int result = 0;

		mapper = sqlSession.getMapper(QnAMapper.class);

		/*게시글 업데이트*/
		result = mapper.updateQnA(qnaDTO);

		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		sqlSession.close();
		
		
		return result;
	}

	/*업데이트 파일, 게시글*/
	public int updateFileAndQna(QnADTO qnaDTO) {
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		List<QnAFileDTO> qnaFile = qnaDTO.getQnaFileList();
		/*게시글 번호 넣기*/
		qnaFile.get(0).setQnaNum(qnaDTO.getQnaNumber());
		int result = 0;
		
		int fileUpdate = mapper.updateQnAFile(qnaFile.get(0));
		int qnaUpdate = mapper.updateQnA(qnaDTO);
		
		if(fileUpdate > 0 && qnaUpdate > 0) {
			result = 1;
		}
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	/* 파일테이블 인서트, 게시글 업데이트*/
	public int updateQandInsertF(QnADTO qnaDTO) {
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		List<QnAFileDTO> qnaFile = qnaDTO.getQnaFileList();
		/*게시글 번호 넣기*/
		qnaFile.get(0).setQnaNum(qnaDTO.getQnaNumber());
		
		/*최종 값*/
		int result = 0;
		
		/*파일 인서트*/
		int fileInsert = mapper.insertWhenUpdateFile(qnaFile.get(0));
		
		/* 게시글 업데이트 */
		int qnaUpdate = mapper.updateQnA(qnaDTO);
		
		if(fileInsert > 0 && qnaUpdate > 0) {
			result = 1;
		}
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	/*파일 데이터 삭제*/
	public int deleteQnaFile(Map<String, String> parameter) {
		SqlSession sqlSession = getSqlSession();
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		int result =  mapper.fileDeleteQnAWhenUpdate(parameter);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public List<QnADTOAndMemberDTO> selectMyAllQna(Map<String, Object> myQnA) {
		System.out.println(" 나의 문의 조회 서블릿 도착~~~");
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		List<QnADTOAndMemberDTO> qnaList = mapper.selectAllMyQna(myQnA);
		System.out.println(qnaList);
		sqlSession.close();
		return qnaList;
	}

	public int selectMyQnaTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		/*검색조건 들고 xml한테 갈것임*/
		int totalCount = mapper.selectMyQnaTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}
	
	public int selectWaithingTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		/*검색조건 들고 xml한테 갈것임*/
		int totalCount = mapper.selectWaithingTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	public List<QnADTOAndMemberDTO> selectWaitingQna(Map<String, Object> waithingQnA) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		List<QnADTOAndMemberDTO> qnaList = mapper.selectWaithingQnA(waithingQnA);
		
		sqlSession.close();
		
		return qnaList;
	}

	public int insertAnswer(Map<String, String> answer) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		int result = mapper.insertAnswer(answer);
		
		if(result > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteAnswer(String qnaNumber) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(QnAMapper.class);
		
		int deleteAnswerResult = mapper.deleteAnswer(qnaNumber);
		
		if(deleteAnswerResult > 0) {
			sqlSession.commit();
		}else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return deleteAnswerResult;
	}

	
	
}

















