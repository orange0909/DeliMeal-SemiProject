package com.delimeal.meal.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class MealPerDayDTO implements Serializable {
	private static final long serialVersionUID = -1973779576414121266L;
	
	private java.sql.Date deliveryDate;
	private MealDTO meal;
	private String mealCategoryCode;
	
	public MealPerDayDTO() { }

	public MealPerDayDTO(Date deliveryDate, MealDTO meal, String mealCategoryCode) {
		this.deliveryDate = deliveryDate;
		this.meal = meal;
		this.mealCategoryCode = mealCategoryCode;
	}

	public java.sql.Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(java.sql.Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public MealDTO getMeal() {
		return meal;
	}

	public void setMeal(MealDTO meal) {
		this.meal = meal;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	@Override
	public String toString() {
		return "MealPerDayDTO [deliveryDate=" + deliveryDate + ", meal=" + meal + ", mealCategoryCode="
				+ mealCategoryCode + "]";
	}
}
