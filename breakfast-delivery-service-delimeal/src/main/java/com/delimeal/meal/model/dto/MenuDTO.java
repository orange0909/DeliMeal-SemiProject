package com.delimeal.meal.model.dto;

import java.io.Serializable;

public class MenuDTO implements Serializable {
	private static final long serialVersionUID = 7318216730293607006L;
	
	private String menuCode;
	private String menuName;
	private String menuAllergie;
	private String menuCalinfo;
	private String menuNutrient;
	private String menuAttachName;
	
	public MenuDTO() { }
	
	public MenuDTO(String menuCode, String menuName, String menuAllergie, String menuCalinfo, String menuNutrient,
			String menuAttachName) {
		this.menuCode = menuCode;
		this.menuName = menuName;
		this.menuAllergie = menuAllergie;
		this.menuCalinfo = menuCalinfo;
		this.menuNutrient = menuNutrient;
		this.menuAttachName = menuAttachName;
	}

	public String getMenuCode() {
		return menuCode;
	}

	public void setMenuCode(String menuCode) {
		this.menuCode = menuCode;
	}

	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuAllergie() {
		return menuAllergie;
	}

	public void setMenuAllergie(String menuAllergie) {
		this.menuAllergie = menuAllergie;
	}

	public String getMenuCalinfo() {
		return menuCalinfo;
	}

	public void setMenuCalinfo(String menuCalinfo) {
		this.menuCalinfo = menuCalinfo;
	}

	public String getMenuNutrient() {
		return menuNutrient;
	}

	public void setMenuNutrient(String menuNutrient) {
		this.menuNutrient = menuNutrient;
	}

	public String getMenuAttachName() {
		return menuAttachName;
	}

	public void setMenuAttachName(String menuAttachName) {
		this.menuAttachName = menuAttachName;
	}

	@Override
	public String toString() {
		return "MenuDTO [menuCode=" + menuCode + ", menuName=" + menuName + ", menuAllergie=" + menuAllergie
				+ ", menuCalinfo=" + menuCalinfo + ", menuNutrient=" + menuNutrient + ", menuAttachName="
				+ menuAttachName + "]";
	}
}
