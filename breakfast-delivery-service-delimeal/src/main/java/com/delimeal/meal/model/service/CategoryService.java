package com.delimeal.meal.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dao.CategoryMapper;
import com.delimeal.meal.model.dto.CategoryDTO;

public class CategoryService {
	private CategoryMapper mapper;
	
	public int insertCategory(CategoryDTO category) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(CategoryMapper.class);
		int result = mapper.insertCategory(category);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int selectTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(CategoryMapper.class);

		int totalCount = mapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	public List<CategoryDTO> selectCategory(SelectChriteria selectChriteria) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(CategoryMapper.class);
		
		List<CategoryDTO> categoryList = mapper.selectCategory(selectChriteria);
		
		sqlSession.close();
		
		return categoryList;
	}

	public CategoryDTO selectCategoryDetail(String categoryCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(CategoryMapper.class);
		
		CategoryDTO category = mapper.selectCategoryDetail(categoryCode);
		sqlSession.close();
		
		return category;
	}

	public int deleteCategory(String categoryName) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(CategoryMapper.class);
		int result = mapper.deleteCategory(categoryName);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int updateCategory(CategoryDTO category) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(CategoryMapper.class);
		int result = mapper.updateCategory(category);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}
}
