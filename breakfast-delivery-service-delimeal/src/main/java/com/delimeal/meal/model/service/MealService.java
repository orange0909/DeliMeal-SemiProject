package com.delimeal.meal.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dao.MealMapper;
import com.delimeal.meal.model.dao.MenuMapper;
import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.dto.MealPerDayDTO;
import com.delimeal.meal.model.dto.MenuDTO;

public class MealService {
	private MealMapper mapper;
	
	public List<MealPerDayDTO> selectMealPerDay(SelectChriteria selectChriteria) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		List<MealPerDayDTO> mealPerDayList = mapper.selectMealPerDay(selectChriteria);
		
		sqlSession.close();
		
		return mealPerDayList;
	}

	public int selectTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int totalCount = mapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}

	public List<CategoryDTO> selectAllCategory() {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		List<CategoryDTO> categoryList = mapper.selectAllCategory();
		
		sqlSession.close();
		
		return categoryList;
	}

	public MealDTO selectMealDetail(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		MealDTO meal = mapper.selectMealDetail(mealCode);
		
		sqlSession.close();
		
		return meal;
		
	}

	public List<MenuDTO> selectMenuByMealCode(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		List<MenuDTO> menuList = mapper.selectMenuByMealCode(mealCode);
		
		sqlSession.close();
		
		return menuList;
	}

	public int deleteMeal(String mealName) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.deleteMeal(mealName);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;

	}

	public int deleteMealInMenu(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.deleteMealInMenu(mealCode);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int deleteMealInDay(String mealCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.deleteMealInDay(mealCode);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public String selectMealCodeByMealName(String mealName) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		String mealCode = mapper.selectMealCodeByMealName(mealName);
		
		sqlSession.close();
		
		return mealCode;
	}

	public int insertMeal(MealDTO meal) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.insertMeal(meal);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public String selectMenuCodeByMenuName(String menuName) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		String menuCode = mapper.selectMenuCodeByMenuName(menuName);
		
		sqlSession.close();
		
		return menuCode;
	}

	public int insertMealInMenu(Map<String, String> mealPerMenu) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.insertMealInMenu(mealPerMenu);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	/*
	 * public String selectMealCategoryCodeByMealName(String mealName) { SqlSession
	 * sqlSession = getSqlSession();
	 * 
	 * mapper = sqlSession.getMapper(MealMapper.class);
	 * 
	 * String categoryCode = mapper.selectMealCategoryCodeByMealName(mealName);
	 * 
	 * sqlSession.close();
	 * 
	 * return categoryCode; }
	 */

	public int insertMealPerDay(Map<String, String> data) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.insertMealPerDay(data);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public List<String> selectAllMenuNameList() {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		List<String> allMenuNameList = mapper.selectAllMenuNameList();
		
		sqlSession.close();
		
		return allMenuNameList;
	}

	public List<String> selectCategoryName() {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		List<String> categoryName = mapper.selectCategoryName();
		
		sqlSession.close();
		
		return categoryName;
	}

	public String selectCategoryCodeByCategoryName(String mealCategoryName) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		String mealCategoryCode = mapper.selectCategoryCodeByCategoryName(mealCategoryName);
		
		sqlSession.close();
		
		return mealCategoryCode;
	}

	public String selectRecentMealCode() {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		String mealCategoryCode = mapper.selectRecentMealCode();
		
		sqlSession.close();
		
		return mealCategoryCode;
	}

	public int updateMeal(MealDTO meal) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.updateMeal(meal);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int updateMealInMenu(Map<String, String> mealPerMenu) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.updateMealInMenu(mealPerMenu);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}

	public int updateMealPerDay(Map<String, String> data) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MealMapper.class);
		
		int result = mapper.updateMealPerDay(data);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}



}
