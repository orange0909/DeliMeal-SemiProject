package com.delimeal.meal.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dao.MenuMapper;
import com.delimeal.meal.model.dto.MenuDTO;

public class MenuService {
	private MenuMapper mapper;
	
	public int selectTotalCount(Map<String, String> searchMap) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MenuMapper.class);

		int totalCount = mapper.selectTotalCount(searchMap);
		
		sqlSession.close();
		
		return totalCount;
	}
	public List<MenuDTO> selectMenu(SelectChriteria selectChriteria) {

		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MenuMapper.class);
		
		List<MenuDTO> menuList = mapper.selectMenu(selectChriteria);
		
		sqlSession.close();
		
		return menuList;
	
	}
	public int insertMenu(MenuDTO menu) {
		
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MenuMapper.class);
		
		int result = mapper.insertMenu(menu);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}
	public MenuDTO selectMenuDetail(String menuCode) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MenuMapper.class);
		
		MenuDTO menu = mapper.selectMenuDetail(menuCode);
		
		sqlSession.close();
		
		return menu;
	}
	public int deleteMenu(String menuName) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MenuMapper.class);
		
		int result = mapper.deleteMenu(menuName);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}
	public int updateMenu(MenuDTO menu) {
		SqlSession sqlSession = getSqlSession();
		
		mapper = sqlSession.getMapper(MenuMapper.class);
		
		int result = mapper.updateMenu(menu);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result;
	}


}
