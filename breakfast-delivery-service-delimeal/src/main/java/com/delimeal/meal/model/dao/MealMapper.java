package com.delimeal.meal.model.dao;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.dto.MealPerDayDTO;
import com.delimeal.meal.model.dto.MenuDTO;

public interface MealMapper {
	List<MealPerDayDTO> selectMealPerDay(SelectChriteria selectChriteria);

	int selectTotalCount(Map<String, String> searchMap);

	List<CategoryDTO> selectAllCategory();

	MealDTO selectMealDetail(String mealCode);

	List<MenuDTO> selectMenuByMealCode(String mealCode);

	int deleteMeal(String mealName);

	int deleteMealInMenu(String mealCode);

	int deleteMealInDay(String mealCode);

	String selectMealCodeByMealName(String mealName);

	int selectMealCodeByMealName(MealDTO meal);

	String selectMenuCodeByMenuName(String menuName);

	int insertMealInMenu(Map<String, String> mealPerMenu);

	/* String selectMealCategoryCodeByMealName(String mealName); */

	int insertMealPerDay(Map<String, String> data);

	List<String> selectAllMenuNameList();

	int insertMeal(MealDTO meal);

	List<String> selectCategoryName();

	String selectCategoryCodeByCategoryName(String mealCategoryName);

	String selectRecentMealCode();

	String selectMealCodeByMealName();

	int updateMeal(MealDTO meal);

	int updateMealInMenu(Map<String, String> mealPerMenu);

	int updateMealPerDay(Map<String, String> data);
}
