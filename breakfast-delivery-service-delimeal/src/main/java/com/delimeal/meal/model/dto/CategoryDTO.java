package com.delimeal.meal.model.dto;

import java.io.Serializable;

public class CategoryDTO implements Serializable{

	private static final long serialVersionUID = 7245380976873569036L;

	private String categoryCode;
	private String categoryName;
	private String categoryDetail;
	private int categoryPrice;
	private String categoryType;
	private String categoryAttachName;
	private String categorySubs;
	
	public CategoryDTO() {
	}

	public CategoryDTO(String categoryCode, String categoryName, String categoryDetail, int categoryPrice, String categoryType, String categoryAttachName, String categorySubs) {
		super();
		this.categoryCode = categoryCode;
		this.categoryName = categoryName;
		this.categoryDetail = categoryDetail;
		this.categoryPrice = categoryPrice;
		this.categoryType = categoryType;
		this.categoryAttachName = categoryAttachName;
		this.categorySubs = categorySubs;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public String getCategoryDetail() {
		return categoryDetail;
	}

	public void setCategoryDetail(String categoryDetail) {
		this.categoryDetail = categoryDetail;
	}

	public int getCategoryPrice() {
		return categoryPrice;
	}

	public void setCategoryPrice(int categoryPrice) {
		this.categoryPrice = categoryPrice;
	}

	public String getCategoryType() {
		return categoryType;
	}

	public void setCategoryType(String categoryType) {
		this.categoryType = categoryType;
	}

	public String getCategoryAttachName() {
		return categoryAttachName;
	}

	public void setCategoryAttachName(String categoryAttachName) {
		this.categoryAttachName = categoryAttachName;
	}

	public String getCategorySubs() {
		return categorySubs;
	}

	public void setCategorySubs(String categorySubs) {
		this.categorySubs = categorySubs;
	}

	@Override
	public String toString() {
		return "CategoryDTO [categoryCode=" + categoryCode + ", categoryName=" + categoryName + ", categoryDetail=" + categoryDetail + ", categoryPrice=" + categoryPrice + ", categoryType=" + categoryType + ", categoryAttachName=" + categoryAttachName + ", categorySubs=" + categorySubs + "]";
	}
}