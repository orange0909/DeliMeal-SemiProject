package com.delimeal.meal.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dto.CategoryDTO;

public interface CategoryMapper {

	int insertCategory(CategoryDTO category);

	int selectTotalCount(Map<String, String> searchMap);

	List<CategoryDTO> selectCategory(SelectChriteria selectChriteria);

	CategoryDTO selectCategoryDetail(String categoryCode);

	int deleteCategory(String categoryName);

	int updateCategory(CategoryDTO category);

}
