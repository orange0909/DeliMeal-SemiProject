package com.delimeal.meal.model.dao;

import java.util.List;
import java.util.Map;

import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dto.MenuDTO;

public interface MenuMapper {

	List<MenuDTO> selectMenu(SelectChriteria selectChriteria);

	int selectTotalCount(Map<String, String> searchMap);

	int insertMenu(MenuDTO menu);

	MenuDTO selectMenuDetail(String menuCode);

	int deleteMenu(String menuName);

	int updateMenu(MenuDTO menu);

}
