package com.delimeal.meal.model.dto;

import java.io.Serializable;

public class MealDTO implements Serializable {
	private static final long serialVersionUID = -852614719839524519L;
	
	private String mealCode;
	private String mealName;
	private String mealDetail;
	private String mealCalinfo;
	private String mealAttachName;
	private String mealCategoryCode;
	
	public MealDTO() { }

	public MealDTO(String mealCode, String mealName, String mealDetail, String mealCalinfo, String mealAttachName,
			String mealCategoryCode) {
		this.mealCode = mealCode;
		this.mealName = mealName;
		this.mealDetail = mealDetail;
		this.mealCalinfo = mealCalinfo;
		this.mealAttachName = mealAttachName;
		this.mealCategoryCode = mealCategoryCode;
	}

	public String getMealCode() {
		return mealCode;
	}

	public void setMealCode(String mealCode) {
		this.mealCode = mealCode;
	}

	public String getMealName() {
		return mealName;
	}

	public void setMealName(String mealName) {
		this.mealName = mealName;
	}

	public String getMealDetail() {
		return mealDetail;
	}

	public void setMealDetail(String mealDetail) {
		this.mealDetail = mealDetail;
	}

	public String getMealCalinfo() {
		return mealCalinfo;
	}

	public void setMealCalinfo(String mealCalinfo) {
		this.mealCalinfo = mealCalinfo;
	}

	public String getMealAttachName() {
		return mealAttachName;
	}

	public void setMealAttachName(String mealAttachName) {
		this.mealAttachName = mealAttachName;
	}

	public String getMealCategoryCode() {
		return mealCategoryCode;
	}

	public void setMealCategoryCode(String mealCategoryCode) {
		this.mealCategoryCode = mealCategoryCode;
	}

	@Override
	public String toString() {
		return "MealDTO [mealCode=" + mealCode + ", mealName=" + mealName + ", mealDetail=" + mealDetail
				+ ", mealCalinfo=" + mealCalinfo + ", mealAttachName=" + mealAttachName + ", mealCategoryCode="
				+ mealCategoryCode + "]";
	}
}
