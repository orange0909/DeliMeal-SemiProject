package com.delimeal.meal.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.common.paging.Pagingnation;
import com.delimeal.common.paging.SelectChriteria;
import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.meal.model.service.MenuService;

@WebServlet("/admin/menu")
public class MenuSelectAll extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MenuService menuService = new MenuService();
		
		/* 페이징 처리 ------------*/
		/* 보려고 하는 페이지 */
		String currentPage = request.getParameter("currentPage");
		int pageNo = 1;
		
		if(currentPage != null && !"".equals(currentPage)) {
			pageNo = Integer.parseInt(currentPage);
		}
		
		/*검색조건*/
		String searchCondition = request.getParameter("searchCondition");
		
		/*검색어*/
		String searchValue = request.getParameter("searchValue");
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("searchValue::" + searchValue);
		/*전체 게시글 수(문제의 그것)*/
		int totalCount = menuService.selectTotalCount(searchMap);
		
		/*한 페이지에 보여줄 게시글 수*/
		int limit = 10;
		/*한번에 보여질 페이징 버튼(1~5) 개수*/
		int buttonAmount = 5;
		
		/*총 페이지 수 계산*/
		int maxPage = (int)Math.ceil((double) totalCount/ limit);
		
		/* 현재 페이지에서 보여줄 페이징 첫번째 버튼 숫자*/
		int startPage = (int)(Math.ceil((double) pageNo / buttonAmount) -1) * buttonAmount + 1;
		
		/* 현재 페이지에서 보여줄 페이징 마지막 버튼 숫자*/
		int endPage = (startPage + buttonAmount) -1;
		
		/*현재 보여줄 페이지 보다 총 페이지가 더 적을 경우는 마지막 페이지가 max페이지*/
		if(maxPage < endPage) {
			endPage = maxPage;
		}
		
		/* 보여줄 게시글이 없는 상태 == 총 페이지도, 끝나는 페이지도 없는 상태.*/
		if(maxPage == 0 && endPage == 0) {
			maxPage = startPage;
			endPage = startPage;
		}
		
		/* 화면에 나타날 게시글 번호 계산 */
		/* 게시글과 연관됨.
		 * 다음 페이지로 넘겼을때 보여줄번호!
		 * 끝나는 번호!*/
		int startRow;
		int endRow;
		
		startRow = (pageNo - 1) * limit + 1;
		endRow = startRow + limit - 1;

		/*페이징 처리에 관한 정보를 담고있는 DTO개념*/
		SelectChriteria selectChriteria = null;
		
		/*검색 조건과 검색값의 유무에 따라 페이지를 보여주는게 다르다. 같은 메소드이나 넘겨지는 값이 다를때*/
		if(searchValue != null) {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount, searchCondition, searchValue);
		}else {
			selectChriteria = Pagingnation.getSelectChriteria(pageNo, totalCount, limit, buttonAmount);
		}
		
		List<MenuDTO> menuList = menuService.selectMenu(selectChriteria);
		
		
		String path="";
		if(menuList != null) {
			path = "/WEB-INF/views/subs/adminMenuMainPage.jsp";
			
			/*조회된 카테고리*/
			request.setAttribute("menuList",  menuList);
			/* 페이징 처리에 사용하면 좋을. */
			request.setAttribute("selectCriteria", selectChriteria);
		}else {
			path= "/WEB-INF/views/subs/";
		}

		request.getRequestDispatcher(path).forward(request, response);
	}


}
