package com.delimeal.meal.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.service.MenuService;
import com.google.gson.Gson;

@WebServlet("/admin/menuDelete")
public class MenuDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = "/WEB-INF/views/subs/adminMenuMainPage.jsp";
		request.getRequestDispatcher(path).forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String menuName = request.getParameter("name");
		System.out.println("이름이 뭘까" + menuName);
		int result = new MenuService().deleteMenu(menuName);
		
		Gson gson = new Gson();
		
		System.out.println("MenuDeleteResult: " + result);
		
		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		
		if (result > 0) {
			out.print(new Gson().toJson("success"));
		} else {
			out.print(new Gson().toJson("fail"));
		}

		out.flush();
		out.close();
	
	}

}
