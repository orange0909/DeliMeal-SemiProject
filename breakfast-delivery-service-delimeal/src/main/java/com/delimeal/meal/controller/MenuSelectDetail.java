package com.delimeal.meal.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.meal.model.service.MenuService;

@WebServlet("/admin/menuSelectDetail")
public class MenuSelectDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		MenuService menuService = new MenuService();
		String menuCode = request.getParameter("menuCode");
		
		MenuDTO menu = menuService.selectMenuDetail(menuCode);
		System.out.println(menu);
		
		
		String path="";
		if(menu != null) {
			path = "/WEB-INF/views/subs/adminMenuInfoPage.jsp";
			
			request.setAttribute("menu",  menu);
		}

		
		request.getRequestDispatcher(path).forward(request, response);
	}
}
