package com.delimeal.meal.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.service.CategoryService;
import com.google.gson.Gson;

@WebServlet("/admin/categoryUpdate")
public class CategoryUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;
   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = "/WEB-INF/views/subs/adminCategoryInfoPage.jsp";
		request.getRequestDispatcher(path).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("왜안오냐고..");
		System.out.println(ServletFileUpload.isMultipartContent(request));
		/* 파일업로드 ------ */
		if (ServletFileUpload.isMultipartContent(request)) {
			String rootLocation = getServletContext().getRealPath("/");
			int maxFileSize = 1024 * 1024 * 10;
			String encodingType = "UTF-8";

			String fileUploadDirectory = rootLocation + "resources/food";

			File directory = new File(fileUploadDirectory);

			if (!directory.exists()) {
				System.out.println("폴더 생성 : " + directory.mkdirs());
			}

			Map<String, String> parameter = new HashMap<>();
			List<Map<String, String>> fileList = new ArrayList<>();

			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			fileItemFactory.setRepository(new File(fileUploadDirectory));
			fileItemFactory.setSizeThreshold(maxFileSize);

			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			String randomFileName = "";
			try {
				List<FileItem> fileItems = fileUpload.parseRequest(request);

				for (FileItem item : fileItems) {
					System.out.println(item);
				}
				for (int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);

					if (!item.isFormField()) {

						if (item.getSize() > 0) {

							String filedName = item.getFieldName();
							String originFileName = item.getName();

							int dot = originFileName.lastIndexOf(".");
							String ext = originFileName.substring(dot);

							randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;

							File storeFile = new File(fileUploadDirectory + "/" + randomFileName);
							
							System.out.println("스토어파일------------------" + storeFile);
							item.write(storeFile);

							Map<String, String> fileMap = new HashMap<>();
							fileMap.put("filedName", filedName);
							fileMap.put("originFileName", originFileName);
							fileMap.put("savedFileName", randomFileName);
							fileMap.put("savePath", fileUploadDirectory);

							fileList.add(fileMap);

						}

					} else {
						parameter.put(item.getFieldName(),
								new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));

					}
				}

				System.out.println("parameter : " + parameter);
				System.out.println("fileList : " + fileList);

				/* 파라미터 값 받아오기 */
				String categoryName = parameter.get("name");
				String categoryDetail = parameter.get("intro");
				int categoryPrice = Integer.valueOf(parameter.get("price"));
				String categoryType = parameter.get("classify");
				String categoryAttachName = randomFileName;
				String categoryCode = parameter.get("code");
				
				CategoryDTO category = new CategoryDTO();
				category.setCategoryName(categoryName);
				category.setCategoryDetail(categoryDetail);
				category.setCategoryPrice(categoryPrice);
				category.setCategoryType(categoryType);
				category.setCategoryAttachName(categoryAttachName);
				category.setCategoryCode(categoryCode);
				

				int result = new CategoryService().updateCategory(category);

				Gson gson = new Gson();
				
				System.out.println("CategoryUpdateResult: " + result);
				
				response.setContentType("application/json; charset=UTF-8");
				PrintWriter out = response.getWriter();
				
				if (result > 0) {
					out.print(new Gson().toJson("success"));
				} else {
					out.print(new Gson().toJson("fail"));
				}

				out.flush();
				out.close();
			} catch (Exception e) {
				int cnt = 0;
				for (int i = 0; i < fileList.size(); i++) {
					Map<String, String> file = fileList.get(i);

					File deleteFile = new File(fileUploadDirectory + "/" + file.get("savedFileName"));
					boolean isDeleted = deleteFile.delete();

					if (isDeleted) {
						cnt++;
					}
				}

				if (cnt == fileList.size()) {
					System.out.println("업로드에 실패한 모든 사진 삭제");
					e.printStackTrace();
				} else {
					e.printStackTrace();
				}
			}
		}
	}

}
