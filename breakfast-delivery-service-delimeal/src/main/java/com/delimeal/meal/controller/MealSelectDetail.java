package com.delimeal.meal.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.dto.MenuDTO;
import com.delimeal.meal.model.service.MealService;

@WebServlet("/admin/mealSelectDetail")
public class MealSelectDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		MealService mealService = new MealService();
		
		/* 식단코드를 가져온다 */
		String mealCode = request.getParameter("mealCode");
		
		/* 식단별날짜를 가져온다 */
		java.sql.Date date = java.sql.Date.valueOf(request.getParameter("date"));
		
		/* 식단을 가져온다 */
		MealDTO meal = mealService.selectMealDetail(mealCode);
		
		/* 모든 메뉴의 정보를 가져온다 */
		List<MenuDTO> menu = mealService.selectMenuByMealCode(mealCode);
		
		/* 모든 카테고리를 가져온다 */
		List<CategoryDTO> categoryList = mealService.selectAllCategory();
		
		/* 모든 메뉴 이름을 가져온다 */
		List<String> allMenuNameList = mealService.selectAllMenuNameList();
		
		
		String path="";
		if(meal != null) {
			path = "/WEB-INF/views/subs/adminMealInfoPage.jsp";
			
			request.setAttribute("meal",  meal);
			request.setAttribute("menu",  menu);
			request.setAttribute("date", date);
			request.setAttribute("categoryList", categoryList);
			request.setAttribute("menuList", allMenuNameList);
		}

		
		request.getRequestDispatcher(path).forward(request, response);
	}
}
