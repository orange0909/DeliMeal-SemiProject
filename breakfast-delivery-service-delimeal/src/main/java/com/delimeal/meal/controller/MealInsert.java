package com.delimeal.meal.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.delimeal.meal.model.dto.MealDTO;
import com.delimeal.meal.model.service.MealService;
import com.google.gson.Gson;

@WebServlet("/admin/mealInsert")
public class MealInsert extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String path = "/WEB-INF/views/subs/adminMealMainPage.jsp";
		request.getRequestDispatcher(path).forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println("서블렛 왔어?? 왔다고 해줘");

		System.out.println(ServletFileUpload.isMultipartContent(request));
		/* 파일업로드 ------ */
		if (ServletFileUpload.isMultipartContent(request)) {
			System.out.println("여기는 오니??");
			String rootLocation = getServletContext().getRealPath("/");
			int maxFileSize = 1024 * 1024 * 10;
			String encodingType = "UTF-8";

			String fileUploadDirectory = rootLocation + "resources/food";

			File directory = new File(fileUploadDirectory);

			if (!directory.exists()) {
				System.out.println("폴더 생성 : " + directory.mkdirs());
			}

			Map<String, String> parameter = new HashMap<>();
			List<Map<String, String>> fileList = new ArrayList<>();

			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			fileItemFactory.setRepository(new File(fileUploadDirectory));
			fileItemFactory.setSizeThreshold(maxFileSize);

			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			String randomFileName = "";
			try {
				List<FileItem> fileItems = fileUpload.parseRequest(request);

				for (FileItem item : fileItems) {
					System.out.println(item);
				}
				for (int i = 0; i < fileItems.size(); i++) {
					FileItem item = fileItems.get(i);

					if (!item.isFormField()) {

						if (item.getSize() > 0) {

							String filedName = item.getFieldName();
							String originFileName = item.getName();

							int dot = originFileName.lastIndexOf(".");
							String ext = originFileName.substring(dot);

							randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;

							File storeFile = new File(fileUploadDirectory + "/" + randomFileName);

							item.write(storeFile);

							Map<String, String> fileMap = new HashMap<>();
							fileMap.put("filedName", filedName);
							fileMap.put("originFileName", originFileName);
							fileMap.put("savedFileName", randomFileName);
							fileMap.put("savePath", fileUploadDirectory);

							fileList.add(fileMap);

						}

					} else {
						parameter.put(item.getFieldName(),
								new String(item.getString().getBytes("ISO-8859-1"), "UTF-8"));

					}
				}

				System.out.println("parameter : " + parameter);
				System.out.println("fileList : " + fileList);
				
				// 여기까지는 온다.
				
				/* 파라미터 값 받아오기 */
				String mealName = parameter.get("name");
				String mealDetail = parameter.get("intro");
				String mealAttchName = randomFileName;
				String mealCategoryName = parameter.get("categoryName");
				
				/* 카테고리이름을 코드로 바꿔주기 */
				String mealCategoryCode = new MealService().selectCategoryCodeByCategoryName(mealCategoryName);
				
				/* 파라미터로 가져온 식단날짜 */
				java.sql.Date date = java.sql.Date.valueOf(parameter.get("date"));

				MealDTO meal = new MealDTO();
				meal.setMealName(mealName);
				meal.setMealAttachName(mealAttchName);
				meal.setMealDetail(mealDetail);
				meal.setMealCategoryCode(mealCategoryCode);
	
				
				System.out.println("meal의 정보에요:"  + meal);
				
				// 식단정보들 담음
				int result = new MealService().insertMeal(meal);
				
				System.out.println("meal이 담겼어요" + result);

				// 메뉴이름들이담긴 리스트 가져오기
				String[] menuNameList = parameter.get("textArray").split(",");
				
				// 메뉴이름을 코드로 변환
				MealService service = new MealService();
				
				String recentMealCode = service.selectRecentMealCode();
				int result2 = 0;
				
				
				/* selectRecentMealCode */
				for (String menuName : menuNameList) {
					String menuCode = service.selectMenuCodeByMenuName(menuName);
					
					Map<String, String> mealPerMenu = new HashMap<String, String>();
					
					mealPerMenu.put("menuCode", menuCode);
					mealPerMenu.put("mealCode", recentMealCode);

					result2 += service.insertMealInMenu(mealPerMenu);
				}

				System.out.println("메뉴네임리스트:  " + menuNameList);
				
				Map<String, String> data = new HashMap<String, String>();
				data.put("date", date.toString());
				data.put("mealCode", recentMealCode);
				data.put("mealCategoryCode", mealCategoryCode);

				int result3 = new MealService().insertMealPerDay(data);

				Gson gson = new Gson();

				System.out.println("MenuInsertResult: " + result);

				response.setContentType("application/json; charset=UTF-8");
				PrintWriter out = response.getWriter();

				if (result > 0 && result2 >= menuNameList.length && result3 > 0) {
					out.print(new Gson().toJson("success"));
				} else {
					out.print(new Gson().toJson("fail"));
				}

				out.flush();
				out.close();
			} catch (Exception e) {
				int cnt = 0;
				for (int i = 0; i < fileList.size(); i++) {
					Map<String, String> file = fileList.get(i);

					File deleteFile = new File(fileUploadDirectory + "/" + file.get("savedFileName"));
					boolean isDeleted = deleteFile.delete();

					if (isDeleted) {
						cnt++;
					}
				}

				if (cnt == fileList.size()) {
					System.out.println("업로드에 실패한 모든 사진 삭제");
					e.printStackTrace();
				} else {
					e.printStackTrace();
				}
			}
		}
	}

}
