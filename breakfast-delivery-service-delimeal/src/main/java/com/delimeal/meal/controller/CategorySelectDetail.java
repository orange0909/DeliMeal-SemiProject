package com.delimeal.meal.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.dto.CategoryDTO;
import com.delimeal.meal.model.service.CategoryService;

@WebServlet("/admin/categorySelectDetail")
public class CategorySelectDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		CategoryService categoryService = new CategoryService();
		String categoryCode = request.getParameter("categoryCode");
		
		CategoryDTO category = categoryService.selectCategoryDetail(categoryCode);
		System.out.println(category);
		
		
		String path="";
		if(category != null) {
			path = "/WEB-INF/views/subs/adminCategoryInfoPage.jsp";
			
			request.setAttribute("category",  category);
		}
		
		
		request.getRequestDispatcher(path).forward(request, response);
	}	
	
	

	

}
