package com.delimeal.meal.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.meal.model.service.MealService;
import com.google.gson.Gson;


@WebServlet("/admin/mealDelete")
public class MealDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = "/WEB-INF/views/subs/adminMealMainPage.jsp";
		request.getRequestDispatcher(path).forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String mealName = request.getParameter("name");
		String mealCode = request.getParameter("code");
		
		if (mealCode != null && mealCode != "") {
			System.out.println("이름이 뭘까" + mealName);
			System.out.println("코드는 뭘까" + mealCode);
			
			int result3 = new MealService().deleteMealInDay(mealCode);
			int result2 = new MealService().deleteMealInMenu(mealCode);
			int result = new MealService().deleteMeal(mealName);
			
			Gson gson = new Gson();
			
			System.out.println("MealDeleteResult: " + result);
			
			response.setContentType("application/json; charset=UTF-8");
			PrintWriter out = response.getWriter();
			
			if (result > 0 && result2 > 0 && result3 > 0) {
				out.print(new Gson().toJson("success"));
			} else {
				out.print(new Gson().toJson("fail"));
			}
	
			out.flush();
			out.close();
		} else {
			System.out.println("코드가 없나?" + mealCode);
			
			mealCode = new MealService().selectMealCodeByMealName(mealName);
			
			System.out.println("코드값 생겼나?" + mealCode);
			int result3 = new MealService().deleteMealInDay(mealCode);
			int result2 = new MealService().deleteMealInMenu(mealCode);
			int result = new MealService().deleteMeal(mealName);
			
			Gson gson = new Gson();
			
			System.out.println("MealDeleteResult: " + result);
			
			response.setContentType("application/json; charset=UTF-8");
			PrintWriter out = response.getWriter();
			
			if (result > 0 && result2 > 0 && result3 > 0) {
				out.print(new Gson().toJson("success"));
			} else {
				out.print(new Gson().toJson("fail"));
			}
	
			out.flush();
			out.close();
			
		}
	
	}

}
