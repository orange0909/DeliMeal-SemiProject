package com.delimeal.promotionAdmin.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.delimeal.promotionAdmin.model.service.UpdateBirthdayMenuService;

@WebServlet("/pa/UpdatePromotionProcess")
public class UpdatePromotionProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private String rootLocation; // 파일이 저장될 경로
	private int maxFileSize;
	private String encodingType;
	
    public void init() throws ServletException {
    	ServletContext context = getServletContext();
    	
    	rootLocation = getServletContext().getRealPath("/");
    	maxFileSize = Integer.valueOf(context.getInitParameter("max-file-size"));
    	encodingType = context.getInitParameter("encoding-type");
    	
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UpdateBirthdayMenuService service = new UpdateBirthdayMenuService();
		HttpSession session = request.getSession();
		
		if(ServletFileUpload.isMultipartContent(request)) {
			String randomFileName = "";
			String alertMessage = null;
			
			System.out.println("파일 저장 root경로: " + rootLocation);
			System.out.println("인코딩 방식: " + encodingType);
			
			String fileUploadDirectory = rootLocation + "resources/promotion";
			System.out.println("fileUploadDirectory: " + fileUploadDirectory);
			
			File directory = new File(fileUploadDirectory);
			
			/* 파일 경로가 없는 경우 디렉토리 생성 */
			if(!directory.exists()) {
				System.out.println("폴더 생성(중간 폴더 포함 모두): " + directory.mkdirs());
			}
			
			Map<String, String> parameter = new HashMap<>();
			
			/* 파일을 업로드 할 시 최대 크기나 임시 저장할 폴더의 경로 등을 포함하기 위한 instance */
			DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();
			fileItemFactory.setRepository(new File(fileUploadDirectory)); 	// 저장 공간을 알려주기
			fileItemFactory.setSizeThreshold(maxFileSize);					// 파일의 maxsize를 알려준다
			
			ServletFileUpload fileUpload = new ServletFileUpload(fileItemFactory);
			
			try {
				List<FileItem> fileItem = fileUpload.parseRequest(request);
				
				System.out.println("item객체 안의 element 추출");
				for(FileItem item : fileItem) {
					System.out.println(item);
				}
				System.out.println("추출끝");
				
				for(int i = 0; i < fileItem.size(); i++) {
					FileItem item = fileItem.get(i);
					
					if(!item.isFormField()) { // 파싱된게 file타입일 경우
						
						/* 사이즈가 0을 초과해야 저장할 가치가 있는 파일이다. */
						if(item.getSize() > 0) {
							
							String fieldName = item.getFieldName();
							String originFilename = item.getName();
							
							System.out.println("input태그의 name: " + fieldName);
							
							/* 저장될 파일의 이름 UUID로 변경하는 과정 */
							
							/* 1. 저장할 새로운 이름 생성(rename) */
							/* 올린 파일의 확장자 추출 */
							int dot = originFilename.lastIndexOf(".");
							String ext = originFilename.substring(dot);  // dot의 index부터 끝까지 자르기
							
							System.out.println("올린 파일 확장자: " + ext);
							
							/* UUID + 확장자로 fileName지정 */
							randomFileName = UUID.randomUUID().toString().replace("-", "") + ext;
							
							/* 2. 저장경로에 rename한 이름으로 파일 저장 */
							File storeFile = new File(fileUploadDirectory + "/" + randomFileName);
							
							/* 저장한다 */
							item.write(storeFile);
							/* 파일이름을 DB에 들어갈 randomFileName map에 분기 처리해서 저장 */
							System.out.println("item의 FieldName출력: " + fieldName);
							if(fieldName.equals("promotionPictureLeft")) {
								parameter.put("code", "PR_00001");
								parameter.put("fileName", randomFileName);								
							} else if(fieldName.equals("promotionPictureMiddle")){
								parameter.put("code", "PR_00002");
								parameter.put("fileName", randomFileName);								
							} else {
								parameter.put("code", "PR_00003");
								parameter.put("fileName", randomFileName);								
							}
						}
					} else { // 업로드가 아닌 일반 데이터인 경우(type!="file"), 링크
						
						System.out.println("input 태그의 name:" + item.getFieldName());
						
						/* 기존에 encType이 multipart/form-data가 아닌 경우 getParmeter()로 뽑던 것들 */
						String value = new String(item.getString().getBytes("ISO-8859-1"), encodingType);
						System.out.println("input 태그의 value: " + value);
						
						/* 파일이 아닌 데이터들은 Map에 담는다. */
						parameter.put(item.getFieldName(),
								new String(item.getString().getBytes("ISO-8859-1"), encodingType));	
				
					}
				}
				
				System.out.println("map에담긴 element확인");
				System.out.println(parameter);
				
				/* Map에담긴 정보가 없을 경우 redirect처리 해준다 */
				List<String> valueList = new ArrayList<>();
				Set<String> keys = parameter.keySet();
				Iterator<String> keyIter = keys.iterator();
				while(keyIter.hasNext()) {
					String key = keyIter.next();
					String value = parameter.get(key);
					valueList.add(value);
				}
				if(valueList.get(0) == null || valueList.get(0) == "") {
					alertMessage = "데이터를 업로드해 주세요.";
					session.setAttribute("alertMessage", alertMessage);
					response.sendRedirect(request.getContextPath() + "/pa/PromotionMangeRedirect");
				} else {
					int result = 0;
					result = service.updatePromotion(parameter);
					/* INSERT문 실행시 성공페이지, 미실행시 실패 페이지로 이동한다 */
					if(result > 0) {
						RequestDispatcher rd = request.getRequestDispatcher("/pa/PromotionView");
						rd.forward(request, response);
					} else {
						/* rollback됐을 경우 파일 삭제 */
						if(randomFileName != "" && randomFileName != null) {
							File deleteFile = new File(fileUploadDirectory + "/" + randomFileName);							
						}
						RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-admin/adminRegisterFailurePage.jsp");
						rd.forward(request, response);
					}					
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			System.out.println("encType이 존재하지 않고 넘어왔다.");
			RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-admin/adminUpdateFailurePage.jsp");
			rd.forward(request, response);
		}
	}
}
