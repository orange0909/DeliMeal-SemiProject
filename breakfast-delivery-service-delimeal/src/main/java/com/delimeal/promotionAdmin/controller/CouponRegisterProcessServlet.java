package com.delimeal.promotionAdmin.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionAdmin.model.dto.CouponDTO;
import com.delimeal.promotionAdmin.model.service.RegisterCouponService;

@WebServlet("/pa/CouponRegisterProcess")
public class CouponRegisterProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/* <input>의 value들을 꺼낸다 */
		String couponName = request.getParameter("couponName");
		String couponDiscount = request.getParameter("couponDiscount");
		String couponExpireDate = request.getParameter("couponExpireDate");
		String couponDescriptoin = request.getParameter("couponDescriptoin");
		
		/* dto에 들어갈 자료형으로 변환해준다 */
		System.out.println(couponName);
		System.out.println(couponDiscount);
		int discountInt = Integer.parseInt(couponDiscount);
		double discount = (double)discountInt / 100;
		System.out.println("int형으로 변형된 discount: " + discount);
		System.out.println(couponExpireDate);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date expireDate = java.sql.Date.valueOf(couponExpireDate);
		System.out.println(couponDescriptoin);
		
		/* DTO에 뽑아온 data를 세팅한다 */
		CouponDTO newCoupon = new CouponDTO();
		newCoupon.setName(couponName);
		newCoupon.setDiscountRate(discount);
		newCoupon.setDate(expireDate);
		if(couponDescriptoin.equals("") || couponDescriptoin.equals(null)) {
			newCoupon.setDescription("-");
		} else {
			newCoupon.setDescription(couponDescriptoin);			
		}
		System.out.println(newCoupon);
		
		RegisterCouponService service = new RegisterCouponService();
		int result = service.registerNewCoupon(newCoupon);
		
		if(result > 0) {
			System.out.println("성공!");
			request.setAttribute("couponInfo", newCoupon);
			RequestDispatcher rd = request.getRequestDispatcher("/pa/RegisterNewCouponSuccess");
			rd.forward(request, response);
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/pa/RegisterNewCouponFailure");
			rd.forward(request, response);
		}
		
	}
}
