package com.delimeal.promotionAdmin.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionAdmin.model.service.UpdateBirthdayMenuService;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

@WebServlet("/pa/UpdateSuccessPage")
public class UpdateSuccessPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String code = (String)request.getAttribute("code");
		
		UpdateBirthdayMenuService service = new UpdateBirthdayMenuService();
		BirthDayMenuDTO result = service.selectBirthDayMenuByCode(code);
		
		request.setAttribute("bday", result);
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-admin/adminUpdateSuccessPage.jsp");
		rd.forward(request, response);
	}

}
