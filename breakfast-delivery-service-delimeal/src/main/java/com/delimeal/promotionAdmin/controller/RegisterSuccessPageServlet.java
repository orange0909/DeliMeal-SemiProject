package com.delimeal.promotionAdmin.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionAdmin.model.service.UpdateBirthdayMenuService;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

@WebServlet("/pa/RegisterSuccessPage")
public class RegisterSuccessPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		UpdateBirthdayMenuService service = new UpdateBirthdayMenuService();
		
		/* 새로 추가된 생일메뉴의 메뉴코드를 서브쿼리를 이용해 메뉴정보를 조회해온다 */
		BirthDayMenuDTO result = service.selectMaxBirthdayMenuCode();
		System.out.println(result);
		
		request.setAttribute("bday", result);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-admin/adminRegisterSuccessPage.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
