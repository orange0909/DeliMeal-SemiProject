package com.delimeal.promotionAdmin.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionAdmin.model.service.ManageBirthdayMenuService;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

@WebServlet("/pa/ManageBirthdayMenu")
public class ManageBirthdayMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ManageBirthdayMenuService service = new ManageBirthdayMenuService();
		List<BirthDayMenuDTO> bdayMenu = new ArrayList<>();
		bdayMenu = service.selectAllBirthdayMenu();
		System.out.println(bdayMenu);
		
		request.setAttribute("BirthdayMenu", bdayMenu);
		
		RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/views/promotion-admin/adminManageBirthdayMenu.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
