package com.delimeal.promotionAdmin.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionAdmin.model.dto.IssuedCouponDTO;
import com.delimeal.promotionAdmin.model.service.IssueCouponService;

@WebServlet("/pa/IssueCouponProcess")
public class IssueCouponProcessServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String memberCode = request.getParameter("memberCode");
		String couponNumber = request.getParameter("couponNumber");
		String expireDate = request.getParameter("couponExpireDate");
		
		System.out.println(memberCode);
		System.out.println(couponNumber);
		System.out.println(expireDate);
		
		Map<String,String> data = new HashMap<>();
		data.put("memberCode", memberCode);
		data.put("couponNumber", couponNumber);
		data.put("expireDate", expireDate);
		
		System.out.println(data);
		
		IssueCouponService service = new IssueCouponService();
		int result = service.IssueCouponToMemeber(data);
		
		if(result > 0) {
			System.out.println("쿠폰 발행 성공");
			IssuedCouponDTO issuedData = new IssuedCouponDTO();
			issuedData = service.selectInesertedCouponData(memberCode);
			request.setAttribute("issuedData", issuedData);
			RequestDispatcher rd = request.getRequestDispatcher("/pa/IssueNewCouponSuccess");
			rd.forward(request, response);
			
		} else {
			RequestDispatcher rd = request.getRequestDispatcher("/pa/RegisterNewCouponFailure");
			rd.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
