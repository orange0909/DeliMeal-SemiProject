package com.delimeal.promotionAdmin.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delimeal.promotionAdmin.model.service.UpdateBirthdayMenuService;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;
import com.google.gson.Gson;

@WebServlet("/pa/UpdateBirthdayMenuAjax")
public class UpdateBirthdayMenuAjaxServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String menuCode = request.getParameter("menuCode");
		System.out.println(menuCode);
		
		UpdateBirthdayMenuService service = new UpdateBirthdayMenuService();
		BirthDayMenuDTO bdayMenu = service.selectBirthDayMenuByCode(menuCode);
		
		String jsonString = new Gson().toJson(bdayMenu);
		
		response.setContentType("application/json; charset=UTF-8");
		PrintWriter out = response.getWriter();
		out.print(jsonString);
		out.flush();
		out.close();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

}
