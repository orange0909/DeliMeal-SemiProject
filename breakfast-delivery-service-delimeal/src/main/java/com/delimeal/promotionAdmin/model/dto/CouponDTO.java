package com.delimeal.promotionAdmin.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class CouponDTO implements Serializable{
	private static final long serialVersionUID = -1537492327901568915L;
	
	private String number;
	private String name;
	private double discountRate;
	private java.sql.Date date;
	private String description;

	public CouponDTO() {
	}

	public CouponDTO(String number, String name, double discountRate, Date date, String description) {
		this.number = number;
		this.name = name;
		this.discountRate = discountRate;
		this.date = date;
		this.description = description;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDiscountRate() {
		return discountRate;
	}

	public void setDiscountRate(double discountRate) {
		this.discountRate = discountRate;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "CouponDTO [number=" + number + ", name=" + name + ", discountRate=" + discountRate + ", date=" + date
				+ ", description=" + description + "]";
	}
}