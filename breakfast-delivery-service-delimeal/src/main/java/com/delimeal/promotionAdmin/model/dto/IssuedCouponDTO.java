package com.delimeal.promotionAdmin.model.dto;

import java.io.Serializable;
import java.sql.Date;

public class IssuedCouponDTO implements Serializable{
	private static final long serialVersionUID = -8414745429126125499L;
	
	private String issuedCouponCode;
	private java.sql.Date exprieDate;
	private String isValidate;
	private String memberCode;
	private String couponCode;

	public IssuedCouponDTO() {
		super();
	}

	public IssuedCouponDTO(String issuedCouponCode, Date exprieDate, String isValidate, String memberCode,
			String couponCode) {
		this.issuedCouponCode = issuedCouponCode;
		this.exprieDate = exprieDate;
		this.isValidate = isValidate;
		this.memberCode = memberCode;
		this.couponCode = couponCode;
	}

	public String getIssuedCouponCode() {
		return issuedCouponCode;
	}

	public void setIssuedCouponCode(String issuedCouponCode) {
		this.issuedCouponCode = issuedCouponCode;
	}

	public java.sql.Date getExprieDate() {
		return exprieDate;
	}

	public void setExprieDate(java.sql.Date exprieDate) {
		this.exprieDate = exprieDate;
	}

	public String getIsValidate() {
		return isValidate;
	}

	public void setIsValidate(String isValidate) {
		this.isValidate = isValidate;
	}

	public String getMemberCode() {
		return memberCode;
	}

	public void setMemberCode(String memberCode) {
		this.memberCode = memberCode;
	}

	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "IssuedCouponDTO [issuedCouponCode=" + issuedCouponCode + ", exprieDate=" + exprieDate + ", isValidate="
				+ isValidate + ", memberCode=" + memberCode + ", couponCode=" + couponCode + "]";
	}
	
}