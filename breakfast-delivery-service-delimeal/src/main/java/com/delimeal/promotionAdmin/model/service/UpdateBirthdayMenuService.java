package com.delimeal.promotionAdmin.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionAdmin.model.dao.UpdateBirthdayMenuMapper;
import com.delimeal.promotionCustomer.model.dao.SelectBirthdayMenuMapper;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

public class UpdateBirthdayMenuService {

	public BirthDayMenuDTO selectBirthDayMenuByCode(String menuCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		BirthDayMenuDTO menu = null;
		menu = mapper.selectBirthDayMenuByCode(menuCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return menu;
	}

	public int updateBirthdayMenu(Map<String, String> parameter) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		UpdateBirthdayMenuMapper mapper = sqlSession.getMapper(UpdateBirthdayMenuMapper.class);
		
		int result = 0;
		result = mapper.updateBirthdayMenu(parameter);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		if(result > 0) {
			sqlSession.commit();			
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

	public int RegisterBirthdayMenu(Map<String, String> parameter) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		UpdateBirthdayMenuMapper mapper = sqlSession.getMapper(UpdateBirthdayMenuMapper.class);
		
		int result = 0;
		result = mapper.registerBirthdayMenu(parameter);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		if(result > 0) {
			sqlSession.commit();			
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

	public BirthDayMenuDTO selectMaxBirthdayMenuCode() {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		UpdateBirthdayMenuMapper mapper = sqlSession.getMapper(UpdateBirthdayMenuMapper.class);
		
		BirthDayMenuDTO result = null;
		result = mapper.selectMaxBirthdayMenuCode();
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

	public int UpdateBirthdayMenu(Map<String, String> parameter) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		UpdateBirthdayMenuMapper mapper = sqlSession.getMapper(UpdateBirthdayMenuMapper.class);
		
		int result = 0;
		result = mapper.UpdateBirthdayMenu(parameter);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		if(result > 0) {
			sqlSession.commit();			
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

	public int updatePromotion(Map<String, String> parameter) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		UpdateBirthdayMenuMapper mapper = sqlSession.getMapper(UpdateBirthdayMenuMapper.class);
		
		int result = 0;
		result = mapper.updatePromotion(parameter);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		if(result > 0) {
			sqlSession.commit();			
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

}
