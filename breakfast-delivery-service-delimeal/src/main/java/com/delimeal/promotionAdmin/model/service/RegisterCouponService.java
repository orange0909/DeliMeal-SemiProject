package com.delimeal.promotionAdmin.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionAdmin.model.dao.RegisterCouponMapper;
import com.delimeal.promotionAdmin.model.dto.CouponDTO;

public class RegisterCouponService {

	public int registerNewCoupon(CouponDTO newCoupon) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		RegisterCouponMapper mapper = sqlSession.getMapper(RegisterCouponMapper.class);
		
		int result = 0;
		result = mapper.registerNewCoupon(newCoupon);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

}
