package com.delimeal.promotionAdmin.model.dao;

import java.util.Map;

import com.delimeal.promotionAdmin.model.dto.IssuedCouponDTO;

public interface IssueCouponMapper {

	int IssueCouponToMemeber(Map<String, String> data);

	IssuedCouponDTO selectInesertedCouponData(String memberCode);

}
