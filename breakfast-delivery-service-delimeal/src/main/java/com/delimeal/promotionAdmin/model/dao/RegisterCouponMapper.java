package com.delimeal.promotionAdmin.model.dao;

import com.delimeal.promotionAdmin.model.dto.CouponDTO;

public interface RegisterCouponMapper {

	int registerNewCoupon(CouponDTO newCoupon);

}
