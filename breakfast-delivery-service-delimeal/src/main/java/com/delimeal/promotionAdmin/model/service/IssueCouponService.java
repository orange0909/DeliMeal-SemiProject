package com.delimeal.promotionAdmin.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionAdmin.model.dao.IssueCouponMapper;
import com.delimeal.promotionAdmin.model.dto.IssuedCouponDTO;
import com.delimeal.promotionCustomer.model.dao.SelectBirthdayMenuMapper;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

public class IssueCouponService {

	public int IssueCouponToMemeber(Map<String, String> data) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		IssueCouponMapper mapper = sqlSession.getMapper(IssueCouponMapper.class);
		
		int result = 0;
		result = mapper.IssueCouponToMemeber(data);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		return result;
	}

	public IssuedCouponDTO selectInesertedCouponData(String memberCode) {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		IssueCouponMapper mapper = sqlSession.getMapper(IssueCouponMapper.class);
		
		IssuedCouponDTO result = new IssuedCouponDTO();
		result = mapper.selectInesertedCouponData(memberCode);
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return result;
	}

}
