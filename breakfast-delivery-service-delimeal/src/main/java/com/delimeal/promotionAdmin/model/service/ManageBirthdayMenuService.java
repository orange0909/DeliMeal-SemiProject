package com.delimeal.promotionAdmin.model.service;

import static com.delimeal.common.mybatis.Template.getSqlSession;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.delimeal.promotionCustomer.model.dao.SelectBirthdayMenuMapper;
import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

public class ManageBirthdayMenuService {

	public List<BirthDayMenuDTO> selectAllBirthdayMenu() {
		SqlSession sqlSession = getSqlSession();
		System.out.println("Service객체에서 DAO 가기전");
		SelectBirthdayMenuMapper mapper = sqlSession.getMapper(SelectBirthdayMenuMapper.class);
		
		List<BirthDayMenuDTO> menuList = new ArrayList<>();
		menuList = mapper.selectMenu();
		System.out.println("Service객체에서 DAO 간후" + mapper);
		
		sqlSession.close();
		
		return menuList;
	}

}
