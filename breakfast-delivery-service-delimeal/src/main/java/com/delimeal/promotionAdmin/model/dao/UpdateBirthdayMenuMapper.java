package com.delimeal.promotionAdmin.model.dao;

import java.util.Map;

import com.delimeal.promotionCustomer.model.dto.BirthDayMenuDTO;

public interface UpdateBirthdayMenuMapper {

	int updateBirthdayMenu(Map<String, String> parameter);

	int registerBirthdayMenu(Map<String, String> parameter);
	
	BirthDayMenuDTO selectMaxBirthdayMenuCode();

	int UpdateBirthdayMenu(Map<String, String> parameter);

	int updatePromotion(Map<String, String> parameter);

}
