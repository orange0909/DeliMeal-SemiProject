window.onload = function() {
    const addressList = document.querySelectorAll('#addressList .address-list-tile');
    
    addressList.forEach((element) => {
        /* 버튼 element 변수에 할당 */
        const selectBtn = element.querySelector('#selectAddr');
        const modifyBtn = element.querySelector('#modifyBtn');
        const deleteBtn = element.querySelector('#deleteBtn');

        /* 각종 데이터 변수에 할당 */
        const addrInfo = {
            addrCode : element.querySelector('#addressCode').innerText,
            addrName : element.querySelector('#addressName').innerText,
            addrZipcode : element.querySelector('#zipcode').innerText,
            addr : element.querySelector('#address').innerText.split(', ')[0],
            addrDetail : element.querySelector('#address').innerText.split(', ')[1],
            deliveryReq : element.querySelector('#deliveryReq').innerText
        }

        /* 선택 버튼 누르면 창 꺼지고 필요한 값들을 parent 창에 넘겨줌 */
        selectBtn.onclick = function() {
            window.opener.updateDeliveryAddress(addrInfo);
            window.close();
        };

        /* 수정 버튼 누르면 위쪽의 수정용 패널로 값이 옮겨짐 */
        modifyBtn.onclick = function() {
            updateEditPanel(addrInfo);
            document.querySelector('#saveBtn').innerText = '저장';
        }

        /* 삭제 버튼 누르면 삭제됨 */
        deleteBtn.onclick = function() {
            deleteAddress(addrInfo);
        }
    });
}

/* 수정용 패널에 일괄적으로 값을 넣는 함수 */
function updateEditPanel(addrInfo) {
    const editPanel = {
        addrCodeBox: document.querySelector('#addressEditBox #addrCode'),
        addrNameBox: document.querySelector('#addressEditBox #addrName'),
        addrZipcodeBox: document.querySelector('#addressEditBox #addrZipcode'),
        addrBox: document.querySelector('#addressEditBox #addr'),
        addrDetailBox: document.querySelector('#addressEditBox #addrDetail'),
        deliveryReqBox: document.querySelector('#addressEditBox #deliveryReq')
    };

    if(addrInfo.addrCode != null) {
        editPanel.addrCodeBox.value = addrInfo.addrCode;
    }
    if(addrInfo.addrName != null) {
        editPanel.addrNameBox.value = addrInfo.addrName;
    }
    if(addrInfo.addrZipcode != null) {
        editPanel.addrZipcodeBox.value = addrInfo.addrZipcode;
    }
    if(addrInfo.addr != null) {
        editPanel.addrBox.value = addrInfo.addr;
    }
    if(addrInfo.addrDetail != null) {
        editPanel.addrDetailBox.value = addrInfo.addrDetail;
    }
    if(addrInfo.deliveryReq != null) {
        editPanel.deliveryReqBox.value = addrInfo.deliveryReq;
    }
}

function save() {
    const code = document.querySelector('#addressEditBox #addrCode').value;
    if(code != null && code != '') {
        modifyAddress();
    } else {
        insertAddress();
    }
}

/*
    배송지 업데이트 AJAX code

    addressInfo
    addrCode: 주소 테이블의 PK인 코드
    addrZipcode: 우편번호
    addr: 주소1
    addrDetail: 주소2 (상세주소)
*/
function modifyAddress() {
    $.ajax({
        url: '/delimeal/member/manageAddress',
        type: 'POST',
        data: {
            dmlType: 'update',
            addrCode: document.querySelector('#addressEditBox #addrCode').value,
            addrName: document.querySelector('#addressEditBox #addrName').value,
            addrZipcode: document.querySelector('#addressEditBox #addrZipcode').value,
            addr: document.querySelector('#addressEditBox #addr').value,
            addrDetail: document.querySelector('#addressEditBox #addrDetail').value,
            deliveryReq: document.querySelector('#addressEditBox #deliveryReq').value
        },
        success: function (data) {
            if(data == 'success') {
                alert('배송지 수정에 성공했습니다.');
                location.reload();
            } else {
                alert('배송지 수정에 실패했습니다.')
            }
        },
        error: function() {
            alert('배송지 수정에 실패했습니다.')
        }
    });
}

/*
    배송지 추가 AJAX code

    addressInfo
    addrCode: 주소 테이블의 PK인 코드
    addrZipcode: 우편번호
    addr: 주소1
    addrDetail: 주소2 (상세주소)
*/
function insertAddress() {
    $.ajax({
        url: '/delimeal/member/manageAddress',
        type: 'POST',
        data: {
            dmlType: 'insert',
            addrName: document.querySelector('#addressEditBox #addrName').value,
            addrZipcode: document.querySelector('#addressEditBox #addrZipcode').value,
            addr: document.querySelector('#addressEditBox #addr').value,
            addrDetail: document.querySelector('#addressEditBox #addrDetail').value,
            deliveryReq: document.querySelector('#addressEditBox #deliveryReq').value
        },
        success: function (data) {
            if(data == 'success') {
                alert('배송지 추가에 성공했습니다.');
                location.reload();
            } else {
                alert('배송지 추가에 실패했습니다.')
            }
        },
        error: function() {
            alert('배송지 추가에 실패했습니다.')
        }
    });
}

/*
    배송지 삭제 AJAX code

    addressInfo
    addrCode: 주소 테이블의 PK인 코드
    addrZipcode: 우편번호
    addr: 주소1
    addrDetail: 주소2 (상세주소)
*/
function deleteAddress(addrInfo) {
    $.ajax({
        url: '/delimeal/member/manageAddress',
        type: 'POST',
        data: {
            dmlType: 'delete',
            addrCode: addrInfo.addrCode
        },
        success: function (data) {
            if(data == 'success') {
                alert('배송지 삭제에 성공했습니다.');
                location.reload();
            } else {
                alert('배송지 삭제에 실패했습니다.')
            }
        },
        error: function() {
            alert('배송지 삭제에 실패했습니다.')
        }
    });
}

function openFindPostCode() {
    new daum.Postcode({
        oncomplete: function(data) {
            const addrInfo = {
                addrZipcode: data.zonecode,
                addr: data.address
            }

            updateEditPanel(addrInfo);
        }
    }).open();
}