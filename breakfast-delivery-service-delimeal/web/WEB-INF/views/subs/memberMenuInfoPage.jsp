<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMenu.css">
	
	<title>메뉴 정보</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
	<div id="content">
		<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
		<jsp:include page="memberAside.jsp"/>
		<%-- 섹션 --%>
		<section>
			<article>
				<%-- 타이틀 --%>
				<h2 id="title">메뉴 정보</h2>
				<div>
					<div id="mealInfo">
						<div id="description">
							<div id="mealImage" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ requestScope.meal.mealAttachName }')"></div>
							<div id="mealDescBox">
								<div>
									<span id="categoryName">${ requestScope.category.categoryName }</span>
									<span id="mealName">${ requestScope.meal.mealName }</span>
									<span id="mealDesc">${ requestScope.meal.mealDetail }</span>
								</div>
								<hr class="dashed">
								<div id="buyOptions">
									<div id=kcalBox>
										<div>총 칼로리</div>
										<div id="kcal"><fmt:formatNumber value="${ requestScope.meal.mealCalinfo }" type="number"/> kcal</div>
									</div>
									<div id="btnBox">
										<button class="btn btn-outline-success">주문하러가기</button>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div id="menuList">
							<c:forEach var="list" items="${ requestScope.menuList }">
								<div class="tile">
									<div id="menuImage" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ list.menuAttachName }')"></div>
									<div id="menuDescBox">
										<span id="menuName">${ list.menuName }</span>
										<div id="kcalBox">
											<span class="desc-title">칼로리</span>
											<span id="menuKcal"><fmt:formatNumber value="${ list.menuCalinfo }" type="number"/> kcal</span>
										</div>
										<div id="nutritionBox">
											<span class="desc-title">영양 정보</span>
											<span id="nutrition">${ list.menuNutrient }</span>
										</div>
										<div id="allergyBox">
											<span class="desc-title">알러지 정보</span>
											<span id="allergy">${ list.menuAllergie }</span>
										</div>
									</div>
								</div>
							</c:forEach>
						</div>
					</div>
				</div>
			</article>
		</section>
	</div>
		
</body>

</html>