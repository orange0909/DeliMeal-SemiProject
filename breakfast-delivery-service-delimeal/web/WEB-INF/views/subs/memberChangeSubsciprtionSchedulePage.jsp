<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	<style>
        #currentSubscribeView{
            width: 100%;
            display: flex;
            flex-direction: column;
            color: #8E8E8E;
        }
        .subsInfo{
            display: flex;
            flex-direction: column;
            justify-content: center;
            background-color: #BAEF61;
            margin-top: 30px;
            border: 1.5px solid rgba(132, 192, 4, 0.85);
            padding: 20px 15px;
            width: 80%;
            gap: 5px;
        }
	</style>
	<meta charset="UTF-8">
	<title>구독 일정 변경</title>
</head>
<body>
	<!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
    <div id="content">
    	<!-- 사이드바 -->
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
	    <!-- 섹션 -->
        <section>
            <article>
                <h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">구독정보 조회</h5><div class="sub-title-tail"></div>
                </div>
                <form action="${pageContext.servletContext.contextPath}/subs/ChangeSubscriptionSchedule">
                <div id="currentSubscribeView">
                    <h7 id="viewTitle">구독정보</h7>
                    <div id="SubscribeView"></div>
                        <c:forEach var="subs" items="${subscriptionInfos}" varStatus="st">
                            <div id="subsInfo${st.index}" class="subsInfo">
                                <table>
                                  <tr>
                                      <td>
                                          <div id="CurrentSubsCategoryName${st.index}" >현재 구독중인 식단: ${ subs.mealCategoryName }</div>
                                        </td>
                                      <td rowspan="2">
                                          <div class="radio"><input type="radio" id="${ subs.subsCode }" name="currnetsubsCode" value="${ subs.subsCode }" ></div>
                                        </td>
                                  </tr>
                                  <tr>
                                      <td>
                                        <div id="BirthdayMenu_name${st.index}" class="MenuName">구독기간: ${ subs.startDate } ~ ${ subs.expireDate }</div>
                                      </td>
                                  </tr>  
                                </table>
                            </div>
                        </c:forEach>
                </div>
                    <label for="changeDate">날짜선택: </label>
                    <input type="date" id="changeDate" name="changeDate">
                    <br>
                    <button>일정변경</button>
                </form>
            </article>
        </section>
     </div>
</body>
</html>