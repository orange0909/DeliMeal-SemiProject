<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/pay.css">
    <%-- 개인 페이지 --%>
    
    <title>식단 카테고리</title>
</head>

<body>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content" class="no-sidebar">
		<section>
			<article>
			
			</article>
		</section>
	</div>
</body>

</html>