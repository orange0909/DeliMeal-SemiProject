<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html>

<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet"
	href="${ pageContext.servletContext.contextPath }/resources/css/common.css">

<style>
.mealImage {
	margin-left: 4%;
	width: 35%;
	height: 320px;
	float: left;
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center;
}



.mealInfo {
	float: left;
	width: 55%;
	margin-left: 6%;
}

#mealName {
	font-style: normal;
	font-weight: 200;
	font-size: 26px;
	color: #3c8028;
}

.buttonBox {
	/* margin-left: 55%; */
	/* background: blue; */
	width: 30%;
	height: 100px;
}

.bt {
	background: #98bc8d;
	border-style: none;
	width: 9%;
	height: 40px;
	color: white;
	font-size: 15px;
}

.modifyBt {
	margin-left: 78.5%;
}

.deleteBt {
	margin-left: 2%;
}

.meal-info-cell {
	display: flex;
	flex-direction: row;
	width: 100%;
}

.meal-info-cell h4 {
	width: 220px;
	font-size: 19px;
	margin-right: 20px;
}

.meal-info-cell span {
	width: 100%;
	text-overflow: ellipsis;
	overflow: hidden;
}
/*--- 팝업 */
#popupWrap {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 620px;
	height: 650px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 17%;
	left: 33%;
	z-index: 99;
	display: none;
}

#popupWrap pre {
	font-family: 'Noto Sans KR', sans-serif;
}

#popupTitle {
	font-style: normal;
	font-weight: 600;
	font-size: 30px;
	color: #98bc8d;
	margin-top: 10px;
	margin-left: 4.04%;
}

#textBox {
	/* background: red; */
	line-height: 50px;
	margin-top: 3px;
	padding-left: 6%;
	font-size: 17px;
	font-weight: 400;
}

#name, #categoryCodeSelect{
	width: 180px;
	height: 25px;
	
}

#textBox input, #calinfo, #image, #categoryCodeSelect {
	font-size: 13px;
}

#textBox textarea {
	line-height: 20px;
	font-size: 13px;
}

#textBox pre {
	float: left;
}

#intro {
	resize: none;
}

#cancel, #modify {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 20px;
	cursor: pointer;
}

#cancel {
	margin-left: 75%;
	margin-right: 1%;
}

/* 성공---- */
#success, #fail {
	width: 500px;
	height: 210px;
	top: 17%;
	left: 38%;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	z-index: 99;
	display: none;
}

#result {
	margin-left: 44%;
	width: 60px;
	height: 60px;
}

#popupMessage {
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
}

#popupMessage2 {
	text-align: center;
	color: #e60000;
	font-weight: 600;
	font-size: 19px;
}

#success button, #fail button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 1px;
	cursor: pointer;
}

#confirm {
	margin-left: 77.7%;
}

/* 카테고리 간편삭제 */
#deletePopupWrap {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#deletePopupWrap>#popupMessage {
	margin-top: 27px;
	text-align: center;
	color: #3C8028;
	font-weight: 600;
	font-size: 19px;
}

#deletePopupWrap>#textBox {
	margin-left: 10%;
	line-height: normal;
}

#deletePopupWrap>#textBox>#text {
	border: 1px solid #dfdfdf;
	width: 50%;
	height: 28px;
	padding-left: 1.5%;
}

#deletePopupWrap>#textBox>#text::placeholder {
	font-weight: 400;
	font-size: 13px;
	color: #dfdfdf;
}

#deletePopupWrap>button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 34px;
	cursor: pointer;
}

#deletePopupWrap>#cancel {
	margin-left: 64%;
	margin-right: 1%;
}

#result {
	margin-left: 44%;
	width: 60px;
	height: 60px;
}
/* 삭제성공 */
#deleteSuccess {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#deleteSuccess>button, #deleteFail>button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 1px;
	cursor: pointer;
}

/* 삭제실패 */
#deleteFail {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#fail #popupMessage, #deleteFail #popupMessage {
	color: red;
}

#mask {
	position: absolute;
	z-index: 20;
	width: 100%;
	background-color: #000;
	display: none;
	left: 0;
	top: 0;
}

#popupTitle {
	font-style: normal;
	font-weight: 600;
	font-size: 30px;
	color: #98bc8d;
	margin-top: 10px;
	margin-left: 4.04%;
}

#popupMessage {
	margin-top: 10px;
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
}

#deleteSuccess>#popupMessage, #deleteFail>#popupMessage {
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
	margin-top: 0px;
}

#fail #popupMessage, #deleteFail #popupMessage {
	color:red;
}
#confirm {
	margin-left: 77.7%;
}

#textBox {
	/* background: red; */
	line-height: 45px;
	margin-top: 3px;
	padding-left: 6%;
	font-size: 16px;
	font-weight: 400;
}

#textBox pre {
	float: left;
}

#textBox pre:nth-of-type(3) {
	font-size: 14px;
}

#textBox #menuVal {
	margin-right: 4px;
	font-size: 13px;
	color: #3c8028;
}

#addMenu, #clearMenu {
	width: 60px;
	height: 30px;
	background: #98bc8d;
	color: white;
	border-radius: 30px;
	border: 5px;
	border-color: white;
	border-style: double;
	cursor: pointer;
	line-height: normal;
	font-size: 13px;
}

#day {
	height: 25px;
}

#intro {
	resize: none;
}

#cancel, #modify {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 20px;
	cursor: pointer;
}

#cancel {
	margin-left: 75%;
	margin-right: 1%;
}

#menuName {
	height: 25px;
}

/* 메뉴들 */

#menuList {
	width: 100%;
	

}
#menu {
	width:100%;
	height: auto;
	margin-left: 4%;
	margin-bottom:40px;

}
#menuImg {
	width:180px;
    height:180px;
    /* background:red; */
	float: left;
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center;
 
}



#menuInfo {
	width:calc(100% - 350px);
	float: left;
	padding-left: 15px;
    

}

.menu-info-cell {
	display: flex;
	flex-direction: row;
	width: 100%;
}

.menu-info-cell h4 {
	width: 220px;
	font-size:15px;
	margin-right:20px;

	
}

.menu-info-cell span {
	width: 100%;
	text-overflow: ellipsis;
    overflow: hidden;
	font-size:15px;

}
#menuName{
	font-size: 20px;
	font-weight: 500;

}
#menuText span{
	font-size:14px;
	margin-right:5px;
}
</style>
<title>Insert title here</title>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
	<%-- 사이드바 + 섹션 --%>
	<div id="content">
		<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
		<jsp:include page="/WEB-INF/views/subs/adminAside.jsp" />
		<%-- 섹션 --%>
		<section>
			<article>
				<h2 id="title">식단 정보</h2>
				<div>
					<div class="mealImage" style="background-image:url('${ pageContext.servletContext.contextPath }/resources/food/${ requestScope.meal.mealAttachName }') ;">
					</div>
					<div class="mealInfo">
						<div id="mealName">
							<c:out value="${ requestScope.meal.mealName }" />
						</div>
						<hr>
						<div class="meal-info-cell">
							<h4>식단 소개</h4>
							<span id="mealDetail"><c:out
									value="${ requestScope.meal.mealDetail }" /></span>
						</div>
						<hr>
						<div class="meal-info-cell">
							<h4>총 칼로리</h4>
							<span id="mealCalinfo"><c:out
									value="${ requestScope.meal.mealCalinfo }" />kcal</span>
						</div>
						<hr>
						<div class="meal-info-cell">
							<h4>식단날짜</h4>
							<span id="date"><c:out value="${ date }" /></span>
						</div>
					</div>
					<button class="bt modifyBt" name="modifyBt">수정</button>
					<button class="bt deleteBt" name="deleteBt">삭제</button>
				</div>
				<hr>
				<!--  메뉴 리스트 -->
				<div id="menuList">
					<c:forEach var="menu" items="${ requestScope.menu }">
                    <div id="menu">
                        <div id="menuImg" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ menu.menuAttachName }');"></div>
                        <div id="menuInfo">
							<div id="menuName"><c:out value="${ menu.menuName }"/></div>
							<br>
							<div class="menu-info-cell">
								<h4 style="color:red;">알레르기 주의성분</h4>
								<span class="menuAllergie"><c:out value="${ menu.menuAllergie }"/></span>
							</div>
							<br>
							<div class="menu-info-cell">
							<h4>칼로리</h4><span id="menuCalinfo"><c:out value="${ menu.menuCalinfo }"/>kcal</span>
							</div>
							<br>
							<div class="menu-info-cell">
							<h4>영양정보</h4><span id="menuNutrient"><c:out value="${ menu.menuNutrient }"/></span>
							</div>
                        </div>
						<br clear="both">
                    </div>
                    </c:forEach>
				</div>
			</article>
		</section>
	</div>
	<div id="mask"></div>
	<!-- 식단 삭제 -->
	<div id="deletePopupWrap">
		<div id="popupTitle">식단 삭제</div>
		<div id="popupMessage">
			정말
			<mark><c:out value="${ requestScope.meal.mealName }"/></mark>
			을 삭제하시겠습니까?
		</div>
		<button id="cancel" class="deleteCancel">취소</button>
		<button id="next" class="delete">삭제</button>
	</div>
	<div id="deleteSuccess">
		<div id="popupTitle">식단 삭제</div>
		<img id="result"
			src="${ pageContext.servletContext.contextPath }/resources/image/success.png"
			alt="">
		<div id="popupMessage">식단 삭제 성공!!</div>
		<button id="confirm" class="successDeleteConfirm">확인</button>
	</div>
	<div id="deleteFail">
		<div id="popupTitle">식단 삭제</div>
		<img id="result"
			src="${ pageContext.servletContext.contextPath }/resources/image/failure.png"
			alt="">
		<div id="popupMessage">식단 삭제 실패!!</div>
		<button id="confirm" class="failDeleteConfirm">확인</button>
	</div>
	<!-- 식단 수정!!! -->
	<div id="popupWrap">
		<div id="popupTitle">식단 수정</div>
		<div id="textBox">
			<pre>수정할 식단의 이름을 입력하세요 :          </pre>
			<input id="name" value="${ requestScope.meal.mealName }" type="text">
			<br clear="both">
			<pre>수정할 식단의 메뉴들을 입력하세요 :       </pre>
			<input list="menuNameList" id="menuName" name="menuName"> 
			<button id="addMenu" onclick="add()">추가</button>
			<button id="clearMenu" onclick="reset()">초기화</button> 
			<br clear="both">
			<datalist id="menuNameList">
				<c:forEach var="menu" items="${ requestScope.menuList }">
					<option value="${ menu }">
				</c:forEach>
			</datalist>
			<pre>추가된 메뉴: </pre> <div id="menuText"></div>
			<br clear="both">
			<pre>수정할 식단의 소개를 입력하세요:           </pre> 
			<textarea name="intro" id="intro" cols="26" rows="5"></textarea>
			<br clear="both">
			<pre>수정할 식단의 날짜를 입력하세요:           </pre>
			<input id="day" type="date">
			<br clear="both">
			<pre>수정할 식단의 카테고리를 선택하세요     </pre>
			<select name="categoryCodeSelect" id="categoryCodeSelect">
				<c:forEach var="category" items="${ requestScope.categoryList }">
					<option class="categoryNameValue">${ category.categoryName }</option>
				</c:forEach>
			</select>
			<br clear="both"> 
			<pre>수정할 식단의 사진을 선택하세요 :          </pre>
			<input id="image" type="file" name="singlefile">
		</div>
		<button id="cancel" class="updateCancel">취소</button>
		<button id="modify">수정</button>
	</div>
	<div id="success">
		<div id="popupTitle">식단 수정</div>
		<img id="result"
			src="${ pageContext.servletContext.contextPath }/resources/image/success.png"
			alt="">
		<div id="popupMessage">식단 수정 성공!!</div>
		<button id="confirm" class="successInsertConfirm">확인</button>
	</div>
	<div id="fail">
		<div id="popupTitle">식단 수정</div>
		<img id="result"
			src="${ pageContext.servletContext.contextPath }/resources/image/failure.png"
			alt="">
		<div id="popupMessage">식단 수정 실패!!</div>
		<button id="confirm" class="failInsertConfirm">확인</button>
	</div>
</body>
<script>
	// 메뉴 추가하기 
	var textArray = [];
	
	function add() {
		const list = document.querySelector('#textBox div#menuText');
		const addText = $("#textBox #menuName").val();
	
		textArray.push(addText);
	
		const element = document.createElement('span');
		element.innerText = addText;
	
		list.appendChild(element);
		document.querySelector('#textBox input#menuName').value = '';
	}
	
    function reset() {
        const list = document.querySelector('div#menuText');

        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }

        textArray = [];
    }

</script>
<script>
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();

	function popupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#popupWrap").show();

	}

	$(".modifyBt").click(function(e) {
		e.preventDefault();
		popupWrapByMask();
	});

	// 취소버튼
	$(".updateCancel").click(function(e) {
		e.preventDefault();
		$("#mask, #popupWrap").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$("#popupWrap").hide();

	});

	/* ------------성공 */
	function successPopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#popupWrap").hide();
		$("#success").show();

	}
	$(".successInsertConfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #success").hide();

	});

	$("#mask").click(function() {
		$(this).hide();
		$("#success").hide();

	});

	/* ------------실패 */
	function failPopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#popupWrap").hide();
		$("#fail").show();

	}

	$(".failInsertConfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #fail").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$("#fail").hide();

	});

	/* 식단 삭제의 경우 */
	function deletePopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#deletePopupWrap").show();

	}

	// 삭제 버튼을 눌렀을때
	$(".deleteBt").click(function(e) {
		e.preventDefault();
		deletePopupWrapByMask();
	});

	// 취소 버튼을 눌렀을때
	$(".deleteCancel").click(function(e) {
		e.preventDefault();
		$(".deleteText").val('');
		$("#mask, #deletePopupWrap").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$(".deleteText").val('');
		$("#deletePopupWrap").hide();

	});

	/* ------------성공 */
	function successDeletePopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#deletePopupWrap").hide();
		$("#deleteSuccess").show();

	}
	$(".successDeleteConfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #deleteSuccess").hide();

	});

	$("#mask").click(function() {
		$(this).hide();
		$("#deleteSuccess").hide();

	});

	/* ------------실패 */
	function failDeletePopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#deletePopupWrap").hide();
		$("#deleteFail").show();

	}

	$(".failDeleteConfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #deleteFail").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$("#deleteFail").hide();

	});
</script>
<script>
// 식단 등록 눌렀을때
$("#modify").click(function() {
	const formData = new FormData();
	
	const name = $("#name").val();
	const intro = $("#intro").val();
	const date = $("#day").val();
	const image = $("#image")[0].files[0];
	const categoryName = $(".categoryNameValue").val();
	
	formData.append("name", name);
	formData.append("intro", intro);
	formData.append("date", date);
	formData.append("image", image);
	formData.append("categoryName", categoryName);
	formData.append("code", "${ requestScope.meal.mealCode }");
	formData.append("textArray", textArray.toString()); 

	$.ajax({
		url : "${pageContext.servletContext.contextPath}/admin/mealUpdate",
		type : "post",
		processData: false, 
		contentType: false, 
		data :  formData,
		success : function(data) {
			console.log(data);
			if(data == "success"){
				successPopupWrapByMask();
			} else if(data == "fail"){
				failPopupWrapByMask();
			}
		},
		error : function(request, status) {
			failPopupWrapByMask();
		}
	});
	
});

	
	$(".delete").click(function() {
		const name = "${ requestScope.meal.mealName }";
		const code = "${ requestScope.meal.mealCode }";

		if (name == null) {
			failDeletePopupWrapByMask();
		} else {
			$.ajax({
				url : "${pageContext.servletContext.contextPath}/admin/mealDelete",
				type : "post",
				data : {
						name : name,
						code : code
				},
				success : function(data) {
					console.log(data);
					if (data == "success") {
						successDeletePopupWrapByMask();
					} else if (data == "fail") {
						failDeletePopupWrapByMask();
					}
				},
				error : function(request, status) {
					failDeletePopupWrapByMask();
				}
			});
		}
	});
</script>
</html>