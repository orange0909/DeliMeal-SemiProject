<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	
	<title>전체 식단</title>
</head>

<body>
	<!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
	<div id="content">
		<!-- 사이드바 -->
		<jsp:include page="memberAside.jsp"/>
		<!-- 섹션 -->
		<section>
			<article>
				<c:choose>
					<c:when test="${ requestScope.by == 'all' }">
						<h2 id="title">전체 식단</h2>
					</c:when>
					<c:when test="${ requestScope.by == 'popularity' }">
						<h2 id="title">인기 식단</h2>
					</c:when>
					<c:when test="${ requestScope.by == 'healthy' }">
						<h2 id="title">건강 식단</h2>
					</c:when>
					<c:when test="${ requestScope.by == 'premium' }">
						<h2 id="title">프리미엄 식단</h2>
					</c:when>
				</c:choose>
				<div>
					<%-- 식단 카테고리 리스트 시작 --%>
					<div id="categoryGallery">
						<c:forEach var="list" items="${ requestScope.categoryList }">
							<div class="tile">
								<a href="meal/info?pid=${ list.categoryCode }">
									<%-- 식단 카테고리의 이미지 --%>
									<div class="image-box" style="background-image: url(${ pageContext.servletContext.contextPath }/resources/food/${ list.categoryAttachName })"></div>
									<%-- 식단 카테고리 정보랑 버튼 --%>
									<div id="mealInfoAndBuyBtn">
										<div id="mealInfo" class="info-box">
											<span id="mealName">${ list.categoryName }</span>
											<span id="mealDesc">${ list.categoryDetail }</span>
											<span id="mealPrice">
												<%-- jstl 포멧 라이브러리 사용해서 , 추가 --%>
												<fmt:formatNumber value="${ list.categoryPrice }" type="number"/>원 / 1주
											</span>
										</div>
										<button id="btnBuyMeal" class="btn btn-normal"></button>
									</div>
									<%-- 인기 식단 페이지를 요청했다면 구독 수를 출력함 --%>
									<c:if test="${ requestScope.by == 'popularity' }">
										<span id="mealSubs">구독 수 : <span id="numberOfSubs">${ list.categorySubs }</span>명</span>
									</c:if>
								</a>
							</div>
						</c:forEach>
					</div>
				</div>
			</article>
		</section>
	</div>
		
</body>

</html>