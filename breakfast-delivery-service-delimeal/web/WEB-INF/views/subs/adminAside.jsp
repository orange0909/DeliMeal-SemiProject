<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<aside>
		<!-- 사이드바 타이틀 -->
		<div id="asideTitle">식단관리</div>
		<ul class="aside-menu-box">
			<!-- 사이드바 메뉴 1 -->
			<li class="aside-menu">
				<div class="aside-menu-subbox">
					<div class="aside-menu-name">
						<div class="selected-menu"></div>
						<span>식단 관리</span>
					</div>
					<div class="toggle-btn"></div>
				</div> 
				<!-- 사이드바 서브메뉴 (펼치기/접기) -->
				<ul class="aside-submenu">
					<li onclick="meal()">식단 관리</li>
					<li onclick="menu()">메뉴 관리</li>
					<li onclick="category()">카테고리 관리</li>
				</ul>
			</li>
		</ul>
	</aside>

<script>
	// 서브 메뉴토글
	const toggleElements = document.getElementsByClassName("aside-menu-subbox");

	for (let i = 0; i < toggleElements.length; i++) {
		const element = toggleElements[i];

		element.addEventListener("click", function() {
			element.children[1].classList.toggle('active');

			if (element.nextElementSibling.style.maxHeight){
				element.nextElementSibling.style.maxHeight = null;
			} else {
				element.nextElementSibling.style.maxHeight = element.nextElementSibling.scrollHeight + "px";
			}
		});
	}

	// 현재 페이지의 intent에 의해 사이드바의 active 상태 변경
	// intent, contextPath는 header에서 이미 선언해둠

	// main: 사이드바 메뉴 순번
	// sub: 사이드바 서브메뉴 순번
	let main, sub;
	console.log(intent);
	switch (intent) {
		case '/admin/category':
			main = 1; sub = 3; break;
		case '/admin/menu':
			main = 1; sub = 2; break;
		case '/admin/meal':
			main = 1; sub = 1; break;
	}
	
	if(main && sub) {
		const preOpenedElement = document.querySelector('.aside-menu-box :nth-child(' + main + ') .aside-submenu');
		const preSelectedMenu = document.querySelector('.aside-menu');
		preSelectedMenu.classList.add('active');
		const preSelectedSubMenu = document.querySelector('.aside-menu-box :nth-child(' + main + ') .aside-submenu :nth-child(' + sub + ')');
		preSelectedSubMenu.classList.add('active');
		preOpenedElement.style.maxHeight = preOpenedElement.scrollHeight + "px";
	}
	
	function meal() {
		location.href = "${ pageContext.servletContext.contextPath }/admin/meal";
	}
	function menu() {
		location.href = "${ pageContext.servletContext.contextPath }/admin/menu";
	}
	function category() {
		location.href = "${ pageContext.servletContext.contextPath }/admin/category";
	}
</script>
</body>
</html>