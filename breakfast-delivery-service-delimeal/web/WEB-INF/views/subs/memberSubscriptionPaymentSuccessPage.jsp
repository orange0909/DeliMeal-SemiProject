<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMenu.css">
    <meta charset="UTF-8">
    <title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
        <jsp:include page="/WEB-INF/views/template/aside.jsp"/>
        <section>
            <article>
	            <h2 id="title">구독 서비스</h2>
	                <div id="subTitle">
	                    <h5 class="sub-title">구독 중인 서비스 변경</h5><div class="sub-title-tail"></div>
	                </div>
            </article>
        </section>
    </div>
</body>
</html>