<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <style>
        #mainContent{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
    </style>
	<meta charset="UTF-8">
	<title>환불 처리 페이지</title>
</head>
<body>
	<!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
    <div id="content">
    	<!-- 사이드바 -->
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
	    <!-- 섹션 -->
        <section>
            <article>
                <h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">환불 처리</h5><div class="sub-title-tail"></div>
                </div>
                    <div id="mainContent">
                        <div><h1>환불 완료!</h1></div>
                        <div><h2>환불한 서비스 이름: ${ currentServiceName }</h2></div>
                        <div><h2>환불받을 가격: ${ refundPrice } 원</h2></div>
                    </div>
            </article>
       </section>
</body>
</html>