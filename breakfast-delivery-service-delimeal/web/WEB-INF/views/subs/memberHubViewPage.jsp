<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=844d689934cdd325da70129eacc33f04&libraries=services,clusterer"></script>
    <title>허브 위치 조회</title>
</head>

<body>
	<!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
    <div id="content">
    	<!-- 사이드바 -->
        <!-- commit용 주석-->
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
	    <!-- 섹션 -->
	    <section>
            <article>
                <h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">허브위치 조회</h5><div class="sub-title-tail"></div>
                </div>
                <div id="currentPage">
                	<h5 class="current-page">허브위치 조회</h5>
                </div>
                <div id="map" style="width:100%;height:350px;">
                </div> 
                <script>
                        var mapContainer = document.getElementById('map'), // 지도를 표시할 div     
                            mapOption = {         
                                center: new kakao.maps.LatLng( 37.5666805, 126.9784147), // 지도의 중심좌표        
                                level: 9 // 지도의 확대 레벨    
                            };   
                        // 지도를 표시할 div와  지도 옵션으로  지도를 생성합니다
                        var map = new kakao.maps.Map(mapContainer, mapOption);
                        
                        // 마커를 표시할 위치와 title 객체 배열입니다 
                        var positions = [
                            {
                                title: '광진허브', 
                                latlng: new kakao.maps.LatLng(37.5433578, 127.0948014)
                            },
                            {
                                title: '서대문허브', 
                                latlng: new kakao.maps.LatLng(37.5811738, 126.897814)
                            },
                            {
                                title: '강서허브', 
                                latlng: new kakao.maps.LatLng(37.5213275, 126.8527396)
                            },
                            {
                                title: '강남허브',
                                latlng: new kakao.maps.LatLng(37.4801831, 127.1048388)
                            },
                            {
                                title: '노도강허브',
                                latlng: new kakao.maps.LatLng(37.6550339, 127.0500594)
                            },
                            {
                                title: '성북허브',
                                latlng: new kakao.maps.LatLng(37.6201636, 127.0116818)
                            }
                        ];

                        // 마커 이미지의 이미지 주소입니다
                        var imageSrc = "https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png";
                        
                        for (var i = 0; i < positions.length; i ++) {
    
                        // 마커 이미지의 이미지 크기 입니다
                        var imageSize = new kakao.maps.Size(24, 35); 
                        
                        // 마커 이미지를 생성합니다    
                        var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize); 
                        
                        // 마커를 생성합니다
                        var marker = new kakao.maps.Marker({
                            map: map, // 마커를 표시할 지도
                            position: positions[i].latlng, // 마커를 표시할 위치
                            title : positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
                            image : markerImage // 마커 이미지 
                        });
                    }

                </script>
            </article>
	    </section>
    </div>
        
</body>

</html>