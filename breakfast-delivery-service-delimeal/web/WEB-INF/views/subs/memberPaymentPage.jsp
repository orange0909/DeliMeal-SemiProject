<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	<%-- 개인 페이지 --%>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/pay.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
	<script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.8.js"></script>
	
	<title>주문결제</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
	<div id="content" class="no-sidebar">
		<%-- 섹션 --%>
		<section>
			<article>
				<%-- 타이틀 --%>
				<h2 id="title">주문결제</h2>
				<div id="paymentWrapper">
					<%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
					<div class="payment-box">
						<h4 id="boxTitle">배송지 정보</h4>
						<hr>
						<div class="payment-inner-box">
							<div id="memberInfoAndAddrBtn">
								<div id="memberInfo">
									<span id="name">${ requestScope.member.name }</span>
									<span id="phone">${ requestScope.member.phone }</span>
								</div>
								<button id="addrBtn" class="btn btn-outline-success btn-sm" onclick="openAddressManager()">배송지 변경</button>
							</div>
							<div id="deliveryInfo">
								<input type="text" name="deliveryCode" id="deliveryCode" style="display: none;" value="${ requestScope.deliveryAddr.addrCode }" readonly>
								<span id="zipcode">${ requestScope.deliveryAddr.addrZipcode }</span>
								<c:choose>
									<c:when test="${ empty requestScope.deliveryAddr.addrDetail }">
										<span id="fullAddr">${ requestScope.deliveryAddr.addr }</span>
									</c:when>
									<c:otherwise>
										<span id="fullAddr">${ requestScope.deliveryAddr.addr }, ${ requestScope.deliveryAddr.addrDetail } </span>
									</c:otherwise>
								</c:choose>
								<input type="text" class="form-control" name="deliveryRequest" id="deliveryRequest" placeholder="배송시 요청사항" value="${ requestScope.deliveryAddr.deliveryREQ }">
							</div>
							<div id="disposableAndPoint">
								<div id="disposable">
									<input type="checkbox" name="useDisposable" id="useDisposable" ${ requestScope.payInfo.useDisposableYN == "Y" ? "checked" : "" }>
									<label for="useDisposable">일회용 식기 사용</label>
								</div>
								<span id="point" class="pay-accent">적립금 +2%</span>
							</div>
						</div>
					</div>
					<div class="payment-box">
						<h4 id="boxTitle">쿠폰 및 적립금 사용</h4>
						<hr>
						<div id="couponAndPoint" class="payment-inner-box">
							<div class="box-row">
								<span class="title">나의 쿠폰</span>
								<span class="use-amount"></span>
								<select name="coupon" id="coupon" class="form-select form-select-sm box-form">
									<option value="none">사용 안함</option>
									<c:forEach var="coupon" items="${ requestScope.couponList }">
										<option value="${ coupon.issuedCpnCode }">${ coupon.cpnName } - <fmt:formatNumber value="${ coupon.cpnDiscountRate }" type="percent"/></option>
									</c:forEach>
								</select>
								<button id="useCoupon" class="btn btn-outline-success btn-sm">사용하기</button>
							</div>
							<div class="box-row">
								<span class="title">나의 적립금</span>
								<span class="use-amount"><fmt:formatNumber value="${ requestScope.member.point }" type="number"/>원</span>
								<input type="number" name="point" id="point" min="0" max="${ requestScope.member.point }" step="10" class="form-control form-control-sm box-form" value="0">
								<button id="usePoint" class="btn btn-outline-success btn-sm">전액사용</button>
							</div>
						</div>
					</div>
					<div class="payment-box">
						<h4 id="boxTitle">구독 식단</h4>
						<hr>
						<div id="mealCategoryInfoBox">
							<div id="categoryImage" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ requestScope.category.categoryAttachName }')"></div>
							<div id="categoryInfo">
								<span id="mealCategoryName">${ requestScope.category.categoryName }</span>
								<span><span id="price" class="pay-accent"><fmt:formatNumber value="${ requestScope.category.categoryPrice }" type="number"/>원</span> / 5일</span>
								<span><fmt:formatDate value="${ requestScope.deliveryDate }" pattern="yy.MM.dd (E)"/> 부터 배송 예정</span>
							</div>
						</div>
						<hr>
						<div id="categoryAndPriceBox" class="payment-inner-box">
							<div class="box-row">
								<span>구독금액</span>
								<span><span id="mealCategoryPrice"><fmt:formatNumber value="${ requestScope.category.categoryPrice }" type="number"/></span>원 / 5일</span>
							</div>
							<div class="box-row">
								<span>쿠폰할인</span>
								<span><span id="discountPriceByCoupon">0</span>원</span>
							</div>
							<div class="box-row">
								<span>적립금 사용</span>
								<span><span id="discountPriceByPoint">0</span>원</span>
							</div>
							<div class="box-row pay-accent">
								<span>이번 회차 최종 금액</span>
								<span><span id="finalPrice"><fmt:formatNumber value="${ requestScope.category.categoryPrice }" type="number"/></span>원</span>
							</div>
							<div class="box-row pay-accent">
								<span>적립금 적립</span>
								<span><span id="addPoint">500</span>원</span>
							</div>
							<div class="box-row-right">
								<span>다음 회차부터 <span id="nextPrice"><fmt:formatNumber value="${ requestScope.category.categoryPrice }" type="number"/></span>원 / 5일</span>
							</div>
						</div>
					</div>
					<div class="payment-box">
						<h4 id="boxTitle">결제수단</h4>
						<hr>
						<div class="payment-inner-box">
							<div id="choosePayMethod">
								<div>
									<input type="radio" name="payMethod" id="payco" value="PAYCO" ${ requestScope.payInfo.payMethod == "PAYCO" ? "checked" : "" }>
									<label for="payco">PAYCO</label>
								</div>
								<div>
									<input type="radio" name="payMethod" id="kakaopay" value="KAKAOPAY" ${ requestScope.payInfo.payMethod == "kakaopay" ? "checked" : "" }>
									<label for="kakaopay">카카오페이</label>
								</div>
								<div>
									<input type="radio" name="payMethod" id="naverpay" value="NAVERPAY" ${ requestScope.payInfo.payMethod == "naverpay" ? "checked" : "" }>
									<label for="naverpay">네이버페이</label>
								</div>
								<div>
									<input type="radio" name="payMethod" id="toss" value="TOSS" ${ requestScope.payInfo.payMethod == "toss" ? "checked" : "" }>
									<label for="toss">토스</label>
								</div>
							</div>
							<div id="savePayInfoBox">
								<div>
									<input type="checkbox" name="savePayInfo" id="savePayInfo">
									<label for="savePayInfo">결제 정보 저장</label>
								</div>
							</div>
							<div id="priceAndPay">
								<span class="pay-accent"><span id="payPrice"><fmt:formatNumber value="${ requestScope.category.categoryPrice }" type="number"/></span>원</span>
								<button id="payBtn" class="btn btn-outline-success btn-sm" onclick="payProcess()">결제</button>
							</div>
						</div>
					</div>
				</div>
			</article>
		</section>
	</div>
	<script>
		const couponSelector = document.querySelector("select[name='coupon']");
		const couponUseButton = document.querySelector("button[id='useCoupon']");
		const pointInputBox = document.querySelector("input[name='point']");
		const pointUseButton = document.querySelector("button[id='usePoint']");

		const mealCategoryPriceBox = document.querySelector('#categoryAndPriceBox #mealCategoryPrice');

		var mealCategoryPrice = parseInt(document.querySelector('#categoryAndPriceBox #mealCategoryPrice').innerText.replace(/,/g, ''));
		var discountPriceByCoupon = 0;
		var discountPriceByPoint = 0;
		var finalPrice = '${ requestScope.category.categoryPrice }';
		var useCouponCode = 'none';

		couponUseButton.onclick = function() {
			if(couponSelector.value != 'none') {
				$.ajax({
					url: "${ pageContext.servletContext.contextPath }/member/getCouponInfo",
					type: "POST",
					data: {
						issuedCouponCode: couponSelector.value
					},
					success: changeCoupon,
					error: function(request, status) {
						alert("code: " + request.status + "\n" + "message: " + request.responseText);
					}
				});
			} else {
				resetCoupon();
			}

			useCouponCode = couponSelector.value;
		}
		pointInputBox.onchange = function() {
			validateMaxPoint();
			changeUsePoint();
		}
		pointUseButton.onclick = () => {
			useAllPoint();
			changeUsePoint();
		}

		document.querySelector('#disposable #useDisposable').onclick = function() {
			if(this.checked) {
				document.querySelector('span#point.pay-accent').style.visibility = 'visible';
				updatePoint();
			} else {
				document.querySelector('span#point.pay-accent').style.visibility = 'hidden';
				resetPoint();
			}
		}

		/* 쿠폰 초기화 (0으로) */
		function resetCoupon() {
			changeCoupon(null);
			updatePrice(null, 0);
		}

		/* 쿠폰 변경 */
		function changeCoupon(couponInfo) {
			const discountPriceByCouponBox = document.querySelector('#categoryAndPriceBox #discountPriceByCoupon');

			const mealCategoryPrice = parseInt(mealCategoryPriceBox.innerText.replace(/,/g, ''));

			discountPriceByCoupon = (couponInfo != null ? couponInfo.cpnDiscountRate : -0) * mealCategoryPrice * -1;
			discountPriceByCouponBox.innerText = discountPriceByCoupon.toLocaleString();

			updatePrice(null, discountPriceByCoupon);
		}

		/* 
			가격 업데이트 
			업데이트 하지 않을 부분은 null로 채워서 호출하면 가격 업데이트 함
		*/
		function updatePrice(point, coupon) {
			if(point != null) {
				discountPriceByPoint = point;
			}
			if(coupon != null) {
				discountPriceByCoupon = coupon;
			}

			const finalPriceBox = document.querySelector('#categoryAndPriceBox #finalPrice');
			const payPrice = document.querySelector('#priceAndPay #payPrice');

			finalPrice = mealCategoryPrice - discountPriceByPoint + discountPriceByCoupon;
			finalPriceBox.innerText = finalPrice.toLocaleString();
			payPrice.innerText = finalPrice.toLocaleString();

			updatePoint();
		}

		function validateMaxPoint() {
			let max = ${ requestScope.member.point };

			if(pointInputBox.value % 10 != 0) {
				pointInputBox.value = pointInputBox.value - pointInputBox.value % 10
			}
			if(pointInputBox.value < 0 || pointInputBox.value == '') {
				pointInputBox.value = 0;
			}
			if(pointInputBox.value > max) {
				pointInputBox.value = max;
			}
		}

		function changeUsePoint() {
			const discountPriceByPointBox = document.querySelector('#categoryAndPriceBox #discountPriceByPoint');

			discountPriceByPointBox.innerText = (pointInputBox.value * -1).toLocaleString();

			updatePrice(pointInputBox.value, null);
		}

		function useAllPoint() {
			let max = ${ requestScope.member.point };

			pointInputBox.value = max;
		}

		function openAddressManager() {
			const url = "/delimeal/member/manageAddress";
			const name = "Address Management";

			const option = "width=400, height=600, top=200, left=300"

			const addressManager = window.open(url, name, option);
		}

		/* 팝업이 닫히면서 값을 반환받는 함수 */
		function updateDeliveryAddress(data) {

			const deliveryAddr = data;
			console.log(deliveryAddr);
			/* 배송지 정보 엘리먼트(노드) */
			const element = document.querySelector('#deliveryInfo');

			/* zipcode 변경 */
			element.querySelector('#zipcode').innerText = deliveryAddr.addrZipcode;
			/* 주소 변경 */
			element.querySelector('#fullAddr').innerText = deliveryAddr.addr + ', ' + deliveryAddr.addrDetail;
			/* 배송 요청 사항 변경 */
			element.querySelector('#deliveryRequest').innerText = deliveryAddr.deliveryReq;
			/* 배송지 코드 변경 */
			element.querySelector('#deliveryCode').value = deliveryAddr.addrCode;
		}

		function updatePoint() {
			const pointBox = document.querySelector('#categoryAndPriceBox #addPoint');
			const finalPrice = parseInt(document.querySelector('#categoryAndPriceBox #finalPrice').innerText.replace(/,/g, ''));

			pointBox.innerText = (finalPrice * 0.02).toLocaleString();

		}

		function resetPoint() {
			const pointBox = document.querySelector('#categoryAndPriceBox #addPoint');
			pointBox.innerText = 0;
		}

		window.onload = function() {
			if(document.querySelector('#disposable #useDisposable').checked) {
				updatePoint();
			} else {
				resetPoint();
			}
		}

		function payProcess() {
			const payInfo = {
				subsCode: '',
				deliveryAddrCode: document.querySelector('#deliveryInfo #deliveryCode').value,
				useDisposable: document.querySelector('#disposable #useDisposable').checked,
				useCoupon: useCouponCode,
				usePoint: document.querySelector('#couponAndPoint #point').value,
				addPoint: parseInt(document.querySelector('#categoryAndPriceBox #addPoint').innerText.replace(/,/g, '')),
				payPrice: parseInt(document.querySelector('#priceAndPay #payPrice').innerText.replace(/,/g, '')),
				payMethod: document.querySelector('#choosePayMethod input[name="payMethod"]:checked').value,
				savePayInfo: document.querySelector('#savePayInfoBox #savePayInfo').checked
			};

			$.ajax({
				url: '${ pageContext.servletContext.contextPath }/subs/getPayInfo',
				type: 'POST',
				data: {
					categoryCode:  '${ requestScope.category.categoryCode }',
					couponCode: payInfo.useCoupon,
					addrCode: payInfo.deliveryAddrCode
				},
				success: function (data) {
					pay(data);
				},
				error: function() {
					console.log('getSubsCode 실패!!');
				}
			});

			// const form = document.createElement('form');

			// form.setAttribute('method', 'post');
			// form.setAttribute('action', '${ pageContext.servletContext.contextPath }/subs/payProcess');
			// document.charset = 'UTF-8';

			// for(let key in payInfo) {
			// 	let hiddenField = document.createElement('input');
			// 	hiddenField.setAttribute('type', 'hidden');
			// 	hiddenField.setAttribute('name', key);
			// 	hiddenField.setAttribute('value', data[key]);
			// 	form.appendChild(hiddenField);
			// }

			// document.body.appendChild(form);

			// form.submit();
		}

		function pay(payInfo) {
			var IMP = window.IMP;
			var fullAddr = '';
			if(payInfo.addr.addrDetail != null) {
				fullAddr = payInfo.addr.addr + ', ' + payInfo.addr.addrDetail;
			} else {
				fullAddr = payInfo.addr.addr;
			}

			IMP.init("imp20570126");

			console.log(payInfo);
			console.log(finalPrice);

			const payArgs = {
				merchant_uid: payInfo.subsCode,
				name: payInfo.mealCategoryName,
				amount: finalPrice,
				buyer_email: payInfo.buyer.email,
				buyer_name: payInfo.buyer.name,
				buyer_tel: payInfo.buyer.phone,
				buyer_addr: fullAddr,
				buyer_postcode: String(payInfo.addr.addrZipcode)
			};

			console.log("payArgs: ", payArgs);

			IMP.request_pay(payArgs, function (rsp) {
				if (rsp.success) {
					$.ajax({
						url: "${ pageContext.servletContext.contextPath }/subs/payProcess",
						method: "POST",
						data: {
							buyerAddr: rsp.buyer_addr,
							buyerEmail: rsp.buyer_email,
							buyerName: rsp.buyer_name,
							buyerPostcode: rsp.buyer_postcode,
							buyerTel: rsp.buyer_tel,
							subsCode: rsp.merchant_uid,
							categoryName: rsp.name,
							payMethod: rsp.pg_provider,
							payId: rsp.pg_tid,
							paidPrice: rsp.paid_amount,
							discountPriceByCoupon: discountPriceByCoupon,
							useCouponCode: useCouponCode,
							discountPriceByPoint: discountPriceByPoint,
							status: rsp.status,
							deliveryDate: '${ requestScope.deliveryDate }'
						},
						success : function(data) {
							if (data == 'success') {
								alert('결제에 성공했습니다. 메인으로 돌아갑니다.');
								location.replace('${ pageContext.servletContext.contextPath }');
							} else if (data == 'failure') {
								alert('결제에 실패했습니다.');
							}
						}
					});
				} else {
					alert('결제에 실패했습니다.');
				}
			});
		}
	</script>
</body>

</html>