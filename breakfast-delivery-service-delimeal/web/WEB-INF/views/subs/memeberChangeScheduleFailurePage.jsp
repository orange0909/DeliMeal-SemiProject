<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <style>
        #main{
            display: flex;
            flex-direction: column;
            justify-content: center;
            height: 100%;
            align-items: center;
            gap: 15px;
        }
    </style>
    <meta charset="UTF-8">
    <title>주말을 선택한 경우 나오는 오류 페이지</title>
</head>
<body>
    <!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
    <div id="content">
    	<!-- 사이드바 -->
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
	    <!-- 섹션 -->
        <section>
            <article>
                <h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">구독일정 변경</h5><div class="sub-title-tail"></div>
                </div>
                <div id="main">
                    <h1>변경 실패!</h1>
                    <img src="/delimeal/resources/image/failure.png">
                    <h3>처음부터 다시 수행해 주세요.</h3>
                </div>
            </article>
        </section>
</body>
</html>