<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <style>
        #menulist{
            display: flex;
            flex-direction: row;
            width: 100%;
        }
        .buttons{
            border: 0px;
            background-color: white;
        }
        .mealCategoryPic{
            border: 1px solid #63C246;
        }
        #mealCategoryName{
            padding: 5px;
        }
        #pleasePickMenu{
            font-size: 15pt;
        }
    </style>
    <meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
    <div id="content">
    	<!-- 사이드바 -->
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
	    <!-- 섹션 -->
        <section>
            <article>
                <h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">구독중인 서비스 변경</h5><div class="sub-title-tail"></div>
                </div>
                <div id="pleasePickMenu">변경하실 서비스를 선택해 주세요</div>
                <form action="${ pageContext.servletContext.contextPath }/subs/BeforeChangePayment" method="get">
                    <div id="menu_list">
                        <c:forEach var="mealCategory" items="${mealList}" varStatus="st">
                            <button id="mealCategoryButton${st.index}" class="buttons" name="selectedMeal" value="${mealCategory.code}">
                                <div id="mealCategoryView${st.index}" class="mealCategoryItem">
                                    <img src="../resources/food/${mealCategory.image}" class="mealCategoryPic" width="170px" height="150px" class="picture">
                                    <div id="mealCategoryName">${mealCategory.name}<div>
                                </div>
                            </button>
                        </c:forEach>      
                    </div>
                </form>
            </article>
        </section>
    </div>
</body>
</html>