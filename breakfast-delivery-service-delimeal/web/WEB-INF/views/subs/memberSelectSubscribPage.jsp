<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <style>
        #currentSubscribeView{
            width: 100%;
            display: flex;
            flex-direction: column;
            color: #6f6f6f;
        }
        .subsInfo{
            display: flex;
            flex-direction: column;
            justify-content: center;
            background-color: #BAEF61;
            margin-top: 30px;
            border: 1.5px solid rgba(132, 192, 4, 0.85);
            padding: 20px 15px;
            width: 80%;
            gap: 5px;
        }
        #changeSubscribe{
            display: flex;
            flex-direction: column;
            gap: 10px;
        }
        .changeButton{
            background-color: rgba(132, 192, 4, 0.85);
            color: white;
            border-radius: 20px 20px 20px 20px;
            width: 20%;
            text-align: center;
            padding: 5px;
            cursor: pointer;
        }
        .subscribeFont{
            margin-left: 3%;
            color: #8E8E8E;
        }
    </style>
<title>Insert title here</title>
</head>
<body>
	<!-- 헤더 -->
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<!-- 사이드바 + 섹션 -->
    <div id="content">
    	<!-- 사이드바 -->
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
	    <!-- 섹션 -->
        <section>
            <article>
                <h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">구독정보 조회</h5><div class="sub-title-tail"></div>
                </div>
                <div id="currentSubscribeView">
                    <h7 id="viewTitle">구독정보</h7>
                    <div id="SubscribeView">
                        <c:forEach var="subs" items="${subscriptionInfos}" varStatus="st">
                            <div id="subsInfo${st.index}" class="subsInfo">
                                <div id="CurrentSubsCategoryName${st.index}" >현재 구독중인 식단: ${ subs.mealCategoryName }</div>
                                <div id="BirthdayMenu_name${st.index}" class="MenuName">구독기간: ${ subs.startDate } ~ ${ subs.expireDate }</div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <div id="changeSubscribe">
                    <h7 id="changeSubscribeTitle">구독정보 수정하기</h7>
                    <div class="changeButton" onclick="location.href='${pageContext.servletContext.contextPath}/subs/GoToRefund'">구독 해지</div>
                    <div class="changeButton" onclick="location.href='${pageContext.servletContext.contextPath}/subs/GoToChangeSchedule'">구독 일정 변경</div>
                </div>
            </article>
        </section>
    </div>
</body>
</html>