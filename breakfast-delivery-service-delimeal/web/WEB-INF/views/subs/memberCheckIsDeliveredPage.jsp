<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
		$.noConflict();  //Not to conflict with other scripts
		jQuery(document).ready(function($) {
			$( "#datepicker" ).datepicker({
			changeMonth: true
		   ,changeYear: true
		   ,monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'] //달력의 월 부분 텍스트
			});
		});
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<style>
		#inputArea{
			display: flex;
			gap: 10px;
		}
		#showIsDelivered{
			margin-top: 20px;
			width: 80%;
			height: 80%;
		}
		table {
			border: 1px solid #898888;
			border-collapse: collapse;
		}
		th, td {
			text-align: center;
			border: 1px solid #898888;
		}
		.tablerow1{
			background-color: #E5E5E5;
		}
		#datepicker{
			border: 1px solid #898888;
			border-radius: 15px 15px 15px 15px;
		}
		#send-date{
			height: 100%;
			background-color: rgba(132, 192, 4, 0.85);
			border-radius: 15px 15px 15px 15px;
			border: 1px solid white;
			color: white;
		}
	</style>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
	    <jsp:include page="/WEB-INF/views/subs/memberAside.jsp"/>
		<section>
			<article>
				<h2 id="title">구독 서비스</h2>
                <div id="subTitle">
                    <h5 class="sub-title">배송여부 조회</h5><div class="sub-title-tail"></div>
                </div>
                <div id="currentPage">
                	<h5 class="current-page">배송여부 조회</h5>
                </div>
				<div id="inputArea">
					<!-- 이전 검색기록 안 뜨도록 autocomplete기능 off, 날짜선택 쉽게 하기 위해 jQuery의 datepicker사용 -->
					<input type="text" id="datepicker" autocomplete="off">
					<button type="submit" id="send-date">날짜 선택</button>
				</div>
				<div id="tableArea">
					<table id="showIsDelivered">
						<tr id="tableTitle">
							<td class="tablerow1">배송지</td>
							<td class="tablerow1">배송 일자</td>
							<td class="tablerow1">배송 식단</td>
							<td class="tablerow1">배송 완료 여부</td>
						</tr>
											<script>
						$("#send-date").click(function(){
							const date = document.getElementById("datepicker").value;
	
							// 날짜 입력을 하지 않은 경우 alert창을 띄우고 focus를 잡아준다
							if(date === null || date === ""){
								alert("날짜를 입력해 주세요");
								$('#datepicker').focus();
							// 날짜가 입력 된 경우 ajax로 비동기통신을 진행한다.
							} else{
								
								$.ajax({
									url: "${pageContext.servletContext.contextPath}/subs/checkIsDelivered",
									type: "get",
									data: {
										date: date
									},
									success: function(data){
										// 비동기 통신 성공 시 테이블 내의 내용이 조회한 값으로 변경된다.
										let jsonArray = JSON.parse(data);
										console.table(jsonArray);
										console.log(jsonArray[0]);
										for(i=0; i<jsonArray.length; i++){
											console.table(jsonArray[i]);
											const table = document.getElementById('showIsDelivered');
											const newRow = table.insertRow();
											const DepositCell = newRow.insertCell(0);
											DepositCell.innerText = (jsonArray[i].addr + jsonArray[i].addrDetail);
											const dateCell = newRow.insertCell(1);
											dateCell.innerText = jsonArray[i].deliveryDate;
											const mealCell = newRow.insertCell(2);
											mealCell.innerText = jsonArray[i].mealName;
											const isDeliveredCell = newRow.insertCell(3);
											isDeliveredCell.innerText = jsonArray[i].isFinished;

										}
									},
									error: function(request, status){
										alert("배송 정보가 존재하지 않습니다")
									}
								})
							}
						});
					</script>
					</table>
				</div>
			</article>
		</section>
	</div>

</body>
</html>