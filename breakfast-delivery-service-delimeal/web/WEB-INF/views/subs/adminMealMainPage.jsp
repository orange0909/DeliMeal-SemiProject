<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<html>

<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet"
	href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

<style>
.articleTopMiniLineTopBar>p {
	line-height: 10px;
	margin-left: 0.5%;
	font-style: normal;
	font-weight: 600;
	font-size: 17px;
	color: #3c8028;
}

.greenLine {
	width: 10%;
	height: 3px;
	background: #3c8028;
	float: left;
}

.grayLine {
	width: 90%;
	height: 3px;
	background: #d7d7d7;
	float: left;
}

.articleTopInnerValue {
	height: 90px;
	/* background: red; */
}

.articleTopInnerValue button {
	background: #98bc8d;
	border-style: none;
	width: 15.8%;
	height: 39px;
	margin-top: 21px;
	color: white;
	font-style: normal;
	font-weight: 400;
	font-size: 18px;
	float: left;
}

.articleTopInnerValue button:nth-child(1) {
	margin-left: 33%;
}

.articleTopInnerValue button:nth-child(2) {
	margin-left: 1.7%;
}

.articleTopInnerValue form {
	width:28%;
	height:30px;
	margin-top:28px;
	margin-left:5%;
}


.search-area {
	width: 70%;
	height: 100%;
	float: left;
}

#searchValue {
	width: 73%;
	height:100%;
	border-top: 3px solid #98bc8d;
	border-left: 3px solid #98bc8d;
	border-bottom: 3px solid #98bc8d;
	border-right-style:none;
	padding-left: 4.5%;
	float:left;
}

#searchValue::placeholder {
	font-weight: 400;
	font-size: 15px;
	color: #dfdfdf;
}
#searchIcon {
	float:left;
	background: white;
	width: 20%;
	height: 32px;
	border: 3px solid #98bc8d;
	margin-top: 0px;
	position:relative;


}

#searchIcon img {
	width: 20px;
	
	position: absolute;
	top:3px;
	left:17%;
}


/* 카테고리필터 */
.articleTopToggle {
	float:left;
	padding-left: 0.5%;
	width: 30%;
	height: calc(100% + 2px);
	border: 3px solid #98bc8d;
	font-style: normal;
	font-weight: 600;
	font-size: 16px;
	color: rgba(0, 0, 0, 0.473);
}

.articleTopMiniLineUnderBar {
	height: 3px;
	background: #aca6a6;
}

.articleMidBox {
	width: 100%;
	/* height: 900px; */
	display: flex;
	flex-wrap: wrap;
	align-content: flex-start;
}

.box {
	width: 20%;
	padding: 3px;
	height: 220px;
	/* background: blue;  */
	margin-top: 40px;
	cursor: pointer;
	text-align: center;
}

.boxImage {
	width: 100%;
	height: 180px;
	/* background: red; */
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}

.boxImage img {
	width: 100%;
	/* height: 210px; */
	/* background: red; */
}

.articlePagingBtBox {
	margin: 0 auto;
	width: 100%;
}

.boxText1 {
	display: none;
}

.articleTopToggle {
	/* display: none; */
}



/*--- 팝업 */
#popupWrap {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 620px;
	height: 650px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 17%;
	left: 33%;
	z-index: 99;
	display: none;
}

#popupWrap pre {
	font-family: 'Noto Sans KR', sans-serif;
}

#popupTitle {
	font-style: normal;
	font-weight: 600;
	font-size: 30px;
	color: #98bc8d;
	margin-top: 10px;
	margin-left: 4.04%;
}


#name, #categoryCodeSelect{
	width: 180px;
	height: 25px;
	
}

#textBox input, #calinfo, #image, #categoryCodeSelect {
	font-size: 13px;
}

#textBox textarea {
	line-height: 20px;
	font-size: 13px;
}

#textBox pre {
	float: left;
}


#cancel, #add {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 20px;
	cursor: pointer;
}

#cancel {
	margin-left: 75%;
	margin-right: 1%;
}

/* 성공---- */
#success, #fail {
	width: 500px;
	height: 210px;
	top: 17%;
	left: 38%;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	z-index: 99;
	display: none;
}

#result {
	margin-left: 44%;
	width: 60px;
	height: 60px;
}

#popupMessage {
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
}

#popupMessage2 {
	text-align: center;
	color: #e60000;
	font-weight: 600;
	font-size: 19px;
}

#success button, #fail button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 1px;
	cursor: pointer;
}

#confirm {
	margin-left: 77.7%;
}
/* 카테고리 간편삭제 */
#deletePopupWrap {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#deletePopupWrap>#popupMessage {
	margin-top: 18px;
	text-align: center;
	color: #3C8028;
	font-weight: 600;
	font-size: 19px;
}

#deletePopupWrap>#textBox {
	margin-left: 10%;
	line-height: normal;
}

#deletePopupWrap>#textBox>#text {
	border: 1px solid #dfdfdf;
	width: 50%;
	height: 28px;
	padding-left: 1.5%;
}

#deletePopupWrap>#textBox>#text::placeholder {
	font-weight: 400;
	font-size: 13px;
	color: #dfdfdf;
}

#deletePopupWrap>button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 11px;
	cursor: pointer;
}

#deletePopupWrap>#cancel {
	margin-left: 64%;
	margin-right: 1%;
}


/* 삭제성공 */
#deleteSuccess {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#deleteSuccess>button , #deleteFail>button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 1px;
	cursor: pointer;
}

/* 삭제실패 */

#deleteFail{
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;

}

#fail #popupMessage, #deleteFail #popupMessage {
color:red;
}
#mask {
	position: absolute;
	z-index: 20;
	width: 100%;
	background-color: #000;
	display: none;
	left: 0;
	top: 0;
}

        #textBox {
            /* background: red; */
            line-height: 45px;
            margin-top: 3px;
            padding-left: 6%;
            font-size: 16px;
            font-weight: 400;
        }
        #textBox pre {
            float: left;
        }
        #textBox pre:nth-of-type(3){
            font-size: 14px;
        }

        #textBox #menuText {
            margin-right: 4px;
            font-size: 13px;
            color: #3c8028;
           
        }
		#menuText span {
		 margin-right: 5px;
		}
        #addMenu, #clearMenu{
            width: 60px;
            height:30px;
            background: #98bc8d;
            color: white;
            border-radius: 30px;
            border: 5px;
            border-color: white;
            border-style:double;
            cursor: pointer;
            line-height: normal;
            font-size:13px;
        }
		#date {
		height:25px;
		
		}

        #intro {

            resize: none;
        }


        #cancel, #add{
            width: 60px;
            height: 30px;
            font-size: 15px;
            color: white;
            border-style: none;
            background: #98bc8d;
            border-radius: 4px;
            margin-top: 20px;
            cursor: pointer;  
        }
    

        #cancel {
            margin-left: 75%;
            margin-right: 1%;
        }
        
        #menuName {
        	height:25px;
        }
        
   
</style>
<title>Insert title here</title>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
	<%-- 사이드바 + 섹션 --%>
	<div id="content">
		<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
		<jsp:include page="/WEB-INF/views/subs/adminAside.jsp" />
		<%-- 섹션 --%>
		<section>
			<article>
				<%-- 타이틀 --%>
				<h2 id="title">식단 관리</h2>
				<div>
					<div class="articleTopBox">
						<div class="articleTopMiniLineTopBar">
							<p>식단</p>
							<div class="greenLine"></div>
							<div class="grayLine"></div>
						</div>
						<div class="articleTopInnerValue">
							<button id="articleTopBt1" name="articleTopBt1">추가</button>
							<button id="articleTopBt2" name="articleTopBt2">간편삭제</button>
							<form action="${ pageContext.servletContext.contextPath }/admin/meal" method="get" style="display:inline-block">
								<div id="search">
										<!-- 검색 -->
										<div class="search-area" align="center">
											    <input type="hidden" name="currentPage" value="1">
										        <input type="search" id="searchValue" name="searchValue" class="searchBar" placeholder="식단 검색" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
									    		<button type="submit" id="searchIcon"><img src="${ pageContext.servletContext.contextPath }/resources/image/searchicon.png"></button> 	
										</div>
								</div>
								
								<select name="searchCondition" class="articleTopToggle" onchange="filterAjax()">
									<option class="searchCondition" value="all" ${ requestScope.selectCriteria.searchCondition eq "all" ? "selected" : "" }>전체</option>
									<c:forEach var="category" items="${ requestScope.categoryList }">
										<option class="searchCondition" value="${ category.categoryCode }" ${ requestScope.selectCriteria.searchCondition eq category.categoryCode  ? "selected" : "" }>${ category.categoryName }</option>
									</c:forEach>
								</select>
							</form>		
							
						</div>
						<div class="articleTopMiniLineUnderBar"></div>
					</div>
					<div class="articleMidBox">
						<c:forEach var="mealPerDay" items="${ requestScope.mealPerDayList }">
						<div class="box" onclick="location.href='${ pageContext.servletContext.contextPath }/admin/mealSelectDetail?mealCode=${ mealPerDay.meal.mealCode }&date=${ mealPerDay.deliveryDate }'">
							<div class="boxImage" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ mealPerDay.meal.mealAttachName }')">
							</div>
							<div class="boxText3">${ mealPerDay.deliveryDate }</div>
							<div class="boxText2">${ mealPerDay.meal.mealName }</div>
						</div>
						</c:forEach>
					</div>
				</div>
					<div class="articlePagingBtBox">
						<!-- 페이징  -->
					</div>
						 <jsp:include page="../subs/adminMealPaging.jsp"/> 
			</article>
		</section>
	</div>
	<div id="mask"></div>
	<div id="popupWrap">
		<div id="popupTitle">식단 등록</div>
		<div id="textBox">
			<pre>등록할 식단의 이름을 입력하세요 :          </pre>
			<input id="name" type="text">
			<br clear="both">
			<pre>등록할 식단의 메뉴들을 입력하세요 :       </pre>
			<input list="menuList" id="menuName" name="menuName"> 
			<button id="addMenu" onclick="add()">추가</button>
			<button id="clearMenu" onclick="reset()">초기화</button> 
			<br clear="both">
			<datalist id="menuList">
				<c:forEach var="menu" items="${ requestScope.menuList }">
					<option value="${ menu }">
				</c:forEach>
			</datalist>
			<pre>추가된 메뉴: </pre> <div id="menuText"></div>
			<br clear="both">
			<pre>등록할 식단의 소개를 입력하세요:           </pre> 
			<textarea name="intro" id="intro" cols="26" rows="5"></textarea>
			<br clear="both">
			<pre>등록할 식단의 날짜를 입력하세요:           </pre>
			<input id="date" type="date">
			<br clear="both">
			<pre>등록할 식단의 카테고리를 선택하세요     </pre>
			<select name="categoryCodeSelect" id="categoryCodeSelect">
				<c:forEach var="category" items="${ requestScope.categoryList }">
					<option class="categoryNameValue">${ category.categoryName }</option>
				</c:forEach>
			</select>
			<br clear="both"> 
			<pre>등록할 식단의 사진을 선택하세요 :          </pre>
			<input id="image" type="file" name="singlefile">
		</div>
		<button id="cancel">취소</button>
		<button id="add">등록</button>
	</div>
	<div id="success">
		<div id="popupTitle">식단 등록</div>
		<img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/success.png" alt="">
		<div id="popupMessage">식단 등록 성공!!</div>
		<button id="confirm" class="successconfirm">확인</button>
	</div>
	<div id="fail">
		<div id="popupTitle">식단 등록</div>
		<img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/failure.png" alt="">
		<div id="popupMessage2">식단 등록 실패!!</div>
		<button id="confirm" class="failconfirm">확인</button>
	</div>
	<!-- 식단 간편삭제 -->
	<div id="deletePopupWrap" >
		<div id="popupTitle">식단 삭제</div>
   	 	<div id="popupMessage">삭제를 원하는 식단의 이름을 입력하세요</div>
    	<div id="textBox">
       	 <input type="text" id="text" class="deleteText" placeholder="식단 이름을 입력하세요">
    	</div>
   	 	<button id="cancel" class="deleteCancel">취소</button>
    	<button id="next" class="delete">삭제</button>
	</div>
	<div id="deleteSuccess">
		 <div id="popupTitle">식단 삭제</div>
	    <img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/success.png" alt="">
	    <div id="popupMessage">식단 삭제 성공!!</div>
	    <button id="confirm" class="successDeleteConfirm">확인</button>
    </div>
    <div id="deleteFail">
		 <div id="popupTitle">식단 삭제</div>
	    <img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/failure.png" alt="">
	    <div id="popupMessage">식단 삭제 실패!!</div>
	    <button id="confirm" class="failDeleteConfirm">확인</button>
    </div>
</body>
<script>
	// 메뉴 추가하기
	var textArray = [];
	
	function add() {
		const list = document.querySelector('div#menuText');
		const addText = $("#menuName").val();
	
		textArray.push(addText);
	
		const element = document.createElement('span');
		element.innerText = addText;
	
		list.appendChild(element);
		document.querySelector('input#menuName').value = '';
	}
	
    function reset() {
        const list = document.querySelector('div#menuText');

        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }

        textArray = [];
    }

</script>
<script>
	// 식단 카테고리별로 보기 (카테고리 필터 기능)
	function filterAjax() {
		const searchCondition = $("select.articleTopToggle").val();
		const submitBtn = document.querySelector('#search button[type="submit"]');
		
		submitBtn.click();
	}
</script>
<script type="text/javascript">
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();

	function popupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#popupWrap").show();

	}

	$("#articleTopBt1").click(function(e) {
		e.preventDefault();
		popupWrapByMask();
	});

	$("#cancel").click(function(e) {
		e.preventDefault();
		$("#mask, #popupWrap").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$("#popupWrap").hide();

	});

	/* ------------성공 */
	function successPopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#popupWrap").hide();
		$("#success").show();

	}
	$(".successconfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #success").hide();

	});

	$("#mask").click(function() {
		$(this).hide();
		$("#success").hide();

	});

	/* ------------실패 */
	function failPopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#popupWrap").hide();
		$("#fail").show();

	}

	$(".failconfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #fail").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$("#fail").hide();

	});
	
	/* 메뉴 삭제의 경우 */
	function deletePopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#deletePopupWrap").show();

	}
	
	
	// 간편삭제 버튼을 눌렀을때
	$("#articleTopBt2").click(function(e) {
		e.preventDefault();
		deletePopupWrapByMask();
	});
	
	// 취소 버튼을 눌렀을때
	$(".deleteCancel").click(function(e) {
		e.preventDefault();
		$(".deleteText").val('');
		$("#mask, #deletePopupWrap").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$(".deleteText").val('');
		$("#deletePopupWrap").hide();

	});
	
	

	/* ------------성공 */
	function successDeletePopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#deletePopupWrap").hide();
		$("#deleteSuccess").show();

	}
	$(".successDeleteConfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #deleteSuccess").hide();

	});

	$("#mask").click(function() {
		$(this).hide();
		$("#deleteSuccess").hide();

	});

	/* ------------실패 */
	function failDeletePopupWrapByMask() {

		//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
		$("#mask").css({
			"width" : maskWidth,
			"height" : maskHeight
		});

		$("#mask").fadeIn(0);
		$("#mask").fadeTo("slow", 0.9);

		$("#deletePopupWrap").hide();
		$("#deleteFail").show();

	}

	$(".failDeleteConfirm").click(function(e) {
		e.preventDefault();
		$("#mask, #deleteFail").hide();

	});

	//검은 막을 눌렀을 때
	$("#mask").click(function() {
		$(this).hide();
		$("#deleteFail").hide();

	});
</script>
<script>
	
	// 식단 등록 눌렀을때
	
	$("#add").click(function() {
		const formData = new FormData();
		
		const name = $("#name").val();
		const intro = $("#intro").val();
		const date = $("#date").val();
		const image = $("#image")[0].files[0];
		const categoryName = $(".categoryNameValue").val();
		
		
		if(name == null || intro == null || date == null || image == null) {
			failPopupWrapByMask();
		} else {
			formData.append("name", name);
			formData.append("intro", intro);
			formData.append("date", date);
			formData.append("image", image);
			formData.append("categoryName", categoryName);
			formData.append("textArray", textArray.toString()); 
	
			$.ajax({
				url : "${pageContext.servletContext.contextPath}/admin/mealInsert",
				type : "post",
				processData: false, 
				contentType: false, 
				data :  formData,
				success : function(data) {
					console.log(data);
					if(data == "success"){
						successPopupWrapByMask();
					} else if(data == "fail"){
						failPopupWrapByMask();
					}
				},
				error : function(request, status) {
					failPopupWrapByMask();
				}
			});
		}
		
	});
	
	$(".delete").click(function() {
		const name = $(".deleteText").val();
		const code = "${ requestScope.meal.code }";
		if(name == null) {
			failDeletePopupWrapByMask();
		} else {
			$.ajax({
				url : "${pageContext.servletContext.contextPath}/admin/mealDelete",
				type : "post",
				data : { 
						 name : name ,
					     code : code
					   },
				success : function(data) {
					console.log(data);
					if(data == "success"){
						successDeletePopupWrapByMask();
					} else if(data == "fail"){
						failDeletePopupWrapByMask();
					}
				},
				error : function(request, status) {
					failDeletePopupWrapByMask();
				}
			});
		}
		
	});
</script>
</html>