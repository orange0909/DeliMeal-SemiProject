<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>  
<html>

<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp" />
<script src="https://code.jquery.com/jquery-3.5.1.min.js"
	type="text/javascript"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet"
	href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	
<style>
.menuImage {
	margin-left: 4%;
	width: 35%;
	height: 320px;
	float: left;
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center;
}


.menuInfo {
	float: left;
	width: 55%;
	margin-left: 6%;
}

#menuName {
	font-style: normal;
	font-weight: 200;
	font-size: 26px;
	color: #3c8028;
}
/* 
h4 {
	display:inline-block;
	font-size: 18px;
	width: 30%;
	
}

.menuInfo li {
	list-style:none; 
	display:inline-block;
	max-width:350px;

	
} */


.buttonBox {
	/* margin-left: 55%; */
	/* background: blue; */
	width: 30%;
	height: 100px;
}



.bt {
	background: #98bc8d;
	border-style: none;
	width: 9%;
	height: 40px;
	color: white;
	font-size: 15px;
}

.modifyBt {
	margin-left: 78.5%;
}

.deleteBt {
	margin-left: 2%;
}

.menu-info-cell {
	display: flex;
	flex-direction: row;
	width: 100%;
}

.menu-info-cell h4 {
	width: 220px;
	font-size:19px;
	margin-right:20px;

	
}



.menu-info-cell span {
	width: 100%;
	text-overflow: ellipsis;
    overflow: hidden;

}
/*--- 팝업 */
#popupWrap {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 620px;
	height: 590px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 17%;
	left: 33%;
	z-index: 99;
	display: none;
}

#popupWrap pre {
	font-family: 'Noto Sans KR', sans-serif;
}

#popupTitle {
	font-style: normal;
	font-weight: 600;
	font-size: 30px;
	color: #98bc8d;
	margin-top: 10px;
	margin-left: 4.04%;
}

#textBox {
	/* background: red; */
	line-height: 50px;
	margin-top: 3px;
	padding-left: 6%;
	font-size: 17px;
	font-weight: 400;
}

#name, #price {
	width: 160px;
	height: 25px;
}

#textBox input, #classify, #image {
	font-size: 13px;
}

#textBox textarea {
	line-height: 20px;
	font-size: 13px;
	resize:none;
}

#textBox pre {
	float: left;
}

#intro {
	resize: none;
}

#cancel, #modify {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 20px;
	cursor: pointer;
}

#cancel {
	margin-left: 75%;
	margin-right: 1%;
}

/* 성공---- */
#success, #fail {
	width: 500px;
	height: 210px;
	top: 17%;
	left: 38%;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	z-index: 99;
	display: none;
}

#result {
	margin-left: 44%;
	width: 60px;
	height: 60px;
}

#popupMessage {
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
}

#popupMessage2 {
	text-align: center;
	color: #e60000;
	font-weight: 600;
	font-size: 19px;
}

#success button, #fail button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 1px;
	cursor: pointer;
}

#confirm {
	margin-left: 77.7%;
}

/* 카테고리 간편삭제 */
#deletePopupWrap {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#deletePopupWrap>#popupMessage {
	margin-top: 27px;
	text-align: center;
	color: #3C8028;
	font-weight: 600;
	font-size: 19px;
}

#deletePopupWrap>#textBox {
	margin-left: 10%;
	line-height: normal;
}

#deletePopupWrap>#textBox>#text {
	border: 1px solid #dfdfdf;
	width: 50%;
	height: 28px;
	padding-left: 1.5%;
}

#deletePopupWrap>#textBox>#text::placeholder {
	font-weight: 400;
	font-size: 13px;
	color: #dfdfdf;
}

#deletePopupWrap>button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 34px;
	cursor: pointer;
}

#deletePopupWrap>#cancel {
	margin-left: 64%;
	margin-right: 1%;
}

#result {
	margin-left: 44%;
	width: 60px;
	height: 60px;
}
/* 삭제성공 */
#deleteSuccess {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;
}

#deleteSuccess>button , #deleteFail>button {
	width: 60px;
	height: 30px;
	font-size: 15px;
	color: white;
	border-style: none;
	background: #98bc8d;
	border-radius: 4px;
	margin-top: 1px;
	cursor: pointer;
}

/* 삭제실패 */

#deleteFail{
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
	width: 500px;
	height: 210px;
	background: white;
	border: 2px solid #98bc8d;
	border-radius: 20px;
	position: absolute;
	top: 23%;
	left: 40%;
	z-index: 99;
	display: none;

}

#fail #popupMessage, #deleteFail #popupMessage {
color:red;
}
#mask {
	position: absolute;
	z-index: 20;
	width: 100%;
	background-color: #000;
	display: none;
	left: 0;
	top: 0;
}

#popupTitle {
	font-style: normal;
	font-weight: 600;
	font-size: 30px;
	color: #98bc8d;
	margin-top: 10px;
	margin-left: 4.04%;
}
#popupMessage {
	margin-top:10px;
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
}
#deleteSuccess > #popupMessage, #deleteFail > #popupMessage {
	text-align: center;
	color: #98bc8d;
	font-weight: 600;
	font-size: 19px;
	margin-top:0px;
}
#confirm {
	margin-left: 77.7%;
}
</style>
<title>Insert title here</title>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
	<%-- 사이드바 + 섹션 --%>
	<div id="content">
		<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
		<jsp:include page="/WEB-INF/views/subs/adminAside.jsp" />
		<%-- 섹션 --%>
		<section>
			<article>
				<h2 id="title">메뉴 정보</h2>
				<div>
					<div class="menuImage" style="background-image:url('${ pageContext.servletContext.contextPath }/resources/food/${ requestScope.menu.menuAttachName }');" >
					</div>
					<div class="menuInfo">
						<div id="menuName"><c:out value="${ requestScope.menu.menuName }"/></div>
						<hr>
						<div class="menu-info-cell">
							<h4 style="color:red;">알레르기 주의성분</h4>
							<span class="menuAllergie"><c:out value="${ requestScope.menu.menuAllergie }"/></span>
						</div>
						<hr>
						<div class="menu-info-cell">
						<h4>칼로리</h4><span id="menuCalinfo"><c:out value="${ requestScope.menu.menuCalinfo }"/>kcal</span>
						</div>
						<hr>
						<div class="menu-info-cell">
						<h4>영양정보</h4><span id="menuNutrient"><c:out value="${ requestScope.menu.menuNutrient }"/></span>
						</div>
					</div>
						<button class="bt modifyBt" name="modifyBt">수정</button>
						<button class="bt deleteBt" name="deleteBt">삭제</button>
				</div>
			</article>
		</section>
	</div>
<div id="mask"></div>
	<!-- 메뉴 삭제 -->
	<div id="deletePopupWrap" >
		<div id="popupTitle">메뉴 삭제</div>
   	 	<div id="popupMessage">정말 <mark><c:out value="${ requestScope.menu.menuName }"/></mark> 을 삭제하시겠습니까?</div>
   	 	<button id="cancel" class="deleteCancel">취소</button>
    	<button id="next" class="delete">삭제</button>
	</div>
	<div id="deleteSuccess">
		 <div id="popupTitle">메뉴 삭제</div>
	    <img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/success.png" alt="">
	    <div id="popupMessage">메뉴 삭제 성공!!</div>
	    <button id="confirm" class="successDeleteConfirm">확인</button>
    </div>
    <div id="deleteFail">
		 <div id="popupTitle">메뉴 삭제</div>
	    <img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/failure.png" alt="">
	    <div id="popupMessage">메뉴 삭제 실패!!</div>
	    <button id="confirm" class="failDeleteConfirm">확인</button>
    </div>
    <!-- 메뉴 수정!!! -->
	<div id="popupWrap" >
		<div id="popupTitle">메뉴 수정</div>
		<div id="textBox">
			<pre>수정할 메뉴의 이름을 입력하세요 :                    </pre>
			<input id="name" type="text" required><br clear="both">
			<pre>알레르기 주의성분을 입력하세요 :                     </pre>
			<textarea name="allergie" id="allergie" cols="30" rows="5"></textarea>
			<br clear="both">
			<pre>칼로리를 입력하세요 :                                       </pre>
			<input id="calinfo" type="text"><br clear="both">
			<pre>영양정보를 입력하세요 :                                   </pre>
			<textarea name="nutrient" id="nutrient" cols="30" rows="5"></textarea>
			<br clear="both"> 
			<pre>수정할 메뉴의 사진을 선택하세요 :                     </pre>
			<input id="image" type="file" name="singlefile">
		</div>
		<button id="cancel" class="updateCancel">취소</button>
		<button id="modify">수정</button>
	</div>
	<div id="success">
		<div id="popupTitle">메뉴 수정</div>
		<img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/success.png" alt="">
		<div id="popupMessage">메뉴 수정 성공!!</div>
		<button id="confirm" class="successInsertConfirm">확인</button>
	</div>
	<div id="fail">
		<div id="popupTitle">메뉴 수정</div>
		<img id="result" src="${ pageContext.servletContext.contextPath }/resources/image/failure.png" alt="">
		<div id="popupMessage">메뉴 수정 실패!!</div>
		<button id="confirm" class="failInsertConfirm">확인</button>
	</div>
</body>
<script>
var maskHeight = $(document).height();
var maskWidth = $(window).width();

function popupWrapByMask() {

	//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
	$("#mask").css({
		"width" : maskWidth,
		"height" : maskHeight
	});

	$("#mask").fadeIn(0);
	$("#mask").fadeTo("slow", 0.9);

	$("#popupWrap").show();

}

$(".modifyBt").click(function(e) {
	e.preventDefault();
	popupWrapByMask();
});


// 취소버튼
$(".updateCancel").click(function(e) {
	e.preventDefault();
	$("#mask, #popupWrap").hide();

});

//검은 막을 눌렀을 때
$("#mask").click(function() {
	$(this).hide();
	$("#popupWrap").hide();


});


/* ------------성공 */
function successPopupWrapByMask() {

	//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
	$("#mask").css({
		"width" : maskWidth,
		"height" : maskHeight
	});

	$("#mask").fadeIn(0);
	$("#mask").fadeTo("slow", 0.9);

	$("#popupWrap").hide();
	$("#success").show();

}
$(".successInsertConfirm").click(function(e) {
	e.preventDefault();
	$("#mask, #success").hide();

});

$("#mask").click(function() {
	$(this).hide();
	$("#success").hide();

});

/* ------------실패 */
function failPopupWrapByMask() {

	//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
	$("#mask").css({
		"width" : maskWidth,
		"height" : maskHeight
	});

	$("#mask").fadeIn(0);
	$("#mask").fadeTo("slow", 0.9);

	$("#popupWrap").hide();
	$("#fail").show();

}

$(".failInsertConfirm").click(function(e) {
	e.preventDefault();
	$("#mask, #fail").hide();

});

//검은 막을 눌렀을 때
$("#mask").click(function() {
	$(this).hide();
	$("#fail").hide();

});

/* 카테고리 삭제의 경우 */
function deletePopupWrapByMask() {

	//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
	$("#mask").css({
		"width" : maskWidth,
		"height" : maskHeight
	});

	$("#mask").fadeIn(0);
	$("#mask").fadeTo("slow", 0.9);

	$("#deletePopupWrap").show();

}


// 삭제 버튼을 눌렀을때
$(".deleteBt").click(function(e) {
	e.preventDefault();
	deletePopupWrapByMask();
});

// 취소 버튼을 눌렀을때
$(".deleteCancel").click(function(e) {
	e.preventDefault();
	$(".deleteText").val('');
	$("#mask, #deletePopupWrap").hide();

});

//검은 막을 눌렀을 때
$("#mask").click(function() {
	$(this).hide();
	$(".deleteText").val('');
	$("#deletePopupWrap").hide();

});



/* ------------성공 */
function successDeletePopupWrapByMask() {

	//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
	$("#mask").css({
		"width" : maskWidth,
		"height" : maskHeight
	});

	$("#mask").fadeIn(0);
	$("#mask").fadeTo("slow", 0.9);

	$("#deletePopupWrap").hide();
	$("#deleteSuccess").show();

}
$(".successDeleteConfirm").click(function(e) {
	e.preventDefault();
	$("#mask, #deleteSuccess").hide();

});

$("#mask").click(function() {
	$(this).hide();
	$("#deleteSuccess").hide();

});

/* ------------실패 */
function failDeletePopupWrapByMask() {

	//마스크의 높이와 너비를 화면 것으로 만들어 전체 화면을 채운다.
	$("#mask").css({
		"width" : maskWidth,
		"height" : maskHeight
	});

	$("#mask").fadeIn(0);
	$("#mask").fadeTo("slow", 0.9);

	$("#deletePopupWrap").hide();
	$("#deleteFail").show();

}

$(".failDeleteConfirm").click(function(e) {
	e.preventDefault();
	$("#mask, #deleteFail").hide();

});

//검은 막을 눌렀을 때
$("#mask").click(function() {
	$(this).hide();
	$("#deleteFail").hide();

});
</script>
<script>
$("#modify").click(function() {
	const formData = new FormData();
	
	const name = $("#name").val();
	const allergie = $("#allergie").val();
	const calinfo = $("#calinfo").val();
	const nutrient = $("#nutrient").val();
	const image = $("#image")[0].files[0];
	
	
	formData.append("name", name);
	formData.append("allergie", allergie);
	formData.append("calinfo", calinfo);
	formData.append("nutrient", nutrient);
	formData.append("image", image);
	formData.append("code", "${ requestScope.menu.menuCode }");

	$.ajax({
		url : "${pageContext.servletContext.contextPath}/admin/menuUpdate",
		type : "post",
		processData: false, 
		contentType: false, 
		data : formData,
		success : function(data) {
			console.log(data);
			if(data == "success"){
				successPopupWrapByMask();
			} else if(data == "fail"){
				failPopupWrapByMask();
			}
		},
		error : function(request, status) {
			failPopupWrapByMask();
		}
	});
	
});

$(".delete").click(function() {
	const name = "${ requestScope.menu.menuName }";
	
	if(name == null) {
		failDeletePopupWrapByMask();
	} else {
		$.ajax({
			url : "${pageContext.servletContext.contextPath}/admin/menuDelete",
			type : "post",
			data : { name : name },
			success : function(data) {
				console.log(data);
				if(data == "success"){
					successDeletePopupWrapByMask();
				} else if(data == "fail"){
					failDeletePopupWrapByMask();
				}
			},
			error : function(request, status) {
				failDeletePopupWrapByMask();
			}
		});
	}
	
});
</script>
</html>