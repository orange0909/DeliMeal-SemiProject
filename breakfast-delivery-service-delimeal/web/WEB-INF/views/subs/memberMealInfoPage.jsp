<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
	
	<title>DeliMeal</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
	<div id="content">
		<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
		<jsp:include page="memberAside.jsp"/>
		<%-- 섹션 --%>
		<section>
			<article>
				<%-- 타이틀 --%>
				<h2 id="title">식단 정보</h2>
				<div id="categoryInfo">
					<div id="description">
						<div id="categoryImage" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ requestScope.category.categoryAttachName }')"></div>
						<form id="buyForm" action="${ pageContext.servletContext.contextPath }/subs/payment" method="post">
							<div>
								<span id="categoryName">${ requestScope.category.categoryName }</span>
								<span id="categoryDesc">${ requestScope.category.categoryDetail }</span>
							</div>
							<hr class="dashed">
							<div id="buyOptions">
								<div>
									<div>배송요일</div>
									<div>주 5회 (월 ~ 금)</div>
								</div>
								<%-- <div>
									<div>수량</div>
									<div>
										<input type="number" name="amount" id="amount" min="1" step="1" value="1">
									</div>
								</div> --%>
								<div>
									<div>첫 배송일</div>
									<div>
										<input type="date" name="deliveryDate" id="deliveryDate" onchange="validateDeliveryDate()">
									</div>
								</div>
							</div>
							<hr class="dashed">
							<div id="buy">
								<div id="price">
									<div>총 주문 금액</div>
									<div><fmt:formatNumber value="${ requestScope.category.categoryPrice }" type="number"/>원</div>
								</div>
								<div>
									<button id="buyBtn" type="submit" class="btn btn-outline-success">주문하기</button>
								</div>
							</div>
							<input type="text" name="pid" value="${ requestScope.category.categoryCode }" style="display: none" readonly>
						</form>
					</div>
					<div>식단 구성</div>
					<div id="mealList">
						<c:forEach var="list" items="${ requestScope.mealList }">
							<div class="tile">
								<a href="../menu/info?pid=${ list.meal.mealCode }">
									<div class="image-box" style="background-image: url('${ pageContext.servletContext.contextPath }/resources/food/${ list.meal.mealAttachName }')"></div>
									<div id="mealInfo">
										<span id="weekday"><fmt:formatDate value="${ list.deliveryDate }" pattern="M월 d일 E요일"/></span>
										<span id="mealName">${ list.meal.mealName }</span>
										<span id="mealDesc">${ list.meal.mealDetail }</span>
									</div>
								</a>
							</div>
						</c:forEach>
					</div>
				</div>
			</article>
		</section>
	</div>
	<script>
		const deliveryDate = document.querySelector('#deliveryDate');

		const today = new Date();
		today.setDate(today.getDate())

		deliveryDate.valueAsDate = today;

		function validateDeliveryDate() {
			const deliveryDate = this;

			console.log(deliveryDate);
		}
	</script>
</body>

</html>