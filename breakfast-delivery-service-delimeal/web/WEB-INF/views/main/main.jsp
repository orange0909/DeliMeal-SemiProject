<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css"> 
<meta charset="UTF-8">
<title>Welcome to Delimeal</title>
<style>
	#mainContent {
		display: flex;
		flex-direction: column;
		min-width: 800px;
		max-width: 1200px;
		margin: 0 auto;
	}

	#mainContent .desc-box {
		display: flex;
		flex-direction: row;
		justify-content: space-between;
		padding: 30px;
	}

	#mainContent .accent {
		color: var(--mainColor2);
	}

	#mainContent #desc1 {
		display: flex;
		align-items: flex-end;
		color: var(--subText);
		font-size: x-large;
	}
	
	#mainContent #desc2 {
		display: flex;
		flex-direction: column;
		align-items: flex-end;
		color: var(--subText);
		font-size: -webkit-xxx-large;
		font-size: xxx-large;
		line-height: 95%;
	}

	#mainContent #fullBanner {
		background-image: url('${ pageContext.servletContext.contextPath }/resources/image/main.jpg');
		background-repeat: no-repeat;
		background-size: cover;
		background-position: center;
		width: 100%;
		height: calc(100vh - 271px);
	}
</style>
</head>
<body>
	<c:choose>
        <c:when test='${ sessionScope.loginMember.type == "M" }'>
	        <jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
        </c:when>
        <c:otherwise>
	        <jsp:include page="/WEB-INF/views/template/header.jsp"/>
        </c:otherwise>
    </c:choose>
	<div id="mainContent">
		<div class="desc-box">
			<div id="desc1">
				<span>당신의 맛있는 하루, <span class="accent">델리밀</span>에서 시작하세요.</span>
			</div>
			<div id="desc2">
				<span><b>D</b>elivery?</span>
				<span><b>D</b>elicious!</span>
			</div>
		</div>
		<div id="fullBanner"></div>
	</div>
</body>
</html>