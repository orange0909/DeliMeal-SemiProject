<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
	<style>
		.updateButton{
            height: 100%;
			background-color: rgba(132, 192, 4, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            padding: 5px;
            margin-top: 5px;
        }
		table {
			border-collapse: collapse;
		}
		th, td {
			padding: 10px;
		}
	</style>
	<meta charset="UTF-8">
	<title>Insert title here</title>
</head>
<body>
	<script>
		window.onload() = function(){
			if("${sessionScope.alertMessage}" != null){
				alert("${alertMessage}");
			}
		};
	</script>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
	            <h2 id="title">광고 관리</h2>
	                <div id="subTitle">
	                    <h5 class="sub-title">광고 관리</h5><div class="sub-title-tail"></div>
	                </div>
	                    <div id="RegisterTitle">
	                        <h5>등록할 광고 사진 첨부</h5>
	                    </div>
					<form action="${pageContext.servletContext.contextPath}/pa/UpdatePromotionProcess" method="post" encType="multipart/form-data">
						<table>
							<tr>
								<td>연결될 링크 URL</td>
							</tr>
							<tr>
								<td><input type="text" id="fileLink" name="fileLink"></td>
							</tr>
							<tr>
								<td>광고1 사진</td>
								<td rowspan="2">
									<div id="RightupdateButton">
										<div id="updateButtonArea"><button class="updateButton">광고1 등록</button></div>
									</div>
								</td>
							</tr>
							<tr>
								<td><input type="file" id="pictureData" name="promotionPictureLeft"></td>
							</tr>
							<tr>
								<td>광고2 사진</td>
								<td rowspan="2">
									<div id="RightupdateButton">
										<div id="updateButtonArea"><button class="updateButton">광고2 등록</button></div>
									</div>
								</td>
							</tr>
							<tr>
								<td><input type="file" id="pictureData" name="promotionPictureMiddle"></td>
							</tr>
							<tr>
								<td>광고3 사진</td>
								<td rowspan="2">
									<div id="RightupdateButton">
										<div id="updateButtonArea"><button class="updateButton">광고3 등록</button></div>
									</div>
								</td>
							</tr>
							<tr>
								<td><input type="file" id="pictureData" name="promotionPictureRight"></td>
							</tr>
						</table>
					</form>
            </article>
        </section>
</body>
</html>