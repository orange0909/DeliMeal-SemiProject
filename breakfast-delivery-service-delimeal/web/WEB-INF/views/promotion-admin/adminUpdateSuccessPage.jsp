<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        #successMessage{
            margin: auto;
            text-align: center;
            margin-top: 8%;
        }
        table {
			border: 1px solid #898888;
			border-collapse: collapse;
		}
		.tableData{
			border: 1px solid #898888;
			text-align: center;
			padding: 5px;
		}
		#tableHeader{
			background-color: #E5E5E5;
		}
        #succcessText{
            color: #63C246;
        }
        #checkPlease{
            color: #828282;
        }
    </style>
	<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
                <h2 id="title">생일 메뉴 관리</h2>
                <div id="subTitle">
                    <h5 class="sub-title">생일 메뉴 수정</h5><div class="sub-title-tail"></div>
                </div>
                <div id="successMessage">
                    <h1 id="succcessText">생일메뉴 등록에 성공하셨습니다!</h1>
                    <br>
                    <h3 id="checkPlease">등록된 생일메뉴 정보를 확인해주세요</h3>
                    <br>
                    <table id="successMenuTable">
                        <table id="successMenuTable">
                            <tr id="tableHeader">
                                <td class="tableData">생일 메뉴 번호</td>
                                <td class="tableData">메뉴 이름</td>
                                <td class="tableData">메뉴 사진</td>
                                <td class="tableData">알러지 정보</td>
                                <td class="tableData">메뉴 정보</td>
                                <td class="tableData">칼로리 정보</td>
                            </tr>
                            <tr>
                                <td class="tableData">${bday.code}</td>
                                <td class="tableData">${bday.name}</td>
                                <td class="tableData"><img src="../resources/food/${bday.picture}" width="150px" height="130px" class="picture"></td>
                                <td class="tableData">${bday.allergy}</td>
                                <td class="tableData">${bday.nutrient}</td>
                                <td class="tableData">${bday.calorie}</td>
                            </tr>
                        </table>
                    </table>
                </div>
            </article>
        </section>
</body>
</html>