<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        #mainDescription{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            gap: 10px;
        }
        table{
            text-align: center;
            width: 80%;
            table-layout: fixed;
            text-align : center;
            border: 1px solid #898888;
			border-collapse: collapse;
        }
        td{
			border: 1px solid #898888;
			text-align: center;
			padding: 5px;
		}
        .nameCell{
            background-color: #E5E5E5;
        }
    </style>
    <meta charset="UTF-8">
    <title>쿠폰 등록 성공 페이지</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
        	    <h2 id="title">쿠폰 등록</h2>
                <div id="mainDescription">
                    <h1>쿠폰 등록 성공!</h1>
                    <table>
                        <tr>
                            <td class="nameCell">쿠폰 이름</td>
                            <td class="nameCell">할인률</td>
                            <td class="nameCell">쿠폰 만료일</td>
                            <td class="nameCell">쿠폰 설명</td>
                        </tr>
                        <tr>
                            <td>${ couponInfo.name }</td>
                            <td>${ couponInfo.discountRate }</td>
                            <td>${ couponInfo.date }</td>
                            <td>${ couponInfo.description }</td>
                        </tr>
                    </table>
                </div>
</body>
</html>