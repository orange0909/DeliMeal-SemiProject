<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<aside>
	<div id="asideTitle">프로모션관리</div>
	<ul class="aside-menu-box">
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span>생일식단 관리</span>
				</div>
				<div class="toggle-btn"></div>
			</div>
			<ul class="aside-submenu">
				<li><a href="${ pageContext.servletContext.contextPath }/pa/ManageBirthdayMenu">생일 메뉴 조회</a></li>
				<li><a href="${ pageContext.servletContext.contextPath }/pa/UpdateBirthdayMenu">생일 메뉴 수정</a></li>
				<li><a href="${ pageContext.servletContext.contextPath }/pa/RegisterBirthdayMenu">생일 메뉴 등록</a></li>
			</ul>
		</li>
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span onclick="location.href='${pageContext.servletContext.contextPath}/pa/PromotionMangeRedirect'">광고 관리</span>
		</li>
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span>쿠폰관리</span>
				</div>
				<div class="toggle-btn"></div>
			</div>
			<ul class="aside-submenu">
				<li><a href="${ pageContext.servletContext.contextPath }/pa/CouponRegister">쿠폰등록</a></li>
				<li><a href="${ pageContext.servletContext.contextPath }/pa/IssueCoupounPage">쿠폰발급</a></li>
			</ul>
		</li>
	</ul>
</aside>

<script>
	// 서브 메뉴토글
	const toggleElements = document.getElementsByClassName("aside-menu-subbox");

	for (let i = 0; i < toggleElements.length; i++) {
		const element = toggleElements[i];

		element.addEventListener("click", function() {
			element.children[1].classList.toggle('active');

			if (element.nextElementSibling.style.maxHeight){
				element.nextElementSibling.style.maxHeight = null;
			} else {
				element.nextElementSibling.style.maxHeight = element.nextElementSibling.scrollHeight + "px";
			}
		});
	}

	// 현재 페이지의 intent에 의해 사이드바의 active 상태 변경
	// intent, contextPath는 header에서 이미 선언해둠

	// main: 사이드바 메뉴 순번
	// sub: 사이드바 서브메뉴 순번
	let main, sub;
	switch (intent) {
		case '/subs/meal?by=all':
			main = 1; sub = 1; break;
		case '/subs/meal?by=popularity':
			main = 1; sub = 2; break; 
		case '/subs/meal?by=healthy':
			main = 1; sub = 3; break;
		case '/subs/meal?by=premium':
			main = 1; sub = 4; break;
	}
	
	if(main && sub) {
		const preOpenedElement = document.querySelector('.aside-menu-box :nth-child(' + main + ') .aside-submenu');
		const preSelectedMenu = document.querySelector('.aside-menu');
		preSelectedMenu.classList.add('active');
		const preSelectedSubMenu = document.querySelector('.aside-menu-box :nth-child(' + main + ') .aside-submenu :nth-child(' + sub + ')');
		preSelectedSubMenu.classList.add('active');
		preOpenedElement.style.maxHeight = preOpenedElement.scrollHeight + "px";
	}
</script>
