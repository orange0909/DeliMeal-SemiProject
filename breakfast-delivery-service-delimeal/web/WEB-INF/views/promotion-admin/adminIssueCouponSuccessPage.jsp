<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        #mainContent{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
        }
        table{
            text-align: center;
            width: 85%;
            text-align : center;
            border: 1px solid #898888;
			border-collapse: collapse;
        }
        td{
			border: 1px solid #898888;
			text-align: center;
			padding: 5px;
		}
        .nameCell{
            background-color: #E5E5E5;
        }
    </style>
    <meta charset="UTF-8">
    <title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
        	    <h2 id="title">쿠폰 발급</h2>
                <div id="subTitle">
                    <h5 class="sub-title">쿠폰 발급 성공</h5><div class="sub-title-tail"></div>
                </div>
                <div id="mainContent">
                    <h1>쿠폰 발급 성공!</h1>
                    <table>
                        <tr>
                            <td class="nameCell">쿠폰 일련번호</td>
                            <td class="nameCell">만료 기간</td>
                            <td class="nameCell">사용가능여부</td>
                            <td class="nameCell">회원 코드</td>
                            <td class="nameCell">발급된 쿠폰 종류</td>
                        </tr>
                        <tr>
                            <td>${ issuedData.issuedCouponCode }</td>
                            <td>${ issuedData.exprieDate }</td>
                            <td>${ issuedData.isValidate }</td>
                            <td>${ issuedData.memberCode }</td>
                            <td>${ issuedData.couponCode }</td>
                        </tr>
                    </table>
                </div>
            </article>
        </section>
</body>
</html>