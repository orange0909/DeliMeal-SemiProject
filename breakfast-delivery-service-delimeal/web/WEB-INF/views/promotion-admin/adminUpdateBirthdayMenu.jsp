<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        .inputext{
			border: 1px solid #898888;
			border-radius: 10px 10px 10px 10px;
		}
        #searchButton{
			height: 100%;
			background-color: rgba(132, 192, 4, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            margin-left: 5px;
            width: 55px;
		}
        table{
            width: 95%;
            table-layout: fixed;
            text-align : center;
            border: 1px solid #898888;
			border-collapse: collapse;
        }
        .tableData{
			border: 1px solid #898888;
			text-align: center;
			padding: 5px;
		}
        #tableHeader{
			background-color: #E5E5E5;
		}
        #udateInfo{
            display: flex;
            flex-direction: column;
        }
        .inputArea{
            margin:10px 0;
            flex-direction: row;
        }
        .inputLabel {
            margin-right:10px ;
        }
        #nameData, #pictureData {
            margin-left: 23px;
        }
        #nutrientData {
            margin-left: 16px;
        }
        #pictureButton{
            height: 100%;
			background-color: rgba(142, 142, 142, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            margin-left: 5px;
            width: 55px;
        }
        #updateButtonArea{
            display: flex;
            justify-content: center;
        }
        #updateButton{
            height: 100%;
			background-color: rgba(132, 192, 4, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            width: 10%;
        }
        .inputLabel{
            font-size: 15pt;
        }
    </style>
	<meta charset="UTF-8">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
        	    <h2 id="title">생일 메뉴 관리</h2>
                <div id="subTitle">
                    <h5 class="sub-title">생일 메뉴 수정</h5><div class="sub-title-tail"></div>
                </div>
                <div id="currentPage">
                	<h5 class="current-page">수정할 메뉴 번호 검색</h5>
                </div>
                <div id="searchByCode">
                    <input type="text" id="menuCode" class="inputext">
                    <button id="searchButton"> 검색</button>
                </div>
                <script>
                    $("#searchButton").click(function(){
                        console.log(document.getElementById("menuCode").value);
                        const menuCode = document.getElementById("menuCode").value;

                        if(menuCode === null || menuCode === ""){
                            alert("메뉴코드를 입력해 주세요");
                            $('#menuCode').focus();
                        } else{
                            
                            $.ajax({
                                url: "${pageContext.servletContext.contextPath}/pa/UpdateBirthdayMenuAjax",
                                type: "get",
                                data: {
                                    menuCode: menuCode
                                },
                                success: function(data){
                                    // 비동기 통신 성공 시 테이블 내의 내용이 조회한 값으로 변경된다.
                                    console.table(data);
                                    document.getElementById("codeData").value = data.code;
                                    document.getElementById("selectedCode").innerText = data.code;
                                    document.getElementById("selectedName").innerText = data.name;
                                    document.getElementById("selectedPicture").innerHTML = "<img src=../resources/food/"+data.picture+" width=\"85%\" height=\"85%\">";
                                    document.getElementById("selectedAllergy").innerText = data.allergy;
                                    document.getElementById("selectedCalories").innerText = data.calorie;
                                    document.getElementById("selectNutrient").innerText = data.nutrient;
                                },
                                error: function(request, status){
                                    alert("조회에 실패했습니다.");
                                }
                            })
                        }
                    });
                </script>
                <div id="menuTable">
                    <table>
                        <tr id="tableHeader">
                            <td class="tableData">생일 메뉴 번호</td>
                            <td class="tableData">메뉴 이름</td>
                            <td class="tableData">메뉴 사진</td>
                            <td class="tableData">알러지 정보</td>
                            <td class="tableData">칼로리 정보</td>
                            <td class="tableData">메뉴 정보</td>
                        </tr>
                        <tr>
                            <td id="selectedCode" class="tableData">-</td>
                            <td id="selectedName" class="tableData">-</td>
                            <td id="selectedPicture" class="tableData">-</div></td>
                            <td id="selectedAllergy" class="tableData">-</td>
                            <td id="selectedCalories" class="tableData">-</td>
                            <td id="selectNutrient" class="tableData">-</td>
                        </tr>
                    </table>
                    <br>
                    <div id="currentPage">
                        <h5 class="current-page">수정할 메뉴 정보 입력</h5>
                    </div>
                </div>
                <form action="${pageContext.servletContext.contextPath}/pa/UpdateProcessServlet" method="post" encType="multipart/form-data">
                    <div id="udateInfo">
                	    <div id="codeTitle" class="inputArea">
                            <label for="codeData" class="inputLabel">메뉴코드 </label>
                            <input type="text" id="codeData" class="inputext" name="updateCode">
                        </div>
                        <div id="menuTitle" class="inputArea">
                            <label for="nameData" class="inputLabel">메뉴이름 </label>
                            <input type="text" id="nameData" class="inputext" name="updateName">
                        </div>
                        <div id="menuPicture" class="inputArea">
                            <label for="pictureData" class="inputLabel">메뉴사진 </label>
                            <input type="file" id="pictureData" name="updatePicture">
                        </div>
                        <div id="menuAllergy" class="inputArea">
                            <label for="AllergyData" class="inputLabel">알러지 정보 </label>
                            <input type="text" id="AllergyData" class="inputext" name="updateAllergy">
                        </div>
                        <div id="menuCalorie" class="inputArea">
                            <label for="calorieData" class="inputLabel">칼로리 정보 </label>
                            <input type="text" id="calorieData" class="inputext" name="updateCalorie">
                        </div>
                        <div id="menunutrient" class="inputArea">
                            <label for="nutrientData" class="inputLabel">메뉴 정보 </label>
                            <input type="text" id="nutrientData" class="inputext" name="updateNutrient">
                        </div>
                        <div id="updateButtonArea"><button id="updateButton">수정</button></div>
                    </div>
                </form>
            </article>
        </section>
</body>
</html>