<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        #pictureButton{
            height: 100%;
			background-color: rgba(142, 142, 142, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            margin-left: 5px;
            width: 55px;
        }
        #registerButtonArea{
            display: flex;
            justify-content: center;
        }
        #registerButton{
            height: 100%;
			background-color: rgba(132, 192, 4, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            width: 10%;
            padding: 5px;
            margin-top: 5px;
        }
        td{
            padding: 10px;
        }
        .inputLabel{
            font-size: 15pt;
        }
    </style>
    <meta charset="UTF-8">
    <title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
        	    <h2 id="title">생일 메뉴 관리</h2>
                <div id="subTitle">
                    <h5 class="sub-title">생일 메뉴 등록</h5><div class="sub-title-tail"></div>
                </div>
                    <div id="RegisterTitle">
                        <h5>등록할 생일 메뉴 정보 입력</h5>
                    </div>
                <form action="${pageContext.servletContext.contextPath}/pa/RegisterProcessServlet" method="post" encType="multipart/form-data">
                    <table>
                        <tr>
                            <td><label for="nameData" class="inputLabel">메뉴이름 </label></td>
                            <td><input type="text" id="nameData" class="inputext" name="reigsterName"></td>
                        </tr>
                        <tr>
                            <td><label for="pictureData" class="inputLabel">메뉴사진 </label></td>
                            <td><input type="file" id="pictureData" name="reigsterPicture"></td>
                        </tr>
                        <tr>
                            <td><label for="AllergyData" class="inputLabel">알러지 정보 </label></td>
                            <td><input type="text" id="AllergyData" class="inputext" name="reigsterAllergy"></td>
                        </tr>
                        <tr>
                            <td><label for="calorieData" class="inputLabel">칼로리 정보 </label></td>
                            <td> <input type="text" id="nutrientData" class="inputext" name="reigsterCalorie"></td>
                        </tr>
                        <tr>
                            <td><label for="nutrientData" class="inputLabel">메뉴 정보 </label></td>
                            <td><input type="text" id="nutrientData" class="inputext" name="reigsterNutrient"></td>
                        </tr>
                    </table>
                    <div id="registerInfo">
                        <div id="registerButtonArea"><button id="registerButton">등록</button></div>
                    </div>
                </form>
            </article>
        </section>
</body>
</html>