<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        td{
          padding: 5px;  
        }
        #tableArea{
            display: flex;
            flex-direction: column;
        }
        #registerButton{
            justify-content: center;
			background-color: rgba(132, 192, 4, 0.85);
			border-radius: 10px 10px 10px 10px;
			border: 1px solid white;
			color: white;
            width: 10%;
            padding: 5px;
            margin-top: 5px;
        }
        #registerButtonArea{
            display: flex;
            justify-content: center;
            margin-top: 15%;
        }
    </style>
	<meta charset="UTF-8">
	<title>쿠폰 발급 페이지</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
        	    <h2 id="title">쿠폰 발급</h2>
                <div id="subTitle">
                    <h5 class="sub-title">발급할 쿠폰 정보 입력</h5><div class="sub-title-tail"></div>
                </div>
                <div id="tableArea">
                    <form action="${ pageContext.servletContext.contextPath}/pa/IssueCouponProcess">
                        <table>
                            <tr>
                                <td>회원번호</td>
                                <td><input type="text" name="memberCode" placeholder="쿠폰을 발급할 회원 번호입력"></td>
                            </tr>
                            <tr>
                                <td>쿠폰번호</td>
                                <td><input type="text" name="couponNumber" placeholder="발급할 쿠폰 번호 입력"></td>
                            </tr>
                            <tr>
                                <td>만료기간</td>
                                <td><input type="date" name="couponExpireDate"></td>
                            </tr>
                        </table>
                        <div id="registerInfo">
                            <div id="registerButtonArea"><button id="registerButton">등록</button></div>
                        </div>
                    </form>
                </div>
            </article>
        </section>
</body>
</html>