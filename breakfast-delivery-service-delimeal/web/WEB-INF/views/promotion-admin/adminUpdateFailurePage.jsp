<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <style>
        #failureArea{
            margin: auto;
            display: flex;
            margin-top: 10%;
            justify-content: center;
        }
        #failureAreaColumn{
            flex-direction: column;
            color: #63C246;
        }
        #pictureArea{
            display: flex;
            height: 88%;
            justify-content: center;
    		align-items: center;
        }
        #returnButton{
            justify-content: center;
            margin-top: 20px;
            background-color: rgba(132, 192, 4, 0.85);
            color: white;
            border-radius: 10px 10px 10px 10px;
            text-align: center;
            padding: 8px;
            cursor: pointer;
            border: 1px solid white;
            width: 35%;
        }
        #buttonArea{
            display: flex;
            justify-content: center;
        }
    </style>
    <meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
                <h2 id="title">생일 메뉴 관리</h2>
                <div id="subTitle">
                    <h5 class="sub-title">생일 메뉴 수정</h5><div class="sub-title-tail"></div>
                </div>
                <div id="failureArea">
                    <div id="failureAreaColumn">
                        <h2>생일 메뉴 변경 실패!</h2>
                        <div id="pictureArea"><img src="${ pageContext.servletContext.contextPath}/resources/image/failure.png" width="150px" height="130px" class="picture"></div>
                        <h4 id="pleaseReTry">처음부터 다시 수행해 주세요</h4>
                        <div id="buttonArea">
                            <button onclick="location.href='${pageContext.servletContext.contextPath}/pa/UpdateBirthdayMenu'" id="returnButton">돌아가기</button>
                        </div>
                    </div>
                </div>
            </article>
        </section>
    </div>
</body>
</html>