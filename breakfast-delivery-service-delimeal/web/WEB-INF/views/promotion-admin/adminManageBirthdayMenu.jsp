<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<style>
		table {
			border: 1px solid #898888;
			border-collapse: collapse;
		}
		.tableData{
			border: 1px solid #898888;
			text-align: center;
			padding: 5px;
		}
		#tableHeader{
			background-color: #E5E5E5;
		}
	</style>
	<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
	    <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
		<section>
			<article>
                <div id="subTitle">
                    <h5 class="sub-title">생일메뉴 관리</h5><div class="sub-title-tail"></div>
                </div>
                <div id="currentPage">
                	<h5 class="current-page">전체 생일메뉴 조회</h5>
                </div>
                <table id="BirthdayMenuManageTalbe">
					<tr id="tableHeader">
						<td class="tableData">생일 메뉴 번호</td>
						<td class="tableData">메뉴 이름</td>
						<td class="tableData">메뉴 사진</td>
						<td class="tableData">알러지 정보</td>
						<td class="tableData">칼로리 정보</td>
						<td class="tableData">메뉴 정보</td>
					</tr>
					<c:forEach var="bdayMenu" items="${BirthdayMenu}" varStatus="st">
						<tr id="BirthdayMenu${st.index}">
							<td class="tableData">${ bdayMenu.code }</td>
							<td class="tableData">${ bdayMenu.name}</td>
							<td class="tableData"><img src="../resources/food/${bdayMenu.picture}" width="150px" height="130px" class="picture"></td>
							<td class="tableData">${ bdayMenu.allergy }</td>
							<td class="tableData">${ bdayMenu.calorie }</td>
							<td class="tableData">${ bdayMenu.nutrient }</td>
						</tr>
					</c:forEach> 
                </table>
			</article>
		</section>
</body>
</html>