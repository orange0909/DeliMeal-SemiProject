<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
    <meta charset="UTF-8">
    <title>쿠폰 등록, 발급 실패 패이지</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/adminheader.jsp" />
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-admin/promotionAdminAside.jsp"/>
        <section>
            <article>
                <article>
                    <h2 id="title">쿠폰 관리</h2>
                    <div id="subTitle">
                        <h5 class="sub-title">쿠폰 등록 및 발급</h5><div class="sub-title-tail"></div>
                    </div>
                    <div id="main">
                        <h1>쿠폰 등록 및 발급 실패!</h1>
                        <img src="/delimeal/resources/image/failure.png">
                        <h3>처음부터 다시 수행해 주세요.</h3>
                    </div>
            </article>
        </section>
</body>
</html>