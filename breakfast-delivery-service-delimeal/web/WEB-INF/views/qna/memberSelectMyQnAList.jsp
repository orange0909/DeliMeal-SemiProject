<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/subs/memberMeal.css">
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
 	 <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
  	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
   	
    <title>식단 카테고리</title>
</head>

<body>

	<jsp:include page="/WEB-INF/views/template/header.jsp"/>

    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="../qna/memberServiceAside.jsp"/> 
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">내가 쓴 문의</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">나의 문의글 조회</h5><div class="sub-title-tail"></div>
                </div>
                <div class="container mt-3">
                    <table class="table table-hover" style="text-align: center">
	    				<thead>
					      <tr id="index">
					        <th>글번호</th>
					        <th>문의제목</th>
					        <th>답변여부</th>
					        <th>작성일</th>
					      </tr>
					    </thead>
					    <tbody id="qnaBar">
					    	<c:forEach var="qna" items="${ requestScope.qnaList }">
					    	  <tr>
					    		<td id="qnaNum">${ qna.qnaNumber }</td>
					    		<td>${ qna.qnaName }</td>
					    		
					    		<c:choose>
					    			<c:when test="${qna.qnaAnswerYN eq '답변 완료'}"> 
					    				<td id="qnaAnswerYes" style="text-color: white;">${ qna.qnaAnswerYN }</td> 
					    			</c:when>
					    			<c:when test="${qna.qnaAnswerYN eq '답변 대기중'}"> 
					    				<td id="qnaAnswerYes">${ qna.qnaAnswerYN }</td> 
					    			</c:when>
					    		</c:choose>
					    		
					    		<td><fmt:formatDate value="${qna.qnaDate}" pattern="yy/MM/dd"/></td>
					    	  </tr>
					    	</c:forEach>
					    </tbody>
					  </table>
                </div>
                
                <br>
		<!-- 페이징 -->
		<jsp:include page="../paging/memberMyQnAPaging.jsp"/>
		
		<br>
				<!-- 검색 -->
				<div class="search-area" align="center">
					<form action="${ pageContext.servletContext.contextPath }/qna/selectMyQnA" method="get" style="display:inline-block">
					    <input type="hidden" name="currentPage" value="1">
					    <select id="searchCondition" name="searchCondition">
							<option value="title" ${ requestScope.selectCriteria.searchCondition eq "title"? "selected": "" }>제목</option>
							<option value="contents" ${ requestScope.selectCriteria.searchCondition eq "contents"? "selected": "" }>내용</option>
						</select>
				        <input type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
					
						<button type="submit">검색하기</button>	
						</form>		
				</div>
		
				<!-- 상세보기로 이동하기 -->
				<script>
		 			if(document.getElementsByTagName("td")) {
						const $tds = document.getElementsByTagName("td");
						
						
						for(let i = 0; i < $tds.length; i++){
							
							$tds[i].onmouseenter = function() {
								this.parentNode.style.backgroundColor = "#BAEF61";
								this.parentNode.style.cursor = "pointer";
							}
							
							$tds[i].onmouseout = function() {
								this.parentNode.style.backgroundColor = "white";
							}
							
			 				$tds[i].onclick = function() {
			 					const qnaNum = this.parentNode.children[0].innerText;
								location.href = "${ pageContext.servletContext.contextPath }/qna/detail?qnaNum=" + qnaNum 
									} 
							} 
		 			}
				</script>
            </article>
	    </section>
    </div>
        
</body>

</html>