<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>    
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    <!-- 제목 바 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- 내용 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- 버튼 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <title>문의 작성</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="../qna/memberServiceAside.jsp"/> 
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">문의 하기</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">문의글 작성</h5><div class="sub-title-tail"></div>
                </div>
                <div>
                    <form action="${ pageContext.servletContext.contextPath }/qna/insert" method="post" encType="multipart/form-data" >
                    	<!-- 제목입력 -->
                    	<table class="table">
                    	<tr>
						<td>제목 </td>
						<td></td>
						<td><input required="required" type="text" size="80" name="title" style="border-radius: 8px; border:solid 1px #6C6C6C;" placeholder="제목을 입력하세요."></td>
						</tr>
                    	</table>
                    	<!-- 내용입력 -->
                    	<textarea required="required" class="form-control col-sm-5" name="body" rows="5" style=" resize: none;"></textarea>
						<!-- 기능 버튼들 -->				     	
				     	<table style="width: 950px; ">
				     	<br>
				     		<tr>
				     		<!-- 파일 첨부 -->
				     		<td>
				     			<input type="file" id="file" name="qnaFile" multiple="multiple" accept="image/*">
				     			<!-- 삭제 버튼 연결 (아직 아무것도 연결되지 않음) -->
				     			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16" onclick="deleteFile()" style="cursor: pointer">
 							 		<path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
  							 		<path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
					    		</svg>
					    	</td>
				     		<!-- 작성,이전 버튼  -->
				     		<td style="float: right; position: relative;">
				     			<button type="button" class="btn btn-outline-dark" id="backToList" onclick="back()" style="text-align: center">목록으로</button>
				     			<button type="submit" class="btn btn-outline-dark"  style="text-align: center" >작성하기</button>
				     		</td>
				     		</tr>
				     	</table>
                    </form>
                </div>
            </article>
	    </section>
    </div>
    
    <script>
    	/* 뒤로가기 */
    	function back() {
    		location.href='${ pageContext.servletContext.contextPath }/qna/${ sessionScope.listType == "my" ? "selectMyQnA" : "list"}';
		}
    	
    	/* 업로드 한 파일 옆 엑스박스 눌렸을때 */
    	function deleteFile() {
			$(file).val("");
		}
    	
    </script>
        
</body>

</html>