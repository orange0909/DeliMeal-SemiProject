<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <aside>
   <!-- 사이드바 타이틀 -->
   <div id="asideTitle">고객센터</div>
   <ul class="aside-menu-box">
      <!-- 사이드바 메뉴 1 -->
      <li class="aside-menu" onclick="selectQnA()">
         <div class="aside-menu-subbox" >
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>전체 문의 조회</span>
            </div>
         </div>
      </li>
      <!-- 사이드바 메뉴 2 -->
      <li class="aside-menu" onclick="waitingQnA()">
         <div class="aside-menu-subbox">
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>문의 답변 대기 목록</span>
            </div>
         </div>

      <!-- 사이드바 메뉴 3 -->
      <li class="aside-menu" onclick="selectFeedback()">
         <div class="aside-menu-subbox">
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>메뉴 피드백 조회</span>
            </div>
         </div>

      <li class="aside-menu" onclick="waitingFeedback()">
         <div class="aside-menu-subbox">
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>메뉴 피드백 답변 <br>대기 목록</span>
            </div>
         </div>
         
      </li>
   </ul>
</aside>

<script>
      
       var listType = '/${sessionScope.listType}'
          var intentSimple = intent.split('?')
            
      console.log(intentSimple[0] + listType)

      
   switch (intentSimple[0] + listType) {
      case '/qna/list/all' :
            document.getElementsByClassName('aside-menu')[0].classList.add('active');
            break;
      case '/qna/detail/all' :
            document.getElementsByClassName('aside-menu')[0].classList.add('active');
            break;
      case '/qna/waitingList/waitingList' :
            document.getElementsByClassName('aside-menu')[1].classList.add('active');
            break; 
      case '/qna/detail/waitingList' :
            document.getElementsByClassName('aside-menu')[1].classList.add('active');
            break; 
      case '/feedback/adminSelectList/all' :
            document.getElementsByClassName('aside-menu')[2].classList.add('active');
            break; 
      case '/feedback/adminSelectList/waitingList' :
            document.getElementsByClassName('aside-menu')[2].classList.add('active');
            break; 
      case '/feedback/adminSelectList/waiting' :
            document.getElementsByClassName('aside-menu')[2].classList.add('active');
            break; 
      case '/feedback/detail/all' :
            document.getElementsByClassName('aside-menu')[2].classList.add('active');
            break; 
      case '/feedback/adminWaithingList/all' :
            document.getElementsByClassName('aside-menu')[3].classList.add('active');
            break; 
      case '/feedback/detail/waiting' :
            document.getElementsByClassName('aside-menu')[3].classList.add('active');
            break; 
      case '/feedback/adminWaithingList/waiting' :
            document.getElementsByClassName('aside-menu')[3].classList.add('active');
            break; 
      }
      
 
   
   function selectQnA() {
      location.href = "${ pageContext.servletContext.contextPath }/qna/list"; 
   }
   
   function waitingQnA() {
      location.href = "${ pageContext.servletContext.contextPath }/qna/waitingList";
   }
   
   function selectFeedback() {
      location.href = "${ pageContext.servletContext.contextPath }/feedback/adminSelectList";
   }
   
   function waitingFeedback() {
      location.href = "${ pageContext.servletContext.contextPath }/feedback/adminWaithingList";
      
   }
   
   
</script>