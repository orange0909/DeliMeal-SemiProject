<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

 <aside>
   <!-- 사이드바 타이틀 -->
   <div id="asideTitle">고객센터</div>
   <ul class="aside-menu-box">
      <!-- 사이드바 메뉴 1 -->
      <li class="aside-menu" onclick="selectQnA()">
         <div class="aside-menu-subbox" >
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>전체 문의 조회</span>
            </div>
         </div>
      </li>
      <!-- 사이드바 메뉴 2 -->
      <li class="aside-menu" onclick="createQnA()">
         <div class="aside-menu-subbox">
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>문의하기</span>
            </div>
         </div>

      <!-- 사이드바 메뉴 3 -->
      <li class="aside-menu" onclick="selectQnAMine()">
         <div class="aside-menu-subbox">
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span>내가 쓴 문의</span>
            </div>
         </div>
         
      </li>
   </ul>
</aside>

<script>
   
   // 현재 페이지의 intent에 의해 사이드바의 active 상태 변경
   // intent, contextPath는 header에서 이미 선언해둠
   var listType = '/${sessionScope.listType}'
   var intentSimple = intent.split('?')
   
   console.log(intentSimple[0] + listType)

   var test = location.href.substring(location.href.indexOf(contextPath) + 9);

   
   switch (intentSimple[0] + listType) {
      case '/qna/list/all':
         document.getElementsByClassName('aside-menu')[0].classList.add('active');
         break;
      case '/qna/detail/all':
         document.getElementsByClassName('aside-menu')[0].classList.add('active');
         break;
      case '/qna/insert/my':
         document.getElementsByClassName('aside-menu')[1].classList.add('active');
         break; 
      case '/qna/insert/all':
         document.getElementsByClassName('aside-menu')[1].classList.add('active');
         break; 
      case '/qna/selectMyQnA/my':
         document.getElementsByClassName('aside-menu')[2].classList.add('active');
         break; 
      case '/qna/detail/my':
         document.getElementsByClassName('aside-menu')[2].classList.add('active');
         break; 
   }

   
   
   function selectQnA() {
      document.getElementsByClassName('aside-menu')[0].classList.add('active');
      location.href = "${ pageContext.servletContext.contextPath }/qna/list"; 
   }
   function createQnA() {
      location.href = "${ pageContext.servletContext.contextPath }/qna/insert"; 
   }
   function selectQnAMine() {
      location.href = "${ pageContext.servletContext.contextPath }/qna/selectMyQnA"; 
   }
</script>