<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<title>비밀번호 찾기</title>
<style>
    * {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
    }

    ul {
        list-style: none;
    }

    * {
    }
    #title{
    	margin-top: 150px;
    }
    #cancle_btn {
		border: 1px solid rgb(153, 153, 154);
		color: rgb(96, 95, 95);
		background-color: rgb(245, 246, 242);
		padding: 8px;
		padding-left: 40px;
		padding-right: 40px;
		margin-top: 70px;
		border-radius: 0px;
		text-align: center;
	}
	#confirm_btn {
		border: 1px solid rgb(153, 153, 154);
		color: white;
			background-color: rgba(132, 192, 4);
			padding: 8px;
			padding-left: 40px;
			padding-right: 40px;
			margin-top: 70px;
			border-radius: 0px;
			text-align: center;
	}
	    .box{
	    font-size: 1em;
	    background-color: #fff;
	    border: 1px solid #b7b7b7;
	    color: #666;
	    padding: 0 20px;
	    width: 100%;
	    height: 40px;
	    box-sizing: border-box;
	    vertical-align: middle;
		margin-bottom: 10px;
		margin-left: 20px;
	}
</style>
</head>
<body>
    <jsp:include page="../template/header.jsp"/>

        <div align="center">
            <h2 id="title">비밀번호 변경</h2>
            <h6><small>새로운 비밀번호를 입력하여 주세요.</small></h6>
            <br>
            <br>
   		 <form id="modifyPwd" action="/delimeal/member/modifyPwd" method="post">
            <table>
                <tr>
                    <td>새 비밀번호</td> 
                    <td><input class="box" type="password" id="password1" name="password" placeholder="비밀번호" required><br></td> 
                </tr>
                <tr> 
                    <td>비밀번호 확인</td> 
                    <td><input class="box" type="password" id="password2" name="confirmPassword" placeholder="비밀번호 확인" required><br></td>
                    <td><span id="chkNotice"></span></td>
                </tr>  
            </table>
                <input id="cancle_btn" type="submit" value="취소">
                <input id="confirm_btn" type="submit" value="확인"> 
    		    <input type="text" name="id" style="display:none;" value="${requestScope.id}">
    		    
    		</form>
        </div>
<script>
	
	$(function(){
	    $('#password1').keyup(function(){
	      $('#chkNotice').html('');
	    });
	
	    $('#password2').keyup(function(){
	
	        if($('#password1').val() != $('#password2').val()){
	          $('#chkNotice').text('    비밀번호가 일치하지 않습니다.');
	        } else{
	          $('#chkNotice').text('    비밀번호가 일치합니다.');
	        }
	  });
});
</script> 

</body>
</html>