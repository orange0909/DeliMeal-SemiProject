<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    <style>
		.member-info-box {
			margin-top: 100px;
			width: 150%;
	  		display: flex;
	  		flex-direction: column;
	  		align-items: center;
	    	justify-content: center;
	    	gap: 12px;
		}
		
		.txt-box {
		    display: flex;
		    flex-direction: column;
		    flex-grow: 1;
		    gap: 5px;
		    margin-bottom: 6px;
		}
		
		.txt-box input {
		    width: 100%;
		    border: none;
		    border: solid 1px #BABABA;
		    border-radius: 8px;
		    padding: 5px 10px;
		}
		
		.txt-box2 {
		    display: flex;
		    flex-direction: column;
		    flex-grow: 1;
		    gap: 5px;
		    margin-bottom: 6px;
		}
		
		.txt-box2 input {
		    width: 36.3%;
		    border: none;
		    border: solid 1px #BABABA;
		    border-radius: 8px;
		    padding: 5px 10px;
		}
		
		.search {
		    display: flex;
		    flex-direction: column;
		    flex-grow: 1;
		    gap: 5px;
		    margin-bottom: 6px;
		}
		
		.btn {
		    border: solid 1px #3C8028;
		    border-radius: 4px;
		    color: #3C8028;
		}
		
		.btn:hover {
		    background: #3C8028;
		    color: white;
		}
	</style>
    <title>회원 정보 관리</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="adminAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">회원 정보 관리</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h6 class="sub-title">회원 기본 정보</h6><div class="sub-title-tail"></div>
                </div>
                <section>
		<div class="member-info-box">
			<form id="memberSearchForm" action="${ pageContext.servletContext.contextPath }/admin/memberInfo" method="post">
				<div class="txt-box">
					<h5 align="center">회원 정보 조회</h5>
					<input type="text" name="userName" id="userName" placeholder="이 름">
				<!--  <input type="text" name="phone" id="phone" placeholder="휴대폰 번호">-->
				</div>
				<div class="txt-box2">
				<table>
					<tr>
						<td>
							<select size="1" name="hp1">
								<option value="010">010</option>
								<option value="070">070</option>
								<option value="031">031</option>
								<option value="011">011</option>
							</select>
							<b>-</b>
							<input type="text" name="hp2" size="3" required="required" onkeyup="goFocus(this)">
							<b>-</b>
							<input type="text" name="hp3" size="3" required="required">
						</td>
					</tr>
				</table>
				</div>
				<div class='search'>
					<button type="submit" class="btn" id="searchButton">조 회</button>
				</div>
			</form>
		</div>	
	</section>
</body>
</html>