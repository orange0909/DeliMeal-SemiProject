<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <%-- 개인 페이지 --%>
    
    <title>회원탈퇴</title>
    
<style>
* {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
}

ul {
	list-style: none;
}

/* * {
	color: gray;
} */

#first{
 	background-color: ;
 	width: 900px;
 	height: 100px;
 	margin-left: 90px;
 	font-size: 90%;
 	color: gray;
}   
#box{
	border:1px solid darkgray; 
	width: 770px; 
	height: 80px;
	margin-top: 40px;
}
#agreement{
	margin-left: 10px;
	color: black;
	opacity: 0.8;
	font-weight: ;
}	
#checkbox{
	margin-top: 5px;
}
.deleteBtn {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgba(132, 192, 4);
	padding: 8px;
	padding-left: 50px;
	padding-right: 50px;
	margin-top: 70px;
	margin-left: 300px;
	border-radius: px;
	text-align: center;
}
</style>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2>회원탈퇴</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title"></h5><div class="sub-title-tail"></div>
                </div>
                <div>
                    <%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
                    <div>
                    	<form name="delete" action="/delimeal/mypage/deleteAccount" method="post">
                    		<div id="first" align="left" style="border:1px;" class="table-area">
								<br>
								<br>								
								<small>•</small> 적립된 델리밀 포인트는 탈퇴 즉시 모두 소멸되며, 복원이 불가능 합니다. 
								<br>
								<br>
								<small>•</small> 탈퇴 처리 시, 각 해당 사이트의 이용내역은 모두 삭제 처리 되며, 삭제 후 복원하여 사용할 수 없습니다. 단, 법령에 특별한 
								<br>
								<small>•</small> 규정이 있는 경우에는 관련 법령에 따라 보유 및 이용합니다. (자세한 내용은 개인정보처리방침을 참조하시기 바랍니다.)
								<br>
								<br>
								<small>•</small> 구독 중인 상품이 있을 경우 구독해지를 하셔야 하며, 탈퇴 시 구매내역 및 회원인 동안 보유하고 계셨던 각종 쿠폰은 모두 
								<br>
								삭제됩니다.
								<br>
								<br>
								<small>•</small> 회원 재가입은 탈퇴 후 5일이 지난 이후에 가능합니다.
								<br>
								<br>
							<section id="box">
								<input id="checkbox" value="agreement" class="checkbbox" type="checkbox" name="agree">
								  <h6 id="agreement"><small>안내 사항을 모두 확인하였으며, 탈퇴하는 사이트의 이용 데이터가 복구가 불가함에 동의합니다.
								  <br>
								또한 탈퇴 아이디(ID호출)의 재사용이 불가함에 동의합니다.
								  </small></h6>
							</section>
								<button id="delete" class="deleteBtn" type="button" onclick="successDelete()">탈퇴하기</button>
								<button id="btn" type="submit" style="display:none;" ></button>
							</div>
							<div>
						    </div>	
                    	</form>
                    </div>
                </div>
            </article>
	    </section>
    </div>
<script>
 	function successDelete(){
 		console.log(document.getElementById('checkbox').checked);
 	   	 if (!document.getElementById('checkbox').checked) {
	        alert("안내 사항에 동의하셔야 탈퇴가 가능합니다.");
	   	 } else {
	   		 document.getElementById('btn').click();
	   	 }
	};
</script>
</body>
</html>