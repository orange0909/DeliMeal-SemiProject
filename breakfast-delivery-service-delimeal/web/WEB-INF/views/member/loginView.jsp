<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>로그인</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
	<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
	<style>
		body {
			height: 100vh;
			display: flex;
			align-items: center;
			justify-content: center;
			font-family: 'Noto Sans KR', sans-serif;
		}

		#loginBox {
			display: flex;
			flex-direction: column;
			gap: 15px;
			align-items: center;
			max-width: 500px;
			min-width: 300px;
			width: 80%;
			margin: 0 auto;
			padding: 10px;
			margin-bottom: 15vh;
		}

		.logo {
			width: 150px;
			height: 150px;
			text-align: center;
			background-image: url('${ pageContext.servletContext.contextPath }/resources/image/logo.png');
			background-size: cover;
		}

		#loginForm {
			width: 100%;
			display: flex;
			flex-direction: column;
			gap: 5px;
		}

		#idAndPasswordBox {
			display: flex;
			flex-direction: row;
			gap: 12px;
		}
		
		.txt-box {
			display: flex;
			flex-direction: column;
			flex-grow: 1;
			gap: 5px;
		}

		.txt-box input {
			width: 100%;
			border: none;
			border: solid 1px #BABABA;
			border-radius: 8px;
			padding: 5px 10px;
		}
		
		.btn {
			border: solid 1px #3C8028;
			border-radius: 8px;
			height: 100%;
			color: #3C8028;
		}

		.btn:hover {
			background: #3C8028;
			color: white;
		}

		#keepLoggedIn, #keepLoggedIn + label {
			accent-color: #3C8028;
			cursor: pointer;
		}

		#memberFunction a {
			text-decoration: none;
			color: inherit;
		}

		#memberFunction a:hover {
			color: #3C8028
		}

		.fnt-sm {
			font-size: 14px;
		}

		.fnt-warn {
			color: red;
		}
	</style>
</head>
<body>
	<section id="loginBox">
		<div class="logo"></div>
		<form id="loginForm" action method="post">
			<div id="idAndPasswordBox">
				<div class="txt-box">
					<input type="text" name="userId" id="userId" placeholder="아이디" value="${ requestScope.inputtedId }">
					<input type="password" name="password" id="password" placeholder="비밀번호">
				</div>
				<div class="btn-box">
					<button type="button" class="btn" id="loginButton" onclick="login()">로그인</button>
				</div>
			</div>
			<div>
				<input type="checkbox" name="keepLoggedIn" id="keepLoggedIn">
				<label for="keepLoggedIn" class="fnt-sm">로그인 상태 유지</label>
			</div>
			<div>
				<span id="message" class="fnt-sm fnt-warn">${ requestScope.loginFailureMessage }</span>
			</div>
		</form>
		<div id="memberFunction">
			<a href="/delimeal/member/findId" class="fnt-sm">아이디 찾기</a> | <a href="/delimeal/member/findPwd" class="fnt-sm">비밀번호 찾기</a> | <a href="/delimeal/member/regist" class="fnt-sm">회원가입</a>
		</div>
	</section>
	<script>
		const userIdTxtBox = document.getElementById("userId");
		const passwordTxtBox = document.getElementById("password");
		const messageElement = document.getElementById("message");

		userIdTxtBox.addEventListener("keyup", keyListener);
		passwordTxtBox.addEventListener("keyup", keyListener);

		function keyListener(e) {
			if(e.key == "Enter") {
				document.getElementById("loginButton").click();
			}
		}

		function login() {
			const loginForm = document.getElementById("loginForm");

			if(userIdTxtBox.value == "") {
				messageElement.textContent = "아이디를 입력해주세요.";
				return;
			}

			if(passwordTxtBox.value == "") {
				messageElement.textContent = "비밀번호를 입력해주세요.";
				return;
			}

			loginForm.submit();
		}
	</script>
</body>
</html>