<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    
    <title>나의 쿠폰 조회</title>
    
<style>
    * {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
    }

    ul {
        list-style: none;
    }

/*  	#first{
 		width: 700px;
 		height: 100px;
 		display: flex;
	    padding: 30px 40px;
	    margin-bottom: 30px;
	    margin-top: 50px;
	    margin-left: 150px;
		opacity: 0.7;
		border-color: 1px solid gray;
    color: #333;
    /* text-align: center; */
    box-sizing: border-box;
    vertical-align: top;
	    font-size: 15px;
		
 	}   */
 	

 #box{
 	display: flex;
    background-color: #113e3a;
    padding: 30px 40px;
    margin-bottom: 30px;
    margin-left: 300px;
 }	
 #coupon{
 	margin-left:400px;
 }
 
 #name{
 	color: rgba(132, 192, 4);
 } 	
 #hey{
 	text-decoration:underline;
 }
</style>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">마이페이지</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div  id="subTitle">
                    <h5 class="sub-title"></h5><div class="sub-title-tail"></div>
                </div>
                <div>
                    <%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
                    <div align="center" >
	                    <h2><small>반갑습니다</small>
	                    <big><strong id="name">${ requestScope.memberName }</strong></big>님.</h2>
	                    <br>
	                    <h5>보유 쿠폰  <big id="hey" onclick="location.href='/delimeal/mypage/coupons'"><strong>${ requestScope.result } <strong></strong></big>장</h5>
                    </div>
                </div>
            </article>
	    </section>
    </div>
        
</body>

</html>