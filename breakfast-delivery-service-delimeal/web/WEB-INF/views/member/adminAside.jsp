<%@ page language="java" contentType="text/html; charset=UTF-8"
   pageEncoding="UTF-8"%>
<aside>
   <!-- 사이드바 타이틀 -->
   <div id="asideTitle">회원 관리</div>
   <ul class="aside-menu-box">
      <!-- 사이드바 메뉴 1 -->
      <li class="aside-menu">
         <div class="aside-menu-subbox">
            <div class="aside-menu-name">
               <div class="selected-menu"></div>
               <span onclick="memberInfo">회원 정보 관리</span>
            </div>
            <!-- <div class="toggle-btn"></div> -->
         </div>
         <!-- 사이드바 서브메뉴 (펼치기/접기) -->
         <!--<ul class="aside-submenu">
            <li>회원 정보 관리</li>
            <li>구독 관리</li>
            <li>환불 처리</li>
         </ul>-->
      </li>
   </ul>
</aside>

<script>
   // 서브 메뉴토글
   const toggleElements = document.getElementsByClassName("aside-menu-subbox");

   for (let i = 0; i < toggleElements.length; i++) {
      const element = toggleElements[i];

      element.addEventListener("click", function() {
         element.children[1].classList.toggle('active');

         if (element.nextElementSibling.style.maxHeight){
            element.nextElementSibling.style.maxHeight = null;
         } else {
            element.nextElementSibling.style.maxHeight = element.nextElementSibling.scrollHeight + "px";
         }
      });
   }

   // 현재 페이지의 intent에 의해 사이드바의 active 상태 변경
   // intent, contextPath는 header에서 이미 선언해둠

   switch (intent) {
      case '/admin/memberInfo':
         document.getElementsByClassName('aside-menu-subbox')[0].classList.add('active');
      break;
   }
   
   function memberInfo() {
      location.href = "${ pageContext.servletContext.contextPath }/admin/memberInfo"; 
   }
</script>