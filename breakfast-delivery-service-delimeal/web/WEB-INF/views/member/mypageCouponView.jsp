<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <%-- 개인 페이지 --%>
    
    <title>나의 쿠폰 조회</title>
    
<style>
#line{
	display: flex;
    align-items: center;
    width: 100%;
    min-height: 80px;
    border-bottom: 1px solid #e1e1e1;
    padding: 10px 0;
    box-sizing: border-box;
}
#idSubtitle{
	margin-top:500px;
}
.form_table{
    padding: 20px 45px;
    font-size: 0.866em;
    background-color: #f7f7f7;
    color: #333;
    /* text-align: center; */
    width: 300px;
    box-sizing: border-box;
    border-bottom: 1px solid #ddd;
    vertical-align: top;
}
#message{
	color: gray;
	font-size: 13px;
}
	
</style>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2>쿠폰 조회</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title"></h5><div class="sub-title-tail"></div>
                </div>
                <div>
                    <%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
                    <div>
                    	<form action="/delimeal/mypage/coupons" method="get">
                    		<div id="line" class="table-area">
								<table align="center" id="listArea">
									<tr align="center">
										<th class="form_table" width="180px">쿠폰번호</th>
										<th class="form_table" width="200px">쿠폰이름</th>
										<th class="form_table" width="300px">설명</th>
										<th class="form_table" width="80px">할인률</th>
										<th class="form_table" width="150px">만료일</th>
									</tr>
									<c:forEach items="${ issuedCouponList }" var="coupon">
										<tr align="center">
											<td><c:out value="${ coupon.issuedCpnCode }"/></td>
											<td><c:out value="${ coupon.cpnName }"/></td>
											<td><c:out value="${ coupon.cpnDescription }"/></td>
											<td><c:out value="${ coupon.cpnDiscountRate }%"/></td>
											<%-- <td><c:out value="${ coupon.issuedIsValidate }"/></td> --%>
											<td><c:out value="${ coupon.issuedExpDate }"/></td>
										</tr>
									</c:forEach>
								</table>
							</div>
                    	</form>
                    </div>

                </div>
                <div id="subTitle">
                    <h5 class="sub-title"></h5>
                <div class="sub-title-tail"></div>
                </div>
                <div align="right">
                  <h6 id="message"><small>•</small> 유효기간이 지난 쿠폰은 사용할 수 없습니다.</h6>
				  <h6 id="message"><small>•</small> 환불/부분환불/취소 시 환급 또는 재발행 되지 않습니다.</h6>
                </div>				  
            </article>
	    </section>
    </div>
</body>
</html>