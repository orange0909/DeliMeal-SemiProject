<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
 <meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <%-- 개인 페이지 --%>
    
    <title>회원 정보 수정</title>
    
<style>
* {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
}

ul {
	list-style: none;
}

/* * {
	color: gray;
} */

#first{
    font-family: 'Roboto', 'Noto Sans KR', Arial, sans-serif;
    color: #666;
    font-size: 15px;
    line-height: 1.5;
    letter-spacing: 0;
    font-weight: normal;
    min-width: 320px;
    background-color: #fff;
    word-break: keep-all;
}   
#box{
	border:1px solid darkgray; 
	width: 800px; 
	height: 500px;
	margin-top: 40px;
	margin-left: 100px;
	
}
#agreement{
	margin-left: 10px;
	color: black;
	opacity: 0.8;
	font-weight: ;
}	
#checkbox{
	margin-top: 5px;
}
#btn {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgba(132, 192, 4);
	padding: 8px;
	padding-left: 50px;
	padding-right: 50px;
	margin-top: 70px;
	border-radius: px;
	text-align: center;
}
#password{
	color: lightgray;
}
#font{
	font-color: gray;
	opacity: 0.5;
}
.form_table{
    padding: 20px 25px;
    font-size: 0.866em;
    background-color: #f7f7f7;
    color: #333;
    /* text-align: center; */
    width: 150px;
    box-sizing: border-box;
    border-bottom: 1px solid #ddd;
    vertical-align: top;
    /* border-top: 1px solid black; */
}
.contents{
	margin-left: 50px;
	border-bottom: 1px solid #ddd;
	color:#666;
	font-color: gray;
    /* border-top: 1px solid black; */
}
#totheleft{
	margin-left:10px;
}
</style>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2>회원 정보 수정</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title"></h5><div class="sub-title-tail"></div>
                </div>
                <div>
                    <%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
                    <div>
                    	<form name="selectMemberForUpdating" action="/delimeal/mypage/updateMember" method="post">
                    		<div id="box" align="center" style="border:1px;" class="table-area">
								<table>
								<tr>
									<td class="form_table">이름</td>
									<td class="contents">${requestScope.member.name}</td>	
								</tr>
								<tr>
									<td class="form_table">아이디</td>
									<td class="contents">${requestScope.member.id}</td>	
								</tr>
								<tr>
									<td class="form_table">비밀번호</td>
									<td class="contents"><input size="50" id="password1" name="password" type="text" value=""></td>	
								</tr>
								<tr>
									<td class="form_table">비밀번호 확인</td>
									<td class="contents"><input size="50"id="password2" type="text" value=""></td>	
									<td><span id="chkNotice"></span></td>
								</tr>
								<tr>
									<td class="form_table">생년월일</td>
									<td class="contents">${requestScope.member.bday}</td>	
								</tr>
								<tr>
									<td class="form_table">성별</td>
									<td class="contents">${requestScope.member.gender}</td>
								</tr>
								<tr>
									<td class="form_table">이메일</td>
									<td class="contents"><input size="50"name="email" type="text" value="${requestScope.member.email}"></td>	
								</tr>
								<tr>
									<td class="form_table">휴대전화 번호</td>
									<td class="contents"><input size="50"name="phone" id="phoneNumber" class="phoneNumber" type="text" value="${requestScope.member.phone}"></td>	
								</tr>																
								</table>
								<button id="btn" type="submit" onclick="confirmPwd()">수정</button>
							</div>
							
							
							<div>
						    </div>	
                    	</form>
                    </div>
                </div>
            </article>
	    </section>
    </div>
    
    
<!--     <script type="text/javascript">
		function confirmPwd(){
			var p1 = document.getElementById('password1').value();
			var p2 = document.getElementById('password2').value();
			
			if( p1 != p2){
				alert("비밀번호가 일치하지 않습니다.")
			} else {
				alert("회원정보가 변경되었습니다.");
			}
		}
    
    </script>     -->
    
<!-- /* 비밀번호 일치 스크립트 */ -->
<script type="text/javascript">
	
	$(function(){
	    $('#password1').keyup(function(){
	      $('#chkNotice').html('');
	    });
	
	    $('#password2').keyup(function(){
	
	        if($('#password1').val() != $('#password2').val()){
	          $('#chkNotice').text('비밀번호가 일치하지 않습니다.');
	           $('#chkNotice').attr('color', 'red'); 
	        } else{
	          $('#chkNotice').text('비밀번호가 일치합니다.');
	           $('#chkNotice').attr('color', 'blue'); 
	        }
	  });
});
</script>   
<script>
	$(document).on("keyup", "#phoneNumber", function() { 
		$(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
	});
</script>    
</body>

</html>