<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<aside>
	<!-- 사이드바 타이틀 -->
	<div id="asideTitle">My page</div>
	<ul class="aside-menu-box">
		<!-- 사이드바 메뉴 1 -->
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span onclick="location.href='/delimeal/mypage/favMenu'">선호 메뉴</span>
				</div>
			</div>
		</li>
		<!-- 사이드바 메뉴 2 -->
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span onclick="location.href='/delimeal/mypage/coupons'">쿠폰 조회</span>
				</div>
			</div>
		</li>
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span>회원 정보</span>
				</div>
				<div class="toggle-btn"></div>
			</div>
			<!-- 사이드바 서브메뉴 (펼치기/접기) -->
			<ul class="aside-submenu">
				<li onclick="location.href='/delimeal/mypage/updateMember'">회원 정보 수정</li>
				<li onclick="location.href='/delimeal/mypage/deleteAccount'">회원 탈퇴</li>
			</ul>
		</li>
		
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span onclick="location.href='/delimeal/mypage/feedbackList'">메뉴 피드백</span>
				</div>
			</div>
		</li>
	</ul>
</aside>

<script>
	// 서브 메뉴토글
	const toggleElements = document.getElementsByClassName("aside-menu-subbox");

	for (let i = 0; i < toggleElements.length; i++) {
		const element = toggleElements[i];

		element.addEventListener("click", function() {
			element.children[1].classList.toggle('active');

			if (element.nextElementSibling.style.maxHeight){
				element.nextElementSibling.style.maxHeight = null;
			} else {
				element.nextElementSibling.style.maxHeight = element.nextElementSibling.scrollHeight + "px";
			}
		});
	}

	// 현재 페이지의 intent에 의해 사이드바의 active 상태 변경
	// intent, contextPath는 header에서 이미 선언해둠

	var intentSimple = intent.split('?')
	console.log(intentSimple[0])
	
	switch (intentSimple[0]) {
		case '/mypage/feedbackList':
			document.getElementsByClassName('aside-menu')[3].classList.add('active');
			break;
		case '/feedback/detail':
			document.getElementsByClassName('aside-menu')[3].classList.add('active');
			break;
	}
</script>
