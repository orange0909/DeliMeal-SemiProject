<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<title>비밀번호 찾기</title>
<style>
    * {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
    }

    ul {
        list-style: none;
    }

    * {
    }
    #title{
    	margin-top: 150px;
    }
    .box{
	    font-size: 1em;
	    background-color: #fff;
	    border: 1px solid #b7b7b7;
	    color: #666;
	    padding: 0 20px;
	    width: 100%;
	    height: 40px;
	    box-sizing: border-box;
	    vertical-align: middle;
		margin-bottom: 10px;
		margin-left: 20px;
	}
	#cancle_btn {
		border: 1px solid rgb(153, 153, 154);
		color: rgb(96, 95, 95);
		background-color: rgb(245, 246, 242);
		padding: 8px;
		padding-left: 40px;
		padding-right: 40px;
		margin-left: px;
		margin-top: 40px;
		border-radius: 0px;
		text-align: center;
	}
	#confirm_btn {
		border: 1px solid rgb(153, 153, 154);
		color: white;
		background-color: rgba(132, 192, 4);
		padding: 8px;
		padding-left: 40px;
		padding-right: 40px;
		margin-top: 40px;
		margin-left: 5px;
		border-radius: 0px;
		text-align: center;
	}
.phoneNumber{
	    font-size: 1em;
	    background-color: #fff;
	    border: 1px solid #b7b7b7;
	    color: #666;
	    padding: 0 20px;
	    width: 100%;
	    height: 40px;
	    box-sizing: border-box;
	    vertical-align: middle;
		margin-bottom: 10px;
		margin-left: 20px;

}
</style>
</head>
<body>

	<jsp:include page="../template/header.jsp"/>

    <div align="center" class="c">
        <h2 id="title">비밀번호 찾기</h2>
        <br>
        <h6>
        	<small>회원 정보에 등록된 정보와 일치하여야  
            <br>비밀번호를 재설정 할 수 있습니다.
       		</small>
       	</h6>	
        <br>
        <br>    
        <form id="findPwd" action="/delimeal/member/findPwd" method="post">
            <table id="contents">
                <tr>
                    <td><label>아이디</label></td>
                    <td><input id="id" class="box" type="text" name="id" placeholder="아이디" required></td>
                </tr>  
                <tr>
                    <td><label>이름</label></td>
                    <td><input id="name" class="box" type="text" name="name" placeholder="이름" required></td>
                </tr>  
                <tr>  
                    <td><label>휴대폰 번호</label></td>
                    <td><input id="phone" type="text" class="phoneNumber" name="phone" placeholder="- 없이 숫자만 입력" required></td>
        </table>
        <div class="" align="center">
                    <button type="button" id="cancle_btn" onclick="location.href='/delimeal/login'">취소</button>
                    <button type="submit" id="confirm_btn" onclick="location.href='/delimeal/member/modifyPwd'">확인</button>
                    
        </div>
        </form>    
    </div>
    
<!--  휴대폰번호 자동 하이픈 삽입 -->
 <script>
   $(document).on("keyup", ".phoneNumber", function() { 
      $(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
   });
</script> 
</body>
</html>