<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    
    <title>선호 메뉴 등록</title>
    
<style>
#content{
	margin-bottom: 300px;
}
#move{
	margin-top:50px;
}
#mymenu{
}
#line{
	display: flex;
    align-items: center;
    width: 100%;
    min-height: 80px;
    border-bottom: 1px solid #e1e1e1;
    padding: 10px 0;
    box-sizing: border-box;
}
#idSubtitle{
	margin-top:500px;
}
.form_table{
    padding: 20px 45px;
    font-size: 0.866em;
    background-color: #f7f7f7;
    color: #333;
    /* text-align: center; */
    width: 900px;
    box-sizing: border-box;
    border: 1px solid #ddd;
    vertical-align: top;
    margin-bottom: 150px;
}
#btn {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgba(132, 192, 4);
	padding: 8px;
	padding-left: 50px;
	padding-right: 50px;
	margin-top: 70px;
	margin-bottom: 80px;
	text-align: center;
}
	
</style>
</head>
<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2>선호 메뉴</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title"></h5><div class="sub-title-tail"></div>
                </div>
                <div align="center">
                    <%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
                    <br>
					<h6>선호메뉴를 등록하시면 선호메뉴에 대한 정보를 확인하실 수 있습니다.
					<br>메뉴 이름을 입력하거나 검색 칸을 눌러 선택해주세요.</h6>
					<br>
                    <div class="form_table">
                    	<form action="/delimeal/mypage/favMenu" method="post">
							<div id="move" align="center">
                    		<h6 id="mymenu">나의 선호메뉴
                    		<br id="font"> [     ${ requestScope.result }     ]</h6>
                    			<br>
                    			<br>
								<h5><label>선호 메뉴 검색</label></h5>
								<br>
								<input name="favoriteMenu" type="text" list="favoriteMenuList">
							<datalist id="favoriteMenuList">
								<c:forEach var="list" items="${ requestScope.menuList }">
									<option value="${ list.menuName }">
								</c:forEach>
							</datalist>
							<br>
							<br>
							<button id="btn" type="submit">등록</button>
							</div>
                    	</form>
                    </div>
                </div>
            </article>
	    </section>
    </div>
</body>
</html>