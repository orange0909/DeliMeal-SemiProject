<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<aside>
	<!-- 사이드바 타이틀 -->
	<div id="asideTitle">배송 관리</div>
	<ul class="aside-menu-box">
		<!-- 사이드바 메뉴 1 -->
		<li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span>배송 여부 조회 및 수정</span>
				</div>
				<!-- <div class="toggle-btn"></div> -->
			</div>
			<!-- 사이드바 서브메뉴 (펼치기/접기) -->
			<!--  <ul class="aside-submenu">
				<li>전체 식단</li>
				<li>인기 식단</li>
				<li>건강 식단</li>
				<li>프리미엄 식단</li>
			</ul>
		</li>
		<!-- 사이드바 메뉴 2 -->
		<!-- <li class="aside-menu">
			<div class="aside-menu-subbox">
				<div class="aside-menu-name">
					<div class="selected-menu"></div>
					<span>구독 정보</span>
				</div>
				<div class="toggle-btn"></div>
			</div> -->
			<!-- 사이드바 서브메뉴 (펼치기/접기) -->
			<!-- <ul class="aside-submenu">
				<li>구독 정보 조회</li>
				<li>배송 여부 조회</li>
				<li>허브 위치 조회</li>
			</ul>
		</li>-->
	</ul>
</aside>

<script>
	// 서브 메뉴토글
	const toggleElements = document.getElementsByClassName("aside-menu-subbox");

	for (let i = 0; i < toggleElements.length; i++) {
		const element = toggleElements[i];

		element.addEventListener("click", function() {
			element.children[1].classList.toggle('active');

			if (element.nextElementSibling.style.maxHeight){
				element.nextElementSibling.style.maxHeight = null;
			} else {
				element.nextElementSibling.style.maxHeight = element.nextElementSibling.scrollHeight + "px";
			}
		});
	}

	// 현재 페이지의 intent에 의해 사이드바의 active 상태 변경
	// intent, contextPath는 header에서 이미 선언해둠

	// main: 사이드바 메뉴 순번
	// sub: 사이드바 서브메뉴 순번
	let main, sub;
	switch (intent) {
		case '/subs/meal':
			main = 1; sub = 1; break;
		default:
			main = 1; sub = 1; break;
	}
	
	const preSelectedMenu = document.querySelector('.aside-menu');
	preSelectedMenu.classList.add('active');
	
	
</script>
