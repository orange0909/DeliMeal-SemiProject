<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap"
	rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<title>회원가입</title>
<style>
* {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
}
ul {
	list-style: none;
}
* {
}
.title {
	margin-top: 100px;
	margin-left: 20;
	margin-bottom: 100px;
	font-weight: 700;
}
#btn_group {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgb(152, 154, 156);
	padding: 6px;
	margin-left: 3px;
	border-radius: 0px;
	vertical-align: middle;
	margin-bottom: 10px;
}
#phoneCheck {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgb(152, 154, 156);
	padding: 6px;
	margin-left: 3px;
	border-radius: 0px;
	vertical-align: middle;
	margin-bottom: 10px;
}
#box{
    font-size: 1em;
    background-color: #fff;
    border: 1px solid #b7b7b7;
    color: #666;
    padding: 0 20px;
    width: 100%;
    height: 40px;
    box-sizing: border-box;
    vertical-align: middle;
	margin-bottom: 10px;
}
.phoneNumber{
	font-size: 1em;
    background-color: #fff;
    border: 1px solid #b7b7b7;
    color: #666;
    padding: 0 20px;
    width: 100%;
    height: 40px;
    box-sizing: border-box;
    vertical-align: middle;
	margin-bottom: 10px;
}
.email-box{
	font-size: 1em;
    background-color: #fff;
    border: 1px solid #b7b7b7;
    color: #666;
    padding: 0 20px;
    height: 40px;
    box-sizing: border-box;
    vertical-align: middle;
}
#contents {
	margin: auto;
}
#list {
	font-size: 95%;
	font-weight: lighter;
}
#info {
	font-size: 75%;
}
.clickBox {
	color: green;
	margin-right: 50px;
}
.checkbt {
	border: 0 solid rgb(119, 97, 97);
	color: white;
	background-color: lightgray;
	border-radius: 40;
	margin-left: px;
}
.cancel_btn {
	border: 1px solid rgb(153, 153, 154);
	color: rgb(96, 95, 95);
	background-color: rgb(245, 246, 242);
	padding: 8px;
	padding-left: 70px;
	padding-right: 70px;
	margin-left: 600px;
	margin-top: 70px;
	border-radius: 0px;
	text-align: center;
}
.join_btn {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgba(132, 192, 4);
	padding: 8px;
		padding-left: 55px;
	padding-right: 55px;
	margin-top: 70px;
	margin-left: 5px;
	border-radius: 0px;
	text-align: center;
}
#gender-font {
	font-size: 100%;
	font-weight: lighter;
}
#signUp{
	margin-left: 650px;
	align: center;
}
.box{
    font-size: 1em;
    background-color: #fff;
    border: 1px solid #b7b7b7;
    color: #666;
    padding: 0 20px;
    width: 100%;
    height: 40px;
    box-sizing: border-box;
    vertical-align: middle;
	margin-bottom: 10px;
}
.bdayBox{
	width: 30px;
}
#announce{
	margin-left: 770px;
}
</style>
</head>
<body>

	<jsp:include page="../template/header.jsp"/>

	<div class="title">
		<h1 id="signUp">회원가입 <small>sign up</small></h1>
		<br> <br> <br>
		<form id="joinform" action="/delimeal/member/regist" method="post">
			<table id="contents">
				 <h6 id="announce"><small>* 표시는 반드시 입력하셔야 하는 항목입니다.</small></h6> 
				<tr class="txt-box">
					<td id="list" width="120" height="35"><label>아이디*</label></td>
					<td><small><input type="text" name="id" id="id" class="box" maxlength="15" placeholder="아이디" required/></small></td>
					<td><input type="button" id="btn_group" value="중복확인"></td> 
					<td><span id="result"></span></td>
				</tr>
				<tr>
					<td id="list" width="120" height="35"><label>비밀번호*</label></td>
					<td><small><input id="password1" size="23" class="box" type="password" maxlength="13"
						name="password" placeholder="비밀번호" onkeydown="passwordcheck();" required></small></td>
				</tr>
				<tr>
					<td id="list" width="120" height="35"><label>비밀번호 확인*</label></td>
					<td><small><input id="password2" size="23" class="box" type="password" name="password2"
						placeholder="비밀번호 확인" required></small></td>
					 <td><span id="chkNotice"></span></td>
				</tr>
				<tr>
					<td id="list" width="120" height="35"><label>이름*</label></td>
					<td><small><input id="name" class="box" size="23" type="text" name="name" placeholder="이름" required></small></td>
				</tr>
				<tr>
					<td id="list"><label>휴대폰 번호*</label></td>
					<td><small><input class="phoneNumber" size="25" type="text" id="phone" name="phone"
						 placeholder="- 없이 숫자만 입력" required></small></td>
					<td><input type="button" id="phoneCheck" value="중복확인"></td> 
						
				</tr>
				<tr>
					<td id="list" width="120" height="35"><label>생년월일*</label></td>
					<td><input id="bday" type="date" name="bday" required></td>
				</tr>
				<tr>
				<td id="list" width="120" height="35"><label>성별</label></td>
				<td><input type="radio" id="male" name="gender" value="M" checked> 
					<label id="gender-font" for="male">남</label> 
					<input type="radio" id="female"name="gender" value="F"> 
					<label id="gender-font" for="female">여</label></td>
				</tr>
				<tr>
					<td id="list" width="120" height="35"><label>이메일</label></td>
					<td>
						<input class="box" type="text" id="email" size="23" name="email" placeholder="이메일">
					</td> 
				</table>
			<table>
				<tr>
					<td align="center"><input onclick="location.href='/delimeal'" class="cancel_btn" type="button" value="취소" id="goMain"></td>
					<td><input class="join_btn" id="submit" type="submit" value="가입하기" onclick="location.href='/delimeal/login'" ></td>		
				</tr>
			</table>	
		</form>
	</div>
<script>
/* 아이디 중복 체크 스크립트 */
// 아이디를 서버로 전송 > DB 유효성 검사 > 결과 반환받기
	$('#btn_group').click(function () {

		if ($('#id').val() != '') {
					   
			$.ajax({
				type: 'GET',
				url: '/delimeal/idCheck',
				data: 'id=' + $('#id').val(),
				dataType: 'json',
				success: function(result) {
					
					 console.log(result); 
					if (result == '0') {
						alert('사용 가능한 아이디입니다.');
						$('#password1').focus();
					} else {
						alert('이미 사용중인 아이디입니다.');
					}
				},
				error: function(a, b, c) {
					console.log(a, b, c);
				}
			});
		} else {
			alert('아이디를 입력하세요.');
			$('#id').focus();
		}
	});
</script>

<!-- 휴대폰번호 중복 체크  -->
<script>
	$('#phoneCheck').click(function () {

		if ($('#phone').val() != '') {
					   
			$.ajax({
				type: 'GET',
				url: '/delimeal/CheckPhone',
				data: 'phone=' + $('#phone').val(),
				dataType: 'json',
				success: function(result) {
					
					 console.log(result); 
					if (result == '0') {
						alert('사용 가능한 휴대폰번호입니다.');
						$('#phone').focus();
					} else {
						alert('이미 사용중인 휴대폰번호입니다.');
					}
				},
				error: function(a, b, c) {
					console.log(a, b, c);
				}
			});
		} else {
			alert('휴대폰번호를 입력하세요.');
			$('#phone').focus();
		}
	});
</script>


<!-- /* 비밀번호 일치 스크립트 */ -->
<script>
	$(function(){
	    $('#password1').keyup(function(){
	      $('#chkNotice').html('');
	    });
	
	    $('#password2').keyup(function(){
	
	        if($('#password1').val() != $('#password2').val()){
	          $('#chkNotice').text('비밀번호가 일치하지 않습니다.');
	        } else{
	          $('#chkNotice').text('비밀번호가 일치합니다.');
	        }
	  });
});
</script>  
<!--  휴대폰번호 자동 하이픈 삽입 -->
 <script>
   $(document).on("keyup", ".phoneNumber", function() { 
      $(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
   });
</script> 
<!-- <script>
function joinform_check(){
   
   var password1 = document.getElementById("password1");
   var password2 = document.getElementById("password2");
   
   if (password1.value == "") {
       alert("비밀번호를 입력하세요.");
       password1.focus();
       return false;
     };

     //비밀번호 영문자+숫자+특수조합(8~25자리 입력) 정규식
     var pwdCheck = /^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/;

     if (!pwdCheck.test(password1.value)) {
       alert("비밀번호는 영문자+숫자+특수문자 조합으로 8~25자리로 입력해주세요.");
       password1.focus();
       return false;
      
     };

     if (password2.value !== password1.value) {
       alert("비밀번호가 일치하지 않습니다.");
       password2.focus();
       return false;
     };
};     
</script>    -->  



 <!-- jQuery -->
  <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js" ></script>
  <!-- iamport.payment.js -->
  <script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-{SDK-최신버전}.js"></script>

</body>
</html>