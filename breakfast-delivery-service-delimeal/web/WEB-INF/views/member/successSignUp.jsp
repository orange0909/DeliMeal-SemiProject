<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<title>회원가입 성공</title>
<style>
    * {
	margin: 0;
	padding: 0;
	font-family: 'Noto Sans KR', sans-serif;
    }

    ul {
        list-style: none;
    }

    * {
        color: gray;
    }
	#title{
		margin-top: 150px;
	}

	#hello{
		color: rgba(132, 192, 4);
	}
#login_btn {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgba(132, 192, 4);
	padding: 6px;
	padding-left: 15px;
	padding-right: 15px;
	margin-top: 70px;
	margin-left: 20px;
	border-radius: 0px;
	text-align: center;
}
#goMain_btn {
	border: 1px solid rgb(153, 153, 154);
	color: rgb(96, 95, 95);
	background-color: rgb(245, 246, 242);
	padding: 6px;
	padding-left: 10px;
	padding-right: 10px;
	margin-left: px;
	margin-top: 70px;
	border-radius: 0px;
	text-align: center;
}
</style>
</head>
<body>
	<div id="title" align="center">
	<img src="${ pageContext.servletContext.contextPath }/resources/image/logo.png" style="width: 15vw; min-width: 140px;">
		<br>
		<br>
		<h5>델리밀에 오신 것을 환영합니다!</h5>
		<h1 id="hello">회원 가입이 완료 되었습니다.</h1>

		<input id="login_btn" onclick="location.href='/delimeal/login'" class="cancel_btn" type="button" value="로그인" id="goMain">
		<input id="goMain_btn" onclick="location.href='/delimeal'" class="join_btn" id="submit" type="submit" value="메인으로 이동">
	</div>
	
</body>
</html>