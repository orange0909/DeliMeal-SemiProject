<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
	<style>
		article #wrapper {
			display: flex;
			flex-direction: row;
			justify-content: space-around;
			align-items: center;
		}

		#tableWrapper {
			display: flex;
			flex-direction: column;
			align-items: center;
		}

		.search #searchWrapper {
			display: flex;
			flex-direction: column;
			gap: 10px;
		}

		.search #searchWrapper div {
			display: flex;
			flex-direction: row;
			gap: 10px;
		}
	   
	   .searchBox {
	   	  width: 100%;
	   }
	   
	   .search input {
	          border: none;
	          border: solid 1px #BABABA;
	          border-radius: 8px;
	          padding: 5px 8px;
	   }
	   
	   .update {
	          border: solid 1px #3C8028;
	          border-radius: 4px;
	          color: #3C8028;
	          padding: 0px 5px;
	   }
	      
	   .update:hover {
	          background: #3C8028;
	          color: white;
	   }
	   
	   table {
		  border-collapse: collapse;
		  border-radius: 4px;
	 	  border-style: hidden;
		  box-shadow: 0 0 0 1px #BABABA;
	   }
	   th {
	      box-shadow: 0 0 0 0.5px #BABABA;
	      padding: 5px 20px;
	   }
	   td {
	   	  box-shadow: 0 0 0 0.5px #BABABA;
	      padding: 5px;
	   }
	   
	</style> 
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <title>배송 관리</title>
</head>

<body>
<%-- 헤더 --%>
<jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
<%-- 사이드바 + 섹션 --%>
<div id="content">
    <%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	<jsp:include page="deliveryAside.jsp"/>
	<%-- 섹션 --%>
	<section>
		<article>
        	<%-- 타이틀 --%>
            <h2 id="title">배송 관리</h2>
            <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
            <div id="subTitle">
            	<h6 class="sub-title">배송 여부 조회 및 수정</h6><div class="sub-title-tail"></div>
            </div>
			<div id="wrapper">
				<div class="shippingInfo">
					<table class="shippingInfoTable">
						<tr align="center" width=200 height=160>
							<td><img src="/delimeal/resources/image/deliCar.png" width=130 height=110></td>
							<td><h5>배송중</h5><br>n건</td>
							<td><img src="/delimeal/resources/image/dvComp.png" width=150 height=130></td>
							<td><h5>배송완료</h5><br>n건</td>
						</tr>
						<tr align="center">
							<td colspan="2">상품을 고객에게 배송하는 중 입니다</td>
							<td colspan="2">상품을 고객에게 배송완료 하였습니다</td>
						</tr>
					</table>
				</div>
				
				<div class="search">
					<form action="${ pageContext.servletContext.contextPath }/admin/deliverySearch" Method="get">
						<div id="searchWrapper">
							<label>날짜별검색</label>
							<div>
								<input type="date" class="searchBox" name="date" value="${ requestScope.date }">
								<button type="submit" class="update btn btn-outline-success">검 색</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div id="tableWrapper">
				<form action="${ pageContext.servletContext.contextPath }/admin/deliverySearch" Method="post">
					<table id="deliveryInfoList">
						<tr align="center" >
							<th>수취인</th>
							<th>배송번호</th>
							<th>식단</th>
							<th>연락처</th>
							<th>우편번호</th>
							<th>배송지</th>
							<th>배송일자</th>
							<th>배송 완료 여부</th>
						</tr>
						<c:forEach var="delivery" varStatus="status" items="${ requestScope.deliverySearch }">
							<tr class="deliveryInfo" align="center">
								<td><c:out value="${ delivery.name }"/></td>
								<td id="deliveryCode"><c:out value="${ delivery.deliveryCode }"/></td>
								<td><c:out value="${ delivery.mealName }"/></td>
								<td><c:out value="${ delivery.phone }"/></td>
								<td><c:out value="${ delivery.addrZipcode }"/></td>
								<td><c:out value="${ delivery.addr }"/></td>
								<td><c:out value="${ delivery.deliveryDate }"/></td>
								<td>
									<select id="deliveryAddrFinishYn" name="deliveryAddrFinishYn">
									<c:choose>
									<c:when test="${ delivery.deliveryAddrFinishYn == 'Y'}">
										<option value="">선택</option>
										<option value="Y" selected>배송완료</option>
										<option value="N">배송중</option>
									</c:when>
									<c:when test="${ delivery.deliveryAddrFinishYn == 'N'}">
										<option value="">선택</option>
										<option value="Y">배송완료</option>
										<option value="N" selected>배송중</option>
									</c:when>
									</c:choose>
									</select>
									<button type="button" class="update" id="updateBtn"><small>수 정</small></button>
								</td>
							</tr>
						</c:forEach>
							
					</table>
				</form>
			</div>
   		</section>
	</article>
</div> 
<script>
window.onload = function() {
	const deliveryInfoList = document.querySelectorAll('#deliveryInfoList .deliveryInfo');
	
	deliveryInfoList.forEach((element) => {
		const updateBtn = element.querySelector('#updateBtn');
		const deliveryCode = element.querySelector('#deliveryCode').innerText;
		const deliveryAddrFinishYn = element.querySelector('#deliveryAddrFinishYn');

		updateBtn.onclick = function() {
			if(deliveryAddrFinishYn.value == null || deliveryAddrFinishYn.value == '') {
				alert('배송 완료 여부를 선택해주세요.');
				return;
			}

			$.ajax({
				url: '${ pageContext.servletContext.contextPath }/admin/updateDeliveryFinish',
				type: 'post',
				data: {
					deliveryCode: deliveryCode,
					deliveryAddrFinishYn: deliveryAddrFinishYn.value
				},
				success: function(data) {
					if(data == 'success') {
						alert('변경에 성공하였습니다.');
					} else {
						alert('변경에 실패하였습니다.');
					}
				},
				error : function(request, status) {
					alert("code: " + request.status + "\n" + "massage: " + request.responseText)
				}
			})
		}
	});
}
	

/* $("#delivery").click(function(){
	const deliveryCode = $("#deliveryCode").val();
	const deliveryAddrFinishYn = $("#deliveryAddrFinishYn").text();
	
	console.log(deliveryCode);
	
	$.ajax({
		url: "${pageContext.servletContext.contextPath}/admin/deliverySearch",
		type: "post",
		data: { deliveryCode : deliveryCode,
				deliveryAddrFinishYn : deliveryAddrFinishYn
				},
		success: function(data) {
			console.table(data);
			console.log(date.lastname);
		},
		error: function(request, status) {
			alert("code: " + request.status + "\n"
					+ "massage: " + request.responseText)
		}
	});
}); */
</script>
	
</body>
</html>