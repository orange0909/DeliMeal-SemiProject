<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<script src="http://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
<link
	href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR&display=swap" rel="stylesheet">
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<title>Insert title here</title>
<style>
#tilte{
	margin-top: 150px;
}

#findPwd_btn {
	border: 1px solid rgb(153, 153, 154);
	color: rgb(96, 95, 95);
	background-color: rgb(245, 246, 242);
	padding: 8px;
	padding-left: 40px;
	padding-right: 40px;
	margin-left: px;
	margin-top: 70px;
	border-radius: 0px;
	text-align: center;
}
#login_btn {
	border: 1px solid rgb(153, 153, 154);
	color: white;
	background-color: rgba(132, 192, 4);
	padding: 8px;
	padding-left: 60px;
	padding-right: 60px;
	margin-top: 70px;
	margin-left: 20px;
	border-radius: 0px;
	text-align: center;
}
</style>
</head>
<body>
	<jsp:include page="../template/header.jsp"/>

		<form action="/delimeal/member/findId" method="post">
			<div id="tilte" align="center">
				<h2>아이디 찾기</h2>
				<br>
				<h6><small>입력하신 정보와 일치하는 아이디는 아래와 같습니다.</small></h6>
				<br>
				<br>
				<hr>
				<p>아이디: ${requestScope.result.id}</p>
				<p>가입일: ${requestScope.result.enrollDate}</p>
				
				<hr>
				<input id="findPwd_btn" onclick="location.href='/delimeal/member/findPwd'" class="cancel_btn" type="button" value="비밀번호 찾기">
				<!-- 로그인 화면으로 안가짐. 경로 확인. -->
				<input id="login_btn" class="join_btn" type="button" onclick="location.href='/delimeal/login'" value="로그인">
			</div>
		</form>
</body>
<script>
	$(document).on("keyup", ".phoneNumber", function() { 
		$(this).val( $(this).val().replace(/[^0-9]/g, "").replace(/(^02|^0505|^1[0-9]{3}|^0[0-9]{2})([0-9]+)?([0-9]{4})$/,"$1-$2-$3").replace("--", "-") );
	});
</script>
</html>