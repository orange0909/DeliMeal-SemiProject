<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
	<link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/member/addressManager.css">
	<script src="${ pageContext.servletContext.contextPath }/resources/js/addressManager.js"></script>
	
	<title>배송지 관리</title>
</head>

<body>
	<section>
		<div id="addressEditBox">
			<div id="addrZipcodeBox">
				<input type="text" id="addrName" class="form-control form-control-sm" name="addressName" placeholder="별칭">
				<input type="text" id="addrCode" name="addrCode" value="" style="display: none;" readonly>
				<input type="text" id="addrZipcode" class="form-control form-control-sm" name="addrZipcode" placeholder="우편번호" readonly>
				<button onclick="openFindPostCode()" class="btn btn-outline-success btn-sm">우편번호 찾기</button>
			</div>
			<div>
				<input type="text" class="form-control form-control-sm" name="addr" id="addr" placeholder="주소" readonly>
			</div>
			<div>
				<input type="text" class="form-control form-control-sm" name="addrDetail" id="addrDetail" placeholder="상세주소">
			</div>
			<div id="reqAndSaveBox">
				<input type="text" class="form-control form-control-sm" name="deliveryReq" id="deliveryReq" placeholder="배송요청사항">
				<button id="saveBtn" onclick="save()" class="btn btn-outline-success btn-sm">추가</button>
			</div>
		</div>
		<div id="addressList">
			<c:forEach var="addr" items="${ requestScope.addrList }">
				<div class="address-list-tile">
					<div id="nameAndBtn">
						<span id="addressCode" style="display: none;"><c:out value="${ addr.addrCode }"/></span>
						<span id="addressName"><c:out value="${ addr.addrName }"/></span>
						<div>
							<button id="modifyBtn" class="btn btn-outline-success btn-sm">수정</button>
							<button id="deleteBtn" class="btn btn-outline-success btn-sm">삭제</button>
							<button id="selectAddr" class="btn btn-outline-success btn-sm">선택</button>
						</div>
					</div>
					<div>
						<span id="zipcode"><c:out value="${ addr.addrZipcode }"/></span>
					</div>
					<div>
						<c:choose>
							<c:when test="${ empty addr.addrDetail }">
								<span id="address"><c:out value="${ addr.addr }"/></span>
							</c:when>
							<c:otherwise>
								<span id="address"><c:out value="${ addr.addr }, ${ addr.addrDetail }"/></span>
							</c:otherwise>
						</c:choose>
					</div>
					<div>
						<span id="deliveryReq"><c:out value="${ addr.deliveryReq }"/></span>
					</div>
				</div>
			</c:forEach>
			<div id="addAddress">
				<div id="addBtn"></div>
			</div>
		</div>
		<div>
			
		</div>
	</section>
</body>

</html>