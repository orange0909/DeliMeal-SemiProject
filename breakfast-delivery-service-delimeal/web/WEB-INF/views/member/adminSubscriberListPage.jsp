<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    <style>
	   .member-info-box {
	      margin-top: 100px;
	      width: 100%;
	        display: flex;
	        flex-direction: column;
	        align-items: center;
	       justify-content: center;
	       gap: 5px;
	   }
	      
	   .tbl {
	       display: flex;
	       flex-direction: column;
	       flex-grow: 1;
	       gap: 5px;
	   }
	      
	   .txt-box input {
	          width: 100%;
	          border: none;
	          border: solid 1px #BABABA;
	          border-radius: 8px;
	          padding: 5px 10px;
	   }
	
	   .btn {
	          border: solid 1px #3C8028;
	          border-radius: 4px;
	          color: #3C8028;
	   }
	      
	   .btn:hover {
	          background: #3C8028;
	          color: white;
	   }
	
	   table {
		  border-collapse: collapse;
		  border-radius: 4px;
	 	  border-style: hidden;
		  box-shadow: 0 0 0 1px #BABABA;
	   }
	    th {
	   	  border: solid 1px #BABABA;
	      padding: 5px 20px;
	   }
	   tr, td {
	      border: solid 1px #BABABA;
	      padding: 5px;
	   }
	   .content {
	   		width: 200px;
	   }
   </style> 
    <title>회원 정보 관리</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="adminSubsAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">구독 관리</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h6 class="sub-title">구독자 조회</h6><div class="sub-title-tail"></div>
                </div>
                <section>
				<div class="member-info-box">
			      	<div class="tbl" id="searchBoard">
						<table border="1">
							<tr align="center">
								<th>회원코드</th>
								<th>아이디</th>
								<th>이 름</th>
								<th>이메일</th>
								<th>생년월일</th>
								<th>번 호</th>
								<th>우편번호</th>
								<th>주 소</th>
								<th>구독여부</th>
							</tr>
			      			<c:forEach var="member" items="${ requestScope.subscriberList }">
								<tr align="center">
									<td><c:out value="${ member.code }"/></td>
									<td><c:out value="${ member.id }"/></td>
									<td><c:out value="${ member.name }"/></td>
									<td><c:out value="${ member.email }"/></td>
									<td><c:out value="${ member.bday }"/></td>
									<td><c:out value="${ member.phone }"/></td>
									<td><c:out value="${ member.addrZipcode }"/></td>
									<td><c:out value="${ member.addr }, ${ member.addrDetail }"/></td>
									<td><c:out value="${ member.subscriptionYn }"/></td>
								</tr>
							</c:forEach>
			   			</table>
			   		</div>
			   	</div>
			   		
			  		</section>
			       	</article>
				    </section>
			    </div>
        
</body>
</html>