<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
	    <style>
		   .member-info-box {
		      margin-top: 50px;
		      width: 100%;
		        display: flex;
		        flex-direction: column;
		        align-items: center;
		       justify-content: center;
		       gap: 5px;
		   }
		      
		   .tbl {
		       display: flex;
		       flex-direction: column;
		       flex-grow: 1;
		       gap: 5px;
		       margin-bottom: 100px;
		   }
		      
		   .txt-box input {
		          width: 100%;
		          border: none;
		          border: solid 1px #BABABA;
		          border-radius: 8px;
		          padding: 5px 10px;
		   }
		
		   .btn {
		          border: solid 1px #3C8028;
		          border-radius: 4px;
		          color: #3C8028;
		          padding: 5px 15px;
		          margin: 5px 10px;
		   }
		      
		   .btn:hover {
		          background: #3C8028;
		          color: white;
		   }
		
		   table {
			  border-collapse: collapse;
			  border-radius: 4px;
		 	  border-style: hidden;
			  box-shadow: 0 0 0 1px #BABABA;
		   }
		   tr, td {
		      border: solid 1px #BABABA;
		      padding: 10px;
		   }
		   .content {
		   		width: 340px;
		   }
	   </style>
    <title>회원 정보 관리</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="adminAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">회원 정보 관리</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">회원 기본 정보</h5><div class="sub-title-tail"></div>
                </div>
                <div class="member-info-box">
                	<form id="memberUpdateForm" action="${ pageContext.servletContext.contextPath }/admin/memberUpdate" method="post">
                	<div style="display:none">
                		<input type="text" name="code" value="${ requestScope.searchMember.code }">
                	</div>
					<div class="tbl">
			   			<table>
			   				<tr align="left">
				         		<td class="index">회원코드</td>
			    	     		<td class="content">${ requestScope.searchMember.code }</td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">아이디</td>
			    	     		<td class="content">${ requestScope.searchMember.id }</td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">비밀번호</td>
			    	     		<td class="content"><input type="text" name="password" value="${ requestScope.searchMember.password }"></td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">이 름</td>
			    	     		<td class="content">${ requestScope.searchMember.name }</td>
			      			</tr>
			      			<tr align="left">
					         	<td class="index">이메일</td>
			        	 		<td class="content">${ requestScope.searchMember.email }</td>
			      			</tr>
				      		<tr align="left">
			    	     		<td class="index">생년월일</td>
			       			  	<td class="content">${ requestScope.searchMember.bday }</td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">성 별</td>
			    	     		<td class="content">${ requestScope.searchMember.gender }</td>
			      			</tr>
				      		<tr align="left">
			    	     		<td class="index">휴대폰번호</td>
			        	 		<td class="content"><input type="text" name="phone" value="${ requestScope.searchMember.phone }"></td>
			      			</tr>
				      		<tr align="left">
			    	     		<td class="index">주 소</td>
			        	 		<td class="content"><input type="text" name="addr" value="${ requestScope.addr }"></td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">무료체험여부</td>
			    	     		<td class="content">${ requestScope.searchMember.freeTrialYn }</td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">적립금</td>
			    	     		<td class="content"><input type="text" name="point" value="${ requestScope.searchMember.point }"></td>
			      			</tr>
			      			<tr align="left">
				         		<td class="index">회원타입</td>
			    	     		<td class="content">${ requestScope.searchMember.type }</td>
			      			</tr>
				      		<tr align="left">
			    	     		<td class="index">가입일</td>
			        			<td class="content">${ requestScope.searchMember.enrollDate }</td>
			      			</tr>
			      			<tr align="left">
			    	     		<td class="index">마지막 로그인 날짜</td>
			        			<td class="content">${ requestScope.searchMember.lastLoggedInDate }</td>
			      			</tr>
			   			</table>
				   		<div>
				      		<button type="submit" class="btn" id="updateButton">수 정</button>
				     		<button type="submit" class="btn" id="cancelButton">취 소</button>
				  		</div>
			   		</div>
			   		</form>
				</div>
            </article>
	    </section>
    </div>
        
</body>

</html>