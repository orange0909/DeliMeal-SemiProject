<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<header>
	<div class="header-box">
		<div id="logo" onclick="home()"></div>
		<ul id="menuBar">
			<li class="menu" onclick="member()">
				회원 관리
			</li>
			<li class="menu" onclick="delivery()">
				배송 관리
			</li>
			<li class="menu" onclick="subs()">
				구독 관리
			</li>
			<li class="menu" onclick="meal()">
				식단 관리
			</li>
			<li class="menu" onclick="qna()">
                고객센터
            </li>
            <li class="menu" onclick="promo()">
                프로모션
            </li>
		</ul>
		<div class="btn-box">
			<c:if test="${ empty sessionScope.loginMember }">
				<button id="loginbt" class="btn btn-outline-success" onclick="login()">LOG IN</button>     
			</c:if>
			<c:if test="${ not empty sessionScope.loginMember }">
				<button id="loginbt" class="btn btn-outline-success" onclick="logout()">LOG OUT</button>     
			</c:if>
		</div>
	</div>
</header>
<script>
	// 현재 페이지의 intent에 의해 헤더의 active 상태 변경
	const contextPath = '${ pageContext.servletContext.contextPath }';
	
	const intent = location.href.substring(location.href.indexOf(contextPath) + contextPath.length);

	let menuName = "";
	if(intent.indexOf('/', 2) != -1) {
		menuName = intent.substring(0, intent.indexOf('/', 2));
	} else {
		menuName = intent;
	}
	
	switch (menuName) {
	case '/admin/meal':
		document.getElementsByClassName('menu')[0].classList.add('active');
		break;
	case '/admin/menu':
		document.getElementsByClassName('menu')[0].classList.add('active');
		break;
	case '/admin/category':
		document.getElementsByClassName('menu')[0].classList.add('active');
		break;
	case '/qna' :
		document.getElementsByClassName('menu')[4].classList.add('active');
		break;
	case '/feedback' :
		document.getElementsByClassName('menu')[4].classList.add('active');
		break; 
	case '/member' :
		document.getElementsByClassName('menu')[0].classList.add('active');
		break;
	case '/delivery' :
		document.getElementsByClassName('menu')[1].classList.add('active');
		break;
	case '/subs' :
		document.getElementsByClassName('menu')[2].classList.add('active');
		break;
	}

	function home() {
		location.href = "${ pageContext.servletContext.contextPath }";
	}
	function login() {
		location.href = "${ pageContext.servletContext.contextPath }/login" + "?returnUrl=" + location.href;
	}
	function logout() {
		location.href = "${ pageContext.servletContext.contextPath }/logout" + "?returnUrl=" + location.href;
	}
	function qna() {
		location.href = "${ pageContext.servletContext.contextPath }/qna/list"; 
	}
	function member() {
		location.href = "${ pageContext.servletContext.contextPath }/admin/memberInfo"; 
	}
	function delivery() {
		location.href = "${ pageContext.servletContext.contextPath }/admin/deliverySearch"; 
	}
	function subs() {
		location.href = "${ pageContext.servletContext.contextPath }/admin/subscriberList";
	}
	function meal() {
		location.href= "${ pageContext.servletContext.contextPath }/admin/meal";
	}
	function promo() {
		location.href= "${pageContext.servletContext.contextPath}/pa/ManageBirthdayMenu";
	}
</script>
