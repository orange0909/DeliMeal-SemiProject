<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    
    <title>식단 카테고리</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="aside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">전체 식단</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">문의글 수정</h5><div class="sub-title-tail"></div>
                </div>
                <div>
                    <%-- 내용 작성 (만든 뷰 여기다 넣으면 됨) --%>
                </div>
            </article>
	    </section>
    </div>
        
</body>

</html>