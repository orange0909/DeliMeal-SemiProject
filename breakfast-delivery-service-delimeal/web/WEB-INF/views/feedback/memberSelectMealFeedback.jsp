<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    <!-- 버튼 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <title>식단 선택</title>
    <style type="text/css">
    .btn:hover {
        background-color: #3C8028;
    }
    .btn{
    	float: right;
    	margin: 15px;
    }
    
    input#searchValue {
    	width: 250px;
    }
    
    
    </style>
</head>
<body>

	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="/WEB-INF/views/member/memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">메뉴 피드백</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">식단 선택</h5><div class="sub-title-tail"></div>
                </div>
                <div>
                	<h4> 피드백을 남기실 식단을 선택하세요. </h4>
                </div>
                
                 <div class="container mt-3" style="border:1px solid gray ">
                 	<br>
                 	<form action="${ pageContext.servletContext.contextPath }/feedback/insert" method="get" style="display:inline-block">
                    <table class="table table-hover" style="text-align: center; table-layout: fixed; " >
	    				
	    				<thead>
					      <tr id="index">
					        <th>식단일자</th>
					        <th>구독 카테고리</th>
					        <th>식단이름</th>
					        <th>선택</th>
					      </tr>
					    </thead>
					    
					    <tbody id="feedbackBar">
					    	<c:forEach var="feedback" items="${ requestScope.meal }">
					    	  <tr>
					    		<td><fmt:formatDate value="${feedback.deliveryDate}" pattern="yy/MM/dd"/></td>
					    		<td>${ feedback.mealCate }</td>
					    		<td id="feedNum">
					    			${ feedback.mealName }
					    		</td>
					    		<td>
									<div>
  										<input required="required" class="form-check-input" type="radio" name="mealCode" id="inlineRadio1" value ="${ feedback.mealCode }">
									</div>
					    		</td>
					    	  </tr>
					    	</c:forEach>
					    </tbody>
					  </table>
					  <button type="submit" class="btn btn-outline-success">다음</button>
					  <button type="button"  class="btn btn-outline-success" onclick="window.history.back()">이전</button>
					  
					  </form>
                </div>
                
                <!-- 페이징 -->
			<jsp:include page="../paging/memberFeedbackInsertPaging.jsp"/>
				<!-- 검색 -->
			<div class="search-area" align="center">
				<form action="${ pageContext.servletContext.contextPath }/feedback/selectMeal" method="get" style="display:inline-block">
				    <input type="hidden" name="currentPage" value="1">
				    <select id="searchCondition" name="searchCondition" onchange="changeSearchBox()">
						<option value="null" ${ requestScope.selectCriteria.searchCondition eq "null"? "selected": "" }>--</option>
						<option value="deliDate" ${ requestScope.selectCriteria.searchCondition eq "deliDate"? "selected": "" }>배송일자</option>
						<option value="cate" ${ requestScope.selectCriteria.searchCondition eq "cate"? "selected": "" }>구독카테고리</option>
						<option value="mealName" ${ requestScope.selectCriteria.searchCondition eq "mealName"? "selected": "" }>식단이름</option>
					</select>
					
			        <input type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
			        <script>
			        	changeSearchBox();
			        	
			        	function changeSearchBox() {
			        		const searchCondition = document.querySelector('select#searchCondition');
			        		const searchBox = document.querySelector('input#searchValue');
			        		
			        		if (searchCondition.value == 'deliDate') {
					        	searchBox.type = 'date';
			        		} else {
			        			searchBox.type = 'search';
			        		}
			        		
			        	}
			        	
			        	
			 			if(document.getElementsByTagName("td")) {
							const $tds = document.getElementsByTagName("td");
							
							
							for(let i = 0; i < $tds.length; i++){
								
								$tds[i].onmouseenter = function() {
									this.parentNode.style.backgroundColor = "#BAEF61";
									this.parentNode.style.cursor = "pointer";
								}
								
								$tds[i].onmouseout = function() {
									this.parentNode.style.backgroundColor = "white";
								}
								
								} 
			 			}
			        	
			        </script>
			
					<button type="submit">검색하기</button>	
					</form>		
				</div>

            </article>
	    </section>
    </div>

</body>
</html>