<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %> 
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <%-- 개인 페이지 --%>
    <!-- 제목 바 -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- 버튼 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <!-- qna 내용 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <%-- 버튼 hover 코드 바꾸기 --%>
    <style type="text/css">
    .btn:hover {
        background-color: #3C8028;
    }
    .btn{
    	float: right;
    	margin-left: 5px;
    }
    </style>
    
    <title>문의 상세조회</title>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	     <jsp:include page="../qna/adminServiceAside.jsp"/> 
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">메뉴 피드백 조회</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">피드백 상세조회</h5><div class="sub-title-tail"></div>
                </div>
                <!-- 내용  -->
                <div>
                  <table class="table" >
				      <tr>
				        <td><h5><c:out value="${ requestScope.feedback.feedbackTitle }"/></h5></td>
				        <td>식단 이름: <c:out value="${ requestScope.feedback.mealName}"/></td>
				        <td>카테고리: <c:out value="${ requestScope.feedback.mealCateName}"/></td>
				        <td>작성자: <c:out value="${ requestScope.feedback.memberName}"/></td>
				        <td><fmt:formatDate value="${requestScope.feedback.feedbackDate}" pattern="yy/MM/dd"/></td>
				        <td style="display:none;"><c:out value="${ requestScope.feedback.feedbackNum }"/></td>
				        <td align="center" style="position: relative;">
				            <button type="button" class="btn btn-outline-success" onclick="showList()">목록</button>  &nbsp;
				          	<c:if test='${ requestScope.feedback.answerYN eq "Y"  }'> 
				            	<button type="button" class="btn btn-outline-success" onclick="updateAnswer()">답변 수정</button> &nbsp;
				            </c:if>
				            
				             <c:if test='${requestScope.feedback.answerYN eq "N"}'> 
					            <button type="button" class="btn btn-outline-success" onclick="updateAnswer()">답변 작성</button> &nbsp;
				             </c:if> 
				            
				            <c:if test='${ requestScope.feedback.answerYN eq "Y" }'>
				        		<button type="button" class="btn btn-outline-success" onclick="deleteAnswerfeed()">답변 삭제</button>
				        	</c:if>
				         </td>
				      </tr>
				     </table>
				     <!-- 문의 상세내용 -->
				     <div class="container mt-3" style="height=800px;">
  						<div class="card" style="background-color:white; height:220px;">
   						<div class="card-body"><c:out value="${ requestScope.feedback.feedbackContent }"/></div>
  						</div>
				     </div>

					 <!-- 사진 보기 -->				     
				     <c:if  test="${ not empty requestScope.feedback.fileName }">
					     <img style="margin-top: 16px; margin: 12px; " id="detailImg1" class="detailImg" width="250" height="180" src="${ pageContext.servletContext.contextPath }/resources/cs/${ requestScope.feedback.fileName}"/>
				     </c:if>
				     
				     <!-- 문의 답변내용 -->
				     <div class="container mt-3">
  						<div class="card"style="background-color:#BAEF61; height:220px;">
   							<div class="card-body"><b>답변내용: </b>&nbsp;<c:out value="${ requestScope.feedback.answerContent }"/><br>
  							<h6 style="font-size:small; color:#A0A0A0"><fmt:formatDate value="${ requestScope.feedback.answerDate  }" pattern="yy/MM/dd"/></h6>
   							</div>
  						</div>
				     </div>
				     
				     
                </div>
                
                
            </article>
	    </section>
    </div>
        
    <script>
    	/* 목록 가기 */
    	function showList() {
    		location.href='${ pageContext.servletContext.contextPath }/feedback/${ sessionScope.listType == "all" ? "adminSelectList" : "adminWaithingList"}';
		}
    	
    	/* 답변 수정, 인서트 하기 */
    	function updateAnswer() {
    		var feedNum ='${ requestScope.feedNum }'
    		var answerYN = '${ requestScope.feedback.answerYN }'
    		location.href = "${ pageContext.servletContext.contextPath }/feedback/updateAnswer?feedNum=" + feedNum + "&&answerYN=" + answerYN
		}
    	
    	/* 삭제 하기 */
    	function deleteAnswerfeed() {
    		var answer;
    		var feedbackNum ='${ requestScope.feedback.feedbackNum }'
    		answer = confirm("답변을 삭제하시겠습니까?");
    		
    		if(answer == true){
    			var feedNum ='${ requestScope.feedNum }'
    			location.href = "${ pageContext.servletContext.contextPath }/feedback/deleteAnswer?feedbackNum=" + feedbackNum + "&&feedNum=" + feedNum
    		}
		}
    	
    	
    </script>
</body>
</html>