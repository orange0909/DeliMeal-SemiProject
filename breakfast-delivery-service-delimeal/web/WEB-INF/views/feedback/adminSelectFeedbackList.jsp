<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>   
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
    <!-- 버튼 -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <title>메뉴 피드백</title>
    <style type="text/css">
    .btn:hover {
        background-color: #3C8028;
    }
    .btn{
    	float: right
    }
    </style>
</head>

<body>
	<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="../qna/adminServiceAside.jsp"/> 
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">메뉴 피드백 조회</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">사용자 메뉴 피드백</h5><div class="sub-title-tail"></div>
                </div>
                
                 <div class="container mt-3">
                    <table class="table table-hover" style="text-align: center">
	    				
	    				<thead>
					      <tr id="index">
					        <th>글번호</th>
					        <th>피드백제목</th>
					        <th>식단이름</th>
					        <th>답변여부</th>
					        <th>작성일</th>
					        <th>작성자</th>
					      </tr>
					    </thead>
					    <tbody id="feedbackBar">
					    	<c:forEach var="feedback" items="${ requestScope.feedback }">
					    	  <tr>
					    		<td id="feedNum">${ feedback.feedbackNum }</td>
					    		<td>${ feedback.feedbackTitle }</td>
					    		<td>${ feedback.mealName }</td>
					    		  <c:choose>
					    			  <c:when test="${feedback.answerYN eq '답변 완료'}"> 
					    				<td id="qnaAnswerYes" style="text-color: white;">${ feedback.answerYN }</td> 
					    			  </c:when>
					    			  <c:when test="${feedback.answerYN eq '답변 대기중'}"> 
					    				<td id="qnaAnswerYes">${ feedback.answerYN }</td> 
					    			  </c:when>
					    		  </c:choose>
					    		<td><fmt:formatDate value="${feedback.feedbackDate}" pattern="yy/MM/dd"/></td>
					    		<td>${ feedback.memberName }</td>
					    	  </tr>
					    	</c:forEach>
					    </tbody>
					  </table>
                </div>
                
                <!-- 페이징 -->
			<jsp:include page="../paging/adminFeedbackAllPaging.jsp"/>
		
			<br>
				<!-- 검색 -->
			<div class="search-area" align="center">
				<form action="${ pageContext.servletContext.contextPath }/feedback/adminSelectList" method="get" style="display:inline-block">
				    <input type="hidden" name="currentPage" value="1">
				    <select id="searchCondition" name="searchCondition">
						<option value="title" ${ requestScope.selectCriteria.searchCondition eq "title"? "selected": "" }>제목</option>
						<option value="content" ${ requestScope.selectCriteria.searchCondition eq "content"? "selected": "" }>내용</option>
						<option value="mealName" ${ requestScope.selectCriteria.searchCondition eq "mealName"? "selected": "" }>식단이름</option>
						<option value="memberName" ${ requestScope.selectCriteria.searchCondition eq "memberName"? "selected": "" }>작성자</option>
					</select>
			        <input type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
				
					<button type="submit">검색하기</button>	
					</form>		
				</div>
	
				<!-- 상세보기로 이동하기 -->
				<script>
		 			if(document.getElementsByTagName("td")) {
						const $tds = document.getElementsByTagName("td");
						
						
						for(let i = 0; i < $tds.length; i++){
							
							$tds[i].onmouseenter = function() {
								this.parentNode.style.backgroundColor = "#BAEF61";
								this.parentNode.style.cursor = "pointer";
							}
							
							$tds[i].onmouseout = function() {
								this.parentNode.style.backgroundColor = "white";
							}
							
			 				$tds[i].onclick = function() {
			 					const feedNum = this.parentNode.children[0].innerText;
								location.href = "${ pageContext.servletContext.contextPath }/feedback/detail?feedNum=" + feedNum 
									} 
							} 
		 			}

				</script>
            </article>
	    </section>
    </div>
</body>

</html>