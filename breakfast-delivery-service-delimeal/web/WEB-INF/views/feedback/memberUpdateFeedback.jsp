<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   
 
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <%-- 개인 페이지 --%>
	<title>문의 수정 페이지</title>
</head>
<body>
<%-- 헤더 --%>
	<jsp:include page="/WEB-INF/views/template/header.jsp"/>
	<%-- 사이드바 + 섹션 --%>
    <div id="content">
    	<%-- 사이드바 (사이드바도 페이지에 맞도록 파일이름이랑 내용 수정 필요) --%>
	    <jsp:include page="/WEB-INF/views/member/memberAside.jsp"/>
	    <%-- 섹션 --%>
	    <section>
            <article>
                <%-- 타이틀 --%>
                <h2 id="title">메뉴 피드백 수정</h2>
                <%-- 서브타이틀 + 디바이더 (필요 없으면 삭제하면 됨) --%>
                <div id="subTitle">
                    <h5 class="sub-title">수정하기</h5><div class="sub-title-tail"></div>
                </div>
                <div>
					<form action="${ pageContext.servletContext.contextPath }/mypage/updateFeedback" method="post" encType="multipart/form-data">
					
						<table class="table">
                    	<tr>
						<td>제목 </td>
						<td></td>
						<td><input type="text" size="30" name="title" style="border-radius: 8px; border:solid 1px #6C6C6C;" value="${ requestScope.feedback.feedbackTitle} "></td>
						<td>식단이름: &nbsp; ${ requestScope.feedback.mealName }</td>
						<td>카테고리 이름: &nbsp; ${ requestScope.feedback.mealCateName }</td>
						<!-- 글번호  -->	
						<td><input style="display:none;" type="text"  name="feedbackNum" value="${ requestScope.feedback.feedbackNum}"></td>
						<td><input style="display:none;" type="text"  name="feedNum" value="${ requestScope.feedNum}"></td>
						</tr>
                    	</table>
                    	<!-- 내용수정 -->
                    	<textarea class="form-control col-sm-5" name="body" rows="5" style=" resize: none;"><c:out value="${ requestScope.feedback.feedbackContent }"/></textarea>
						<!-- 기능 버튼들 -->				     	
				     	<table style="width: 950px; ">
				     	<br>
				     		<tr>
				     		<!-- 이미지가 있다면  업로드 된 파일이름 보여주기  -->
					     	  <c:if test="${ not empty requestScope.feedback.fileName }">
					     	  
					     	  	<div id="showOldFile">
				     				<c:out value="${ requestScope.feedback.fileName }"></c:out>
				     			<!-- 엑스박스 -> 온클릭으로 스코프값 보냄 -->
				     				<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16" onclick="deleteOldFile()"style="cursor: pointer">
 							 			<path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
  							 			<path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
					    			</svg>
					    			<input type="text" value="${ requestScope.feedback.fileName }" style="display:none;" name="oldFileName" id="oldFileName">
					    			<input type="text" value="false" style="display:none;" name="deleteYN" id="deleteYN">
					    		</div>
					     	   </c:if>

				     			<br>

				     		 <td>
				     		 	<!-- 파일 업로드 버튼 -->
				     			<input type="file" id="file" name="qnaFile" multiple="multiple" accept="image/*">
				     			<!-- 삭제 버튼 연결 -->
				     			<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-lg" viewBox="0 0 16 16" onclick="deleteFile()"style="cursor: pointer">
 							 		<path fill-rule="evenodd" d="M13.854 2.146a.5.5 0 0 1 0 .708l-11 11a.5.5 0 0 1-.708-.708l11-11a.5.5 0 0 1 .708 0Z"/>
  							 		<path fill-rule="evenodd" d="M2.146 2.146a.5.5 0 0 0 0 .708l11 11a.5.5 0 0 0 .708-.708l-11-11a.5.5 0 0 0-.708 0Z"/>
					    		</svg>
					    	</td>
				     		<!-- 작성,이전 버튼  -->
				     		<td style="float: right; position: relative;">
				     			<button type="button" class="btn btn-outline-dark" id="backToList" onclick="back()" style="text-align: center">이전으로</button>
				     			<button type="submit" class="btn btn-outline-dark"  style="text-align: center" >수정하기</button>
				     		</td>
				     		</tr>
				     	</table>
					
					</form>
                </div>
                
            <script>
    			/* 뒤로가기 */
		    	function back() {
		    		window.history.back();
				}
		    	/* 방금 올린 파일 옆 엑스박스 눌렸을때 */
		    	function deleteFile() {
					$(file).val("");
				}
		    	/* 시간이 된다면 이전 사진 삭제도 시켜보자,, */
		    	function deleteOldFile(){
		    		document.querySelector("#showOldFile").style.display = "none";
		    		document.querySelector("#deleteYN").value = "true";
		    		
		    	}
		    </script>
         
            </article>
	    </section>
    </div>
    
</body>
</html>