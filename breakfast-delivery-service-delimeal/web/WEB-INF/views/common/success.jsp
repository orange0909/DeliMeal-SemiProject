<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>  
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<script>
	(function() {
		const successCode = "${ requestScope.successCode }";
		
		let successMessage = "";
		let movePath = "";
		let feedNum = "";
		let qnaNum = ""
		switch (successCode) {
		case "insertQnAmember":
			  movePath = '${ pageContext.servletContext.contextPath }/qna/${ sessionScope.listType == "my" ? "selectMyQnA" : "list"}';
			  break;
			  
		case "deleteQnAmember":
  				movePath ='${ pageContext.servletContext.contextPath }/qna/${ sessionScope.listType == "my" ? "selectMyQnA" : "list"}';
			  break;
		case "updateQnAmember":
			qnaNum = "${ requestScope.qnaNum }";
			movePath = "${ pageContext.servletContext.contextPath }/qna/detail?qnaNum=" + qnaNum ;
			  break;
			
		case "insertfeedbackmember":
			movePath = "${ pageContext.servletContext.contextPath }/mypage/feedbackList" ;
			  break;
		case "updateFeedbackmember":
			feedNum = "${ requestScope.feedNum }"
			movePath= "${ pageContext.servletContext.contextPath }/feedback/detail?feedNum=" + feedNum ;
			break;	
			
		case "deleteFeedbackmember":
			movePath = "${ pageContext.servletContext.contextPath }/mypage/feedbackList" ;
			  break;
			
		case "updateMember" : 
			successMessage = "회원 정보가 수정되었습니다.";
			movePath = "${ pageContext.servletContext.contextPath }/mypage/updateMember";
			break;
			
		case "deleteMember" : 
			successMessage = "탈퇴 되었습니다.";
			movePath = "${ pageContext.servletContext.contextPath }/logout";
			break;	
			
		case "findPwd" : 
			successMessage = "";
			movePath = "${ pageContext.servletContext.contextPath }/member/modifyPwd";
			break;	
			
		case "modifyPwd" : 
			successMessage = "비밀번호가 변경되었습니다.";
			movePath = "${ pageContext.servletContext.contextPath }/login";
			break;		
		
		case "updateFavoriteMenu" :
			successMessage = "선호 메뉴가 등록되었습니다.";
			movePath = "${ pageContext.servletContext.contextPath }/mypage/favMenu";
			break;
			
		
		case "deleteFeedbackAnswer":
			feedNum = "${ requestScope.feedNum }"
			movePath = "${ pageContext.servletContext.contextPath }/feedback/detail?feedNum=" + feedNum ; 
			  break;
		case "deleteQnAanswer":
			qnaNum = "${ requestScope.qnaNum }";
			movePath ='${ pageContext.servletContext.contextPath }/qna/detail?qnaNum=' + qnaNum;
			break;
		}
			
		console.log("movePath: ", movePath)
		if(successMessage != ""){
			alert(successMessage);
		}
		location.replace(movePath)
	})();
	
	
	</script>
</body>
</html>