<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
    <link rel="stylesheet" href="${ pageContext.servletContext.contextPath }/resources/css/common.css">
    <title>404 ERROR</title>

    <style>
        #content article {
            height: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
        }
    </style>
</head>

<body>
	<%-- 헤더 --%>
    <c:choose>
        <c:when test='${ sessionScope.loginMember.type == "M" }'>
	        <jsp:include page="/WEB-INF/views/template/adminheader.jsp"/>
        </c:when>
        <c:otherwise>
	        <jsp:include page="/WEB-INF/views/template/header.jsp"/>
        </c:otherwise>
    </c:choose>
	<%-- 사이드바 + 섹션 --%>
    <div id="content" class="no-sidebar">
	    <section>
            <article>
                <h1>ERROR</h1>
                <h3>잘못된 접근입니다.</h3>
            </article>
	    </section>
    </div>
        
</body>

</html>