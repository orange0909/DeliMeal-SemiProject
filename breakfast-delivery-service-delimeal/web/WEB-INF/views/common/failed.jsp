<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<script>
		(function() {
			const failedCode = "${ requestScope.failedCode }";
			
			let failedMessage = "";
			let movePath = "";
			
			switch (failedCode) {
			case "haveToLogin":
					movePath = "${ pageContext.servletContext.contextPath }/login" + "?returnUrl=" + location.href
				  break;
			case "findPwd" :
				alert("일치하는 회원 정보가 없습니다.");
				movePath = "${ pageContext.servletContext.contextPath }/member/findPwd";
				break;
				
			case "findId" :
				failedMessage = "일치하는 회원 정보가 없습니다.";
				movePath = "${ pageContext.servletContext.contextPath }/member/findId";
				break;	
				
			case "noDeliHistory":
				alert("주문 내역이 없습니다.")
				movePath= "${ pageContext.servletContext.contextPath }/mypage/feedbackList";
				  break;
			case "deleteFeedbackmember":
				alert("삭제에 실패 했습니다.")
				movePath = "${ pageContext.servletContext.contextPath }/mypage/feedbackList"
				  break;
			case "haveToLoginWhenDelete":
				var qnaNum = "${requestScope.qnaNum}"
				alert("로그인이 필요합니다.")
				movePath = "${ pageContext.servletContext.contextPath }/login" + "?returnUrl=" +  resetPage
				  break;
			case "notYourPost":
				alert("게시글 접근 권한이 없습니다.")
				window.history.back();
				break;
				
			case "notYourFBPost":
				alert("답변 접근 권한이 없습니다.")
				window.history.back();
			}
			console.log("movePath: ", movePath)
			if(failedMessage != ""){
				alert(failedMessage);
			}
			location.replace(movePath)
	
	})();
	
	
	</script>
</body>
</html>