<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
<style type="text/css">
	.button{
		border: 0;
	}
	
	.pagingArea{
		width: fit-content;
		margin: 0 auto;		
	}
	#pagingWrapper {
		width: 100%;
	}
	
</style>
</head>
<body>
	<div id="pagingWrapper">
		<div class="pagingArea" >
			<!-- 맨 앞으로 이동 버튼 -->
		    <button id="startPage">
				<i class="bi bi-chevron-double-left"></i>
			</button>
			
			<!-- 이전 페이지 버튼 -->
			<c:if test="${ requestScope.selectCriteria.pageNo <= 1 }">
				<button disabled>
					<i class="bi bi-chevron-left"></i>
				</button>
			</c:if>
			<c:if test="${ requestScope.selectCriteria.pageNo > 1 }">
				<button id="prevPage">
					<i class="bi bi-chevron-left"></i>
				</button>
			</c:if>
			
			<!-- 숫자 버튼 -->
			<c:forEach var="num" begin="${ requestScope.selectCriteria.startPage }" end="${ requestScope.selectCriteria.endPage }" step="1">
				<c:if test="${ requestScope.selectCriteria.pageNo eq num }">
					<button disabled><c:out value="${ num }"/></button>
				</c:if>
				<c:if test="${ requestScope.selectCriteria.pageNo ne num }">
					<button onclick="pageButtonAction(this.innerText);"><c:out value="${ num }"/></button>
				</c:if>
			</c:forEach>
			
			<!-- 다음 페이지 버튼 -->
			<c:if test="${ requestScope.selectCriteria.pageNo >= requestScope.selectCriteria.maxPage }">
				<button disabled>
					<i class="bi bi-chevron-right"></i>
				</button>
			</c:if>
			<c:if test="${ requestScope.selectCriteria.pageNo < requestScope.selectCriteria.maxPage }">
				<button id="nextPage">
					<i class="bi bi-chevron-right"></i>
				</button>
			</c:if>
			
			<!-- 마지막 페이지로 이동 버튼 -->
			<button id="maxPage">
				<i class="bi bi-chevron-double-right"></i>
			</button> 
		</div>
	</div>
	<script>
	
		const link = "${ pageContext.servletContext.contextPath }/feedback/selectMeal";
		let searchText = "";
		
		if(${ !empty requestScope.selectCriteria.searchCondition? true: false }) {
			searchText += "&searchCondition=${ requestScope.selectCriteria.searchCondition }";
		}
		
		if(${ !empty requestScope.selectCriteria.searchValue? true: false }) {
			searchText += "&searchValue=${ requestScope.selectCriteria.searchValue }";
		}
			
		if(document.getElementById("startPage")) {
			const $startPage = document.getElementById("startPage");
			$startPage.onclick = function() {
				location.href = link + "?currentPage=1" + searchText;
			}
		}
		
		if(document.getElementById("prevPage")) {
			const $prevPage = document.getElementById("prevPage");
			$prevPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectCriteria.pageNo - 1 }" + searchText;
			}
		}
		
		if(document.getElementById("nextPage")) {
			const $nextPage = document.getElementById("nextPage");
			$nextPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectCriteria.pageNo + 1 }" + searchText;
			}
		}
		
		if(document.getElementById("maxPage")) {
			const $maxPage = document.getElementById("maxPage");
			$maxPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectCriteria.maxPage }" + searchText;
			}
		}
		
		function pageButtonAction(text) {
			location.href = link + "?currentPage=" + text + searchText;
		}
	</script>

</body>
</html>