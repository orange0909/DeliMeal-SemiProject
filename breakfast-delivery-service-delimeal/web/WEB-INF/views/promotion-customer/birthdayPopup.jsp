<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<style>
    body{
        width: 600px;
        height: 450px;
        background-color: #FFFFF0;
        border: 3px solid #3C8028;
        border-radius: 20px 20px 20px 20px;
    }
    #congraturation_image{
        margin: auto;
        margin-top: 30px;
        text-align: center;
    }
    #congraturation_title{
        margin: auto;
        margin-top: 15px;
        text-align: center;
        color: rgba(99, 194, 70, 0.76);
        font-size: 25pt;
        font-weight: bold;
    }
    #congraturation_text{
        width: 500px;
        margin: auto;
        margin-top: 15px;
        text-align: center;
        color: rgba(166, 134, 75, 0.76);
        font-size: 15pt;
        font-weight: bold;
    }
    #buttonYes, #buttonNo{
        width: 100px;
        height: 40px;
        background-color: rgba(99, 194, 70, 0.76);
        color: white;
        font-size: 18pt;
        font-weight: bold;
        text-align: center;
        border-radius: 20px 20px 20px 20px;
        border: 0ch;
        outline: 0ch;
    }
    #buttonYes{
        margin-top: 50px;
        margin-left: 160px;
    }
    #buttonNo{
        margin-left: 80px;
    }
</style>
<title>Insert title here</title>
</head>
<body>
    <div id="congraturation_image"><img src="/delimeal/resources/image/congraturation.png" width="100px"></div>
    <div id="congraturation_title">생일 축하드립니다 회원님!</div>
    <div id="congraturation_text">저희 델리밀에선 회원님을 위해 매년 생일마다 생일 특별식단을 제공해 드리고 있습니다.<br>지금 바로 생일날 받아보실 식단을 등록하시겠습니까?</div>
    <button id="buttonYes" onclick="location.href='${pageContext.servletContext.contextPath}/register/BirthdayMenuMain'">네</button>
    <button type="button" id="buttonNo" onclick="closeWindow()">아니요</button>
    <script>
        function closeWindow(){
            window.close();
        }
    </script>
</body>
</html>