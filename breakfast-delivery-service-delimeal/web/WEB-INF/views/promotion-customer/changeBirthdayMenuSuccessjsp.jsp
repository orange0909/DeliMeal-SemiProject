<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
<style>
    #success_font{
        margin: auto;
        color: #63C246;
        font-size: 35pt;
        text-align: center;
    }
    #menu_picture{
        margin: auto;
        text-align: center;
    }
    #menu_name{
        margin: auto;
        color: #C2694C;
        font-size: 35pt;
        text-align: center;
    }
</style>
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-customer/promotionCustomerAside.jsp"/>
        <section>
            <article>
                <div id="success_font">생일 메뉴 변경 완료!</div>
                <div id="menu_picture"><img src="../resources/food/${ bdayMenu.picture }"></div>
                <div id="menu_name">고객님께서 선택하신 ${ bdayMenu.name }(으)로 생일메뉴가 변경 되었습니다</div>
            </article>
        </section>
    </div>
</body>
</html>