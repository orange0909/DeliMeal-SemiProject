<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
<style>
    #change_birthday_menu_title{
    	display: flex;
        flex-direction: row;
    }
    #icon{
        width: 45px;
        height: 45px;
        float: left;
    }
    #change_birthday_menu_font{
        color: #63C246;
        font-size: 25pt;
        margin-bottom: 10px;
        margin-left: 5px;
        float: left;
    }
    #menu_list{
        display: flex;
        flex-direction: row;
        gap: 50px;
    }
    #article{
        display: flex;
        flex-direction: column;
    }
    .picture{
        border: 1px solid #63C246;
    }
    /* flex로 가운데 정렬 할 때는 justify-content속성의 center속성값을 이용한다. */
    .MenuName{
        margin-top: 5px;
        display: flex;
        justify-content: center;
        font-size: 20px;
    }
    /* width 100%를 주지 않으면 radio버튼 크기만큼만 영역이 잡히기 때문에 width 100%로 강제로 영역을 지정해 주어야 한다. */
    .radio{
        display: flex;
        justify-content: center;
        width: 100%;
    }
    #register_button{
        display: flex;
        justify-content: center;
        width: 100%;
    }
    #selected_birthday_menu{
        height: 50px;
        width: 200px;
        background-color: rgba(60, 128, 40, 0.75);
        border: 3px solid #3C8028;
        border-radius: 20px 20px 20px 20px;
        color: white;
        font-size: 18pt;
    }
</style>
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-customer/promotionCustomerAside.jsp"/>
        <section>
            <article>
                <div id="change_birthday_menu_title">
                    <div id="icon">
                        <img src="../resources/image/logo.png" width="45px">
                    </div>
                    <div id="change_birthday_menu_font">변경할 생일메뉴 선택</div>
                </div>
                    
                <form action="${ pageContext.servletContext.contextPath }/pc/ChangeBirthdayMenu" method="get">
                    <div id="menu_list">
                        <c:forEach var="bdayMenu" items="${BirthdayMenu}" varStatus="st">
                            <div id="BirthdayMenu${st.index}" class="Menu">
                                <div id="BirthdayMenu_pic${st.index}"><img src="../resources/food/${bdayMenu.picture}" width="170px" height="150px" class="picture"></div>
                                <div class="radio"><input type="radio" id="${ bdayMenu.code }" name="birthday_menu" value="${ bdayMenu.code }" ></div>
                                <div id="BirthdayMenu_name${st.index}" class="MenuName">${bdayMenu.name}</div>
                            </div>
                        </c:forEach>        
                    </div>
                    <div id="register_button">
                        <button id="selected_birthday_menu">
                            생일 메뉴 변경
                        </button>
                    </div>
                </form>
            </article>
        </section>
    </div>
</body>
</html>