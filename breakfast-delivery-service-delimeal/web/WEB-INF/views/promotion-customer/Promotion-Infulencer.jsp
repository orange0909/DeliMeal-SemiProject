<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
<style>
    #title{
        width:540px;
        display: flex;
        flex-direction: row;
    }
    #bar{
        float: left;
        margin-left: 50px;
        width: 10px; 
        height: 120px; 
        background: #3C8028; 
        margin-bottom: 5px;
    }
    #title_font{
        float: left;
        margin-left: 10px; 
        width: 280px; 
        height: 120px; 
        font-size: 32pt; 
        color: #63C246;
    }
    #title_line{
        display: flex; 
        flex-direction: column;
        margin-left: 50px;
        margin-top: 1px;
        width: 450px; 
        height: 3px; 
        background: #3C8028;
    }

    #insta{
        display: flex;
        flex-direction: row;
        margin-left: 45px;
    }
    #insta_logo{
        width: 100px; 
        height: 100px;
        float: left;
    }
    #insta_font{
        float: left;
        color: #D9C196;
        font-size: 18pt;
        margin-top: 25px;
    }

    #promotion_pic{
        display: flex;
        flex-direction: row;
        gap: 30px;
        align-items: flex-end;
        height: 540px;
    }
    #promotion_image1{
        width: 200px; 
        height: 250px;
    }
    #promotion_image2{
        width: 200px;
        height: 380px;
        margin-bottom: 50px;
    }
    #promotion_image3{
        width: 200px;
        height: 430px;
        margin-bottom: 150px;
    }
</style>
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-customer/promotionCustomerAside.jsp"/>

        <section>
            <article>
                <div id="title">
                    <bar id="bar"></bar>
                    <title_font id="title_font">With <br> Infulencer</title_font>
                </div>
                <div id="title_line"></div>
                <div id="insta">
                    <a href="https://www.instagram.com/" target="_blank">
                        <img src="../resources/image/insta.png" id="insta_logo">
                    </a>
                    <insta_font id="insta_font">델리밀 인스타 주소</insta_font>
                </div>
                <div id="promotion_pic">
                     <a href="http://${promotionPictureLeft.url}" target="_blank" class="promotion_link1">
                        <img src="../resources/promotion/${promotionPictureLeft.name}" id="promotion_image1">
                    </a>    
                     <a href="http://${promotionPictureMiddle.url}" target="_blank" class="promotion_link2">
                        <img src="../resources/promotion/${promotionPictureMiddle.name}" id="promotion_image2">
                    </a>
                     <a href="http://${promotionPictureRight.url}" target="_blank" class="promotion_link3">
                        <img src="../resources/promotion/${promotionPictureRight.name}" id="promotion_image3">
                    </a>            
                    </div>
            </article>
        </section>
    </div>
</body>
</html>