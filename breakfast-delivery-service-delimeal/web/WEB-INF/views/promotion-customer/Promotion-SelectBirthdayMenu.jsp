<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
<style>
    /* flex행렬처럼 생각하면 된다. row를 먼저 정렬하고, 감싸주는 영역을 만들고, 해당 영역의 column을 정렬해주면 된다. */
    #slected_birthday_menu_title{
    	display: flex;
        flex-direction: row;
    }
    #icon{
        width: 45px;
        height: 45px;
        float: left;
    }
    #selected_birthday_menu_font{
        color: #63C246;
        font-size: 25pt;
        margin-bottom: 10px;
        margin-left: 5px;
        float: left;
    }
    #selected_birthday_menu_display{
        background-color: rgba(186, 239, 97, 0.5);
        border-radius: 20px 20px 20px 20px;
        border: 2px solid #3C8028;
        height: 130px;
        display: flex;
        flex-direction: row;
        gap: 20px;
    }
    #selected_birthday_menu_image{
        margin-left: 45px;
        margin-top: 20px;
        display: flex;
        flex-direction: row;
    }
    #selected_birthday_menu_name{
        margin-top: 50px;
        color: #3C8028;
        font-size: 30px;
        display: flex;
        flex-direction: row;
    }
    #selected_birthday_menu_change{
        width: 160px;
        height: 40px;
        margin-top: 45px;
        margin-left: 250px;
        background-color: rgba(60, 128, 40, 0.75);
        border: 2px solid #D9C196;
        border-radius: 20px 20px 20px 20px;
        display: flex;
        flex-direction: row;
    }
    #button_font{
        color: white;
        font-size: 20px;
        text-align: center;
        display: flex;
        justify-content: center;
    }
</style>
<title>Insert title here</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-customer/promotionCustomerAside.jsp"/>
        <section>
            <article>
                <div id="slected_birthday_menu_title">
                    <icon id="icon">
                        <img src="../resources/image/logo.png" width="45px">
                    </icon>
                    <div id="selected_birthday_menu_font">선택한 생일 메뉴</div>
                </div>
                <div id="selected_birthday_menu_display">
                       <div id="selected_birthday_menu_image"><img src="../resources/food/${memberBirthdayMenu.picture}" width="90px" height="90px"></div>
                    <div id="selected_birthday_menu_name">${memberBirthdayMenu.name}</div>
                    <button onclick="location.href='${pageContext.servletContext.contextPath}/pc/selectBirthdayMenu'" id="selected_birthday_menu_change">
                        <div id="button_font">생일 메뉴 변경</div>
                    </button>
                </div>
            </article>
        </section>
    </div>
</body>
</html>