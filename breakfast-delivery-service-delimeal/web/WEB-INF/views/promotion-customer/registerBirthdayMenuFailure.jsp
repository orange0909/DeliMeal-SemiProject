<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/template/importDependencies.jsp"/>
<link rel="stylesheet" href="${ pageContext.servletContext.contextPath}/resources/css/common.css">
<style>
    #failure_font{
        margin: auto;
        color: #63C246;
        font-size: 35pt;
        text-align: center;
    }
    #failure_picture{
        margin: auto;
        margin-top: 20px;
        text-align: center;
    }
    #failure_description{
        margin: auto;
        margin-top: 20px;
        color: #C2694C;
        font-size: 35pt;
        text-align: center;
    }
</style>
<title>Insert title here</title>
</head>
<body>
    <jsp:include page="/WEB-INF/views/template/header.jsp"/>
    <div id="content">
        <jsp:include page="/WEB-INF/views/promotion-customer/promotionCustomerAside.jsp"/>
        <section>
            <article>
                <div id="failure_font">생일 메뉴 등록 실패!</div>
                <div id="failure_picture"><img src="/delimeal/resources/image/failure.png"></div>
                <div id="failure_description">처음부터 다시 수행해 주세요</div>
            </article>
        </section>
    </div>
</body>
</html>